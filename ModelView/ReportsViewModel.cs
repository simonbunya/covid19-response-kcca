﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CHMIS.ModelView
{
    public class ReportsViewModel
    {
        public string Division { get; set; }
        public string StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
