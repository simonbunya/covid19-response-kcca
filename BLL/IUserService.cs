﻿using CHMIS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CHMIS.BLL
{
    public interface IUserService
    {
        ClaimsPrincipal GetUser();
        string GetUserName();
        string GetFullNameID();
        string GetFullName();
        int logins_Today();
        int? getCurrentuserDisease();
        int GetEmployeeID();

        List<ASystemSection> GetAssignedSections();
        List<int> GetAssignedModules();
        List<int> getUserFacilitySupervisorList();

    }
}
