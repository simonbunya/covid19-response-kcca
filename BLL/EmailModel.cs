﻿using CHMIS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace CHMIS.BLL
{
    public class EmailModel
    {
        private CHMISContext _context;
        public EmailModel(CHMISContext context)
        {
            this._context = context;
        }

        public void ComfirmEmail(string email, string callbackUrl)
        {
            try
            {
                using (var message = new MailMessage("dontreplycovid19@gmail.com", email))
                {
                    // foreach (staWebTemplate_RecipientEmail objxx in objx)

                    message.Subject = "Confirm your email" + "on" + " " + DateTime.Now.ToLongDateString(); 
                    message.Body = "Dear User, " + "<br/><br/>" + "Kindly be notified that your account has been created on City Health Information System (CHIS), " + "<br/><br/>" + $"Please confirm here your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>." + "<br/><br/>" +"Regards" + "<br/><br/>" + "CHIS Team,<br/><br/>" + DateTime.Now + ".";
                    message.IsBodyHtml = true;
                    using (SmtpClient client = new SmtpClient
                    {
                        EnableSsl = true,
                        Host = "smtp.gmail.com",
                        //Host = "smtp.office365.com",
                        //Port = 587,
                        Port = 587,

                        Credentials = new NetworkCredential("dontreplycovid19@gmail.com", "Kcca@Kcca.1")
                    })
                    {
                        //client.EnableSsl = true;
                        client.Send(message);
                    }
                }
            }
            catch
            {

            }

        }

        public void SendAssigmentEmail(string CaseNo, string FirstName, string LastName, string tel, int? supervisor )
        {
            var person = _context.AEmployees.Where(o => o.EmployeeId == supervisor).ToList();
            if (person.Count > 0)
            {
                using (var message = new MailMessage("dontreplycovid19@gmail.com", person[0].Email))
                {
                    message.Subject = "New COVID-19 Case-Contact Person Assigned" + "on" + " " + DateTime.Now.ToLongDateString();
                    message.Body = "Dear " + person[0].Name +  " , " + "<br/><br/>" + "Kindly be notified that there has been a New COVID19 Case Contact Person Assigned to you in names of : " + FirstName+ "  "+ LastName + " , with Contact case No. " + CaseNo + " and telephone number: " + tel + " for following up and monitoring.  <br/><br/> Please log into <a href='https://chis.go.ug/'>CHMIS-APP<a/>  Web portal to view the details of the Contact person.<br/><br/> Regards, <br/> CHMIS - Team,<br/><br/>" + DateTime.Now + ".";
                    message.IsBodyHtml = true;
                    for (int i = 1; i <= (person.Count - 1); i++)
                    {
                        message.CC.Add(person[i].Email);
                    }

                    using (SmtpClient client = new SmtpClient
                    {
                        EnableSsl = true,
                        Host = "smtp.gmail.com",
                        //Host = "smtp.office365.com",
                        //Port = 587,
                        Port = 587,
                        Credentials = new NetworkCredential("dontreplycovid19@gmail.com", "Kcca@Kcca.1")
                    })
                    {
                        client.Send(message);
                    }
                }
            }

        }

    }
}
