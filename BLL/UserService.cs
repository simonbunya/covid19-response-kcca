﻿using CHMIS.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CHMIS.BLL
{
    public class UserService : IUserService
    {
        private readonly IHttpContextAccessor accessor;
        private CHMISContext _context;
        public UserService(CHMISContext Context, IHttpContextAccessor accessor)
        {
            this.accessor = accessor;
            this._context = Context;
        }
        public int? getCurrentuserDisease()
        {
            int? result = null;
            var user = accessor?.HttpContext?.User.Identity.Name;
            string userid = _context.AspNetUsers.FirstOrDefault(o => o.UserName == user).Id;
            var login = _context.ALoginLog.Where(o => o.UserId == userid).ToList().OrderByDescending(o=> o.Id).FirstOrDefault();
            if(login != null)
            {
                result = login.Disease;
            }
            return result;
        }

        public string GetFullName()
        {
            var user = accessor?.HttpContext?.User.Identity.Name;
            string userid = _context.AspNetUsers.FirstOrDefault(o => o.UserName == user).Id;
            return _context.AEmployees.FirstOrDefault(o => o.UserId == userid).Name;
        }

        public string GetFullNameID()
        {
            var user = accessor?.HttpContext?.User.Identity.Name;
            string userid = _context.AspNetUsers.FirstOrDefault(o => o.UserName == user).Id;
            return userid;
        }

        public int GetEmployeeID()
        {
            var user = accessor?.HttpContext?.User.Identity.Name;
            string userid = _context.AspNetUsers.FirstOrDefault(o => o.UserName == user).Id;
            return _context.AEmployees.FirstOrDefault(o => o.UserId == userid).EmployeeId;
        }

        public ClaimsPrincipal GetUser()
        {
            return accessor?.HttpContext?.User as ClaimsPrincipal;
        }

        public string GetUserName()
        {
          return accessor?.HttpContext?.User.Identity.Name;
        }

        public int logins_Today()
        {
            var _logins = _context.ALoginLog.ToList();
            var logins = _logins.Where(o => Convert.ToDateTime(o.LogInDate).Date == DateTime.Now.Date).ToList().Count;
            return logins;
        }

        public List<int> GetAssignedModules()
        {
            List<int> items = new List<int>();
            //var userid = HttpContext.Current.User.Identity.GetUserId();
            var user = accessor?.HttpContext?.User.Identity.Name;
            string userid = _context.AspNetUsers.FirstOrDefault(o => o.UserName == user).Id;
            var assignment = _context.ASelecetedModules.FirstOrDefault(o => o.UserId == userid);
            if (assignment != null)
            {
                if (assignment.SelectedModules.Contains(","))
                {
                    var mod = assignment.SelectedModules.Split(',');
                    foreach (var n in mod)
                    {
                        items.Add(Convert.ToInt32(n));
                    }
                }
                else
                {
                    items.Add(Convert.ToInt32(assignment.SelectedModules));
                }

            }
            return items;
        }
        public List<ASystemSection> GetAssignedSections()
        {
            List<ASystemSection> items = new List<ASystemSection>();
            //var userid = HttpContext.Current.User.Identity.GetUserId();
            var user = accessor?.HttpContext?.User.Identity.Name;
            string userid = _context.AspNetUsers.FirstOrDefault(o => o.UserName == user).Id;
            var assignment = _context.ASelecetedSections.FirstOrDefault(o => o.UserId == userid);
            if (assignment != null)
            {
                if (assignment.SelectedSectors.Contains(","))
                {
                    var mod = assignment.SelectedSectors.Split(',');
                    foreach (var n in mod)
                    {
                        int code = Convert.ToInt32(n);
                        items.Add(_context.ASystemSection.FirstOrDefault(o => o.SectionId == code));
                    }
                }
                else
                {
                    int code = Convert.ToInt32(assignment.SelectedSectors);
                    items.Add(_context.ASystemSection.FirstOrDefault(o => o.SectionId == code));
                }

            }
            return items;
        }

         public List<int> getUserFacilitySupervisorList()
         {
            string UserName = accessor?.HttpContext?.User.Identity.Name;
            var facility = _context.AspNetUsers.FirstOrDefault(o => o.UserName == UserName).Facility;

            //string UserName = HttpContext.Current.User.Identity.GetUserName();
            //var userStore = new UserStore<ApplicationUser>(context);
            //var districts = new UserManager<ApplicationUser>(userStore).Users.SingleOrDefault(a => a.UserName == UserName).DistrictSupervor;

            //A shorter approach would have been 
            // var districtsints = districts.Split(',').Select(x => Int32.Parse(x)); 

            Char delimiter = ',';
            String[] subfacility = null;// districts.Split(delimiter); 

            List<int> facilityarray = new List<int>();
            if (facility != null)
            {
                if (facility.TrimEnd().Length > 0)
                {
                    subfacility = facility.Split(delimiter);
                    foreach (string facilitycode in subfacility)
                    {
                        int num1;
                        bool res = int.TryParse(facilitycode, out num1);
                        if (res == true)
                        {
                            facilityarray.Add(num1);
                        }
                    }
                }
            }
            return (facilityarray);

        }
    }
}
