﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CHMIS.BLL;
using CHMIS.Models;
using CHMIS.ModelView;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Syncfusion.Drawing;
using Syncfusion.XlsIO;
using Syncfusion.DocIO;
using Syncfusion.DocIO.DLS;
using Syncfusion.JavaScript.DataSources;
using Syncfusion.Linq;
using System.Collections;
using Microsoft.AspNetCore.Http;
using System.Globalization;
using Microsoft.EntityFrameworkCore;

namespace CHMIS.Controllers
{
    public class ReportsController : Controller
    {
        private CHMISContext _context;
        private IWebHostEnvironment hostingEnv;
        private IUserService userInfor;
        private IHttpContextAccessor httpContext;

        public ReportsController(CHMISContext Context, IWebHostEnvironment env, IUserService _userinfo, IHttpContextAccessor _httpContext)
        {
            this._context = Context;
            this.hostingEnv = env;
            this.userInfor = _userinfo;
            this.httpContext = _httpContext;
        }
        public IActionResult SitRepReports()
        {
            ViewBag.division = _context.ACounty.ToList();
            return View();
        }

        public IActionResult CombinedSitRepReports()
        {
            ViewBag.division = _context.ACounty.ToList();
            return View();
        }

        //word document
        [HttpPost]
        public ActionResult WordExportSitRep(ReportsViewModel value)
        {
            //var data = _context.ViewReportSitRep.Where(x=>x.Division== value.Division && x.ReportingDate == value.StartDate).ToList();
            CultureInfo current = new CultureInfo("en-US");
            string format = "yyyy/MM/dd";
            DateTime _date = DateTime.Now;
            //IEnumerable data = null;
            DateTime edate = DateTime.Now;

            if (DateTime.TryParseExact(value.StartDate, format, current, DateTimeStyles.None, out _date))
            {
                edate = _date;
            }
            var data = _context.ViewReportSitRep.Where(o => o.ReportingDate.Value.Date == edate.Date && o.Division == value.Division).OrderBy(o => o.SitRepCategoryId).ToList();

            if (data.Count > 0)
            {

                var _division = "No Selected Division";// _context.ViewReportSitRep.FirstOrDefault(o => o.ReportingDate.Value.Date == edate.Date && o.Division == value.Division);
                var xdiv = _context.ViewReportSitRep.FirstOrDefault(o => o.ReportingDate.Value.Date == edate.Date && o.Division == value.Division);
                //var xdate = _context.ViewReportSitRep.FirstOrDefault(o => o.ReportingDate.Value.Date == edate.Date && o.Division == value.Division);

                if (xdiv != null)
                {
                    _division = xdiv.CountyName;
                }
                var _datatime = System.DateTime.Now.ToLongDateString();
                if (xdiv != null)
                {
                    _datatime = xdiv.ReportingDate.Value.ToLongDateString();
                }

                //Create a new document
                WordDocument document = new WordDocument();
                //Adding a new section to the document
                IWSection section = document.AddSection();

                //Sets page setup options for LandscapePageSetup.Orientation
                //section.PageSetup.Orientation = PageOrientation.Landscape;

                IWTable Headertable = section.HeadersFooters.Header.AddTable();
                Headertable.ResetCells(1, 1);
                WTableRow Headerrow = Headertable.Rows[0];

                //Headerrow.Height = 25f;
                //Adds a new paragraph to the cell
                IWParagraph HeadercellPara = Headerrow.Cells[0].AddParagraph();

                //string basePath = Server.MapPath("~/");
                string basePath = hostingEnv.WebRootPath;
                FileStream imageStream = new FileStream(basePath + @"/covidlog.png", FileMode.Open, FileAccess.Read);
                //System.Drawing.Image _image = System.Drawing.Image.FromStream(imageStream);
                HeadercellPara.AppendPicture(imageStream);

                Headerrow.Cells[0].CellFormat.Borders.BorderType = BorderStyle.None;
                Headerrow.Cells[0].CellFormat.Borders.Color = Syncfusion.Drawing.Color.FromArgb(255, 255, 255);
                HeadercellPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;

                //HeadercellPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Right;
                //Headerrow.Cells[0].Width = 350;

                // InsertFirstPageHeaderFooter(document, section);
                //Adding a new paragraph to the section
                IWParagraph paragraph = section.AddParagraph();


                #region Title Section
                //Adds a new table to the section
                IWTable table = section.Body.AddTable();
                table.ResetCells(2, 2);
                table.ApplyHorizontalMerge(0, 0, 1);
                table.ApplyHorizontalMerge(1, 0, 1);

                #region first row
                //Gets the table first row
                WTableRow row = table.Rows[0];
                row.Height = 25f;
                //Adds a new paragraph to the cell
                IWParagraph cellPara = row.Cells[0].AddParagraph();
                row.Cells[0].CellFormat.Borders.BorderType = BorderStyle.None;
                row.Cells[0].CellFormat.Borders.Color = Syncfusion.Drawing.Color.FromArgb(255, 255, 255);
                cellPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
                //Appends the text
                string paraText = "KCCA COVID 19 RESPONSE SITUATION REPORT \n As at Date:  " + _datatime;// System.DateTime.Now.ToShortDateString() + "\n Time: " + System.DateTime.Now;
                IWTextRange titletxt = cellPara.AppendText(paraText);
                titletxt.CharacterFormat.FontName = "Times New Roman";
                //titletxt.CharacterFormat.TextColor = Syncfusion.Drawing.Color.FromArgb(192, 0, 0);
                titletxt.CharacterFormat.Bold = true;
                titletxt.CharacterFormat.FontSize = 16f;
                titletxt.CharacterFormat.AllCaps = false;
                //Adds a new paragraph to the next cell
                cellPara = row.Cells[1].AddParagraph();
                row.Cells[1].CellFormat.Borders.BorderType = BorderStyle.None;
                row.Cells[1].CellFormat.Borders.Color = Syncfusion.Drawing.Color.FromArgb(255, 255, 255);
                row.Cells[1].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                row.Cells[1].CellFormat.BackColor = Syncfusion.Drawing.Color.FromArgb(255, 255, 255);
                //Appends the text
                IWTextRange txt = cellPara.AppendText("");
                cellPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
                row.Cells[0].Width = 350;
                row.Cells[1].Width = 150;
                #endregion first row

                #region Second row
                //Gets the table first row
                row = table.Rows[1];
                row.Height = 25f;
                //Adds a new paragraph to the cell
                cellPara = row.Cells[0].AddParagraph();
                row.Cells[0].CellFormat.Borders.BorderType = BorderStyle.None;
                row.Cells[0].CellFormat.Borders.Color = Syncfusion.Drawing.Color.FromArgb(255, 255, 255);
                cellPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Left;
                //Appends the text
                paraText = "\n KCCA COVID 19 Response Situation Report for : " + _division + " and Reporting date of: " + _datatime;
                titletxt = cellPara.AppendText(paraText);
                titletxt.CharacterFormat.FontName = "Times New Roman";
                titletxt.CharacterFormat.Bold = false;
                titletxt.CharacterFormat.FontSize = 11f;
                titletxt.CharacterFormat.AllCaps = false;
                //Adds a new paragraph to the next cell
                cellPara = row.Cells[1].AddParagraph();
                row.Cells[1].CellFormat.Borders.BorderType = BorderStyle.None;
                row.Cells[1].CellFormat.Borders.Color = Syncfusion.Drawing.Color.FromArgb(255, 255, 255);
                row.Cells[1].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                row.Cells[1].CellFormat.BackColor = Syncfusion.Drawing.Color.FromArgb(255, 255, 255);
                //Appends the text
                txt = cellPara.AppendText("");
                cellPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
                row.Cells[0].Width = 350;
                row.Cells[1].Width = 150;
                #endregion second row

                #endregion
                //Adds a new paragraph
                section.AddParagraph();

                paragraph = section.AddParagraph();

                string words = "Table 2: Summary of statistics as at:  " + _datatime;// DateTime.Now.ToString("dd") + ", " + DateTime.Now.DayOfWeek + ", " + DateTime.Now.ToString("MMMM") + " " + DateTime.Now.Year;
                txt = paragraph.AppendText(words);
                txt.CharacterFormat.TextColor = Syncfusion.Drawing.Color.FromArgb(192, 0, 0);
                txt.CharacterFormat.FontName = "Times New Roman";
                txt.CharacterFormat.Bold = true;
                txt.CharacterFormat.FontSize = 14f;
                section.AddParagraph();


                #region General Information
                //Adds a new table
                table = section.Body.AddTable();
                table.ResetCells((data.Count + 1), 4);
                row = table.Rows[0];
                row.Height = 20;
                row.Cells[0].Width = 30;
                //Adds a new paragraph
                cellPara = row.Cells[0].AddParagraph();
                //Sets a border type, color, background, and vertical alignment for cell
                row.Cells[0].CellFormat.Borders.BorderType = Syncfusion.DocIO.DLS.BorderStyle.Thick;
                row.Cells[0].CellFormat.Borders.Color = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                row.Cells[0].CellFormat.BackColor = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                row.Cells[0].CellFormat.VerticalAlignment = Syncfusion.DocIO.DLS.VerticalAlignment.Middle;
                txt = cellPara.AppendText("S/N");
                txt.CharacterFormat.FontName = "Calibri Light (Headings)";
                txt.CharacterFormat.Bold = true;
                txt.CharacterFormat.FontSize = 12f;

                // cell 2
                row.Cells[1].Width = 220;
                //Adds a new paragraph
                cellPara = row.Cells[1].AddParagraph();
                //Sets a border type, color, background, and vertical alignment for cell
                row.Cells[1].CellFormat.Borders.BorderType = Syncfusion.DocIO.DLS.BorderStyle.Thick;
                row.Cells[1].CellFormat.Borders.Color = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                row.Cells[1].CellFormat.BackColor = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                row.Cells[1].CellFormat.VerticalAlignment = Syncfusion.DocIO.DLS.VerticalAlignment.Middle;
                txt = cellPara.AppendText("Variable");
                txt.CharacterFormat.FontName = "Calibri Light (Headings)";
                txt.CharacterFormat.Bold = true;
                txt.CharacterFormat.FontSize = 12f;

                // cell 3
                row.Cells[2].Width = 170;
                //Adds a new paragraph
                cellPara = row.Cells[2].AddParagraph();
                //Sets a border type, color, background, and vertical alignment for cell
                row.Cells[2].CellFormat.Borders.BorderType = Syncfusion.DocIO.DLS.BorderStyle.Thick;
                row.Cells[2].CellFormat.Borders.Color = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                row.Cells[2].CellFormat.BackColor = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                row.Cells[2].CellFormat.VerticalAlignment = Syncfusion.DocIO.DLS.VerticalAlignment.Middle;
                txt = cellPara.AppendText("Category");
                txt.CharacterFormat.FontName = "Calibri Light (Headings)";
                txt.CharacterFormat.Bold = true;
                txt.CharacterFormat.FontSize = 12f;

                // cell 4
                row.Cells[3].Width = 60;
                //Adds a new paragraph
                cellPara = row.Cells[3].AddParagraph();
                //Sets a border type, color, background, and vertical alignment for cell
                row.Cells[3].CellFormat.Borders.BorderType = Syncfusion.DocIO.DLS.BorderStyle.Thick;
                row.Cells[3].CellFormat.Borders.Color = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                row.Cells[3].CellFormat.BackColor = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                row.Cells[3].CellFormat.VerticalAlignment = Syncfusion.DocIO.DLS.VerticalAlignment.Middle;
                txt = cellPara.AppendText(" No.");
                txt.CharacterFormat.FontName = "Calibri Light (Headings)";
                txt.CharacterFormat.Bold = true;
                txt.CharacterFormat.FontSize = 12f;


                int count = 1;
                foreach (var n in data)
                {
                    row = table.Rows[count];
                    row.Cells[0].Width = 30;
                    //Adds a new paragraph
                    cellPara = row.Cells[0].AddParagraph();
                    //Sets a border type, color, background, and vertical alignment for cell
                    row.Cells[0].CellFormat.Borders.BorderType = Syncfusion.DocIO.DLS.BorderStyle.Thick;
                    row.Cells[0].CellFormat.Borders.Color = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                    row.Cells[0].CellFormat.BackColor = Syncfusion.Drawing.Color.FromArgb(255, 255, 255);
                    row.Cells[0].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                    txt = cellPara.AppendText(count.ToString());
                    txt.CharacterFormat.TextColor = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                    txt.CharacterFormat.FontSize = 12f;

                    // cell 2
                    row.Cells[1].Width = 220;
                    //Adds a new paragraph
                    cellPara = row.Cells[1].AddParagraph();
                    //Sets a border type, color, background, and vertical alignment for cell
                    row.Cells[1].CellFormat.Borders.BorderType = Syncfusion.DocIO.DLS.BorderStyle.Thick;
                    row.Cells[1].CellFormat.Borders.Color = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                    row.Cells[1].CellFormat.BackColor = Syncfusion.Drawing.Color.FromArgb(255, 255, 255);
                    row.Cells[1].CellFormat.VerticalAlignment = Syncfusion.DocIO.DLS.VerticalAlignment.Middle;
                    if (n.SitRep != null)
                    {
                        txt = cellPara.AppendText(n.SitRep);
                        txt.CharacterFormat.TextColor = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                        txt.CharacterFormat.FontSize = 12f;
                    }

                    // cell 3
                    row.Cells[2].Width = 170;
                    //Adds a new paragraph
                    cellPara = row.Cells[2].AddParagraph();
                    //Sets a border type, color, background, and vertical alignment for cell
                    row.Cells[2].CellFormat.Borders.BorderType = Syncfusion.DocIO.DLS.BorderStyle.Thick;
                    row.Cells[2].CellFormat.Borders.Color = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                    row.Cells[2].CellFormat.BackColor = Syncfusion.Drawing.Color.FromArgb(255, 255, 255);
                    row.Cells[2].CellFormat.VerticalAlignment = Syncfusion.DocIO.DLS.VerticalAlignment.Middle;
                    if (n.Category != null)
                    {
                        txt = cellPara.AppendText(n.Category);
                        txt.CharacterFormat.TextColor = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                        txt.CharacterFormat.FontSize = 12f;
                    }

                    // cell 4
                    row.Cells[3].Width = 60;
                    //Adds a new paragraph
                    cellPara = row.Cells[3].AddParagraph();
                    //Sets a border type, color, background, and vertical alignment for cell
                    row.Cells[3].CellFormat.Borders.BorderType = Syncfusion.DocIO.DLS.BorderStyle.Thick;
                    row.Cells[3].CellFormat.Borders.Color = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                    row.Cells[3].CellFormat.BackColor = Syncfusion.Drawing.Color.FromArgb(255, 255, 255);
                    row.Cells[3].CellFormat.VerticalAlignment = Syncfusion.DocIO.DLS.VerticalAlignment.Middle;
                    if (n.ValueResponse != null)
                    {
                        txt = cellPara.AppendText(n.ValueResponse.ToString());
                        txt.CharacterFormat.TextColor = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                        txt.CharacterFormat.FontSize = 12f;
                    }

                    count = count + 1;
                }

                section = document.AddSection();
                section.AddParagraph();
                paragraph = section.AddParagraph();

                titletxt = paragraph.AppendText("WASH");
                titletxt.CharacterFormat.FontName = "Times New Roman";
                titletxt.CharacterFormat.Bold = true;
                titletxt.CharacterFormat.FontSize = 16f;
                titletxt.CharacterFormat.AllCaps = false;
                titletxt.CharacterFormat.TextColor = Color.FromArgb(192, 0, 0);
                section.AddParagraph();

                section.AddParagraph();
                paragraph = section.AddParagraph();
                if(xdiv.WashComments != null)
                {
                    paragraph.AppendText(xdiv.WashComments);
                }
                section.AddParagraph();


                section.AddParagraph();
                paragraph = section.AddParagraph();
                titletxt = paragraph.AppendText("RISK Communication");
                titletxt.CharacterFormat.FontName = "Times New Roman";
                titletxt.CharacterFormat.Bold = true;
                titletxt.CharacterFormat.FontSize = 16f;
                titletxt.CharacterFormat.AllCaps = false;
                titletxt.CharacterFormat.TextColor = Color.FromArgb(192, 0, 0);
                section.AddParagraph();

                section.AddParagraph();
                paragraph = section.AddParagraph();
                if (xdiv.RiskComments != null)
                {
                    paragraph.AppendText(xdiv.RiskComments);
                }
                section.AddParagraph();


                section.AddParagraph();
                paragraph = section.AddParagraph();
                titletxt = paragraph.AppendText("CO-ORDINATION & LEADERSHIP");
                titletxt.CharacterFormat.FontName = "Times New Roman";
                titletxt.CharacterFormat.Bold = true;
                titletxt.CharacterFormat.FontSize = 16f;
                titletxt.CharacterFormat.AllCaps = false;
                titletxt.CharacterFormat.TextColor = Color.FromArgb(192, 0, 0);
                section.AddParagraph();

                section.AddParagraph();
                paragraph = section.AddParagraph();
                if (xdiv.UpdateComments != null)
                {
                    paragraph.AppendText(xdiv.UpdateComments);
                }
                section.AddParagraph();


                section.AddParagraph();
                paragraph = section.AddParagraph();
                titletxt = paragraph.AppendText("Challenges & Recommendations");
                titletxt.CharacterFormat.FontName = "Times New Roman";
                titletxt.CharacterFormat.Bold = true;
                titletxt.CharacterFormat.FontSize = 16f;
                titletxt.CharacterFormat.AllCaps = false;
                titletxt.CharacterFormat.TextColor = Color.FromArgb(192, 0, 0);
                section.AddParagraph();

                section.AddParagraph();
                paragraph = section.AddParagraph();
                if (xdiv.GeneralComments != null)
                {
                    paragraph.AppendText(xdiv.GeneralComments);
                }
                section.AddParagraph();


                //Adds a new paragraph
                section.AddParagraph();
                paragraph = section.AddParagraph();
                paragraph.AppendText("For and on Behalf of KCCA, Situation Report (SitRep)");
                section.AddParagraph();

                //var _division = _context.ViewReportSitRep.FirstOrDefault(o => o.CountyName);
                //var _submissionDate = 
                //if (officer != null)
                //{
                //    paragraph = section.AddParagraph();
                //    string mofficer = officer.cp_name + "\n" + officer.title_desc + "\nEmail: " + officer.ce_email;
                //    paragraph.AppendText(mofficer);
                //}
                #endregion

                FormatType type = FormatType.Docx;
                string filename = "KCCA COVID 19 SITREP REPORT AS AT " + DateTime.Now.ToString("dd") + ", " + DateTime.Now.DayOfWeek + ", " + DateTime.Now.ToString("MMMM") + " " + DateTime.Now.Year + ".docx";
                string contenttype = "application/vnd.ms-word.document.12";

                MemoryStream ms = new MemoryStream();
                document.Save(ms, type);
                document.Close();
                ms.Position = 0;
                return File(ms, contenttype, filename); 
            }
            else
            {
                return RedirectToAction("SitRepReports");
            }
            //return RedirectToAction("SitRepReports");
        }


        [HttpPost]
        public ActionResult CombinedExportSitRep(ReportsViewModel value)
        {
            //var data = _context.ViewReportSitRep.Where(x=>x.Division== value.Division && x.ReportingDate == value.StartDate).ToList();
            CultureInfo current = new CultureInfo("en-US");
            string format = "yyyy/MM/dd";
            DateTime _date = DateTime.Now;
            //IEnumerable data = null;
            DateTime edate = DateTime.Now;

            if (DateTime.TryParseExact(value.StartDate, format, current, DateTimeStyles.None, out _date))
            {
                edate = _date;
            }
            //var data = _context.ViewReportSitRep.Where(o => o.ReportingDate.Value.Date == edate.Date && o.Division == value.Division).OrderBy(o => o.SitRepCategoryId).ToList();
            var data = _context.ViewReportSitRepCombinedNew.FromSqlRaw("EXECUTE sp_View_Report_SitRep_Combined_New {0}", edate.Date).AsEnumerable()/*.OrderBy(o => o.SitRepCategoryId)*/.ToList();

            if (data.Count > 0)
            {

                var _division = "No Selected Division";// _context.ViewReportSitRep.FirstOrDefault(o => o.ReportingDate.Value.Date == edate.Date && o.Division == value.Division);
                var xdiv = _context.ViewSitRepComments.FirstOrDefault(o => o.ReportingDate.Date == edate.Date);

               
                var _datatime = System.DateTime.Now.ToLongDateString();
                if (xdiv != null)
                {
                    _datatime = xdiv.ReportingDate.ToLongDateString();
                }

                //Create a new document
                WordDocument document = new WordDocument();
                //Adding a new section to the document
                IWSection section = document.AddSection();

                //Sets page setup options for LandscapePageSetup.Orientation
                section.PageSetup.Orientation = PageOrientation.Landscape;

                IWTable Headertable = section.HeadersFooters.Header.AddTable();
                Headertable.ResetCells(1, 1);
                WTableRow Headerrow = Headertable.Rows[0];

                //Headerrow.Height = 25f;
                //Adds a new paragraph to the cell
                IWParagraph HeadercellPara = Headerrow.Cells[0].AddParagraph();

                //string basePath = Server.MapPath("~/");
                string basePath = hostingEnv.WebRootPath;
                FileStream imageStream = new FileStream(basePath + @"/covidlog.png", FileMode.Open, FileAccess.Read);
                //System.Drawing.Image _image = System.Drawing.Image.FromStream(imageStream);
                HeadercellPara.AppendPicture(imageStream);

                Headerrow.Cells[0].CellFormat.Borders.BorderType = BorderStyle.None;
                Headerrow.Cells[0].CellFormat.Borders.Color = Syncfusion.Drawing.Color.FromArgb(255, 255, 255);
                HeadercellPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;

                //HeadercellPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Right;
                //Headerrow.Cells[0].Width = 350;

                // InsertFirstPageHeaderFooter(document, section);
                //Adding a new paragraph to the section
                IWParagraph paragraph = section.AddParagraph();


                #region Title Section
                //Adds a new table to the section
                IWTable table = section.Body.AddTable();
                table.ResetCells(2, 2);
                table.ApplyHorizontalMerge(0, 0, 1);
                table.ApplyHorizontalMerge(1, 0, 1);

                #region first row
                //Gets the table first row
                WTableRow row = table.Rows[0];
                row.Height = 25f;
                //Adds a new paragraph to the cell
                IWParagraph cellPara = row.Cells[0].AddParagraph();
                row.Cells[0].CellFormat.Borders.BorderType = BorderStyle.None;
                row.Cells[0].CellFormat.Borders.Color = Syncfusion.Drawing.Color.FromArgb(255, 255, 255);
                cellPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
                //Appends the text
                string paraText = "KCCA COVID 19 RESPONSE SITUATION REPORT \n As at Date:  " + _datatime;// System.DateTime.Now.ToShortDateString() + "\n Time: " + System.DateTime.Now;
                IWTextRange titletxt = cellPara.AppendText(paraText);
                titletxt.CharacterFormat.FontName = "Times New Roman";
                //titletxt.CharacterFormat.TextColor = Syncfusion.Drawing.Color.FromArgb(192, 0, 0);
                titletxt.CharacterFormat.Bold = true;
                titletxt.CharacterFormat.FontSize = 16f;
                titletxt.CharacterFormat.AllCaps = false;
                //Adds a new paragraph to the next cell
                cellPara = row.Cells[1].AddParagraph();
                row.Cells[1].CellFormat.Borders.BorderType = BorderStyle.None;
                row.Cells[1].CellFormat.Borders.Color = Syncfusion.Drawing.Color.FromArgb(255, 255, 255);
                row.Cells[1].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                row.Cells[1].CellFormat.BackColor = Syncfusion.Drawing.Color.FromArgb(255, 255, 255);
                //Appends the text
                IWTextRange txt = cellPara.AppendText("");
                cellPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
                row.Cells[0].Width = 350;
                row.Cells[1].Width = 150;
                #endregion first row

                #region Second row
                //Gets the table first row
                row = table.Rows[1];
                row.Height = 25f;
                //Adds a new paragraph to the cell
                cellPara = row.Cells[0].AddParagraph();
                row.Cells[0].CellFormat.Borders.BorderType = BorderStyle.None;
                row.Cells[0].CellFormat.Borders.Color = Syncfusion.Drawing.Color.FromArgb(255, 255, 255);
                cellPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Left;
                //Appends the text
                paraText = "\n KCCA COVID 19 Response Situation Report for Reporting date of: " + _datatime;
                titletxt = cellPara.AppendText(paraText);
                titletxt.CharacterFormat.FontName = "Times New Roman";
                titletxt.CharacterFormat.Bold = false;
                titletxt.CharacterFormat.FontSize = 11f;
                titletxt.CharacterFormat.AllCaps = false;
                //Adds a new paragraph to the next cell
                cellPara = row.Cells[1].AddParagraph();
                row.Cells[1].CellFormat.Borders.BorderType = BorderStyle.None;
                row.Cells[1].CellFormat.Borders.Color = Syncfusion.Drawing.Color.FromArgb(255, 255, 255);
                row.Cells[1].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                row.Cells[1].CellFormat.BackColor = Syncfusion.Drawing.Color.FromArgb(255, 255, 255);
                //Appends the text
                txt = cellPara.AppendText("");
                cellPara.ParagraphFormat.HorizontalAlignment = HorizontalAlignment.Center;
                row.Cells[0].Width = 350;
                row.Cells[1].Width = 150;
                #endregion second row

                #endregion
                //Adds a new paragraph
                section.AddParagraph();

                paragraph = section.AddParagraph();

                string words = "Summary of statistics on Surveillance and Laboratory for COVID-19 as at:  " + _datatime;// DateTime.Now.ToString("dd") + ", " + DateTime.Now.DayOfWeek + ", " + DateTime.Now.ToString("MMMM") + " " + DateTime.Now.Year;
                txt = paragraph.AppendText(words);
                txt.CharacterFormat.TextColor = Syncfusion.Drawing.Color.FromArgb(192, 0, 0);
                txt.CharacterFormat.FontName = "Times New Roman";
                txt.CharacterFormat.Bold = true;
                txt.CharacterFormat.FontSize = 14f;
                section.AddParagraph();


                #region General Information
                //Adds a new table
                table = section.Body.AddTable();
                table.ResetCells((data.Count + 1), 9);
                row = table.Rows[0];
                row.Height = 20;
                row.Cells[0].Width = 30;
                //Adds a new paragraph
                cellPara = row.Cells[0].AddParagraph();
                //Sets a border type, color, background, and vertical alignment for cell
                row.Cells[0].CellFormat.Borders.BorderType = Syncfusion.DocIO.DLS.BorderStyle.Thick;
                row.Cells[0].CellFormat.Borders.Color = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                row.Cells[0].CellFormat.BackColor = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                row.Cells[0].CellFormat.VerticalAlignment = Syncfusion.DocIO.DLS.VerticalAlignment.Middle;
                txt = cellPara.AppendText("S/N");
                txt.CharacterFormat.FontName = "Calibri Light (Headings)";
                txt.CharacterFormat.Bold = true;
                txt.CharacterFormat.FontSize = 12f;

                // cell 2
                row.Cells[1].Width = 220;
                //Adds a new paragraph
                cellPara = row.Cells[1].AddParagraph();
                //Sets a border type, color, background, and vertical alignment for cell
                row.Cells[1].CellFormat.Borders.BorderType = Syncfusion.DocIO.DLS.BorderStyle.Thick;
                row.Cells[1].CellFormat.Borders.Color = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                row.Cells[1].CellFormat.BackColor = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                row.Cells[1].CellFormat.VerticalAlignment = Syncfusion.DocIO.DLS.VerticalAlignment.Middle;
                txt = cellPara.AppendText("Variable");
                txt.CharacterFormat.FontName = "Calibri Light (Headings)";
                txt.CharacterFormat.Bold = true;
                txt.CharacterFormat.FontSize = 12f;

                // cell 3
                row.Cells[2].Width = 170;
                //Adds a new paragraph
                cellPara = row.Cells[2].AddParagraph();
                //Sets a border type, color, background, and vertical alignment for cell
                row.Cells[2].CellFormat.Borders.BorderType = Syncfusion.DocIO.DLS.BorderStyle.Thick;
                row.Cells[2].CellFormat.Borders.Color = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                row.Cells[2].CellFormat.BackColor = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                row.Cells[2].CellFormat.VerticalAlignment = Syncfusion.DocIO.DLS.VerticalAlignment.Middle;
                txt = cellPara.AppendText("Category");
                txt.CharacterFormat.FontName = "Calibri Light (Headings)";
                txt.CharacterFormat.Bold = true;
                txt.CharacterFormat.FontSize = 12f;

                // cell 4
                row.Cells[3].Width = 60;
                //Adds a new paragraph
                cellPara = row.Cells[3].AddParagraph();
                //Sets a border type, color, background, and vertical alignment for cell
                row.Cells[3].CellFormat.Borders.BorderType = Syncfusion.DocIO.DLS.BorderStyle.Thick;
                row.Cells[3].CellFormat.Borders.Color = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                row.Cells[3].CellFormat.BackColor = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                row.Cells[3].CellFormat.VerticalAlignment = Syncfusion.DocIO.DLS.VerticalAlignment.Middle;
                txt = cellPara.AppendText("Kampala Central");
                txt.CharacterFormat.FontName = "Calibri Light (Headings)";
                txt.CharacterFormat.Bold = true;
                txt.CharacterFormat.FontSize = 12f;

                // cell 5
                row.Cells[4].Width = 62;
                //Adds a new paragraph
                cellPara = row.Cells[4].AddParagraph();
                //Sets a border type, color, background, and vertical alignment for cell
                row.Cells[4].CellFormat.Borders.BorderType = Syncfusion.DocIO.DLS.BorderStyle.Thick;
                row.Cells[4].CellFormat.Borders.Color = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                row.Cells[4].CellFormat.BackColor = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                row.Cells[4].CellFormat.VerticalAlignment = Syncfusion.DocIO.DLS.VerticalAlignment.Middle;
                txt = cellPara.AppendText("Nakawa");
                txt.CharacterFormat.FontName = "Calibri Light (Headings)";
                txt.CharacterFormat.Bold = true;
                txt.CharacterFormat.FontSize = 12f;

                // cell 5
                row.Cells[5].Width = 60;
                //Adds a new paragraph
                cellPara = row.Cells[5].AddParagraph();
                //Sets a border type, color, background, and vertical alignment for cell
                row.Cells[5].CellFormat.Borders.BorderType = Syncfusion.DocIO.DLS.BorderStyle.Thick;
                row.Cells[5].CellFormat.Borders.Color = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                row.Cells[5].CellFormat.BackColor = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                row.Cells[5].CellFormat.VerticalAlignment = Syncfusion.DocIO.DLS.VerticalAlignment.Middle;
                txt = cellPara.AppendText("Makindye");
                txt.CharacterFormat.FontName = "Calibri Light (Headings)";
                txt.CharacterFormat.Bold = true;
                txt.CharacterFormat.FontSize = 12f;


                // cell 6
                row.Cells[6].Width = 62;
                //Adds a new paragraph
                cellPara = row.Cells[6].AddParagraph();
                //Sets a border type, color, background, and vertical alignment for cell
                row.Cells[6].CellFormat.Borders.BorderType = Syncfusion.DocIO.DLS.BorderStyle.Thick;
                row.Cells[6].CellFormat.Borders.Color = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                row.Cells[6].CellFormat.BackColor = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                row.Cells[6].CellFormat.VerticalAlignment = Syncfusion.DocIO.DLS.VerticalAlignment.Middle;
                txt = cellPara.AppendText("Kawempe");
                txt.CharacterFormat.FontName = "Calibri Light (Headings)";
                txt.CharacterFormat.Bold = true;
                txt.CharacterFormat.FontSize = 12f;

                // cell 7
                row.Cells[7].Width = 60;
                //Adds a new paragraph
                cellPara = row.Cells[7].AddParagraph();
                //Sets a border type, color, background, and vertical alignment for cell
                row.Cells[7].CellFormat.Borders.BorderType = Syncfusion.DocIO.DLS.BorderStyle.Thick;
                row.Cells[7].CellFormat.Borders.Color = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                row.Cells[7].CellFormat.BackColor = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                row.Cells[7].CellFormat.VerticalAlignment = Syncfusion.DocIO.DLS.VerticalAlignment.Middle;
                txt = cellPara.AppendText("Rubaga");
                txt.CharacterFormat.FontName = "Calibri Light (Headings)";
                txt.CharacterFormat.Bold = true;
                txt.CharacterFormat.FontSize = 12f;

                // cell 8
                row.Cells[8].Width = 50;
                //Adds a new paragraph
                cellPara = row.Cells[8].AddParagraph();
                //Sets a border type, color, background, and vertical alignment for cell
                row.Cells[8].CellFormat.Borders.BorderType = Syncfusion.DocIO.DLS.BorderStyle.Thick;
                row.Cells[8].CellFormat.Borders.Color = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                row.Cells[8].CellFormat.BackColor = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                row.Cells[8].CellFormat.VerticalAlignment = Syncfusion.DocIO.DLS.VerticalAlignment.Middle;
                txt = cellPara.AppendText("Total");
                txt.CharacterFormat.FontName = "Calibri Light (Headings)";
                txt.CharacterFormat.Bold = true;
                txt.CharacterFormat.FontSize = 12f;


                int count = 1;
                foreach (var n in data)
                {
                    row = table.Rows[count];
                    row.Cells[0].Width = 30;
                    //Adds a new paragraph
                    cellPara = row.Cells[0].AddParagraph();
                    //Sets a border type, color, background, and vertical alignment for cell
                    row.Cells[0].CellFormat.Borders.BorderType = Syncfusion.DocIO.DLS.BorderStyle.Thick;
                    row.Cells[0].CellFormat.Borders.Color = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                    row.Cells[0].CellFormat.BackColor = Syncfusion.Drawing.Color.FromArgb(255, 255, 255);
                    row.Cells[0].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                    txt = cellPara.AppendText(count.ToString());
                    txt.CharacterFormat.TextColor = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                    txt.CharacterFormat.FontSize = 12f;

                    // cell 2
                    row.Cells[1].Width = 220;
                    //Adds a new paragraph
                    cellPara = row.Cells[1].AddParagraph();
                    //Sets a border type, color, background, and vertical alignment for cell
                    row.Cells[1].CellFormat.Borders.BorderType = Syncfusion.DocIO.DLS.BorderStyle.Thick;
                    row.Cells[1].CellFormat.Borders.Color = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                    row.Cells[1].CellFormat.BackColor = Syncfusion.Drawing.Color.FromArgb(255, 255, 255);
                    row.Cells[1].CellFormat.VerticalAlignment = Syncfusion.DocIO.DLS.VerticalAlignment.Middle;
                    if (n.Variable != null)
                    {
                        txt = cellPara.AppendText(n.Variable);
                        txt.CharacterFormat.TextColor = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                        txt.CharacterFormat.FontSize = 12f;
                    }

                    // cell 3
                    row.Cells[2].Width = 170;
                    //Adds a new paragraph
                    cellPara = row.Cells[2].AddParagraph();
                    //Sets a border type, color, background, and vertical alignment for cell
                    row.Cells[2].CellFormat.Borders.BorderType = Syncfusion.DocIO.DLS.BorderStyle.Thick;
                    row.Cells[2].CellFormat.Borders.Color = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                    row.Cells[2].CellFormat.BackColor = Syncfusion.Drawing.Color.FromArgb(255, 255, 255);
                    row.Cells[2].CellFormat.VerticalAlignment = Syncfusion.DocIO.DLS.VerticalAlignment.Middle;
                    if (n.Category != null)
                    {
                        txt = cellPara.AppendText(n.Category);
                        txt.CharacterFormat.TextColor = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                        txt.CharacterFormat.FontSize = 12f;
                    }

                    // cell 4
                    row.Cells[3].Width = 60;
                    //Adds a new paragraph
                    cellPara = row.Cells[3].AddParagraph();
                    //Sets a border type, color, background, and vertical alignment for cell
                    row.Cells[3].CellFormat.Borders.BorderType = Syncfusion.DocIO.DLS.BorderStyle.Thick;
                    row.Cells[3].CellFormat.Borders.Color = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                    row.Cells[3].CellFormat.BackColor = Syncfusion.Drawing.Color.FromArgb(255, 255, 255);
                    row.Cells[3].CellFormat.VerticalAlignment = Syncfusion.DocIO.DLS.VerticalAlignment.Middle;
                    if (n.KampalaCentralDivision != null)
                    {
                        txt = cellPara.AppendText(n.KampalaCentralDivision.ToString());
                        txt.CharacterFormat.TextColor = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                        txt.CharacterFormat.FontSize = 12f;
                        txt.CharacterFormat.Bold = true;
                    }


                    row.Cells[4].Width = 62;
                    //Adds a new paragraph
                    cellPara = row.Cells[4].AddParagraph();
                    //Sets a border type, color, background, and vertical alignment for cell
                    row.Cells[4].CellFormat.Borders.BorderType = Syncfusion.DocIO.DLS.BorderStyle.Thick;
                    row.Cells[4].CellFormat.Borders.Color = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                    row.Cells[4].CellFormat.BackColor = Syncfusion.Drawing.Color.FromArgb(255, 255, 255);
                    row.Cells[4].CellFormat.VerticalAlignment = Syncfusion.DocIO.DLS.VerticalAlignment.Middle;
                    if (n.NakawaDivision != null)
                    {
                        txt = cellPara.AppendText(n.NakawaDivision.ToString());
                        txt.CharacterFormat.TextColor = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                        txt.CharacterFormat.FontSize = 12f;
                        txt.CharacterFormat.Bold = true;
                    }

                    row.Cells[5].Width = 60;
                    //Adds a new paragraph
                    cellPara = row.Cells[5].AddParagraph();
                    //Sets a border type, color, background, and vertical alignment for cell
                    row.Cells[5].CellFormat.Borders.BorderType = Syncfusion.DocIO.DLS.BorderStyle.Thick;
                    row.Cells[5].CellFormat.Borders.Color = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                    row.Cells[5].CellFormat.BackColor = Syncfusion.Drawing.Color.FromArgb(255, 255, 255);
                    row.Cells[5].CellFormat.VerticalAlignment = Syncfusion.DocIO.DLS.VerticalAlignment.Middle;
                    if (n.MakindyeDivision != null)
                    {
                        txt = cellPara.AppendText(n.MakindyeDivision.ToString());
                        txt.CharacterFormat.TextColor = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                        txt.CharacterFormat.FontSize = 12f;
                        txt.CharacterFormat.Bold = true;
                    }

                    row.Cells[6].Width = 62;
                    //Adds a new paragraph
                    cellPara = row.Cells[6].AddParagraph();
                    //Sets a border type, color, background, and vertical alignment for cell
                    row.Cells[6].CellFormat.Borders.BorderType = Syncfusion.DocIO.DLS.BorderStyle.Thick;
                    row.Cells[6].CellFormat.Borders.Color = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                    row.Cells[6].CellFormat.BackColor = Syncfusion.Drawing.Color.FromArgb(255, 255, 255);
                    row.Cells[6].CellFormat.VerticalAlignment = Syncfusion.DocIO.DLS.VerticalAlignment.Middle;
                    if (n.KawempeDivision != null)
                    {
                        txt = cellPara.AppendText(n.KawempeDivision.ToString());
                        txt.CharacterFormat.TextColor = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                        txt.CharacterFormat.FontSize = 12f;
                        txt.CharacterFormat.Bold = true;
                    }

                    row.Cells[7].Width = 60;
                    //Adds a new paragraph
                    cellPara = row.Cells[7].AddParagraph();
                    //Sets a border type, color, background, and vertical alignment for cell
                    row.Cells[7].CellFormat.Borders.BorderType = Syncfusion.DocIO.DLS.BorderStyle.Thick;
                    row.Cells[7].CellFormat.Borders.Color = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                    row.Cells[7].CellFormat.BackColor = Syncfusion.Drawing.Color.FromArgb(255, 255, 255);
                    row.Cells[7].CellFormat.VerticalAlignment = Syncfusion.DocIO.DLS.VerticalAlignment.Middle;
                    if (n.RubagaDivision != null)
                    {
                        txt = cellPara.AppendText(n.RubagaDivision.ToString());
                        txt.CharacterFormat.TextColor = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                        txt.CharacterFormat.FontSize = 12f;
                        txt.CharacterFormat.Bold = true;
                    }

                    row.Cells[8].Width = 50;
                    //Adds a new paragraph
                    cellPara = row.Cells[8].AddParagraph();
                    //Sets a border type, color, background, and vertical alignment for cell
                    row.Cells[8].CellFormat.Borders.BorderType = Syncfusion.DocIO.DLS.BorderStyle.Thick;
                    row.Cells[8].CellFormat.Borders.Color = Syncfusion.Drawing.Color.FromArgb(0, 0, 0);
                    row.Cells[8].CellFormat.BackColor = Syncfusion.Drawing.Color.FromArgb(255, 255, 255);
                    row.Cells[8].CellFormat.VerticalAlignment = Syncfusion.DocIO.DLS.VerticalAlignment.Middle;
                    if (n.TotalCount != null)
                    {
                        txt = cellPara.AppendText(n.TotalCount.ToString());
                        txt.CharacterFormat.TextColor = Syncfusion.Drawing.Color.FromArgb(192, 0, 0);
                        txt.CharacterFormat.FontSize = 14f;
                        txt.CharacterFormat.Bold = true;
                    }

                    count = count + 1;
                }

                //KAMPALA CENTRAL DIVISION
                if ((xdiv != null) /*&& (xdiv.CountyName == "KAMPALA CENTRAL DIVISION")*/)
                {
                    //_division = xdiv.CountyName;
                    var _central = _context.ViewSitRepComments.FirstOrDefault(o => o.ReportingDate.Date == edate.Date && o.CountyName == "KAMPALA CENTRAL DIVISION");

                    if (_central != null)
                    {
                        section = document.AddSection();
                        section.AddParagraph();
                        paragraph = section.AddParagraph();

                        titletxt = paragraph.AppendText("WASH for Kampala Central Division");
                        titletxt.CharacterFormat.FontName = "Times New Roman";
                        titletxt.CharacterFormat.Bold = true;
                        titletxt.CharacterFormat.FontSize = 16f;
                        titletxt.CharacterFormat.AllCaps = false;
                        titletxt.CharacterFormat.TextColor = Color.FromArgb(192, 0, 0);
                        section.AddParagraph();

                        section.AddParagraph();
                        paragraph = section.AddParagraph();
                        if (_central.WashComments != null)
                        {
                            paragraph.AppendText(_central.WashComments);
                        }
                        section.AddParagraph();


                        section.AddParagraph();
                        paragraph = section.AddParagraph();
                        titletxt = paragraph.AppendText("RISK Communication for Kampala Central Division");
                        titletxt.CharacterFormat.FontName = "Times New Roman";
                        titletxt.CharacterFormat.Bold = true;
                        titletxt.CharacterFormat.FontSize = 16f;
                        titletxt.CharacterFormat.AllCaps = false;
                        titletxt.CharacterFormat.TextColor = Color.FromArgb(192, 0, 0);
                        section.AddParagraph();

                        section.AddParagraph();
                        paragraph = section.AddParagraph();
                        if (_central.RiskComments != null)
                        {
                            paragraph.AppendText(_central.RiskComments);
                        }
                        section.AddParagraph();


                        section.AddParagraph();
                        paragraph = section.AddParagraph();
                        titletxt = paragraph.AppendText("CO-ORDINATION & LEADERSHIP for Kampala Central Division");
                        titletxt.CharacterFormat.FontName = "Times New Roman";
                        titletxt.CharacterFormat.Bold = true;
                        titletxt.CharacterFormat.FontSize = 16f;
                        titletxt.CharacterFormat.AllCaps = false;
                        titletxt.CharacterFormat.TextColor = Color.FromArgb(192, 0, 0);
                        section.AddParagraph();

                        section.AddParagraph();
                        paragraph = section.AddParagraph();
                        if (_central.UpdateComments != null)
                        {
                            paragraph.AppendText(_central.UpdateComments);
                        }
                        section.AddParagraph();


                        section.AddParagraph();
                        paragraph = section.AddParagraph();
                        titletxt = paragraph.AppendText("Challenges & Recommendations for Kampala Central Division");
                        titletxt.CharacterFormat.FontName = "Times New Roman";
                        titletxt.CharacterFormat.Bold = true;
                        titletxt.CharacterFormat.FontSize = 16f;
                        titletxt.CharacterFormat.AllCaps = false;
                        titletxt.CharacterFormat.TextColor = Color.FromArgb(192, 0, 0);
                        section.AddParagraph();

                        section.AddParagraph();
                        paragraph = section.AddParagraph();
                        if (_central.GeneralComments != null)
                        {
                            paragraph.AppendText(_central.GeneralComments);
                        }
                        section.AddParagraph();
                    }
                }

                //NAKAWA DIVISION
                if ((xdiv != null) /*&& (xdiv.CountyName == "NAKAWA DIVISION")*/)
                {
                    //_division = xdiv.CountyName;
                    var _nakawa = _context.ViewSitRepComments.FirstOrDefault(o => o.ReportingDate.Date == edate.Date && o.CountyName == "NAKAWA DIVISION");
                    
                    if (_nakawa != null)
                    {
                        section = document.AddSection();
                        section.AddParagraph();
                        paragraph = section.AddParagraph();

                        titletxt = paragraph.AppendText("WASH for Nakawa Division");
                        titletxt.CharacterFormat.FontName = "Times New Roman";
                        titletxt.CharacterFormat.Bold = true;
                        titletxt.CharacterFormat.FontSize = 16f;
                        titletxt.CharacterFormat.AllCaps = false;
                        titletxt.CharacterFormat.TextColor = Color.FromArgb(192, 0, 0);
                        section.AddParagraph();

                        section.AddParagraph();
                        paragraph = section.AddParagraph();
                        if (_nakawa.WashComments != null)
                        {
                            paragraph.AppendText(_nakawa.WashComments);
                        }
                        section.AddParagraph();


                        section.AddParagraph();
                        paragraph = section.AddParagraph();
                        titletxt = paragraph.AppendText("RISK Communication for Nakawa Division");
                        titletxt.CharacterFormat.FontName = "Times New Roman";
                        titletxt.CharacterFormat.Bold = true;
                        titletxt.CharacterFormat.FontSize = 16f;
                        titletxt.CharacterFormat.AllCaps = false;
                        titletxt.CharacterFormat.TextColor = Color.FromArgb(192, 0, 0);
                        section.AddParagraph();

                        section.AddParagraph();
                        paragraph = section.AddParagraph();
                        if (_nakawa.RiskComments != null)
                        {
                            paragraph.AppendText(_nakawa.RiskComments);
                        }
                        section.AddParagraph();


                        section.AddParagraph();
                        paragraph = section.AddParagraph();
                        titletxt = paragraph.AppendText("CO-ORDINATION & LEADERSHIP for Nakawa Division");
                        titletxt.CharacterFormat.FontName = "Times New Roman";
                        titletxt.CharacterFormat.Bold = true;
                        titletxt.CharacterFormat.FontSize = 16f;
                        titletxt.CharacterFormat.AllCaps = false;
                        titletxt.CharacterFormat.TextColor = Color.FromArgb(192, 0, 0);
                        section.AddParagraph();

                        section.AddParagraph();
                        paragraph = section.AddParagraph();
                        if (_nakawa.UpdateComments != null)
                        {
                            paragraph.AppendText(_nakawa.UpdateComments);
                        }
                        section.AddParagraph();


                        section.AddParagraph();
                        paragraph = section.AddParagraph();
                        titletxt = paragraph.AppendText("Challenges & Recommendations for Nakawa Division");
                        titletxt.CharacterFormat.FontName = "Times New Roman";
                        titletxt.CharacterFormat.Bold = true;
                        titletxt.CharacterFormat.FontSize = 16f;
                        titletxt.CharacterFormat.AllCaps = false;
                        titletxt.CharacterFormat.TextColor = Color.FromArgb(192, 0, 0);
                        section.AddParagraph();

                        section.AddParagraph();
                        paragraph = section.AddParagraph();
                        if (_nakawa.GeneralComments != null)
                        {
                            paragraph.AppendText(_nakawa.GeneralComments);
                        }
                        section.AddParagraph();
                    }

                }

                //MAKINDYE DIVISION
                if ((xdiv != null) /*&& (xdiv.CountyName == "MAKINDYE DIVISION")*/)
                {
                    //_division = xdiv.CountyName;
                    var _makindye = _context.ViewSitRepComments.FirstOrDefault(o => o.ReportingDate.Date == edate.Date && o.CountyName == "MAKINDYE DIVISION");
                   
                    if (_makindye != null)
                    {
                        section = document.AddSection();
                        section.AddParagraph();
                        paragraph = section.AddParagraph();

                        titletxt = paragraph.AppendText("WASH for Makindye Division");
                        titletxt.CharacterFormat.FontName = "Times New Roman";
                        titletxt.CharacterFormat.Bold = true;
                        titletxt.CharacterFormat.FontSize = 16f;
                        titletxt.CharacterFormat.AllCaps = false;
                        titletxt.CharacterFormat.TextColor = Color.FromArgb(192, 0, 0);
                        section.AddParagraph();

                        section.AddParagraph();
                        paragraph = section.AddParagraph();
                        if (_makindye.WashComments != null)
                        {
                            paragraph.AppendText(_makindye.WashComments);
                        }
                        section.AddParagraph();


                        section.AddParagraph();
                        paragraph = section.AddParagraph();
                        titletxt = paragraph.AppendText("RISK Communication for Makindye Division");
                        titletxt.CharacterFormat.FontName = "Times New Roman";
                        titletxt.CharacterFormat.Bold = true;
                        titletxt.CharacterFormat.FontSize = 16f;
                        titletxt.CharacterFormat.AllCaps = false;
                        titletxt.CharacterFormat.TextColor = Color.FromArgb(192, 0, 0);
                        section.AddParagraph();

                        section.AddParagraph();
                        paragraph = section.AddParagraph();
                        if (_makindye.RiskComments != null)
                        {
                            paragraph.AppendText(_makindye.RiskComments);
                        }
                        section.AddParagraph();


                        section.AddParagraph();
                        paragraph = section.AddParagraph();
                        titletxt = paragraph.AppendText("CO-ORDINATION & LEADERSHIP for Makindye Division");
                        titletxt.CharacterFormat.FontName = "Times New Roman";
                        titletxt.CharacterFormat.Bold = true;
                        titletxt.CharacterFormat.FontSize = 16f;
                        titletxt.CharacterFormat.AllCaps = false;
                        titletxt.CharacterFormat.TextColor = Color.FromArgb(192, 0, 0);
                        section.AddParagraph();

                        section.AddParagraph();
                        paragraph = section.AddParagraph();
                        if (_makindye.UpdateComments != null)
                        {
                            paragraph.AppendText(_makindye.UpdateComments);
                        }
                        section.AddParagraph();


                        section.AddParagraph();
                        paragraph = section.AddParagraph();
                        titletxt = paragraph.AppendText("Challenges & Recommendations for Makindye Division");
                        titletxt.CharacterFormat.FontName = "Times New Roman";
                        titletxt.CharacterFormat.Bold = true;
                        titletxt.CharacterFormat.FontSize = 16f;
                        titletxt.CharacterFormat.AllCaps = false;
                        titletxt.CharacterFormat.TextColor = Color.FromArgb(192, 0, 0);
                        section.AddParagraph();

                        section.AddParagraph();
                        paragraph = section.AddParagraph();
                        if (_makindye.GeneralComments != null)
                        {
                            paragraph.AppendText(_makindye.GeneralComments);
                        }
                        section.AddParagraph();
                    }
                }

                //KAWEMPE DIVISION
                if ((xdiv != null) /*&& (xdiv.CountyName == "KAWEMPE DIVISION")*/)
                {
                    //_division = xdiv.CountyName;
                    var _kawempe = _context.ViewSitRepComments.FirstOrDefault(o => o.ReportingDate.Date == edate.Date && o.CountyName == "KAWEMPE DIVISION");
                   
                    if (_kawempe != null)
                    {
                        section = document.AddSection();
                        section.AddParagraph();
                        paragraph = section.AddParagraph();

                        titletxt = paragraph.AppendText("WASH for Kawempe Division");
                        titletxt.CharacterFormat.FontName = "Times New Roman";
                        titletxt.CharacterFormat.Bold = true;
                        titletxt.CharacterFormat.FontSize = 16f;
                        titletxt.CharacterFormat.AllCaps = false;
                        titletxt.CharacterFormat.TextColor = Color.FromArgb(192, 0, 0);
                        section.AddParagraph();

                        section.AddParagraph();
                        paragraph = section.AddParagraph();
                        if (_kawempe.WashComments != null)
                        {
                            paragraph.AppendText(_kawempe.WashComments);
                        }
                        section.AddParagraph();


                        section.AddParagraph();
                        paragraph = section.AddParagraph();
                        titletxt = paragraph.AppendText("RISK Communication for Kawempe Division");
                        titletxt.CharacterFormat.FontName = "Times New Roman";
                        titletxt.CharacterFormat.Bold = true;
                        titletxt.CharacterFormat.FontSize = 16f;
                        titletxt.CharacterFormat.AllCaps = false;
                        titletxt.CharacterFormat.TextColor = Color.FromArgb(192, 0, 0);
                        section.AddParagraph();

                        section.AddParagraph();
                        paragraph = section.AddParagraph();
                        if (_kawempe.RiskComments != null)
                        {
                            paragraph.AppendText(_kawempe.RiskComments);
                        }
                        section.AddParagraph();


                        section.AddParagraph();
                        paragraph = section.AddParagraph();
                        titletxt = paragraph.AppendText("CO-ORDINATION & LEADERSHIP for Kawempe Division");
                        titletxt.CharacterFormat.FontName = "Times New Roman";
                        titletxt.CharacterFormat.Bold = true;
                        titletxt.CharacterFormat.FontSize = 16f;
                        titletxt.CharacterFormat.AllCaps = false;
                        titletxt.CharacterFormat.TextColor = Color.FromArgb(192, 0, 0);
                        section.AddParagraph();

                        section.AddParagraph();
                        paragraph = section.AddParagraph();
                        if (_kawempe.UpdateComments != null)
                        {
                            paragraph.AppendText(_kawempe.UpdateComments);
                        }
                        section.AddParagraph();


                        section.AddParagraph();
                        paragraph = section.AddParagraph();
                        titletxt = paragraph.AppendText("Challenges & Recommendations for Kawempe Division");
                        titletxt.CharacterFormat.FontName = "Times New Roman";
                        titletxt.CharacterFormat.Bold = true;
                        titletxt.CharacterFormat.FontSize = 16f;
                        titletxt.CharacterFormat.AllCaps = false;
                        titletxt.CharacterFormat.TextColor = Color.FromArgb(192, 0, 0);
                        section.AddParagraph();

                        section.AddParagraph();
                        paragraph = section.AddParagraph();
                        if (_kawempe.GeneralComments != null)
                        {
                            paragraph.AppendText(_kawempe.GeneralComments);
                        }
                        section.AddParagraph();
                    }
                }

                //RUBAGA DIVISION
                if ((xdiv != null) /*&& (xdiv.CountyName == "RUBAGA DIVISION")*/)
                {
                    //_division = xdiv.CountyName;
                    var _rubaga = _context.ViewSitRepComments.FirstOrDefault(o => o.ReportingDate.Date == edate.Date && o.CountyName == "RUBAGA DIVISION");
                    if (_rubaga != null)
                    {
                        section = document.AddSection();
                        section.AddParagraph();
                        paragraph = section.AddParagraph();

                        titletxt = paragraph.AppendText("WASH for Rubaga Division");
                        titletxt.CharacterFormat.FontName = "Times New Roman";
                        titletxt.CharacterFormat.Bold = true;
                        titletxt.CharacterFormat.FontSize = 16f;
                        titletxt.CharacterFormat.AllCaps = false;
                        titletxt.CharacterFormat.TextColor = Color.FromArgb(192, 0, 0);
                        section.AddParagraph();

                        section.AddParagraph();
                        paragraph = section.AddParagraph();
                        if (_rubaga.WashComments != null)
                        {
                            paragraph.AppendText(_rubaga.WashComments);
                        }
                        section.AddParagraph();


                        section.AddParagraph();
                        paragraph = section.AddParagraph();
                        titletxt = paragraph.AppendText("RISK Communication for Rubaga Division");
                        titletxt.CharacterFormat.FontName = "Times New Roman";
                        titletxt.CharacterFormat.Bold = true;
                        titletxt.CharacterFormat.FontSize = 16f;
                        titletxt.CharacterFormat.AllCaps = false;
                        titletxt.CharacterFormat.TextColor = Color.FromArgb(192, 0, 0);
                        section.AddParagraph();

                        section.AddParagraph();
                        paragraph = section.AddParagraph();
                        if (_rubaga.RiskComments != null)
                        {
                            paragraph.AppendText(_rubaga.RiskComments);
                        }
                        section.AddParagraph();


                        section.AddParagraph();
                        paragraph = section.AddParagraph();
                        titletxt = paragraph.AppendText("CO-ORDINATION & LEADERSHIP for Rubaga Division");
                        titletxt.CharacterFormat.FontName = "Times New Roman";
                        titletxt.CharacterFormat.Bold = true;
                        titletxt.CharacterFormat.FontSize = 16f;
                        titletxt.CharacterFormat.AllCaps = false;
                        titletxt.CharacterFormat.TextColor = Color.FromArgb(192, 0, 0);
                        section.AddParagraph();

                        section.AddParagraph();
                        paragraph = section.AddParagraph();
                        if (_rubaga.UpdateComments != null)
                        {
                            paragraph.AppendText(_rubaga.UpdateComments);
                        }
                        section.AddParagraph();


                        section.AddParagraph();
                        paragraph = section.AddParagraph();
                        titletxt = paragraph.AppendText("Challenges & Recommendations for Rubaga Division");
                        titletxt.CharacterFormat.FontName = "Times New Roman";
                        titletxt.CharacterFormat.Bold = true;
                        titletxt.CharacterFormat.FontSize = 16f;
                        titletxt.CharacterFormat.AllCaps = false;
                        titletxt.CharacterFormat.TextColor = Color.FromArgb(192, 0, 0);
                        section.AddParagraph();

                        section.AddParagraph();
                        paragraph = section.AddParagraph();
                        if (_rubaga.GeneralComments != null)
                        {
                            paragraph.AppendText(_rubaga.GeneralComments);
                        }
                        section.AddParagraph();
                    }
                }

                //Adds a new paragraph
                section.AddParagraph();
                paragraph = section.AddParagraph();
                paragraph.AppendText("For and on Behalf of KCCA, Combined Situation Report (SitRep)");
                section.AddParagraph();


                #endregion

                FormatType type = FormatType.Docx;
                string filename = "KCCA COVID 19 Combined SITREP REPORT AS AT " + DateTime.Now.ToString("dd") + ", " + DateTime.Now.DayOfWeek + ", " + DateTime.Now.ToString("MMMM") + " " + DateTime.Now.Year + ".docx";
                string contenttype = "application/vnd.ms-word.document.12";

                MemoryStream ms = new MemoryStream();
                document.Save(ms, type);
                document.Close();
                ms.Position = 0;
                return File(ms, contenttype, filename);
            }
            else
            {
                return RedirectToAction("CombinedSitRepReports");
            }
            //return RedirectToAction("SitRepReports");
        }
    }
}
