﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Syncfusion.XlsIO;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using CHMIS.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using CHMIS.BLL;
using Microsoft.EntityFrameworkCore;

namespace CHMIS.Controllers
{
    public class UploadCasesController : Controller
    {
        private IWebHostEnvironment hostingEnv;
        private CHMISContext _context;
        private IUserService userInfor;
        public UploadCasesController(CHMISContext context, IWebHostEnvironment env, IUserService _userinfo)
        {
            this._context = context;
            this.hostingEnv = env;
            this.userInfor = _userinfo;
        }
        public IActionResult UploadCases()
        {
            return View();
        }

        // Instantiate random number generator.  
        private readonly Random _random = new Random();

        // Generates a random number within a range.      
        public int RandomNumber(int min, int max)
        {
            return _random.Next(min, max);
        }

        [AcceptVerbs("Post")]
        public IActionResult SaveCases(IList<IFormFile> UploadFiles)
        {
            try
            {
                foreach (var file in UploadFiles)
                {
                    if (UploadFiles != null)
                    {
                        //var filename = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"') + "-" + DateTime.Now.ToString("dd") + "-" +  DateTime.Now.ToString("MMMM") + " " + DateTime.Now.Minute + " " + DateTime.Now.Second + RandomNumber(0, 1957588) +".xlsx";
                        var filename = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Replace(".xlx","").Replace(".xlsx", "").Trim('"') + "-" + DateTime.Now.Minute + "-" + DateTime.Now.Second + "-" + RandomNumber(0, 19857588) + ".xlsx";
                       
                        var fileSavePath = hostingEnv.WebRootPath + "\\UploadedFiles";
                        filename = fileSavePath + $@"\{filename}";
                        if (!System.IO.File.Exists(filename))
                        {
                            using (FileStream fs = System.IO.File.Create(filename))
                            {
                                file.CopyTo(fs);
                                fs.Flush();
                            }
                        }
                        else
                        {
                            System.IO.File.Delete(filename);
                            using (FileStream fs = System.IO.File.Create(filename))
                            {
                                file.CopyTo(fs);
                                fs.Flush();
                            }

                        }
                        ImportSpreadsheetData(filename);

                        //using (FileStream fs = System.IO.File.Create(filename))
                        //{
                        //    //file.CopyTo(fs);
                        //    //fs.Flush();
                        //    ImportSpreadsheetData(filename);
                        //}

                    }
                }
            }
            catch (Exception e)
            {
                Response.Clear();
                Response.ContentType = "application/json; charset=utf-8";
                Response.StatusCode = 204;
                Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = "No Content";
                Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = e.Message;
            }
            return Content("");
        }
        [AcceptVerbs("Post")]
        public IActionResult RemoveCases(IList<IFormFile> UploadFiles)
        {
            try
            {
                foreach (var file in UploadFiles)
                {
                    var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    var filePath = hostingEnv.WebRootPath + "\\UploadedFiles";
                    var fileSavePath = filePath + "\\" + fileName;
                    if (!System.IO.File.Exists(fileSavePath))
                    {
                        System.IO.File.Delete(fileSavePath);
                    }
                }
            }
            catch (Exception e)
            {
                Response.Clear();
                Response.StatusCode = 200;
                Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = "File removed successfully";
                Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = e.Message;
            }
            return Content("");
        }

        //ImportSpreadsheetData
        private void ImportSpreadsheetData(string file)
        {
            //EmailJob obx = new EmailJob();
            //try
            {
                Stream fileStream = new FileStream(file, FileMode.Open, FileAccess.Read);
                string format = "dd/MM/yyyy";
                CultureInfo current = new CultureInfo("en-US");
                //string filePath = file;

                ExcelEngine excelEngine = new ExcelEngine();
                IApplication application = excelEngine.Excel;
                IWorkbook workbook = application.Workbooks.Open(fileStream);
                IWorksheet worksheet = workbook.Worksheets[0];

                //Import cases
                #region "Import cases"
                if (
                     (worksheet.Range["A1"].Value.Contains("No.")) &&
                     (worksheet.Range["B1"].Value.Contains("Name")) &&
                     (worksheet.Range["C1"].Value.Contains("Case no.")) &&
                     (worksheet.Range["D1"].Value.Contains("Sex")) &&
                     (worksheet.Range["E1"].Value.Contains("Age")) &&
                     (worksheet.Range["F1"].Value.Contains("Village")) &&
                     (worksheet.Range["G1"].Value.Contains("Division")) &&
                     (worksheet.Range["H1"].Value.Contains("Sampling District")) &&
                     (worksheet.Range["I1"].Value.Contains("Sampling site")) &&
                     (worksheet.Range["J1"].Value.Contains("Sampling date")) &&
                     (worksheet.Range["K1"].Value.Contains("Specify if Alert")) &&
                     (worksheet.Range["L1"].Value.Contains("Phone Contact ")) &&
                     (worksheet.Range["M1"].Value.Contains("Testing Lab")) )
                {
                    CoreCases _cases = new CoreCases();

                    for (int row = 2; row <= worksheet.UsedRange.LastRow; row++)
                    {
                        if (worksheet.IsRowVisible(row))
                        {
                            for (int column = 1; column <= worksheet.UsedRange.LastColumn; column++)
                            {
                                switch (column)
                                {
                                    case 1: // sn.
                                        if (!string.IsNullOrEmpty(worksheet.Range[row, column].Value) || (worksheet.Range[row, column].Value != ""))
                                        {
                                            string sn = worksheet.Range[row, column].Value.ToString();
                                        }
                                        break;

                                    case 2: // Name
                                        if(!string.IsNullOrEmpty(worksheet.Range[row, column].Value) || (worksheet.Range[row, column].Value != ""))
                                        {
                                            string Name = worksheet.Range[row, column].Value.ToString();
                                            _cases.FirstName = Name;
                                            _cases.LastName = Name;
                                        }
                                        break;

                                    case 3: // External Case no.
                                        
                                        if (!string.IsNullOrEmpty(worksheet.Range[row, column].Value) || (worksheet.Range[row, column].Value != ""))
                                        {
                                            string ExternalCase = worksheet.Range[row, column].Value.ToString();
                                            _cases.ExternalCaseNo = ExternalCase;
                                        }
                                       
                                        break;
                                    case 4://Sex
                                        if (!string.IsNullOrEmpty(worksheet.Range[row, column].Value) || (worksheet.Range[row, column].Value != ""))
                                        {
                                            string Sex = worksheet.Range[row, column].Value.ToString();
                                            if (!string.IsNullOrEmpty(Sex))
                                            {
                                                int? gender = 0;
                                                if (Sex.Contains("M"))
                                                {
                                                    gender = 1;
                                                }
                                                else
                                                {
                                                    gender = 2;
                                                }
                                                if (gender != 0)
                                                {
                                                    _cases.Sex = Convert.ToInt32(gender);
                                                }
                                            }
                                        }
                                        break;


                                    case 5://Age

                                        if (!string.IsNullOrEmpty(worksheet.Range[row, column].Value) || (worksheet.Range[row, column].Value != ""))
                                        {
                                            string Age = worksheet.Range[row, column].Value.ToString();
                                            _cases.Age = Age;
                                        }
                                        break;


                                    case 6://Village, District of residence
                                        if (!string.IsNullOrEmpty(worksheet.Range[row, column].Value) || (worksheet.Range[row, column].Value != "") )
                                        {
                                            string VillageID = worksheet.Range[row, column].Value.ToString();
                                            if (VillageID.Contains(','))
                                            {
                                                VillageID = worksheet.Range[row, column].Value.ToString().Substring(0, worksheet.Range[row, column].Value.ToString().IndexOf(',')).Trim();
                                            }

                                            var Village = "";
                                            if (!string.IsNullOrEmpty(VillageID))
                                            {
                                                var Village1 = _context.AVillage.FirstOrDefault(a => a.VillageName.Contains("VillageID"));

                                                if (Village1 != null)
                                                //if(!string.IsNullOrEmpty(Village1))
                                                {
                                                    Village = Village1.VillageId;
                                                }
                                                //var Village = Village1.VillageId;// _context.AVillage.FirstOrDefault(a => a.VillageName.Contains("VillageID")).VillageId;
                                                if (Village == "")
                                                {
                                                    _cases.Address = null;
                                                    _cases.VillageId = null;
                                                }
                                                else
                                                {
                                                    _cases.Address = Village;
                                                    _cases.VillageId = Village;
                                                }
                                            }
                                        }
                                        break;

                                    case 7://Division/District
                                        if (!string.IsNullOrEmpty(worksheet.Range[row, column].Value) || (worksheet.Range[row, column].Value != ""))
                                        {
                                            string comments = worksheet.Range[row, column].Value.ToString();
                                            _cases.GeneralComments = comments;
                                        }

                                        break;

                                    case 8://Sampling District
                                        if (!string.IsNullOrEmpty(worksheet.Range[row, column].Value) || (worksheet.Range[row, column].Value != ""))
                                        {
                                            string District = worksheet.Range[row, column].Value.ToString();
                                            _cases.SamplingDistrict = District;
                                        }
                                        break;

                                    case 9://Sampling site or facility
                                        if (!string.IsNullOrEmpty(worksheet.Range[row, column].Value) || (worksheet.Range[row, column].Value != ""))
                                        {
                                            string Samplingfacility = worksheet.Range[row, column].Value.ToString();
                                            _cases.SamplingFacility = Samplingfacility;
                                        }
                                        break;

                                    case 10://Sampling date
                                        if (!string.IsNullOrEmpty(worksheet.Range[row, column].Value) || (worksheet.Range[row, column].Value != ""))
                                        {
                                            string SamplingDate = worksheet.Range[row, column].DateTime.ToLongDateString();
                                            DateTime _date = DateTime.Now;
                                            if (!string.IsNullOrEmpty(SamplingDate))
                                            {
                                                if (!SamplingDate.Contains("January 1, 0001"))
                                                {
                                                    string _sdate = Convert.ToDateTime(SamplingDate).ToString(format);
                                                    if (DateTime.TryParseExact(_sdate, format, current, DateTimeStyles.None, out _date))
                                                    {
                                                        _cases.DateOfReporting = _date;
                                                        _cases.SamplingDate = _date;
                                                    }
                                                }
                                            }
                                        }
                                        break;


                                    case 11: //Specify if Alert, contact or truck driver (TD)
                                        if (!string.IsNullOrEmpty(worksheet.Range[row, column].Value) || (worksheet.Range[row, column].Value != ""))
                                        {
                                            string alert = worksheet.Range[row, column].Value.ToString();
                                            _cases.AlertType = alert;
                                        }
                                        break;

                                    case 12://Phone Contact 
                                        if (!string.IsNullOrEmpty(worksheet.Range[row, column].Value) || (worksheet.Range[row, column].Value != ""))
                                        {
                                            string tel = worksheet.Range[row, column].Value.ToString();
                                            _cases.Tel = tel;
                                        }
                                        break;

                                    case 13://Testing Lab
                                        if (!string.IsNullOrEmpty(worksheet.Range[row, column].Value) || (worksheet.Range[row, column].Value != ""))
                                        {
                                            string TestingLab = worksheet.Range[row, column].Value.ToString();
                                            _cases.TestingLab = TestingLab;
                                        }
                                        break;

                                }
                            }
                            _cases.CaseNo = GetCaseNo();
                            _cases.Disease = 1;
                            _cases.AddedBy = userInfor.GetFullName();
                            _cases.DateAdded = DateTime.Now;

                            CoreCases _table = _context.CoreCases.FirstOrDefault(o => /*o.CaseNo == _cases.CaseNo && */ o.ExternalCaseNo == _cases.ExternalCaseNo);

                            if (_table == null)
                            {
                                CoreCases table = new CoreCases();
                                table.ExternalCaseNo = _cases.ExternalCaseNo;
                                table.FirstName = _cases.FirstName;
                                table.MiddleName = _cases.MiddleName;
                                table.LastName = _cases.LastName;
                                table.WasContact = _cases.WasContact;
                                table.Occupation = _cases.Occupation;
                                table.OccupationSpecify = _cases.OccupationSpecify;
                                table.RiskLevel = _cases.RiskLevel;
                                table.Sex = _cases.Sex;
                                table.DoB = _cases.DoB;
                                table.Age = _cases.Age;
                                table.DateOfReporting = _cases.DateOfReporting;
                                table.RiskReason = _cases.RiskReason;
                                table.Address = _cases.Address;
                                table.DistrictId = _cases.DistrictId;
                                table.CountyId = _cases.CountyId;
                                table.SubcountyId = _cases.SubcountyId;
                                table.ParishId = _cases.ParishId;
                                table.VillageId = _cases.VillageId;
                                table.FacilityUnit = _cases.FacilityUnit;
                                table.Tel = _cases.Tel;
                                table.Email = _cases.Email;
                                table.Longitude = _cases.Longitude;
                                table.Latitude = _cases.Latitude;
                                table.Classification = _cases.Classification;
                                table.AddedBy = userInfor.GetFullName();
                                table.DateAdded = DateTime.Now;
                                //table.DateEdited = DateTime.Now;
                                //table.EditedBy = userInfor.GetFullName();  //_cases.CreatedBy;
                                table.ClassificationSpecify = _cases.ClassificationSpecify;
                                table.DateOnset = _cases.DateOnset;
                                table.CaseDate = _cases.CaseDate;
                                table.DateOfInfection = _cases.DateOfInfection;
                                table.Outcome = _cases.Outcome;
                                table.OutcomeSpecify = _cases.OutcomeSpecify;
                                table.DateOfOutcome = _cases.DateOfOutcome;
                                table.LabTestDate = _cases.LabTestDate;
                                table.LabResults = _cases.LabResults;
                                table.Disease = 1;// userInfor.getCurrentuserDisease();// _cases.Disease;
                                table.Symptoms = _cases.Symptoms;
                                table.SymptomsOthers = _cases.SymptomsOthers;
                                table.NextAction = _cases.NextAction;
                                table.StatusFlag = _cases.StatusFlag;
                                table.GeneralComments = _cases.GeneralComments;
                                table.SamplingDistrict = _cases.SamplingDistrict;
                                table.SamplingFacility = _cases.SamplingFacility;
                                table.SamplingDate = _cases.SamplingDate;
                                table.TestingLab = _cases.TestingLab;
                                table.AlertType = _cases.AlertType;
                                //_context.Entry(table).CurrentValues.SetValues(_cases);
                                //_context.Entry(table).State = EntityState.Modified;
                                 _context.CoreCases.Add(_cases);
                                try
                                {
                                    _context.SaveChanges();
                                }
                                catch
                                {

                                }
                            }
                            else
                            {
                                _table.ExternalCaseNo = _cases.ExternalCaseNo;
                                _table.FirstName = _cases.FirstName;
                                _table.MiddleName = _cases.MiddleName;
                                _table.LastName = _cases.LastName;
                                _table.WasContact = _cases.WasContact;
                                _table.Occupation = _cases.Occupation;
                                _table.OccupationSpecify = _cases.OccupationSpecify;
                                _table.RiskLevel = _cases.RiskLevel;
                                _table.Sex = _cases.Sex;
                                _table.DoB = _cases.DoB;
                                _table.Age = _cases.Age;
                                _table.DateOfReporting = _cases.DateOfReporting;
                                _table.RiskReason = _cases.RiskReason;
                                _table.Address = _cases.Address;
                                _table.DistrictId = _cases.DistrictId;
                                _table.CountyId = _cases.CountyId;
                                _table.SubcountyId = _cases.SubcountyId;
                                _table.ParishId = _cases.ParishId;
                                _table.VillageId = _cases.VillageId;
                                _table.FacilityUnit = _cases.FacilityUnit;
                                _table.Tel = _cases.Tel;
                                _table.Email = _cases.Email;
                                _table.Longitude = _cases.Longitude;
                                _table.Latitude = _cases.Latitude;
                                _table.Classification = _cases.Classification;
                                //_table.AddedBy = userInfor.GetFullName();
                                //_table.DateAdded = DateTime.Now;
                                _table.DateEdited = DateTime.Now;
                                _table.EditedBy = userInfor.GetFullName();  //_cases.CreatedBy;
                                _table.ClassificationSpecify = _cases.ClassificationSpecify;
                                _table.DateOnset = _cases.DateOnset;
                                _table.CaseDate = _cases.CaseDate;
                                _table.DateOfInfection = _cases.DateOfInfection;
                                _table.Outcome = _cases.Outcome;
                                _table.OutcomeSpecify = _cases.OutcomeSpecify;
                                _table.DateOfOutcome = _cases.DateOfOutcome;
                                _table.LabTestDate = _cases.LabTestDate;
                                _table.LabResults = _cases.LabResults;
                                _table.Disease = 1;// userInfor.getCurrentuserDisease();// _cases.Disease;
                                _table.Symptoms = _cases.Symptoms;
                                _table.SymptomsOthers = _cases.SymptomsOthers;
                                _table.NextAction = _cases.NextAction;
                                _table.StatusFlag = _cases.StatusFlag;
                                _table.GeneralComments = _cases.GeneralComments;
                                _table.SamplingDistrict = _cases.SamplingDistrict;
                                _table.SamplingFacility = _cases.SamplingFacility;
                                _table.SamplingDate = _cases.SamplingDate;
                                _table.TestingLab = _cases.TestingLab;
                                _table.AlertType = _cases.AlertType;
                                _context.Entry(_table).CurrentValues.SetValues(_cases);
                                _context.Entry(_table).State = EntityState.Modified;
                                //_context.CoreCases.Add(_cases);
                                try
                                {
                                    _context.SaveChanges();
                                }
                                catch
                                {

                                }
                            }
                            
                            
                            //To Send email alert to the responsible person

                            //EmailJob objemail = new EmailJob();
                            //foreach (var i in x)
                            //{
                            //    if (!string.IsNullOrEmpty(i))
                            //    {
                            //        objx.CountryID = System.Convert.ToInt32(i);
                            //        //objemail.SendEmail(objx.case);
                            //    }
                            //}
                        }
                    }
                }
                #endregion "Import cases" 

                workbook.Close();
                excelEngine.Dispose();
            }

        }

        //get case number code 
        public string GetCaseNo()
        {
            DateTime _date = System.DateTime.Now;
            int year = _date.Year;
            string month = _date.Month.ToString();
            if (month.Length < 2)
                month = "0" + month;

            string day = _date.Day.ToString();
            if (day.Length < 2)
                day = "0" + day;

            //string partial_no = "";
            string partial_no = year + "-" + month + "-" + day;//Day + Month + Year

            string _disease = string.Empty;
            var disease = _context.ADiseaseType.FirstOrDefault(o => o.DiseaseId == userInfor.getCurrentuserDisease());
            if (disease != null)
            {
                _disease = disease.Disease;
            }

            string code = _disease + "-IMPORTED-GO-CASE-" + userInfor.GetEmployeeID() + "-" + partial_no + "- 00";

            int result = 0;
            result = _context.CoreCases.ToList().Count();
            code += (result + 1);
            //var x = new CoreCases()
            //{
            //    CaseNo = code
            //};
            //_context.CoreCases.Add(x);
            //_context.SaveChanges();

            return code;
        }
    }
}
