﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using CHMIS.BLL;
using CHMIS.Models;
using CHMIS.ModelView;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Syncfusion.EJ2.Base;
using Syncfusion.EJ2.DropDowns;

namespace CHMIS.Controllers
{
    public class Account : Controller
    {
        private IWebHostEnvironment hostingEnv;
        private IUserService userInfor;
        private CHMISContext _context;
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly ILogger<Account> _logger;
        private readonly IEmailSender _emailSender;

        public Account(CHMISContext Context,
            UserManager<IdentityUser> userManager,
            SignInManager<IdentityUser> signInManager,
            IWebHostEnvironment env,
            ILogger<Account> logger,
            IEmailSender emailSender, IUserService _userinfo)
        {
            this.hostingEnv = env;
            this.userInfor = _userinfo;
            this._context = Context;
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _emailSender = emailSender;
        }


        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult ChangePassword()
        {
            ResetPasswordViewModel m = new ResetPasswordViewModel();
            var userName = userInfor.GetUser().Identity.Name;
            var user = _context.AspNetUsers.FirstOrDefault(o => o.UserName == userName);
            m.Id = user.Id;
            m.Email = user.Email;
            return View(m);
        }
        [HttpPost]
        public async Task<IActionResult> ChangePassword(ResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByIdAsync(model.Id);
                var response1 = await _userManager.RemovePasswordAsync(user);
                if (response1.Succeeded)
                {
                    var response2 = await _userManager.AddPasswordAsync(user, model.Password);
                    if (response2.Succeeded)
                    {
                        await _signInManager.SignOutAsync();
                        return RedirectToAction("Login", "Account");
                    }
                    else
                    {
                        ModelState.AddModelError("", response2.Errors.ToString());
                    }
                }
                else
                {
                    ModelState.AddModelError("", response1.Errors.ToString());
                }
            }
            ResetPasswordViewModel m = new ResetPasswordViewModel();
            var userName = userInfor.GetUser().Identity.Name;
            var _user = _context.AspNetUsers.FirstOrDefault(o => o.UserName == userName);
            m.Id = _user.Id;
            m.Email = _user.Email;
            return View(m);
        }

        [HttpGet]
        public ViewResult Register()
        {
            ViewBag.SuccessRegistration = null;
            ViewBag.Employee = _context.AEmployees.Select(o => new { o.EmployeeId, o.Email, o.Phone, o.Name }).ToList();
            ViewBag.UserRoles = _context.AspNetRoles.ToList();
            ViewBag.Modules = _context.ASystemModules.ToList();
            ViewBag.Section = _context.ASystemSection.ToList();
            ViewBag.Facility = _context.AFacilityUnit.ToList();
            ViewBag.OperationalArea = _context.AOperationalArea.ToList();

            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new IdentityUser { UserName = model.UserName, Email = model.Email,PhoneNumber = model.PhoneNumber , EmailConfirmed = true };
                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    var _result = await this._userManager.AddToRoleAsync(user, model.UserRole);
                    if (_result.Succeeded)
                    {
                        //if (!string.IsNullOrEmpty(model.Modules.ToString()))
                        {
                            //assign Seleceted modules
                            //if (model.Modules.Count() > 0)
                            if (model.Modules != null)
                            {
                                ASelecetedModules _Modules = new ASelecetedModules()
                                {
                                    UserId = user.Id,
                                    SelectedModules = string.Join(",", model.Modules)
                                };
                                _context.ASelecetedModules.Add(_Modules);
                                _context.SaveChanges();
                            }
                        }

                        //if (!string.IsNullOrEmpty(model.Section.ToString()))
                        {
                            //assign Seleceted Sections
                            //if (model.Section.Count() > 0)
                            if (model.Section != null)
                            {
                                ASelecetedSections _Section = new ASelecetedSections()
                                {
                                    UserId = user.Id,
                                    SelectedSectors = string.Join(",", model.Section)
                                };
                                _context.ASelecetedSections.Add(_Section);
                                _context.SaveChanges();
                            }
                        }

                        var exists = _context.AspNetUsers.Find(user.Id);
                        if (exists != null)
                        {
                            if (exists.Facility != null)
                            {
                                exists.Facility = string.Join(",", model.Facility);
                            }
                           // if (exists.OperationalArea != null)
                            {
                                exists.OperationalArea = model.OperationalArea;
                            }
                            //exists.FacilityCode = System.Convert.ToInt32(model.FacilityCode);
                            
                            exists.LockoutEnabled = false;
                            exists.EmailConfirmed = true;
                            _context.Entry(exists).State = EntityState.Modified;
                            _context.SaveChanges();
                        }

                        var exist = _context.AEmployees.FirstOrDefault(o => o.EmployeeId == model.EmployeeId);
                        if(exist != null)
                        {
                            exist.UserId = user.Id;
                            _context.Entry(exist).State = EntityState.Modified;
                            _context.SaveChanges();

                            var _code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                            _code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(_code));
                            var callbackUrl = new UriBuilder(Request.Scheme, Request.Host.Host, Convert.ToInt32(Request.Host.Port)).ToString() + "Account/ConfirmEmail?userId="+ user.Id + "&&code="+ _code;
                                
                            EmailModel email = new EmailModel(_context);
                            email.ComfirmEmail(model.Email, callbackUrl);

                            ViewBag.SuccessRegistration = "<div class='badge badge-primary' style='background-color:forestgreen;padding:10px;'>"
                                               + "<label>" + exist.Name + " Has been Registered sucessfully </label></div> ";
                        }
                    }
                    ViewBag.Employee = _context.AEmployees.Select(o=> new { o.EmployeeId,o.Email,o.Phone,o.Name}).ToList();
                    ViewBag.UserRoles = _context.AspNetRoles.ToList();
                    ViewBag.Modules = _context.ASystemModules.ToList();
                    ViewBag.Section = _context.ASystemSection.ToList();
                    ViewBag.Facility = _context.AFacilityUnit.ToList();
                    ViewBag.OperationalArea = _context.AOperationalArea.ToList();

                    return View();
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }

            ViewBag.Employee = _context.ViewEmployee.ToList();
            ViewBag.UserRoles = _context.AspNetRoles.ToList();

            return View();
        }
        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            ConfirmEmailModel status = new ConfirmEmailModel();
            try
            {
                status.StatusMessage = "Error confirming your email.";
                var user = _context.AspNetUsers.FirstOrDefault(o => o.Id == userId);
                if (user != null)
                {
                    user.LockoutEnabled = false;
                    user.EmailConfirmed = true;
                    _context.Entry(user).State = EntityState.Modified;
                    _context.SaveChanges();

                    var _user = await _userManager.FindByIdAsync(userId);

                    code = Encoding.UTF8.GetString(WebEncoders.Base64UrlDecode(code));
                    var result = await _userManager.ConfirmEmailAsync(_user, code);
                    //var callbackUrl = new UriBuilder(Request.Scheme, Request.Host.Host, Convert.ToInt32(Request.Host.Port)).ToString() + "Account/Login";

                    status.StatusMessage = result.Succeeded ? "Thank you for confirming your email. Please go to the login page to access the system" : "Error confirming your email. Please contact your IT administrator for further assistence on this issue";
                }
            }
            catch {
                status.StatusMessage = "Error confirming your email. Please contact your IT administrator for further assistence on this issue";
            }
            return View("ConfirmEmail", status);
        }

        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Login", "Account");
        }
        [HttpGet]
        public IActionResult Login(string returnUrl = "")
        {
            ViewBag.disease = _context.ADiseaseType.ToList();
            var model = new LoginViewModel { ReturnUrl = returnUrl };
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = _context.AspNetUsers.FirstOrDefault(o => (o.Email == model.Username || o.UserName == model.Username));
                if (user != null)
                {
                    model.Username = user.UserName;
                }

                var result = await _signInManager.PasswordSignInAsync(model.Username,
                   model.Password, model.RememberMe, false);

                if (result.Succeeded)
                {
                    var roles = _context.AspNetUserRoles.FirstOrDefault(o=> o.UserId== user.Id);
                    if (roles != null)
                    {
                        int _id = 1;
                        var _data = _context.ALoginLog.ToList();
                        if(_data.Count> 0)
                        {
                            _id = _data.Last().Id + 1;
                        }

                        ALoginLog log = new ALoginLog()
                        {
                            Id= _id,
                            UserId = user.Id,
                            Disease = model.DiseaseType,
                            LogInDate = DateTime.Now,
                            Longitude = model.Longitude,
                            Latitude = model.latitude
                        };
                        _context.ALoginLog.Add(log);
                        _context.SaveChanges();
                        //return RedirectToAction("DashBoard", "DashBoard");
                        return RedirectToAction("Index", "Home");
                    }

                }
            }
            ModelState.AddModelError("", "Invalid login attempt");
            ViewBag.disease = _context.ADiseaseType.ToList();
            return View(model);
        }
        public IActionResult EmployeeManagement()
        {
            ViewBag.district = _context.ADistrict.ToList();
            ViewBag.county = _context.ACounty.ToList();
            ViewBag.subcounty = _context.ASubcounty.ToList();
            ViewBag.parish = _context.AParish.ToList();
            ViewBag.village = _context.AVillage.ToList();
            ViewBag.gender = _context.ASex.ToList();
            ViewBag.nationality = _context.ACountry.ToList();
            ViewBag.title2 = _context.ATitle.ToList();
            //ViewBag.TitleName = "Employee Management";
            return View();
        }
        public ActionResult DataSourceEmployee([FromBody] DataManagerRequest dm)
        {
            var _data = _context.ViewEmployee.OrderBy(o=> o.Name).ToList();
            IEnumerable data = _data;
            int _count = _data.Count();

            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                _count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                _count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip != 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take != 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = _count });
        }
        public ActionResult DialogueEmployeeSave([FromBody] CRUDModel<ViewEmployee> value)
        {
            if(value.Action== "insert")
            {
                if(value.Value.EmployeeId > 0)
                {
                    var data = _context.AEmployees.FirstOrDefault(o => o.EmployeeId == value.Value.EmployeeId);
                    data.Name = value.Value.Name;
                    data.Email = value.Value.Email;
                    data.Phone = value.Value.Phone;
                    data.Telephone = value.Value.Telephone;
                    data.NinNo = value.Value.NinNo;
                    data.Gender = value.Value.Gender;
                    data.Nationality = value.Value.Nationality;
                    data.Title = value.Value.Title;
                    data.UserId = value.Value.UserId;
                    data.DateOfBirth = value.Value.DateOfBirth;
                    //data.EditedBy = userInfor.GetFullName();
                    data.EditionDate = DateTime.Now;
                    data.PhysicalAddress = value.Value.PhysicalAddress;
                    _context.Entry(data).State = EntityState.Modified;
                    _context.SaveChanges();
                }
                else
                {
                    AEmployees data = new AEmployees();
                    data.Name = value.Value.Name;
                    data.Email = value.Value.Email;
                    data.Phone = value.Value.Phone;
                    data.Telephone = value.Value.Telephone;
                    data.NinNo = value.Value.NinNo;
                    data.Gender = value.Value.Gender;
                    data.Nationality = value.Value.Nationality;
                    data.Title = value.Value.Title;
                    data.UserId = value.Value.UserId;
                    data.DateOfBirth = value.Value.DateOfBirth;
                   // data.CreatedBy = userInfor.GetFullName();
                    data.CreationDate = DateTime.Now;
                    data.PhysicalAddress = value.Value.PhysicalAddress;
                    data.EmployeeId = EmployeeId();
                    _context.AEmployees.Add(data);
                    _context.SaveChanges();
                }
            }
            else if (value.Action == "update")
            {
                var data = _context.AEmployees.FirstOrDefault(o => o.EmployeeId == value.Value.EmployeeId);
                data.Name = value.Value.Name;
                data.Email = value.Value.Email;
                data.Phone = value.Value.Phone;
                data.Telephone = value.Value.Telephone;
                data.NinNo = value.Value.NinNo;
                data.Gender = value.Value.Gender;
                data.Nationality = value.Value.Nationality;
                data.Title = value.Value.Title;
                data.UserId = value.Value.UserId;
                data.DateOfBirth = value.Value.DateOfBirth;
                //data.EditedBy = userInfor.GetFullName();
                data.EditionDate = DateTime.Now;
                data.PhysicalAddress = value.Value.PhysicalAddress;
                _context.Entry(data).State = EntityState.Modified;
                _context.SaveChanges();
            }
            return Json(value);
        }
        public ActionResult GetEmployeeId()
        {
            int id = EmployeeId();
            AEmployees data = new AEmployees();
            data.EmployeeId = id;
            _context.AEmployees.Add(data);
            _context.SaveChanges();

            return Json(id);
        }
        public ActionResult RemoveEmployee(int id)
        {
            _context.AEmployees.Remove(_context.AEmployees.FirstOrDefault(o=> o.EmployeeId==id));
            int result =_context.SaveChanges();

            return Json(result);
        }
        public int EmployeeId()
        {
            int result = 1;
            var _data = _context.AEmployees.ToList();
            if(_data.Count > 0)
            {
                result = (_data.Last().EmployeeId + 1);
            }
            return result;
        }
        public ActionResult GetImage(int EmployeeId)
        {
            string imagtype = "image/jpg";
            byte[] imageData = new byte[1];
            var exists = _context.AEmployees.FirstOrDefault(o => o.EmployeeId == EmployeeId);
            if (exists != null)
            {
                imageData = exists.Photo;
                imagtype = "image/" + exists.PhotoType;
            }
            return File(imageData, imagtype);
        }
        [AcceptVerbs("Post")]
        public IActionResult EmployeeImageSave(IList<IFormFile> PhotoUpload, int EmployeeId)
        {
            try
            {
                foreach (var file in PhotoUpload)
                {
                    if (PhotoUpload != null)
                    {
                        var filename = Path.GetFileName(file.FileName); ///ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                       // filename = hostingEnv.WebRootPath + $@"\{filename}";
                        var fileSavePath = Path.Combine(hostingEnv.WebRootPath + "\\UploadedFiles\\", filename);
                        if (!System.IO.File.Exists(fileSavePath))
                        {
                            //using (FileStream fs = System.IO.File.Create(filename))
                            //{
                            //    file.CopyTo(fs);
                            //    fs.Flush(); 
                            //}
                            byte[] fileBytes;
                            using (MemoryStream ms = new MemoryStream())
                            {
                                file.OpenReadStream().CopyTo(ms);
                                fileBytes = ms.ToArray();
                            }

                            string ext = Path.GetExtension(filename);
                            var exist = _context.AEmployees.FirstOrDefault(o => o.EmployeeId == EmployeeId);
                            if(exist != null)
                            {
                                exist.Photo = fileBytes;
                                exist.PhotoType = ext;
                                _context.Entry(exist).State = EntityState.Modified;
                                _context.SaveChanges();
                            }
                        }
                        else
                        {
                            Response.Clear();
                            Response.StatusCode = 204;
                            Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = "File already exists.";
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Response.Clear();
                Response.ContentType = "application/json; charset=utf-8";
                Response.StatusCode = 204;
                Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = "No Content";
                Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = e.Message;
            }
            return Content("");
        }
        [AcceptVerbs("Post")]
        public IActionResult EmployeeImageRemove(IList<IFormFile> PhotoUpload, int EmployeeId)
        {
            try
            {
                foreach (var file in PhotoUpload)
                {
                    var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    var filePath = Path.Combine(hostingEnv.WebRootPath + "\\UploadedFiles");
                    var fileSavePath = filePath + "\\" + fileName;
                    if (!System.IO.File.Exists(fileSavePath))
                    {
                        System.IO.File.Delete(fileSavePath);
                    }
                    var exist = _context.AEmployees.FirstOrDefault(o => o.EmployeeId == EmployeeId);
                    if (exist != null)
                    {
                        exist.Photo = null;
                        //exist.PhotoType = null;
                        _context.Entry(exist).State = EntityState.Modified;
                        _context.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                Response.Clear();
                Response.StatusCode = 200;
                Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = "File removed successfully";
                Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = e.Message;
            }
            return Content("");
        }

        public IActionResult PasswordManagement()
        {
            ViewBag.promptbutton = new
            {
                content = "Reset password",
                isPrimary = true
            };
            ViewBag.promptbutton1 = new
            {
                content = "Cancel",
            };
            return View();
        }
        public async Task<IActionResult> ResetUserPassword(string Id, string password)
        {
            string result = string.Empty;
            try
            {
                var user = await _userManager.FindByIdAsync(Id);
                var response1 = await _userManager.RemovePasswordAsync(user);
                if (response1.Succeeded)
                {
                    var response2 = await _userManager.AddPasswordAsync(user, password);
                    if (response2.Succeeded)
                    {

                        result = "User password has been successfully changed";
                    }
                    else
                    {
                        result = "Failed to add new password";
                    }
                }
                else
                {
                    result = "failed to remove the old password";
                }
            }
            catch (Exception ex)
            {
                result = ex.ToString();
            }
            return Json(result);
        }
        public ActionResult GetUsers([FromBody] DataManagerRequest dm)
        {
            var _data = _context.ViewUserManagement.OrderBy(o => o.Name).Where(o=> o.UserId != null).ToList();
            IEnumerable data = _data;
            int _count = _data.Count;

            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                _count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                _count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip > 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take > 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = _count });
        }

        public IActionResult UserManagement()
        {
            ViewBag.Employee = _context.AEmployees.Select(o => new { o.EmployeeId, o.Email, o.Phone, o.Name }).ToList();
            ViewBag.Modules = _context.ASystemModules.ToList();
            ViewBag.Section = _context.ASystemSection.ToList();
            ViewBag.Facility = _context.AFacilityUnit.ToList();
            ViewBag.OperationalArea = _context.AOperationalArea.ToList();
            ViewBag.Roles = _context.AspNetRoles.Where(o => o.Name != "MobileUser").ToList();

            return View();
        }
        public ActionResult DataSourceUserManagementNew([FromBody] DataManagerRequest dm)
        {

            var _data = _context.ViewUserManagementNew.OrderBy(a => a.UserName).ToList();
            IEnumerable data = _data;
            int _count = _data.Count;

            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                _count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                _count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip > 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take > 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = _count });

        }
        public async Task<IActionResult> UpdateUserManagement([FromBody] CRUDModel<ViewUserManagementNew> value)
        {
            var exists = _context.AspNetUsers.Find(value.Value.Id);
            if (exists != null)
            {
                //exists.FacilityCode = value.Value.FacilityCode;
                exists.Facility = string.Join(",", value.Value.Facility);
                exists.OperationalArea = value.Value.OperationalArea;
                exists.LockoutEnabled = value.Value.LockoutEnabled;
                exists.PhoneNumber = value.Value.PhoneNumber;
                exists.UserName = value.Value.UserName;
                exists.Email = value.Value.Email;
                _context.Entry(exists).State = EntityState.Modified;
                _context.SaveChanges();

                var user = await _userManager.FindByIdAsync(value.Value.Id);

                var userRoles = await _userManager.GetRolesAsync(user);
                foreach (string r in userRoles)
                {
                    await _userManager.RemoveFromRoleAsync(user, r);
                }

                await _userManager.AddToRoleAsync(user, value.Value.RoleName);
                if (!string.IsNullOrEmpty(value.Value.SelectedModules))
                {
                    var modules_exist = _context.ASelecetedModules.Find(value.Value.Id);
                    if (modules_exist != null)
                    {
                        modules_exist.SelectedModules = value.Value.SelectedModules;
                        _context.Entry(modules_exist).State = EntityState.Modified;
                    }
                    else
                    {
                        ASelecetedModules _Modules = new ASelecetedModules()
                        {
                            UserId = value.Value.Id,
                            SelectedModules = value.Value.SelectedModules
                        };
                        _context.ASelecetedModules.Add(_Modules);
                    }

                    _context.SaveChanges();
                }
                if (!string.IsNullOrEmpty(value.Value.SelectedSections))
                {
                    var exist_section = _context.ASelecetedSections.Find(value.Value.Id);
                    if (exist_section != null)
                    {
                        exist_section.SelectedSectors = value.Value.SelectedSections;
                        _context.Entry(exist_section).State = EntityState.Modified;
                    }
                    else
                    {
                        ASelecetedSections _Section = new ASelecetedSections()
                        {
                            UserId = value.Value.Id,
                            SelectedSectors = value.Value.SelectedSections
                        };
                        _context.ASelecetedSections.Add(_Section);
                    }

                    _context.SaveChanges();
                }

            }
            return Json(value);
        }


    }
}
