﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using CHMIS.Models;
using System;
using Microsoft.EntityFrameworkCore;
using Syncfusion.EJ2;
using System.Collections;
using Syncfusion.EJ2.Base;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Http;
using System.Net.Http.Headers;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using CHMIS.BLL;

namespace CHMIS.Controllers
{
    public class ContactsController : Controller
    {
        private CHMISContext _context;
        private IWebHostEnvironment hostingEnv;
        private IUserService userInfor;
        public ContactsController(CHMISContext Context, IWebHostEnvironment env, IUserService _userinfo)
        {
            this.userInfor = _userinfo;
            this._context = Context;
            this.hostingEnv = env;
        }
        public IActionResult Contacts()
        { 
            //logins
            var _logins = _context.ALoginLog.AsNoTracking().ToList();
            var logins = _logins.Where(o => Convert.ToDateTime(o.LogInDate).Date == DateTime.Now.Date).ToList().Count;
            ViewBag.LogCount = logins;
            //YesNo
            var YesNo = _context.AYesNo.AsNoTracking().ToList();
            ViewBag.YesNo = YesNo;
            //Classification
            var Classification = _context.AClassification.AsNoTracking().ToList();
            ViewBag.Classification = Classification;
            //County
            var County = _context.ACounty.AsNoTracking().ToList();
            ViewBag.County = County;
            //District
            var District = _context.ADistrict.AsNoTracking().ToList();
            ViewBag.District = District;
            //Sub-county
            var Subcounty = _context.ASubcounty.AsNoTracking().ToList();
            ViewBag.Subcounty = Subcounty;
            //Parish
            var Parish = _context.AParish.AsNoTracking().ToList();
            ViewBag.Parish = Parish;
            //Village
            var Village = _context.AVillage.AsNoTracking().ToList();
            ViewBag.Village = Village;
            //Address
            var Address = _context.VwAdminUnits.AsNoTracking().ToList();
            ViewBag.Address = Address;
            //COVID 19 OUTCOMES
            var Outcomes = _context.AOutcome.AsNoTracking().ToList();
            ViewBag.Outcomes = Outcomes;
            //Disease Type
            var Disease = _context.ADiseaseType.AsNoTracking().ToList();
            ViewBag.Disease = Disease;
            //Facility Type
            var Facility = _context.AFacilityUnit.AsNoTracking().ToList();
            ViewBag.Facility = Facility;
            //Lab Results
            var LabResults = _context.ALabResults.AsNoTracking().ToList();
            ViewBag.LabResults = LabResults;
            //Lab Results
            var Risk = _context.ARiskLevel.AsNoTracking().ToList();
            ViewBag.Risk = Risk;
            //Sex
            var Sex = _context.ASex.AsNoTracking().ToList();
            ViewBag.Sex = Sex;
            //Occupation
            var Occupation = _context.AOccupation.AsNoTracking().ToList();
            ViewBag.Occupation = Occupation;
            //Symptoms
            var Symptoms = _context.ASymptoms.AsNoTracking().ToList();
            ViewBag.Symptoms = Symptoms;
            //Symptoms
            var Relationship = _context.ARelationship.AsNoTracking().ToList();
            ViewBag.Relationship = Relationship;
            ViewBag.Employee = _context.ViewEmployee.ToList();
            ViewBag.Elevate = _context.AElevateContact.Where(x => x.Id == 1).ToList();

            return View();
        }

        //Partial View for adding a new case
        public IActionResult AddPartial([FromBody] CRUDModel<CoreCaseContacts> value)
        {
            //logins
            var _logins = _context.ALoginLog.AsNoTracking().ToList();
            var logins = _logins.Where(o => Convert.ToDateTime(o.LogInDate).Date == DateTime.Now.Date).ToList().Count;
            ViewBag.LogCount = logins;
            //YesNo
            var YesNo = _context.AYesNo.AsNoTracking().ToList();
            ViewBag.YesNo = YesNo;
            //Classification
            var Classification = _context.AClassification.AsNoTracking().ToList();
            ViewBag.Classification = Classification;
            //County
            var County = _context.ACounty.AsNoTracking().ToList();
            ViewBag.County = County;
            //District
            var District = _context.ADistrict.AsNoTracking().ToList();
            ViewBag.District = District;
            //Sub-county
            var Subcounty = _context.ASubcounty.AsNoTracking().ToList();
            ViewBag.Subcounty = Subcounty;
            //Parish
            var Parish = _context.AParish.AsNoTracking().ToList();
            ViewBag.Parish = Parish;
            //Village
            var Village = _context.AVillage.AsNoTracking().ToList();
            ViewBag.Village = Village;
            //Address
            var Address = _context.VwAdminUnits.AsNoTracking().ToList();
            ViewBag.Address = Address;
            //COVID 19 OUTCOMES
            var Outcomes = _context.AOutcome.AsNoTracking().ToList();
            ViewBag.Outcomes = Outcomes;
            //Disease Type
            var Disease = _context.ADiseaseType.AsNoTracking().ToList();
            ViewBag.Disease = Disease;
            //Facility Type
            var Facility = _context.AFacilityUnit.AsNoTracking().ToList();
            ViewBag.Facility = Facility;
            //Lab Results
            var LabResults = _context.ALabResults.AsNoTracking().ToList();
            ViewBag.LabResults = LabResults;
            //Lab Results
            var Risk = _context.ARiskLevel.AsNoTracking().ToList();
            ViewBag.Risk = Risk;
            //Sex
            var Sex = _context.ASex.AsNoTracking().ToList();
            ViewBag.Sex = Sex;
            //Occupation
            var Occupation = _context.AOccupation.AsNoTracking().ToList();
            ViewBag.Occupation = Occupation;
            var Pkeys = GetContactNo(value.Value.CaseNo);
            //case No.
            var Relationship = _context.ARelationship.AsNoTracking().ToList();
            ViewBag.Relationship = Relationship;
            ViewBag.Employee = _context.ViewEmployee.ToList();

            //if (value.Action == "insert")
            //{
            //    var Pkeys = GetContactNo(value.Value.CaseNo);
            //    value.Value.ContactCaseNo = Pkeys.ToString();
            //}

            if (!string.IsNullOrEmpty(value.Value.Symptoms))
            {
                var Symptomsvalue = new int[] { };
                var _Symptoms = value.Value.Symptoms.Split(',');
                int count = 0;
                Symptomsvalue = new int[_Symptoms.Count()];
                foreach (var n in _Symptoms)
                {
                    Symptomsvalue[count] = Convert.ToInt32(n);
                    count++;
                }
                ViewBag.SymptomsValue = Symptomsvalue;
            }
            else
            {
                ViewBag.SymptomsValue = null;
            }

            var Symptoms = _context.ASymptoms.AsNoTracking().ToList();
            ViewBag.Symptoms = Symptoms;
            //value.Value.CaseNo = GetContactNo(CaseNo);
            value.Value.ContactCaseNo = Pkeys.ToString();
            //Symptoms
            //var Symptoms = _context.ASymptoms.AsNoTracking().ToList();
            //ViewBag.Symptoms = Symptoms;
            //ExposureType
            var ExposureType = _context.AExposureType.AsNoTracking().ToList();
            ViewBag.ExposureType = ExposureType;
            ViewBag.Elevate = _context.AElevateContact.Where(x => x.Id == 1).ToList();

            return PartialView("_DialogAddPartial", value.Value);
        }
        public ActionResult EditPartial([FromBody] CRUDModel<CoreCaseContacts> value)
        {
            //logins
            var _logins = _context.ALoginLog.AsNoTracking().ToList();
            var logins = _logins.Where(o => Convert.ToDateTime(o.LogInDate).Date == DateTime.Now.Date).ToList().Count;
            ViewBag.LogCount = logins;
            //YesNo
            var YesNo = _context.AYesNo.AsNoTracking().ToList();
            ViewBag.YesNo = YesNo;
            //Classification
            var Classification = _context.AClassification.AsNoTracking().ToList();
            ViewBag.Classification = Classification;
            //County
            var County = _context.ACounty.AsNoTracking().ToList();
            ViewBag.County = County;
            //District
            var District = _context.ADistrict.AsNoTracking().ToList();
            ViewBag.District = District;
            //Sub-county
            var Subcounty = _context.ASubcounty.AsNoTracking().ToList();
            ViewBag.Subcounty = Subcounty;
            //Parish
            var Parish = _context.AParish.AsNoTracking().ToList();
            ViewBag.Parish = Parish;
            //Village
            var Village = _context.AVillage.AsNoTracking().ToList();
            ViewBag.Village = Village;
            //Address
            var Address = _context.VwAdminUnits.AsNoTracking().ToList();
            ViewBag.Address = Address;
            //COVID 19 OUTCOMES
            var Outcomes = _context.AOutcome.AsNoTracking().ToList();
            ViewBag.Outcomes = Outcomes;
            //Disease Type
            var Disease = _context.ADiseaseType.AsNoTracking().ToList();
            ViewBag.Disease = Disease;
            //Facility Type
            var Facility = _context.AFacilityUnit.AsNoTracking().ToList();
            ViewBag.Facility = Facility;
            //Lab Results
            var LabResults = _context.ALabResults.AsNoTracking().ToList();
            ViewBag.LabResults = LabResults;
            //Lab Results
            var Risk = _context.ARiskLevel.AsNoTracking().ToList();
            ViewBag.Risk = Risk;
            //Sex
            var Sex = _context.ASex.AsNoTracking().ToList();
            ViewBag.Sex = Sex;
            //Occupation
            var Occupation = _context.AOccupation.AsNoTracking().ToList();
            ViewBag.Occupation = Occupation;
            //Symptoms
            var Relationship = _context.ARelationship.AsNoTracking().ToList();
            ViewBag.Relationship = Relationship;
            ViewBag.Employee = _context.ViewEmployee.ToList();

            if (!string.IsNullOrEmpty(value.Value.Symptoms))
            {
                var Symptomsvalue = new int[] { };
                var _Symptoms = value.Value.Symptoms.Split(',');
                int count = 0;
                Symptomsvalue = new int[_Symptoms.Count()];
                foreach (var n in _Symptoms)
                {
                    Symptomsvalue[count] = Convert.ToInt32(n);
                    count++;
                }
                ViewBag.SymptomsValue = Symptomsvalue;
            }
            else
            {
                ViewBag.SymptomsValue = null;
            }

            var Symptoms = _context.ASymptoms.AsNoTracking().ToList();
            ViewBag.Symptoms = Symptoms;
            //ExposureType
            var ExposureType = _context.AExposureType.AsNoTracking().ToList();
            ViewBag.ExposureType = ExposureType;

            //var Symptoms = new string[] { };
            //if (!string.IsNullOrEmpty(value.Value.Symptoms))
            //{
            //    var _Symptoms = value.Value.Symptoms.Split(',');
            //    int count = 0;
            //    Symptoms = new string[_Symptoms.Count()];
            //    foreach (var n in _Symptoms)
            //    {
            //        Symptoms[count] = n;
            //        count++;
            //    }
            //}
            //ViewBag.Symptoms = Symptoms;
            return PartialView("_DialogAddPartial", value.Value);
        }
        public string GetContactNo( string CaseNo)
        {
            //CASE-1594989856-CS
            DateTime _date = System.DateTime.Now;
            int year = _date.Year;
            string month = _date.Month.ToString();
            if (month.Length < 2)
                month = "0" + month;

            string day = _date.Day.ToString();
            if (day.Length < 2)
                day = "0" + day;

            //string partial_no = "";
            string partial_no = year + "-" + month + "-" + day;//Day + Month + Year
            string _disease = string.Empty;
            var disease = _context.ADiseaseType.FirstOrDefault(o => o.DiseaseId == userInfor.getCurrentuserDisease());
            if (disease != null)
            {
                _disease = disease.Disease;
            }


            string code = _disease + "-WEB-CASE-CONTACT-" + userInfor.GetEmployeeID() + "-" + partial_no + "- 00";

            //string code = "COVID-19-" + "CASE-CONTACT-" + partial_no + "- 00";
            int result = 0;
            result = _context.CoreCaseContacts.ToList().Count();
            code += (result + 1);
            var x = new CoreCaseContacts()
            {
                CaseNo = CaseNo,
                ContactCaseNo = code,
                AddedBy = userInfor.GetFullName(),
                DateAdded = DateTime.Now
            };
            _context.CoreCaseContacts.Add(x);
            _context.SaveChanges();

            return code;
        }
        public ActionResult GetCases([FromBody] DataManagerRequest dm)
        {
            IEnumerable data = _context.CoreCases.Where(a => /*a.FirstName != null &&*/ a.DeleteStatus != 1 && a.Disease== userInfor.getCurrentuserDisease()).OrderByDescending(o => o.DateOfReporting).ToList();
            int count = _context.CoreCases.Where(a => /*a.FirstName != null &&*/ a.DeleteStatus != 1 && a.Disease == userInfor.getCurrentuserDisease()).ToList().Count();
            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip != 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take != 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = count }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        public ActionResult DialogInsert([FromBody] CRUDModel<CoreCaseContacts> value)
        {

            CoreCaseContacts _table = _context.CoreCaseContacts.FirstOrDefault(o =>
            (o.ContactCaseNo == value.Value.ContactCaseNo));

            CoreCaseContactsLog _tablelog = _context.CoreCaseContactsLog.FirstOrDefault(o =>
           (o.ContactCaseLogId == value.Value.ContactCaseNo));

            if (_table == null)
            {
                //Save into Core_CaseContacts table

                CoreCaseContacts table = new CoreCaseContacts();
                CoreCaseContactsLog tablelog = new CoreCaseContactsLog();

                table.ContactCaseNo = value.Value.ContactCaseNo;
                table.CaseNo = value.Value.CaseNo;
                table.FirstName = value.Value.FirstName;
                table.MiddleName = value.Value.MiddleName;
                table.LastName = value.Value.LastName;
                table.Occupation = value.Value.Occupation;
                table.OccupationSpecify = value.Value.OccupationSpecify;
                table.VaccinesReceived = value.Value.VaccinesReceived;
                table.Vaccines = value.Value.Vaccines;
                table.Sex = value.Value.Sex;
                table.DoB = value.Value.DoB;
                table.Age = value.Value.Age;
                table.DateOfReporting = value.Value.DateOfReporting;
                table.RiskLevel = value.Value.RiskLevel;
                table.RiskReason = value.Value.RiskReason;
                table.Address = value.Value.Address;
                table.DistrictId = value.Value.DistrictId;
                table.CountyId = value.Value.CountyId;
                table.SubcountyId = value.Value.SubcountyId;
                table.ParishId = value.Value.ParishId;
                table.VillageId = value.Value.VillageId;
                table.FacilityUnit = value.Value.FacilityUnit;
                table.Tel = value.Value.Tel;
                table.Email = value.Value.Email;
                table.Longitude = value.Value.Longitude;
                table.Latitude = value.Value.Latitude;
                table.Classification = value.Value.Classification;
                table.ClassificationSpecify = value.Value.ClassificationSpecify;
                table.DateOnset = value.Value.DateOnset;
                table.CaseDate = value.Value.CaseDate;
                table.DateOfInfection = value.Value.DateOfInfection;
                table.Outcome = value.Value.Outcome;
                table.OutcomeSpecify = value.Value.OutcomeSpecify;
                table.DateOfOutcome = value.Value.DateOfOutcome;
                table.LabTestDate = value.Value.LabTestDate;
                table.LabResults = value.Value.LabResults;
                table.DateOfFirstContact = value.Value.DateOfFirstContact;
                table.DateOfLastContact = value.Value.DateOfLastContact;
                table.IsContactDateEstimated = value.Value.IsContactDateEstimated;
                table.CertaintyLevel = value.Value.CertaintyLevel;
                table.Disease = userInfor.getCurrentuserDisease();// value.Value.Disease;
                table.Symptoms = value.Value.Symptoms;
                table.SymptomsOthers = value.Value.SymptomsOthers;
                table.ExposureType = value.Value.ExposureType;
                table.ExposureTypeSpecify = value.Value.ExposureTypeSpecify;
                table.Relationship = value.Value.Relationship;
                table.RelationshipSpecify = value.Value.RelationshipSpecify;
                table.NextAction = value.Value.NextAction;
                table.StatusFlag = value.Value.StatusFlag;
                table.AssignFollowUpContactPerson = value.Value.AssignFollowUpContactPerson;
                table.GeneralComments = value.Value.GeneralComments;
                table.AddedBy = userInfor.GetFullName();
                table.DateAdded = DateTime.Now;

                ////Save into Core_CaseContactsLog table
                //if (string.IsNullOrEmpty(tablelog.ContactCaseLogId.ToString()) || tablelog.ContactCaseLogId == 0)
                //{
                //    var newCode = CheckCoreCaseConLog();
                //    tablelog.ContactCaseLogId = ++newCode;
                //}

                if (_tablelog == null)
                {
                    tablelog.ContactCaseLogId = CheckCoreCaseConLog();
                    tablelog.ContactCaseNo = value.Value.ContactCaseNo;
                    tablelog.CaseNo = value.Value.CaseNo;
                    tablelog.FirstName = value.Value.FirstName;
                    tablelog.MiddleName = value.Value.MiddleName;
                    tablelog.LastName = value.Value.LastName;
                    tablelog.Occupation = value.Value.Occupation;
                    tablelog.OccupationSpecify = value.Value.OccupationSpecify;
                    tablelog.VaccinesReceived = value.Value.VaccinesReceived;
                    tablelog.Vaccines = value.Value.Vaccines;
                    tablelog.Sex = value.Value.Sex;
                    tablelog.DoB = value.Value.DoB;
                    tablelog.Age = value.Value.Age;
                    tablelog.DateOfReporting = value.Value.DateOfReporting;
                    tablelog.RiskLevel = value.Value.RiskLevel;
                    tablelog.RiskReason = value.Value.RiskReason;
                    tablelog.Address = value.Value.Address;
                    tablelog.DistrictId = value.Value.DistrictId;
                    tablelog.CountyId = value.Value.CountyId;
                    tablelog.SubcountyId = value.Value.SubcountyId;
                    tablelog.ParishId = value.Value.ParishId;
                    tablelog.VillageId = value.Value.VillageId;
                    tablelog.FacilityUnit = value.Value.FacilityUnit;
                    tablelog.Tel = value.Value.Tel;
                    tablelog.Email = value.Value.Email;
                    tablelog.Longitude = value.Value.Longitude;
                    tablelog.Latitude = value.Value.Latitude;
                    tablelog.Classification = value.Value.Classification;
                    tablelog.ClassificationSpecify = value.Value.ClassificationSpecify;
                    tablelog.DateOnset = value.Value.DateOnset;
                    tablelog.CaseDate = value.Value.CaseDate;
                    tablelog.DateOfInfection = value.Value.DateOfInfection;
                    tablelog.Outcome = value.Value.Outcome;
                    tablelog.OutcomeSpecify = value.Value.OutcomeSpecify;
                    tablelog.DateOfOutcome = value.Value.DateOfOutcome;
                    tablelog.LabTestDate = value.Value.LabTestDate;
                    tablelog.LabResults = value.Value.LabResults;
                    tablelog.DateOfFirstContact = value.Value.DateOfFirstContact;
                    tablelog.DateOfLastContact = value.Value.DateOfLastContact;
                    tablelog.IsContactDateEstimated = value.Value.IsContactDateEstimated;
                    tablelog.CertaintyLevel = value.Value.CertaintyLevel;
                    tablelog.Disease = userInfor.getCurrentuserDisease();// value.Value.Disease;
                    tablelog.Symptoms = value.Value.Symptoms;
                    tablelog.SymptomsOthers = value.Value.SymptomsOthers;
                    tablelog.ExposureType = value.Value.ExposureType;
                    tablelog.ExposureTypeSpecify = value.Value.ExposureTypeSpecify;
                    tablelog.Relationship = value.Value.Relationship;
                    tablelog.RelationshipSpecify = value.Value.RelationshipSpecify;
                    tablelog.NextAction = value.Value.NextAction;
                    tablelog.StatusFlag = value.Value.StatusFlag;
                    tablelog.AssignFollowUpContactPerson = value.Value.AssignFollowUpContactPerson;
                    tablelog.GeneralComments = value.Value.GeneralComments;
                    tablelog.AddedBy = userInfor.GetFullName();
                    tablelog.DateAdded = DateTime.Now;
                    //table.AddedBy = userInfor.GetUser().Identity.Name;

                    _context.CoreCaseContactsLog.Add(tablelog);
                    _context.SaveChanges();
                }

                else if (_tablelog != null)
                {
                    tablelog.ContactCaseLogId = CheckCoreCaseConLog();
                    tablelog.ContactCaseNo = value.Value.ContactCaseNo;
                    tablelog.CaseNo = value.Value.CaseNo;
                    tablelog.FirstName = value.Value.FirstName;
                    tablelog.MiddleName = value.Value.MiddleName;
                    tablelog.LastName = value.Value.LastName;
                    tablelog.Occupation = value.Value.Occupation;
                    tablelog.OccupationSpecify = value.Value.OccupationSpecify;
                    tablelog.VaccinesReceived = value.Value.VaccinesReceived;
                    tablelog.Vaccines = value.Value.Vaccines;
                    tablelog.Sex = value.Value.Sex;
                    tablelog.DoB = value.Value.DoB;
                    tablelog.Age = value.Value.Age;
                    tablelog.DateOfReporting = value.Value.DateOfReporting;
                    tablelog.RiskLevel = value.Value.RiskLevel;
                    tablelog.RiskReason = value.Value.RiskReason;
                    tablelog.Address = value.Value.Address;
                    tablelog.DistrictId = value.Value.DistrictId;
                    tablelog.CountyId = value.Value.CountyId;
                    tablelog.SubcountyId = value.Value.SubcountyId;
                    tablelog.ParishId = value.Value.ParishId;
                    tablelog.VillageId = value.Value.VillageId;
                    tablelog.FacilityUnit = value.Value.FacilityUnit;
                    tablelog.Tel = value.Value.Tel;
                    tablelog.Email = value.Value.Email;
                    tablelog.Longitude = value.Value.Longitude;
                    tablelog.Latitude = value.Value.Latitude;
                    tablelog.Classification = value.Value.Classification;
                    tablelog.ClassificationSpecify = value.Value.ClassificationSpecify;
                    tablelog.DateOnset = value.Value.DateOnset;
                    tablelog.CaseDate = value.Value.CaseDate;
                    tablelog.DateOfInfection = value.Value.DateOfInfection;
                    tablelog.Outcome = value.Value.Outcome;
                    tablelog.OutcomeSpecify = value.Value.OutcomeSpecify;
                    tablelog.DateOfOutcome = value.Value.DateOfOutcome;
                    tablelog.LabTestDate = value.Value.LabTestDate;
                    tablelog.LabResults = value.Value.LabResults;
                    tablelog.DateOfFirstContact = value.Value.DateOfFirstContact;
                    tablelog.DateOfLastContact = value.Value.DateOfLastContact;
                    tablelog.IsContactDateEstimated = value.Value.IsContactDateEstimated;
                    tablelog.CertaintyLevel = value.Value.CertaintyLevel;
                    tablelog.Disease = userInfor.getCurrentuserDisease();// value.Value.Disease;
                    tablelog.Symptoms = value.Value.Symptoms;
                    tablelog.SymptomsOthers = value.Value.SymptomsOthers;
                    tablelog.ExposureType = value.Value.ExposureType;
                    tablelog.ExposureTypeSpecify = value.Value.ExposureTypeSpecify;
                    tablelog.Relationship = value.Value.Relationship;
                    tablelog.RelationshipSpecify = value.Value.RelationshipSpecify;
                    tablelog.NextAction = value.Value.NextAction;
                    tablelog.StatusFlag = value.Value.StatusFlag;
                    tablelog.AssignFollowUpContactPerson = value.Value.AssignFollowUpContactPerson;
                    tablelog.GeneralComments = value.Value.GeneralComments;
                    tablelog.DateEdited = DateTime.Now;
                    tablelog.EditedBy = userInfor.GetFullName();
                    //tablelog.EditedBy = userInfor.GetUser().Identity.Name;

                    _context.Entry(tablelog).State = EntityState.Modified;
                }

                _context.CoreCaseContacts.Add(table);
                _context.SaveChanges();

            }
            else
            {
                this.DialogUpdate(value);
            }
            return Json(value.Value);
        }

        // Update using Table CoreCaseContacts
        public ActionResult DialogUpdate([FromBody] CRUDModel<CoreCaseContacts> value)
        {
            CoreCaseContacts table = _context.CoreCaseContacts.FirstOrDefault(o =>
            (o.ContactCaseNo == value.Value.ContactCaseNo));

            CoreCaseContactsLog tablelog = _context.CoreCaseContactsLog.FirstOrDefault(o =>
            (o.ContactCaseLogId == value.Value.ContactCaseNo));

            if (table != null)
            {
                //check this

                // var checker = context.CoreCaseContactsLog.FirstOrDefault(o => o.ContactCaseNo == value.Value.Contact_CaseNo);

                if (tablelog != null)
                {
                    //CoreCaseContactsLog change = new CoreCaseContactsLog();

                    //Save updates into Core_CaseContacts

                    tablelog.ContactCaseNo = CheckCoreCaseConLog();
                    tablelog.CaseNo = value.Value.CaseNo;
                    tablelog.FirstName = value.Value.FirstName;
                    tablelog.MiddleName = value.Value.MiddleName;
                    tablelog.LastName = value.Value.LastName;
                    tablelog.Occupation = value.Value.Occupation;
                    tablelog.OccupationSpecify = value.Value.OccupationSpecify;
                    tablelog.VaccinesReceived = value.Value.VaccinesReceived;
                    tablelog.Vaccines = value.Value.Vaccines;
                    tablelog.Sex = value.Value.Sex;
                    tablelog.DoB = value.Value.DoB;
                    tablelog.Age = value.Value.Age;
                    tablelog.DateOfReporting = value.Value.DateOfReporting;
                    tablelog.RiskLevel = value.Value.RiskLevel;
                    tablelog.RiskReason = value.Value.RiskReason;
                    tablelog.Address = value.Value.Address;
                    tablelog.DistrictId = value.Value.DistrictId;
                    tablelog.CountyId = value.Value.CountyId;
                    tablelog.SubcountyId = value.Value.SubcountyId;
                    tablelog.ParishId = value.Value.ParishId;
                    tablelog.VillageId = value.Value.VillageId;
                    tablelog.FacilityUnit = value.Value.FacilityUnit;
                    tablelog.Tel = value.Value.Tel;
                    tablelog.Email = value.Value.Email;
                    tablelog.Longitude = value.Value.Longitude;
                    tablelog.Latitude = value.Value.Latitude;
                    tablelog.Classification = value.Value.Classification;
                    tablelog.ClassificationSpecify = value.Value.ClassificationSpecify;
                    tablelog.DateOnset = value.Value.DateOnset;
                    tablelog.CaseDate = value.Value.CaseDate;
                    tablelog.DateOfInfection = value.Value.DateOfInfection;
                    tablelog.Outcome = value.Value.Outcome;
                    tablelog.OutcomeSpecify = value.Value.OutcomeSpecify;
                    tablelog.DateOfOutcome = value.Value.DateOfOutcome;
                    tablelog.LabTestDate = value.Value.LabTestDate;
                    tablelog.LabResults = value.Value.LabResults;
                    tablelog.DateOfFirstContact = value.Value.DateOfFirstContact;
                    tablelog.DateOfLastContact = value.Value.DateOfLastContact;
                    tablelog.IsContactDateEstimated = value.Value.IsContactDateEstimated;
                    tablelog.CertaintyLevel = value.Value.CertaintyLevel;
                    tablelog.Disease = userInfor.getCurrentuserDisease();// value.Value.Disease;
                    tablelog.Symptoms = value.Value.Symptoms;
                    tablelog.SymptomsOthers = value.Value.SymptomsOthers;
                    tablelog.ExposureType = value.Value.ExposureType;
                    tablelog.ExposureTypeSpecify = value.Value.ExposureTypeSpecify;
                    tablelog.Relationship = value.Value.Relationship;
                    tablelog.RelationshipSpecify = value.Value.RelationshipSpecify;
                    tablelog.NextAction = value.Value.NextAction;
                    tablelog.StatusFlag = value.Value.StatusFlag;
                    tablelog.AssignFollowUpContactPerson = value.Value.AssignFollowUpContactPerson;

                    tablelog.GeneralComments = value.Value.GeneralComments;
                    tablelog.DateEdited = DateTime.Now;
                  tablelog.EditedBy = userInfor.GetFullName();

                    _context.CoreCaseContactsLog.Add(tablelog);
                    _context.SaveChanges();
                }

                else if (tablelog == null)
                {
                    CoreCaseContactsLog corecalog = new CoreCaseContactsLog();

                    //Save new record into edpm_summitdecisions_log

                    corecalog.ContactCaseLogId = CheckCoreCaseConLog();
                    corecalog.ContactCaseNo = value.Value.ContactCaseNo;
                    corecalog.CaseNo = value.Value.CaseNo;
                    corecalog.FirstName = value.Value.FirstName;
                    corecalog.MiddleName = value.Value.MiddleName;
                    corecalog.LastName = value.Value.LastName;
                    corecalog.Occupation = value.Value.Occupation;
                    corecalog.OccupationSpecify = value.Value.OccupationSpecify;
                    corecalog.VaccinesReceived = value.Value.VaccinesReceived;
                    corecalog.Vaccines = value.Value.Vaccines;
                    corecalog.Sex = value.Value.Sex;
                    corecalog.DoB = value.Value.DoB;
                    corecalog.Age = value.Value.Age;
                    corecalog.DateOfReporting = value.Value.DateOfReporting;
                    corecalog.RiskLevel = value.Value.RiskLevel;
                    corecalog.RiskReason = value.Value.RiskReason;
                    corecalog.Address = value.Value.Address;
                    corecalog.DistrictId = value.Value.DistrictId;
                    corecalog.CountyId = value.Value.CountyId;
                    corecalog.SubcountyId = value.Value.SubcountyId;
                    corecalog.ParishId = value.Value.ParishId;
                    corecalog.VillageId = value.Value.VillageId;
                    corecalog.FacilityUnit = value.Value.FacilityUnit;
                    corecalog.Tel = value.Value.Tel;
                    corecalog.Email = value.Value.Email;
                    corecalog.Longitude = value.Value.Longitude;
                    corecalog.Latitude = value.Value.Latitude;
                    corecalog.Classification = value.Value.Classification;
                    corecalog.ClassificationSpecify = value.Value.ClassificationSpecify;
                    corecalog.DateOnset = value.Value.DateOnset;
                    corecalog.CaseDate = value.Value.CaseDate;
                    corecalog.DateOfInfection = value.Value.DateOfInfection;
                    corecalog.Outcome = value.Value.Outcome;
                    corecalog.OutcomeSpecify = value.Value.OutcomeSpecify;
                    corecalog.DateOfOutcome = value.Value.DateOfOutcome;
                    corecalog.LabTestDate = value.Value.LabTestDate;
                    corecalog.LabResults = value.Value.LabResults;
                    corecalog.DateOfFirstContact = value.Value.DateOfFirstContact;
                    corecalog.DateOfLastContact = value.Value.DateOfLastContact;
                    corecalog.IsContactDateEstimated = value.Value.IsContactDateEstimated;
                    corecalog.CertaintyLevel = value.Value.CertaintyLevel;
                    corecalog.Disease = userInfor.getCurrentuserDisease();// value.Value.Disease;
                    corecalog.Symptoms = value.Value.Symptoms;
                    corecalog.SymptomsOthers = value.Value.SymptomsOthers;
                    corecalog.ExposureType = value.Value.ExposureType;
                    corecalog.ExposureTypeSpecify = value.Value.ExposureTypeSpecify;
                    corecalog.Relationship = value.Value.Relationship;
                    corecalog.RelationshipSpecify = value.Value.RelationshipSpecify;
                    corecalog.NextAction = value.Value.NextAction;
                    corecalog.StatusFlag = value.Value.StatusFlag;
                    corecalog.AssignFollowUpContactPerson = value.Value.AssignFollowUpContactPerson;

                    corecalog.GeneralComments = value.Value.GeneralComments;
                    corecalog.DateEdited = DateTime.Now;

                    _context.CoreCaseContactsLog.Add(corecalog);
                    _context.SaveChanges();
                }

                table.FirstName = value.Value.FirstName;
                table.MiddleName = value.Value.MiddleName;
                table.LastName = value.Value.LastName;
                table.Occupation = value.Value.Occupation;
                table.OccupationSpecify = value.Value.OccupationSpecify;
                table.VaccinesReceived = value.Value.VaccinesReceived;
                table.Vaccines = value.Value.Vaccines;
                table.Sex = value.Value.Sex;
                table.DoB = value.Value.DoB;
                table.Age = value.Value.Age;
                table.DateOfReporting = value.Value.DateOfReporting;
                table.RiskLevel = value.Value.RiskLevel;
                table.RiskReason = value.Value.RiskReason;
                table.Address = value.Value.Address;
                table.DistrictId = value.Value.DistrictId;
                table.CountyId = value.Value.CountyId;
                table.SubcountyId = value.Value.SubcountyId;
                table.ParishId = value.Value.ParishId;
                table.VillageId = value.Value.VillageId;
                table.FacilityUnit = value.Value.FacilityUnit;
                table.Tel = value.Value.Tel;
                table.Email = value.Value.Email;
                table.Longitude = value.Value.Longitude;
                table.Latitude = value.Value.Latitude;
                table.Classification = value.Value.Classification;
                table.ClassificationSpecify = value.Value.ClassificationSpecify;
                table.DateOnset = value.Value.DateOnset;
                table.CaseDate = value.Value.CaseDate;
                table.DateOfInfection = value.Value.DateOfInfection;
                table.Outcome = value.Value.Outcome;
                table.OutcomeSpecify = value.Value.OutcomeSpecify;
                table.DateOfOutcome = value.Value.DateOfOutcome;
                table.LabTestDate = value.Value.LabTestDate;
                table.LabResults = value.Value.LabResults;
                table.DateOfFirstContact = value.Value.DateOfFirstContact;
                table.DateOfLastContact = value.Value.DateOfLastContact;
                table.IsContactDateEstimated = value.Value.IsContactDateEstimated;
                table.CertaintyLevel = value.Value.CertaintyLevel;
                table.Disease = userInfor.getCurrentuserDisease();// value.Value.Disease;
                table.Symptoms = value.Value.Symptoms;
                table.SymptomsOthers = value.Value.SymptomsOthers;
                table.ExposureType = value.Value.ExposureType;
                table.ExposureTypeSpecify = value.Value.ExposureTypeSpecify;
                table.Relationship = value.Value.Relationship;
                table.RelationshipSpecify = value.Value.RelationshipSpecify;
                table.NextAction = value.Value.NextAction;
                table.StatusFlag = value.Value.StatusFlag;
                table.AssignFollowUpContactPerson = value.Value.AssignFollowUpContactPerson;
                table.GeneralComments = value.Value.GeneralComments;
                table.EditedBy= userInfor.GetFullName();
                table.DateEdited = DateTime.Now;
                //context.CoreCaseContacts.Add(table);
                //context.SaveChanges();

                _context.Entry(table).State = EntityState.Modified;
                _context.SaveChanges();

                try
                {

                    TempData["Success"] = "Record Saved Successfully!";
                }

                catch
                {

                }

                //catch (DbEntityValidationException dbEx)
                //{
                //    foreach (var validationErrors in dbEx.EntityValidationErrors)
                //    {
                //        foreach (var validationError in validationErrors.ValidationErrors)
                //        {
                //            TempData["Success"] += string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                //            System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                //        }
                //    }
                //}
            }

            return Json(value.Value);
        }
        public string CheckCoreCaseConLog()
        {
            string code = "0";
            int result = 0;
            result = _context.CoreCaseContactsLog.ToList().Count();
            code += (result + 1);
            var x = new CoreCaseContactsLog()
            {
                ContactCaseLogId = code
            };

            return (code);
        }
        //Save Support Docs
        [AcceptVerbs("Post")]
        public IActionResult Save(IList<IFormFile> UploadFiles, string CaseNo)
        {
            try
            {
                foreach (var file in UploadFiles)
                {
                    if (UploadFiles != null)
                    {
                        var filename = Path.GetFileName(file.FileName); ///ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                       // filename = hostingEnv.WebRootPath + $@"\{filename}";
                        var fileSavePath = Path.Combine(hostingEnv.WebRootPath + "\\UploadedFiles\\", filename);
                        if (!System.IO.File.Exists(fileSavePath))
                        {
                            byte[] fileBytes;
                            using (MemoryStream ms = new MemoryStream())
                            {
                                file.OpenReadStream().CopyTo(ms);
                                fileBytes = ms.ToArray();
                            }

                            string ext = Path.GetExtension(filename);
                            CoreContactsFileAttachment doc = new CoreContactsFileAttachment()
                            {
                                CaseNo = CaseNo,
                                FileType = ext,
                                FileUrl = fileBytes,
                                Details = filename,
                                Id = 1,
                                AddedBy = userInfor.GetFullName(),
                                DateAdded = System.DateTime.Now
                            };
                            var evidence = _context.CoreContactsFileAttachment.OrderByDescending(o => o.Id).FirstOrDefault();
                            if (evidence != null)
                            {
                                doc.Id = (evidence.Id + 1);
                            }
                            _context.CoreContactsFileAttachment.Add(doc);
                            _context.SaveChanges();
                        }
                        else
                        {
                            Response.Clear();
                            Response.StatusCode = 204;
                            Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = "File already exists.";
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Response.Clear();
                Response.ContentType = "application/json; charset=utf-8";
                Response.StatusCode = 204;
                Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = "No Content";
                Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = e.Message;
            }
            return Content("");
        }
        [AcceptVerbs("Post")]
        public IActionResult Remove(IList<IFormFile> UploadFiles, string CaseNo)
        {
            try
            {
                foreach (var file in UploadFiles)
                {
                    var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    var filePath = Path.Combine(hostingEnv.WebRootPath + "\\UploadedFiles");
                    var fileSavePath = filePath + "\\" + fileName;
                    if (!System.IO.File.Exists(fileSavePath))
                    {
                        System.IO.File.Delete(fileSavePath);
                    }
                    var exists = _context.CoreContactsFileAttachment.FirstOrDefault(o => o.Details == fileName && o.CaseNo == CaseNo);
                    if (exists != null)
                    {
                        _context.CoreContactsFileAttachment.Remove(exists);
                        _context.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                Response.Clear();
                Response.StatusCode = 200;
                Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = "File removed successfully";
                Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = e.Message;
            }
            return Content("");
        }
        public ActionResult GetSupportingDocuments([FromBody] DataManagerRequest dm, string CaseNo)
        {
            IEnumerable data = _context.CoreContactsFileAttachment.Where(o => o.CaseNo == CaseNo).Select(o => new { o.Id, o.Details, o.FileType, o.CaseNo }).ToList();
            int count = _context.CoreContactsFileAttachment.Where(o => o.CaseNo == CaseNo).ToList().Count;

            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip != 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take != 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = count });
        }
        public FileResult DownloadEvidenceFile(int? DocumentCode)
        {
            byte[] bytes = null;
            string fileName = string.Empty, contentType = string.Empty;
            var file = _context.CoreContactsFileAttachment.FirstOrDefault(o => o.Id == DocumentCode);
            if (file != null)
            {
                bytes = file.FileUrl;
                fileName = file.Details;
                contentType = "application/" + file.FileType.Replace('.', ' ').Trim();
            }

            return File(bytes, contentType, fileName);
        }

        //Save Photoz
        [AcceptVerbs("Post")]
        public IActionResult SavePhoto(IList<IFormFile> UploadPhoto, string CaseNo)
        {
            try
            {
                foreach (var file in UploadPhoto)
                {
                    if (UploadPhoto != null)
                    {
                        var filename = Path.GetFileName(file.FileName); ///ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                       // filename = hostingEnv.WebRootPath + $@"\{filename}";
                        var fileSavePath = Path.Combine(hostingEnv.WebRootPath + "\\UploadedFiles\\", filename);
                        if (!System.IO.File.Exists(fileSavePath))
                        {
                            byte[] fileBytes;
                            using (MemoryStream ms = new MemoryStream())
                            {
                                file.OpenReadStream().CopyTo(ms);
                                fileBytes = ms.ToArray();
                            }

                            string ext = Path.GetExtension(filename);
                            CoreContactCaseNoPhoto doc = new CoreContactCaseNoPhoto()
                            {
                                ContactCaseNo = CaseNo,
                                FileType = ext,
                                Photo = fileBytes

                            };
                            var evidence = _context.CoreContactCaseNoPhoto.FirstOrDefault();
                            _context.CoreContactCaseNoPhoto.Add(doc);
                            _context.SaveChanges();
                        }
                        else
                        {
                            Response.Clear();
                            Response.StatusCode = 204;
                            Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = "File already exists.";
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Response.Clear();
                Response.ContentType = "application/json; charset=utf-8";
                Response.StatusCode = 204;
                Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = "No Content";
                Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = e.Message;
            }
            return Content("");
        }
        [AcceptVerbs("Post")]
        public IActionResult RemovePhoto(IList<IFormFile> UploadPhoto, string CaseNo)
        {
            try
            {
                foreach (var file in UploadPhoto)
                {
                    var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    var filePath = Path.Combine(hostingEnv.WebRootPath + "\\UploadedFiles");
                    var fileSavePath = filePath + "\\" + fileName;
                    if (!System.IO.File.Exists(fileSavePath))
                    {
                        System.IO.File.Delete(fileSavePath);
                    }
                    var exists = _context.CoreContactCaseNoPhoto.FirstOrDefault(o => o.ContactCaseNo == CaseNo);
                    if (exists != null)
                    {
                        _context.CoreContactCaseNoPhoto.Remove(exists);
                        _context.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                Response.Clear();
                Response.StatusCode = 200;
                Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = "File removed successfully";
                Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = e.Message;
            }
            return Content("");
        }
        public ActionResult GetPhotoz([FromBody] DataManagerRequest dm, string CaseNo)
        {
            IEnumerable data = _context.CoreContactCaseNoPhoto.Where(o => o.ContactCaseNo == CaseNo).Select(o => new { o.Photo, o.FileType, o.ContactCaseNo }).ToList();
            int count = _context.CoreContactCaseNoPhoto.Where(o => o.ContactCaseNo == CaseNo).ToList().Count;

            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip != 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take != 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = count });
        }
        public FileResult DownloadCasePhotoFile(string DocumentCode)
        {
            byte[] bytes = null;
            string fileName = string.Empty, contentType = string.Empty;
            var file = _context.CoreCasePhoto.FirstOrDefault(o => o.CaseNo == DocumentCode);
            if (file != null)
            {
                bytes = file.Photo;
                contentType = "application/" + file.FileType.Replace('.', ' ').Trim();
            }

            return File(bytes, contentType, fileName);
        }

        //remove cases
        public ActionResult RemoveRecord(string CaseNo, string ContactCaseNo)
        {
            _context.CoreCaseContacts.Remove(_context.CoreCaseContacts.FirstOrDefault(o => o.CaseNo == CaseNo && o.ContactCaseNo == ContactCaseNo));
            int result = _context.SaveChanges();

            return Json(result);
        }

        public ActionResult DeleteChildUrl([FromBody] CRUDModel<CoreCaseContacts> value)
        {
            string obj = Request.Headers["additional_key"]; //key1 
            string obj1 = Request.Headers["additional_key1"]; //key2 
            CoreCaseContacts table = _context.CoreCaseContacts.FirstOrDefault(or => or.ContactCaseNo + or.CaseNo == obj + obj1);
            if (table != null)
            {
                if (value.Deleted != null)
                {
                    foreach (var temp in value.Deleted)
                    {
                        temp.DeleteStatus = 1;
                        temp.DeletedBy = userInfor.GetFullName();
                        temp.DateDeleted = System.DateTime.Now;
                        _context.Entry(temp).State = EntityState.Modified;
                        try
                        {
                            _context.SaveChanges();
                        }
                        catch (Exception dbEx)
                        {
                            throw (dbEx);
                        }
                    }
                }
                else
                {
                    table.DeleteStatus = 1;
                    table.DeletedBy = userInfor.GetFullName();
                    table.DateDeleted = System.DateTime.Now;
                    _context.Entry(table).State = EntityState.Modified;
                    try
                    {
                        _context.SaveChanges();
                    }
                    catch (Exception dbEx)
                    {
                        throw (dbEx);
                    }
                }
            }
            //var msg = "Contact Case No. " + obj + " Has been Deleted Successfully !!";
            return Json(value);

        }
    }
}
