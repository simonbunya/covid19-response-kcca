﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CHMIS.BLL;
using CHMIS.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Syncfusion.EJ2.Base;

namespace CHMIS.Controllers
{
    public class ComplianceController : Controller
    {
        private CHMISContext _context;
        private IWebHostEnvironment hostingEnv;
        private IUserService userInfor;
        public ComplianceController(CHMISContext Context, IWebHostEnvironment env, IUserService _userinfo)
        {
            this.userInfor = _userinfo;
            this._context = Context;
            this.hostingEnv = env;
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Compliance()
        {
            ViewBag.Facility = _context.AFacilityUnit.ToList();
            ViewBag.yesNo = _context.AYesNo.ToList();
            ViewBag.employee = _context.ViewEmployee.ToList();
            return View();
        }

        public IActionResult ComplianceVisit()
        {
            ViewBag.Facility = _context.AFacilityUnit.ToList();
            ViewBag.yesNo = _context.AYesNo.ToList();
            ViewBag.employee = _context.ViewEmployee.ToList();
            return View();
        }
        public IActionResult AddPartial([FromBody] CRUDModel<CoreComplianceVisit> value)
        {
            var facility = _context.AFacilityUnit.ToList();
            var m = _context.AspNetUsers.ToList();
            var fac_supervisor = m.FirstOrDefault(a => a.Id == userInfor.GetFullNameID() && a.Facility != null);
            IEnumerable members = _context.AFacilityUnit.ToList();

            if (User.IsInRole("Super Admin") || User.IsInRole("Administrator") || User.IsInRole("KCCA General") || User.IsInRole("Viewer"))
            {
                members = _context.AFacilityUnit.ToList();
            }
            else if (fac_supervisor != null)
            {
                List<AFacilityUnit> dis = new List<AFacilityUnit>();
                string[] fccode = fac_supervisor.Facility.Split(',');
                foreach (var n in fccode)
                {
                    var _facility = facility.FirstOrDefault(a => a.FacilityCode == Convert.ToInt32(n));
                    dis.Add(_facility);
                }
                members = dis;
            }

            ViewBag.Facility = members;// _context.AFacilityUnit.ToList();
            ViewBag.yesNo = _context.AYesNo.ToList();
            ViewBag.employee = _context.ViewEmployee/*.Where(a=>a.Name == userInfor.GetFullName())*/.ToList();
            value.Value.SuperviorCode = userInfor.GetEmployeeID();
            if (value.Value.ComplianceCode == null)
            {
                value.Value.ComplianceCode = GetCaseNo();
            }
            return PartialView("_DialogAddPartial", value.Value);
            //return PartialView("_DialogueComplianceInsertAddPartial", value.Value);
        }

        public ActionResult DataSourceCompliance([FromBody] DataManagerRequest dm)
        {
            //To take care of facilitylist supervisors in several facility
            var facilitylist = userInfor.getUserFacilitySupervisorList();

            IEnumerable data = _context.CoreComplianceVisit.Where(a => a.Disease == userInfor.getCurrentuserDisease() && a.DeleteStatus != 1).OrderByDescending(o => o.FacilityCode).ToList();
            int _count = 0;
            //var muserrole = User.IsInRole("Administrator");
            if (User.IsInRole("Super Admin") || User.IsInRole("Administrator") || User.IsInRole("KCCA General") || User.IsInRole("Viewer"))
            {
                data = _context.CoreComplianceVisit.Where(a => a.Disease == userInfor.getCurrentuserDisease() && a.DeleteStatus != 1).OrderByDescending(o => o.FacilityCode).ToList();
                _count = _context.CoreComplianceVisit.Where(a => a.Disease == userInfor.getCurrentuserDisease() && a.DeleteStatus != 1).OrderByDescending(o => o.FacilityCode).ToList().Count();
            }
            else
            {
                //Write a Where In () like statement
                // The following 2 Statements are equivalent
                data = _context.CoreComplianceVisit.Where(BuildContainsExpression<CoreComplianceVisit, int>(e => (int)e.FacilityCode, facilitylist)).ToList();
                _count = _context.CoreComplianceVisit.Where(BuildContainsExpression<CoreComplianceVisit, int>(e => (int)e.FacilityCode, facilitylist)).ToList().Count();
            }

            //var _data = _context.CoreComplianceVisit.Where(a=> a.Disease == userInfor.getCurrentuserDisease()).OrderByDescending(o => o.FacilityCode).ToList();
            //IEnumerable data = _data;
            //int _count = _data.Count();

            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                _count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                _count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip != 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take != 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = _count });
        }

        public static Expression<Func<TElement, bool>> BuildContainsExpression<TElement, TValue>(Expression<Func<TElement, TValue>> valueSelector, IEnumerable<TValue> values)
        {
            if (null == valueSelector) { throw new ArgumentNullException("valueSelector"); }
            if (null == values) { throw new ArgumentNullException("values"); }
            ParameterExpression p = valueSelector.Parameters.Single();
            // p => valueSelector(p) == values[0] || valueSelector(p) == ...
            if (!values.Any())
            {
                return e => false;
            }
            var equals = values.Select(value => (Expression)Expression.Equal(valueSelector.Body, Expression.Constant(value, typeof(TValue))));
            var body = equals.Aggregate<Expression>((accumulate, equal) => Expression.Or(accumulate, equal));
            return Expression.Lambda<Func<TElement, bool>>(body, p);
        }


        public ActionResult DialogueComplianceInsert([FromBody] CRUDModel<CoreComplianceVisit> value)
        {
            //CoreComplianceVisit table = _context.CoreComplianceVisit.FirstOrDefault(o => o.FacilityCode == value.Value.FacilityCode && o.DateOfVisit == value.Value.DateOfVisit);
            CoreComplianceVisit table = _context.CoreComplianceVisit.FirstOrDefault(o => o.ComplianceCode == value.Value.ComplianceCode);

            if (table == null)
            {
                if (value.Value.DateOfVisit.Value.Hour == 0)
                {
                }
                else
                {
                    var tt = value.Value.DateOfVisit.Value.Hour;
                    var timediff = 24 - tt;
                    value.Value.DateOfVisit = value.Value.DateOfVisit.Value.AddHours(timediff);
                }

                CoreComplianceVisit _table = new CoreComplianceVisit();
                _table.AddedBy = userInfor.GetFullName();
                _table.DateOfVisit = (value.Value.DateOfVisit);
                _table.FacilityCode = value.Value.FacilityCode;
                _table.HandWashers = value.Value.HandWashers;
                _table.TemperatureGuns = value.Value.TemperatureGuns;
                _table.SocialDistancing = value.Value.SocialDistancing;
                _table.RecordKeeping = value.Value.RecordKeeping;
                _table.AirConditioning = value.Value.AirConditioning;
                _table.MaskWearing = value.Value.MaskWearing;
                _table.WashRooms = value.Value.WashRooms;
                _table.Remarks = value.Value.Remarks;
                _table.SuperviorCode = value.Value.SuperviorCode;
                _table.GeneralComments = value.Value.GeneralComments;
                _table.DateAdded = DateTime.Now;
                _table.Disease = userInfor.getCurrentuserDisease();
                _context.CoreComplianceVisit.Add(_table);
                //_context.SaveChanges();
            }
            else
            {
                if (value.Value.DateOfVisit.Value.Hour == 0)
                {
                }
                else
                {
                    var tt = value.Value.DateOfVisit.Value.Hour;
                    var timediff = 24 - tt;
                    value.Value.DateOfVisit = value.Value.DateOfVisit.Value.AddHours(timediff);
                }

                table.EditedBy = userInfor.GetFullName();
                table.DateEdited = DateTime.Now;
                table.DateOfVisit = (value.Value.DateOfVisit);
                table.AddedBy = userInfor.GetFullName();
                table.FacilityCode = value.Value.FacilityCode;
                table.HandWashers = value.Value.HandWashers;
                table.TemperatureGuns = value.Value.TemperatureGuns;
                table.SocialDistancing = value.Value.SocialDistancing;
                table.RecordKeeping = value.Value.RecordKeeping;
                table.AirConditioning = value.Value.AirConditioning;
                table.MaskWearing = value.Value.MaskWearing;
                table.WashRooms = value.Value.WashRooms;
                table.Remarks = value.Value.Remarks;
                table.SuperviorCode = value.Value.SuperviorCode;
                table.GeneralComments = value.Value.GeneralComments;
                table.DateAdded = DateTime.Now;
                table.Disease = userInfor.getCurrentuserDisease();
                _context.Entry(table).CurrentValues.SetValues(value);
                _context.Entry(table).State = EntityState.Modified;
               // _context.SaveChanges();
            }
           
            try
            { _context.SaveChanges(); }
            catch(Exception dbEx)
            {
                throw (dbEx);
            }
            return Json(value);
        }

        public ActionResult DialogueComplianceUpdate([FromBody] CRUDModel<CoreComplianceVisit> value)
        {
            CoreComplianceVisit table = _context.CoreComplianceVisit.FirstOrDefault(o => o.ComplianceCode == value.Value.ComplianceCode);
            if (table != null)
            {
                if (value.Value.DateOfVisit.Value.Hour == 0)
                {
                }
                else
                {
                    var tt = value.Value.DateOfVisit.Value.Hour;
                    var timediff = 24 - tt;
                    value.Value.DateOfVisit = value.Value.DateOfVisit.Value.AddHours(timediff);
                }

                table.EditedBy = userInfor.GetFullName();
                table.DateEdited = DateTime.Now;
                table.DateOfVisit = (value.Value.DateOfVisit);
                table.AddedBy = userInfor.GetFullName();
                table.FacilityCode = value.Value.FacilityCode;
                table.HandWashers = value.Value.HandWashers;
                table.TemperatureGuns = value.Value.TemperatureGuns;
                table.SocialDistancing = value.Value.SocialDistancing;
                table.RecordKeeping = value.Value.RecordKeeping;
                table.AirConditioning = value.Value.AirConditioning;
                table.MaskWearing = value.Value.MaskWearing;
                table.WashRooms = value.Value.WashRooms;
                table.Remarks = value.Value.Remarks;
                table.SuperviorCode = value.Value.SuperviorCode;
                table.GeneralComments = value.Value.GeneralComments;
                table.DateAdded = DateTime.Now;
                table.Disease = userInfor.getCurrentuserDisease();
                _context.Entry(table).CurrentValues.SetValues(value);
                _context.Entry(table).State = EntityState.Modified;
                // _context.SaveChanges();
            }
            else
            {
                if (value.Value.DateOfVisit.Value.Hour == 0)
                {
                }
                else
                {
                    var tt = value.Value.DateOfVisit.Value.Hour;
                    var timediff = 24 - tt;
                    value.Value.DateOfVisit = value.Value.DateOfVisit.Value.AddHours(timediff);
                }

                CoreComplianceVisit _table = new CoreComplianceVisit();
                _table.AddedBy = userInfor.GetFullName();
                _table.DateAdded = DateTime.Now;
                _table.DateOfVisit = (value.Value.DateOfVisit);
                _table.AddedBy = userInfor.GetFullName();
                _table.FacilityCode = value.Value.FacilityCode;
                _table.HandWashers = value.Value.HandWashers;
                _table.TemperatureGuns = value.Value.TemperatureGuns;
                _table.SocialDistancing = value.Value.SocialDistancing;
                _table.RecordKeeping = value.Value.RecordKeeping;
                _table.AirConditioning = value.Value.AirConditioning;
                _table.MaskWearing = value.Value.MaskWearing;
                _table.WashRooms = value.Value.WashRooms;
                _table.Remarks = value.Value.Remarks;
                _table.SuperviorCode = value.Value.SuperviorCode;
                _table.GeneralComments = value.Value.GeneralComments;
                _table.DateAdded = DateTime.Now;
                _table.Disease = userInfor.getCurrentuserDisease();
                _context.CoreComplianceVisit.Add(_table);
                //_context.SaveChanges();
            }

            try
            { _context.SaveChanges(); }
            catch (Exception dbEx)
            {
                throw (dbEx);
            }
            return Json(value);
        }

        //crude save 
        public ActionResult DialogueCrudeSave([FromBody] CRUDModel<CoreComplianceVisit> value)
        {
            if (value.Action == "insert")
            {
                CoreComplianceVisit _table = new CoreComplianceVisit();
                _table.AddedBy = userInfor.GetFullName();
                //_table.ComplianceCode = value.Value.ComplianceCode;
                _table.DateOfVisit = (value.Value.DateOfVisit);
                _table.FacilityCode = value.Value.FacilityCode;
                _table.HandWashers = value.Value.HandWashers;
                _table.TemperatureGuns = value.Value.TemperatureGuns;
                _table.SocialDistancing = value.Value.SocialDistancing;
                _table.RecordKeeping = value.Value.RecordKeeping;
                _table.AirConditioning = value.Value.AirConditioning;
                _table.MaskWearing = value.Value.MaskWearing;
                _table.WashRooms = value.Value.WashRooms;
                _table.Remarks = value.Value.Remarks;
                _table.SuperviorCode = value.Value.SuperviorCode;
                _table.GeneralComments = value.Value.GeneralComments;
                _table.DateAdded = DateTime.Now;
                _table.Disease = userInfor.getCurrentuserDisease();
                _context.CoreComplianceVisit.Add(_table);
                _context.CoreComplianceVisit.Add(value.Value);
                //_context.SaveChanges();
            }

            else if (value.Action == "update")
            {
                value.Value.EditedBy = userInfor.GetFullName();
                value.Value.DateEdited = DateTime.Now;
                value.Value.DateOfVisit = (value.Value.DateOfVisit);
                value.Value.AddedBy = userInfor.GetFullName();
                value.Value.FacilityCode = value.Value.FacilityCode;
                value.Value.HandWashers = value.Value.HandWashers;
                value.Value.TemperatureGuns = value.Value.TemperatureGuns;
                value.Value.SocialDistancing = value.Value.SocialDistancing;
                value.Value.RecordKeeping = value.Value.RecordKeeping;
                value.Value.AirConditioning = value.Value.AirConditioning;
                value.Value.MaskWearing = value.Value.MaskWearing;
                value.Value.WashRooms = value.Value.WashRooms;
                value.Value.Remarks = value.Value.Remarks;
                value.Value.SuperviorCode = value.Value.SuperviorCode;
                value.Value.GeneralComments = value.Value.GeneralComments;
                value.Value.DateAdded = DateTime.Now;
                value.Value.Disease = userInfor.getCurrentuserDisease();
                _context.Entry(value.Value).CurrentValues.SetValues(value);
                _context.Entry(value.Value).State = EntityState.Modified;
                //_context.SaveChanges();
            }
            try
            { _context.SaveChanges(); }
            catch (Exception dbEx)
            {
                throw (dbEx);
            }
            return Json(value);
        }

        public string GetCaseNo()
        {
            //CASE-1594989856-CS
            DateTime _date = System.DateTime.Now;
            int year = _date.Year;
            string month = _date.Month.ToString();
            if (month.Length < 2)
                month = "0" + month;

            string day = _date.Day.ToString();
            if (day.Length < 2)
                day = "0" + day;

            //string partial_no = "";
            string partial_no = year + "-" + month + "-" + day;//Day + Month + Year

            string _disease = string.Empty;
            var disease = _context.ADiseaseType.FirstOrDefault(o => o.DiseaseId == userInfor.getCurrentuserDisease());
            if (disease != null)
            {
                _disease = disease.Disease;
            }


            string code = _disease + "-WEB-COMPLIANCE-" + userInfor.GetEmployeeID() + "-" + partial_no + "- 00";

            //string code = "CODE-" + partial_no + "-";
            int result = 0;
            result = _context.CoreComplianceVisit.ToList().Count();
            code += (result + 1);
            var x = new CoreComplianceVisit()
            {
                ComplianceCode = code,
                AddedBy = userInfor.GetFullName(),
                DateAdded = DateTime.Now
            };
            _context.CoreComplianceVisit.Add(x);
            _context.SaveChanges();

            return code;
        }

        public ActionResult RemoveRecord(string ComplianceCode)
        {
            _context.CoreComplianceVisit.Remove(_context.CoreComplianceVisit.FirstOrDefault(o => o.ComplianceCode == ComplianceCode));
            int result = _context.SaveChanges();

            return Json(result);
        }

        public ActionResult DeleteUrl([FromBody] CRUDModel<CoreComplianceVisit> value)
        {
            string obj = Request.Headers["additional_key"]; //key1 
            //string obj1 = Request.Headers["additional_key1"]; //key2 
            CoreComplianceVisit table = _context.CoreComplianceVisit.FirstOrDefault(or => or.ComplianceCode == obj);
            //_context.Remove(table);
            //_context.SaveChanges();
            if (table != null)
            {
                if (value.Deleted != null)
                {
                    foreach (var temp in value.Deleted)
                    {
                        temp.DeleteStatus = 1;
                        temp.DeletedBy = userInfor.GetFullName();
                        temp.DateDeleted = System.DateTime.Now;
                        _context.Entry(temp).State = EntityState.Modified;
                        try
                        {
                            _context.SaveChanges();
                        }
                        catch (Exception dbEx)
                        {
                            throw (dbEx);
                        }
                    }
                }
                else
                {
                    table.DeleteStatus = 1;
                    table.DeletedBy = userInfor.GetFullName();
                    table.DateDeleted = System.DateTime.Now;
                    _context.Entry(table).State = EntityState.Modified;
                    try
                    {
                        _context.SaveChanges();
                    }
                    catch (Exception dbEx)
                    {
                        throw (dbEx);
                    }
                }
            }
            //var msg = "Case No. " + obj + " Has been Deleted Successfully !!";
            return Json(value);
        }
    }
}
