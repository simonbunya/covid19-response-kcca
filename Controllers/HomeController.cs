﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CHMIS.Models;
using CHMIS.BLL;
using Microsoft.EntityFrameworkCore;

namespace CHMIS.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        CHMISContext _context;
        private IUserService userInfor;
        public HomeController(ILogger<HomeController> logger, CHMISContext Context, IUserService _userinfo)
        {
            _logger = logger;
            _context = Context;
            this.userInfor = _userinfo;
        }


        public ActionResult GetMessages()
        {
            string username = userInfor.GetUser().Identity.Name;
            var data = _context.Messages.Where(o => o.ReceivedBy == username).ToList();

            return Json(data);
        }
        public ActionResult GetUsers()
        {
            var data = _context.ViewUserMessages.ToList();

            return Json(data);
        }
        public ActionResult GetLogedInUsersName()
        {
            var data = userInfor.GetUser().Identity.Name;

            return Json(data);
        }
        public ActionResult GetChat(int ID)
        {
            List<Messages> data = new List<Messages>();
            var chat = _context.Messages.FirstOrDefault(o => o.MessageId == ID);
            if (chat != null)
            {
                var res = _context.Messages.Where(o => (o.SentBy == chat.SentBy && o.ReceivedBy == chat.ReceivedBy) || (o.SentBy == chat.ReceivedBy && o.ReceivedBy == chat.SentBy)).OrderBy(o => o.SentDate).ToList();
                data.AddRange(res);
            }
            foreach (var n in data)
            {
                if (n.ReceivedBy == chat.ReceivedBy)
                {
                    n.IsRead = true;
                    _context.Entry(n).State = EntityState.Modified;
                    _context.SaveChanges();
                }
            }
            return Json(data);
        }
        public ActionResult SaveMessage(Messages value, int ID)
        {
            value.SentDate = DateTime.Now;
            value.SentBy = userInfor.GetUser().Identity.Name;
            value.IsAchieved = false;
            value.IsDeleted = false;
            value.IsRead = false;
            _context.Messages.Add(value);
            _context.SaveChanges();
            var chat = _context.Messages.FirstOrDefault(o => o.MessageId == ID);
            if (chat != null)
            {
                chat.IsRead = true;
                _context.Entry(chat).State = EntityState.Modified;
                _context.SaveChanges();
            }
            return Json(value);
        }
        public IActionResult Index()
        {
            //var user = _context.AspNetUsers.FirstOrDefault(o => o.UserName == "ibs@ibs.co.ug");
            //string pass = base64Decode(user.PasswordHash);C:\IBS Projects 2020\CHMIS\CHMIS\CHMIS_20_7_2020_2\CHMIS\wwwroot\COVID19.sydx
            //ViewBag.DashboardPath =  AppDomain.CurrentDomain.BaseDirectory.Replace("\\", "\\\\") + "wwwroot\\\\COVID19.sydx";
            ViewBag.DashboardPath =  Directory.GetCurrentDirectory().Replace("\\" ,"\\\\")+ "\\\\wwwroot\\\\COVID19.sydx";
            ViewBag.CompliancePath = Directory.GetCurrentDirectory().Replace("\\", "\\\\") + "\\\\wwwroot\\\\Compliance.sydx";


            return View();
        }

        public IActionResult Privacy2()
        {
            return View();
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public static string base64Decode(string sData) //Decode    
        {
            try
            {
                var encoder = new System.Text.UTF8Encoding();
                System.Text.Decoder utf8Decode = encoder.GetDecoder();
                byte[] todecodeByte = Convert.FromBase64String(sData);
                int charCount = utf8Decode.GetCharCount(todecodeByte, 0, todecodeByte.Length);
                char[] decodedChar = new char[charCount];
                utf8Decode.GetChars(todecodeByte, 0, todecodeByte.Length, decodedChar, 0);
                string result = new String(decodedChar);
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in base64Decode" + ex.Message);
            }
        }

    }
}
