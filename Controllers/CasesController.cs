﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using CHMIS.Models;
using System;
using Microsoft.EntityFrameworkCore;
using Syncfusion.EJ2;
using System.Collections;
using Syncfusion.EJ2.Base;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Http;
using System.Net.Http.Headers;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using CHMIS.BLL;
using System.Data.Entity.Validation;

namespace CHMIS.Controllers
{
    public class CasesController : Controller
    {
        private CHMISContext _context;
        private IWebHostEnvironment hostingEnv;
        private IUserService userInfor;
        public CasesController(CHMISContext Context, IWebHostEnvironment env, IUserService _userinfo)
        {
            this.userInfor = _userinfo;
            this._context = Context;
            this.hostingEnv = env;
        }
        // View for adding a new case
        public IActionResult Cases()
        {
            //Risk Level
            var risklevel = _context.ARiskLevel.AsNoTracking().OrderBy(a => a.Id).ToList();
            ViewBag.risklevel = risklevel;
            //Classification
            var classfn = _context.AClassification.AsNoTracking().OrderBy(a => a.Id).ToList();
            ViewBag.classfn = classfn;

            ViewBag.promptbutton = new
            {
                content = "Assign Selected Records",
                isPrimary = true
            };
            ViewBag.promptbutton1 = new
            {
                content = "Cancel",
            };

            //logins
            var _logins = _context.ALoginLog.AsNoTracking().ToList();
            var logins = _logins.Where(o => Convert.ToDateTime(o.LogInDate).Date == DateTime.Now.Date).ToList().Count;
            ViewBag.LogCount = logins;
            //YesNo
            var YesNo = _context.AYesNo.AsNoTracking().ToList();
            ViewBag.YesNo = YesNo;
            //Classification
            var Classification = _context.AClassification.AsNoTracking().ToList();
            ViewBag.Classification = Classification;
            //County
            var County = _context.ACounty.AsNoTracking().ToList();
            ViewBag.County = County;
            //District
            var District = _context.ADistrict.AsNoTracking().ToList();
            ViewBag.District = District;
            //Sub-county
            var Subcounty = _context.ASubcounty.AsNoTracking().ToList();
            ViewBag.Subcounty = Subcounty;
            //Parish
            var Parish = _context.AParish.AsNoTracking().ToList();
            ViewBag.Parish = Parish;
            //Village
            var Village = _context.AVillage.AsNoTracking().ToList();
            ViewBag.Village = Village;
            ViewBag.Employee = _context.ViewEmployee.ToList();
            ViewBag.EmployeeEvacute = _context.ViewEmployee.Where(x=>x.Title == 1).ToList();
           
            //Address
            var Address = _context.VwAdminUnits.AsNoTracking().ToList();
            ViewBag.Address = Address;
            //COVID 19 OUTCOMES
            var Outcomes = _context.AOutcome.AsNoTracking().ToList();
            ViewBag.Outcomes = Outcomes;
            //Disease Type
            var Disease = _context.ADiseaseType.AsNoTracking().ToList();
            ViewBag.Disease = Disease;
            //Facility Type
            var Facility = _context.AFacilityUnit.AsNoTracking().ToList();
            ViewBag.Facility = Facility;
            //Lab Results
            var LabResults = _context.ALabResults.AsNoTracking().ToList();
            ViewBag.LabResults = LabResults;
            //Lab Results
            var Risk = _context.ARiskLevel.AsNoTracking().ToList();
            ViewBag.Risk = Risk;
            //Sex
            var Sex = _context.ASex.AsNoTracking().ToList();
            ViewBag.Sex = Sex;
            //Occupation
            var Occupation = _context.AOccupation.AsNoTracking().ToList();
            ViewBag.Occupation = Occupation;
            //Symptoms
            var Symptoms = _context.ASymptoms.AsNoTracking().ToList();
            ViewBag.Symptoms = Symptoms;
            ViewBag.Elevate = _context.AElevateContact.Where(x => x.Id == 1).ToList();
            return View();
        }

        //Partial View for adding a new case
        public IActionResult AddCasesPartial([FromBody] CRUDModel<CoreCases> value)
        {
            ViewBag.Elevate = _context.AElevateContact.Where(x => x.Id == 1).ToList();
            //logins
            var _logins = _context.ALoginLog.AsNoTracking().ToList();
            var logins = _logins.Where(o => Convert.ToDateTime(o.LogInDate).Date == DateTime.Now.Date).ToList().Count;
            ViewBag.LogCount = logins;
            //YesNo
            var YesNo = _context.AYesNo.AsNoTracking().ToList();
            ViewBag.YesNo = YesNo;
            ViewBag.EmployeeEvacute = _context.ViewEmployee.Where(x => x.Title == 1).ToList();
            //Classification
            var Classification = _context.AClassification.AsNoTracking().ToList();
            ViewBag.Classification = Classification;
            //County
            var County = _context.ACounty.AsNoTracking().ToList();
            ViewBag.County = County;
            //District
            var District = _context.ADistrict.AsNoTracking().ToList();
            ViewBag.District = District;
            //Sub-county
            var Subcounty = _context.ASubcounty.AsNoTracking().ToList();
            ViewBag.Subcounty = Subcounty;
            //Parish
            var Parish = _context.AParish.AsNoTracking().ToList();
            ViewBag.Parish = Parish;
            //Village
            var Village = _context.AVillage.AsNoTracking().ToList();
            ViewBag.Village = Village;
            //Address
            var Address = _context.VwAdminUnits.AsNoTracking().ToList();
            ViewBag.Address = Address;
            //COVID 19 OUTCOMES
            var Outcomes = _context.AOutcome.AsNoTracking().ToList();
            ViewBag.Outcomes = Outcomes;
            //Disease Type
            var Disease = _context.ADiseaseType.AsNoTracking().ToList();
            ViewBag.Disease = Disease;
            //Facility Type
            var Facility = _context.AFacilityUnit.AsNoTracking().ToList();
            ViewBag.Facility = Facility;
            //Lab Results
            var LabResults = _context.ALabResults.AsNoTracking().ToList();
            ViewBag.LabResults = LabResults;
            //Lab Results
            var Risk = _context.ARiskLevel.AsNoTracking().ToList();
            ViewBag.Risk = Risk;
            //Sex
            var Sex = _context.ASex.AsNoTracking().ToList();
            ViewBag.Sex = Sex;
            //Occupation
            var Occupation = _context.AOccupation.AsNoTracking().ToList();
            ViewBag.Occupation = Occupation;
            //case No.
            if (value.Value.CaseNo == null)
            {
                value.Value.CaseNo = GetCaseNo();
            }
            if (!string.IsNullOrEmpty(value.Value.Symptoms))
            {
                var Symptomsvalue = new int[] { };
                var _Symptoms = value.Value.Symptoms.Split(',');
                int count = 0;
                Symptomsvalue = new int[_Symptoms.Count()];
                foreach (var n in _Symptoms)
                {
                    Symptomsvalue[count] = Convert.ToInt32(n);
                    count++;
                }
                ViewBag.SymptomsValue = Symptomsvalue;
            }
            else
            {
                ViewBag.SymptomsValue = null;
            }
            //Symptoms
            var Symptoms = _context.ASymptoms.AsNoTracking().ToList();
            ViewBag.Symptoms = Symptoms;

            return PartialView("_DialogAddPartial", value.Value);
        }
        public ActionResult EditPartial([FromBody] CRUDModel<CoreCases> value)
        {
            ViewBag.Elevate = _context.AElevateContact.Where(x => x.Id == 1).ToList();
            //logins
            var _logins = _context.ALoginLog.AsNoTracking().ToList();
            var logins = _logins.Where(o => Convert.ToDateTime(o.LogInDate).Date == DateTime.Now.Date).ToList().Count;
            ViewBag.LogCount = logins;
            //YesNo
            var YesNo = _context.AYesNo.AsNoTracking().ToList();
            ViewBag.YesNo = YesNo;
            //Classification
            var Classification = _context.AClassification.AsNoTracking().ToList();
            ViewBag.Classification = Classification;
            //County
            var County = _context.ACounty.AsNoTracking().ToList();
            ViewBag.County = County;
            //District
            var District = _context.ADistrict.AsNoTracking().ToList();
            ViewBag.District = District;
            //Sub-county
            var Subcounty = _context.ASubcounty.AsNoTracking().ToList();
            ViewBag.Subcounty = Subcounty;
            //Parish
            var Parish = _context.AParish.AsNoTracking().ToList();
            ViewBag.Parish = Parish;
            //Village
            var Village = _context.AVillage.AsNoTracking().ToList();
            ViewBag.Village = Village;
            //Address
            var Address = _context.VwAdminUnits.AsNoTracking().ToList();
            ViewBag.Address = Address;
            //COVID 19 OUTCOMES
            var Outcomes = _context.AOutcome.AsNoTracking().ToList();
            ViewBag.Outcomes = Outcomes;
            //Disease Type
            var Disease = _context.ADiseaseType.AsNoTracking().ToList();
            ViewBag.Disease = Disease;
            //Facility Type
            var Facility = _context.AFacilityUnit.AsNoTracking().ToList();
            ViewBag.Facility = Facility;
            //Lab Results
            var LabResults = _context.ALabResults.AsNoTracking().ToList();
            ViewBag.LabResults = LabResults;
            //Lab Results
            var Risk = _context.ARiskLevel.AsNoTracking().ToList();
            ViewBag.Risk = Risk;
            //Sex
            var Sex = _context.ASex.AsNoTracking().ToList();
            ViewBag.Sex = Sex;
            //Occupation
            var Occupation = _context.AOccupation.AsNoTracking().ToList();
            ViewBag.Occupation = Occupation;
            //Symptoms

            if (!string.IsNullOrEmpty(value.Value.Symptoms))
            {
                var Symptomsvalue = new int[] { };
                var _Symptoms = value.Value.Symptoms.Split(',');
                int count = 0;
                Symptomsvalue = new int[_Symptoms.Count()];
                foreach (var n in _Symptoms)
                {
                    Symptomsvalue[count] = Convert.ToInt32(n);
                    count++;
                }
                ViewBag.SymptomsValue = Symptomsvalue;
            }
            else
            {
                ViewBag.SymptomsValue = null;
            }

            var Symptoms = _context.ASymptoms.AsNoTracking().ToList();
            ViewBag.Symptoms = Symptoms;

            return PartialView("_DialogEditPartial", value.Value);
            //return PartialView("_DialogAddPartial", value.Value);
        }
        public string GetCaseNo()
        {
            //CASE-1594989856-CS
            DateTime _date = System.DateTime.Now;
            int year = _date.Year;
            string month = _date.Month.ToString();
            if (month.Length < 2)
                month = "0" + month;

            string day = _date.Day.ToString();
            if (day.Length < 2)
                day = "0" + day;

            //string partial_no = "";
            string partial_no = year + "-" + month + "-" + day;//Day + Month + Year

            string _disease = string.Empty;
            var disease = _context.ADiseaseType.FirstOrDefault(o => o.DiseaseId == userInfor.getCurrentuserDisease());
            if (disease != null)
            {
                _disease = disease.Disease;
            }


            string code = _disease + "-WEB-CASE-" + userInfor.GetEmployeeID() + "-" + partial_no + "- 00";


            //string code = "COVID-19-" + "CASE-" + partial_no + "- 00";
            int result = 0;
            result = _context.CoreCases.ToList().Count();
            code += (result + 1);
            var x = new CoreCases()
            {
                CaseNo = code,
                AddedBy = userInfor.GetFullName(),
                DateAdded = DateTime.Now
            };
            _context.CoreCases.Add(x);
            _context.SaveChanges();

            return code;
        }
        public ActionResult GetCases([FromBody] DataManagerRequest dm)
        {
            IEnumerable data = _context.CoreCases.Where(a => /*a.FirstName != null &&*/ a.DeleteStatus != 1 && a.Disease == userInfor.getCurrentuserDisease()).OrderByDescending(o => o.DateOfReporting).ToList();
            int count = _context.CoreCases.Where(a => /*a.FirstName != null &&*/ a.DeleteStatus != 1 && a.Disease == userInfor.getCurrentuserDisease() ).ToList().Count();
            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip != 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take != 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = count }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        public ActionResult DialogUpdate([FromBody] CRUDModel<CoreCases> value)
        {
            CoreCases table = _context.CoreCases.FirstOrDefault(o =>o.CaseNo == value.Value.CaseNo);

            if (table != null)
            {
                table.ExternalCaseNo = value.Value.ExternalCaseNo;
                table.FirstName = value.Value.FirstName;
                table.MiddleName = value.Value.MiddleName;
                table.LastName = value.Value.LastName;
                table.WasContact = value.Value.WasContact;
                table.Occupation = value.Value.Occupation;
                table.OccupationSpecify = value.Value.OccupationSpecify;
                table.RiskLevel = value.Value.RiskLevel;
                table.Sex = value.Value.Sex;
                table.DoB = value.Value.DoB;
                table.Age = value.Value.Age;
                table.DateOfReporting = value.Value.DateOfReporting;
                table.RiskReason = value.Value.RiskReason;
                table.Address = value.Value.Address;
                table.DistrictId = value.Value.DistrictId;
                table.CountyId = value.Value.CountyId;
                table.SubcountyId = value.Value.SubcountyId;
                table.ParishId = value.Value.ParishId;
                table.VillageId = value.Value.VillageId;
                table.FacilityUnit = value.Value.FacilityUnit;
                table.Tel = value.Value.Tel;
                table.Email = value.Value.Email;
                table.Longitude = value.Value.Longitude;
                table.Latitude = value.Value.Latitude;
                table.Classification = value.Value.Classification;
                table.DateEdited = DateTime.Now;
                table.EditedBy = userInfor.GetFullName();  //value.Value.CreatedBy;
                table.ClassificationSpecify = value.Value.ClassificationSpecify;
                table.DateOnset = value.Value.DateOnset;
                table.CaseDate = value.Value.CaseDate;
                table.DateOfInfection = value.Value.DateOfInfection;
                table.Outcome = value.Value.Outcome;
                table.OutcomeSpecify = value.Value.OutcomeSpecify;
                table.DateOfOutcome = value.Value.DateOfOutcome;
                table.LabTestDate = value.Value.LabTestDate;
                table.LabResults = value.Value.LabResults;
                table.Disease = userInfor.getCurrentuserDisease();// value.Value.Disease;
                table.Symptoms = value.Value.Symptoms;
                table.SymptomsOthers = value.Value.SymptomsOthers;
                table.NextAction = value.Value.NextAction;
                table.StatusFlag = value.Value.StatusFlag;
                table.GeneralComments = value.Value.GeneralComments;
                table.SamplingDistrict = value.Value.SamplingDistrict;
                table.SamplingFacility = value.Value.SamplingFacility;
                table.SamplingDate = value.Value.SamplingDate;
                table.TestingLab = value.Value.TestingLab;
                table.AlertType = value.Value.AlertType;
                table.EvacuationFacility = value.Value.EvacuationFacility;
                table.CaseEvacuated = value.Value.CaseEvacuated;
                table.EvacuationOfficer = value.Value.EvacuationOfficer;
                table.EvacuationComments = value.Value.EvacuationComments;
                _context.Entry(table).CurrentValues.SetValues(value);
                _context.Entry(table).State = EntityState.Modified;

                try
                {
                    // Your code...
                    // Could also be before try if you know the exception occurs in SaveChanges
                    _context.SaveChanges();
                    
                    TempData["Success"] = "Record Saved Successfully!";
                }
                catch
                {
                    //foreach (var validationErrors in dbEx.EntityValidationErrors)
                    //{
                    //    foreach (var validationError in validationErrors.ValidationErrors)
                    //    {
                    //        TempData["Success"] += string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    //        System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    //    }
                    //}
                }
            }

            return Json(value.Value);
        }

        public ActionResult DialogInsert([FromBody] CRUDModel<CoreCases> value)
        {
            CoreCases _table = _context.CoreCases.FirstOrDefault(o => o.CaseNo == value.Value.CaseNo);
            //EmailJob obx = new EmailJob();
            if (_table == null)
            {
                CoreCases table = new CoreCases();
                table.CaseNo = value.Value.CaseNo;
                table.ExternalCaseNo = value.Value.ExternalCaseNo;
                table.FirstName = value.Value.FirstName;
                table.MiddleName = value.Value.MiddleName;
                table.LastName = value.Value.LastName;
                table.WasContact = value.Value.WasContact;
                table.Occupation = value.Value.Occupation;
                table.OccupationSpecify = value.Value.OccupationSpecify;
                table.RiskLevel = value.Value.RiskLevel;
                table.Sex = value.Value.Sex;
                table.DoB = value.Value.DoB;
                table.Age = value.Value.Age;
                table.DateOfReporting = value.Value.DateOfReporting;
                table.RiskReason = value.Value.RiskReason;
                table.Address = value.Value.Address;
                table.DistrictId = value.Value.DistrictId;
                table.CountyId = value.Value.CountyId;
                table.SubcountyId = value.Value.SubcountyId;
                table.ParishId = value.Value.ParishId;
                table.VillageId = value.Value.VillageId;
                table.FacilityUnit = value.Value.FacilityUnit;
                table.Tel = value.Value.Tel;
                table.Email = value.Value.Email;
                table.Longitude = value.Value.Longitude;
                table.Latitude = value.Value.Latitude;
                table.Classification = value.Value.Classification;
                table.DateAdded = DateTime.Now;
                table.AddedBy = userInfor.GetFullName();  //value.Value.CreatedBy;
                table.ClassificationSpecify = value.Value.ClassificationSpecify;
                table.DateOnset = value.Value.DateOnset;
                table.CaseDate = value.Value.CaseDate;
                table.DateOfInfection = value.Value.DateOfInfection;
                table.Outcome = value.Value.Outcome;
                table.OutcomeSpecify = value.Value.OutcomeSpecify;
                table.DateOfOutcome = value.Value.DateOfOutcome;
                table.LabTestDate = value.Value.LabTestDate;
                table.LabResults = value.Value.LabResults;
                table.Disease = userInfor.getCurrentuserDisease();// value.Value.Disease;
                table.Symptoms = value.Value.Symptoms;
                table.SymptomsOthers = value.Value.SymptomsOthers;
                table.NextAction = value.Value.NextAction;
                table.StatusFlag = value.Value.StatusFlag;
                table.GeneralComments = value.Value.GeneralComments;
                table.SamplingDistrict = value.Value.SamplingDistrict;
                table.SamplingFacility = value.Value.SamplingFacility;
                table.SamplingDate = value.Value.SamplingDate;
                table.TestingLab = value.Value.TestingLab;
                table.AlertType = value.Value.AlertType;
                table.EvacuationFacility = value.Value.EvacuationFacility;
                table.CaseEvacuated = value.Value.CaseEvacuated;
                table.EvacuationOfficer = value.Value.EvacuationOfficer;
                table.EvacuationComments = value.Value.EvacuationComments;
                _context.CoreCases.Add(table);
                _context.SaveChanges();
            }
            else
            {
                this.DialogUpdate(value);
            }
            return Json(value.Value);
        }
        //Save Support Docs
        [AcceptVerbs("Post")]
        public IActionResult Save(IList<IFormFile> UploadFiles, string CaseNo)
        {
            try
            {
                foreach (var file in UploadFiles)
                {
                    if (UploadFiles != null)
                    {
                        var filename = Path.GetFileName(file.FileName); ///ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                       // filename = hostingEnv.WebRootPath + $@"\{filename}";
                        var fileSavePath = Path.Combine(hostingEnv.WebRootPath + "\\UploadedFiles\\", filename);
                        if (!System.IO.File.Exists(fileSavePath))
                        {
                            byte[] fileBytes;
                            using (MemoryStream ms = new MemoryStream())
                            {
                                file.OpenReadStream().CopyTo(ms);
                                fileBytes = ms.ToArray();
                            }

                            string ext = Path.GetExtension(filename);
                            CoreCaseFileAttachment doc = new CoreCaseFileAttachment()
                            {
                                CaseNo = CaseNo,
                                FileType = ext,
                                FileUrl = fileBytes,
                                Details = filename,
                                Id = 1,
                                AddedBy = userInfor.GetFullName(),
                                DateAdded = System.DateTime.Now
                            };
                            var evidence = _context.CoreCaseFileAttachment.OrderByDescending(o => o.Id).FirstOrDefault();
                            if (evidence != null)
                            {
                                doc.Id = (evidence.Id + 1);
                            }
                            _context.CoreCaseFileAttachment.Add(doc);
                            _context.SaveChanges();
                        }
                        else
                        {
                            Response.Clear();
                            Response.StatusCode = 204;
                            Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = "File already exists.";
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Response.Clear();
                Response.ContentType = "application/json; charset=utf-8";
                Response.StatusCode = 204;
                Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = "No Content";
                Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = e.Message;
            }
            return Content("");
        }
        [AcceptVerbs("Post")]
        public IActionResult Remove(IList<IFormFile> UploadFiles, string CaseNo)
        {
            try
            {
                foreach (var file in UploadFiles)
                {
                    var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    var filePath = Path.Combine(hostingEnv.WebRootPath + "\\UploadedFiles");
                    var fileSavePath = filePath + "\\" + fileName;
                    if (!System.IO.File.Exists(fileSavePath))
                    {
                        System.IO.File.Delete(fileSavePath);
                    }
                    var exists = _context.CoreCaseFileAttachment.FirstOrDefault(o => o.Details == fileName && o.CaseNo == CaseNo);
                    if (exists != null)
                    {
                        _context.CoreCaseFileAttachment.Remove(exists);
                        _context.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                Response.Clear();
                Response.StatusCode = 200;
                Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = "File removed successfully";
                Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = e.Message;
            }
            return Content("");
        }
        public ActionResult GetSupportingDocuments([FromBody] DataManagerRequest dm, string CaseNo)
        {
            IEnumerable data = _context.CoreCaseFileAttachment.Where(o => o.CaseNo == CaseNo).Select(o => new { o.Id, o.Details, o.FileType, o.CaseNo }).ToList();
            int count = _context.CoreCaseFileAttachment.Where(o => o.CaseNo == CaseNo).ToList().Count;

            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip != 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take != 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = count });
        }
        public FileResult DownloadEvidenceFile(int? DocumentCode)
        {
            byte[] bytes = null;
            string fileName = string.Empty, contentType = string.Empty;
            var file = _context.CoreCaseFileAttachment.FirstOrDefault(o => o.Id == DocumentCode);
            if (file != null)
            {
                bytes = file.FileUrl;
                fileName = file.Details;
                //contentType = "application/" + file.FileType.Replace('.', ' ').Trim();
                contentType = GetContentType(file.FileType);
            }

            return File(bytes, contentType, fileName);
        }
        public ActionResult ContactsDataSource([FromBody] DataManagerRequest dm)
        {

            var _data = _context.CoreCaseContacts.AsNoTracking().Where(p=>p.Disease== userInfor.getCurrentuserDisease() && p.DeleteStatus != 1).OrderByDescending(o => o.ContactCaseNo).ToList();
            IEnumerable data = _data;
            int _count = _data.Count();

            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                _count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                _count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip > 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take > 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = _count });
        }

        //Save Photoz
        [AcceptVerbs("Post")]
        public IActionResult SavePhoto(IList<IFormFile> UploadPhoto, string CaseNo)
        {
            try
            {
                foreach (var file in UploadPhoto)
                {
                    if (UploadPhoto != null)
                    {
                        var filename = Path.GetFileName(file.FileName); ///ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                       // filename = hostingEnv.WebRootPath + $@"\{filename}";
                        var fileSavePath = Path.Combine(hostingEnv.WebRootPath + "\\UploadedFiles\\", filename);
                        if (!System.IO.File.Exists(fileSavePath))
                        {
                            byte[] fileBytes;
                            using (MemoryStream ms = new MemoryStream())
                            {
                                file.OpenReadStream().CopyTo(ms);
                                fileBytes = ms.ToArray();
                            }

                            string ext = Path.GetExtension(filename);
                            CoreCasePhoto doc = new CoreCasePhoto()
                            {
                                CaseNo = CaseNo,
                                FileType = ext,
                                Photo = fileBytes
                               
                            };
                            var evidence = _context.CoreCasePhoto.FirstOrDefault();
                            _context.CoreCasePhoto.Add(doc);
                            _context.SaveChanges();
                        }
                        else
                        {
                            Response.Clear();
                            Response.StatusCode = 204;
                            Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = "File already exists.";
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Response.Clear();
                Response.ContentType = "application/json; charset=utf-8";
                Response.StatusCode = 204;
                Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = "No Content";
                Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = e.Message;
            }
            return Content("");
        }
        [AcceptVerbs("Post")]
        public IActionResult RemovePhoto(IList<IFormFile> UploadPhoto, string CaseNo)
        {
            try
            {
                foreach (var file in UploadPhoto)
                {
                    var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    var filePath = Path.Combine(hostingEnv.WebRootPath + "\\UploadedFiles");
                    var fileSavePath = filePath + "\\" + fileName;
                    if (!System.IO.File.Exists(fileSavePath))
                    {
                        System.IO.File.Delete(fileSavePath);
                    }
                    var exists = _context.CoreCasePhoto.FirstOrDefault(o => o.CaseNo == CaseNo);
                    if (exists != null)
                    {
                        _context.CoreCasePhoto.Remove(exists);
                        _context.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                Response.Clear();
                Response.StatusCode = 200;
                Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = "File removed successfully";
                Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = e.Message;
            }
            return Content("");
        }
        public ActionResult GetPhotoz([FromBody] DataManagerRequest dm, string CaseNo)
        {
            IEnumerable data = _context.CoreCasePhoto.Where(o => o.CaseNo == CaseNo).Select(o => new { o.Photo, o.FileType, o.CaseNo }).ToList();
            int count = _context.CoreCasePhoto.Where(o => o.CaseNo == CaseNo).ToList().Count;

            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip != 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take != 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = count });
        }
        public FileResult DownloadCasePhotoFile(string DocumentCode)
        {
            byte[] bytes = null;
            string fileName = string.Empty, contentType = string.Empty;
            var file = _context.CoreCasePhoto.FirstOrDefault(o => o.CaseNo == DocumentCode);
            if (file != null)
            {
                bytes = file.Photo;
                //contentType = "application/" + file.FileType.Replace('.', ' ').Trim();
                contentType = GetContentType(file.FileType);
            }
            

            return File(bytes, contentType, fileName);
        }
        private string GetContentType(string ext)
        {
            var types = GetMimeTypes();
            var _ext = ext.ToLowerInvariant();
            return types[_ext];
        }

        private Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
               {".txt", "text/plain"},
                {".pdf", "application/pdf"},
                {".doc", "application/msword"},
                {".docx", "application/vnd.ms-word.document.12"},
                {".xls", "Application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
                {".xlsx", "Application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
                {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".gif", "image/gif"},
                {".csv", "text/csv"}
            };
        }

        //County Change
        public ActionResult getcounty(string DistrictId)
        {
            var county = _context.ACounty.Where(o => o.DistrictId == DistrictId).AsNoTracking().ToList();
            return Json(county);
        }

        //Division Change
        public ActionResult getdivision(string CountyId)
        {
            var subcounty = _context.ASubcounty.Where(o => o.CountyId == CountyId).AsNoTracking().ToList();
            return Json(subcounty);
        }

        //SubCounty/Parish Change
        public ActionResult getparish(string SubcountyId)
        {
            var parish = _context.AParish.Where(o => o.SubcountyId == SubcountyId).AsNoTracking().ToList();
            return Json(parish);
        }

        //Village Change
        public ActionResult getvillage(string ParishId)
        {
            var village = _context.AVillage.Where(o => o.ParishId == ParishId).AsNoTracking().ToList();
            return Json(village);
        }

        //remove cases
        public ActionResult RemoveRecord(string CaseNo)
        {
            _context.CoreCases.Remove(_context.CoreCases.FirstOrDefault(o => o.CaseNo == CaseNo));
            int result = _context.SaveChanges();

            return Json(result);
        }

        //delete
        public ActionResult DeleteUrl([FromBody] CRUDModel<CoreCases> value)
        {
            string obj = Request.Headers["additional_key"]; //key1 
            //string obj1 = Request.Headers["additional_key1"]; //key2 
            CoreCases table = _context.CoreCases.FirstOrDefault(or => or.CaseNo  == obj );
            //_context.Remove(table);
            //_context.SaveChanges();
            if (table != null)
            {
                if (value.Deleted != null)
                {
                    foreach (var temp in value.Deleted)
                    {
                        temp.DeleteStatus = 1;
                        temp.DeletedBy = userInfor.GetFullName();
                        temp.DateDeleted = System.DateTime.Now;
                        _context.Entry(temp).State = EntityState.Modified;
                        try
                        {
                            _context.SaveChanges();
                        }
                        catch (Exception dbEx)
                        {
                            throw (dbEx);
                        }
                    }
                }
                else
                {
                    table.DeleteStatus = 1;
                    table.DeletedBy = userInfor.GetFullName();
                    table.DateDeleted = System.DateTime.Now;
                    _context.Entry(table).State = EntityState.Modified;
                    try
                    {
                        _context.SaveChanges();
                    }
                    catch (Exception dbEx)
                    {
                        throw (dbEx);
                    }
                }
               
            }
            //var msg = "Case No. " + obj + " Has been Deleted Successfully !!";
            return Json(value);
        }

        public ActionResult SaveEvacuationDetails(int? EmployeeID, int? CaseEvacuated, string EvacuationFacility,string EvacuationComments, string CaseNo)
        {
            CoreCases table = _context.CoreCases.FirstOrDefault(o => o.CaseNo == CaseNo );

            if (table != null)
            {
                table.EvacuationOfficer = EmployeeID;
                table.CaseEvacuated = CaseEvacuated;
                table.EvacuationFacility = EvacuationFacility;
                table.EvacuationComments = EvacuationComments;
                _context.Entry(table).CurrentValues.SetValues(EmployeeID);
                _context.Entry(table).State = EntityState.Modified;
                try
                {
                    _context.SaveChanges();
                }

                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            TempData["Success"] += string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                            System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }
                }
            }
            //send email to assigned person

            //if (EmployeeID != null)
            //{
            //    EmailModel email = new EmailModel(_context);
            //    email.SendAssigmentEmail(ContactCaseNo, table.FirstName, table.LastName, table.Tel, EmployeeID);
            //}

            var msg = "Record Assigned successfully";
            //return Json(msg);
            return RedirectToAction("Cases");
        }


    }
}
