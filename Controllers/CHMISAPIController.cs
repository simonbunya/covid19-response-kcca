﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CHMIS.Models;
using CHMIS.ModelView;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.EntityFrameworkCore;

namespace CHMIS.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CHMISAPIController : ControllerBase
    {
        private readonly SignInManager<IdentityUser> _signInManager;
        private CHMISContext _context;
        private readonly UserManager<IdentityUser> _userManager;
        public CHMISAPIController(SignInManager<IdentityUser> signInManager, UserManager<IdentityUser> userManager, CHMISContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            this._context = context;
        }
        [HttpGet("GetPrimaryData/{userId}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<PrimaryData>> GetPrimaryData(string userId)
        {
            PrimaryData data = new PrimaryData();
            try
            {
                int[] facility = new int[] { };
                var user = _context.AspNetUsers.Find(userId);
                var employee = _context.AEmployees.FirstOrDefault(o => o.UserId == userId);
                var facilities = new string[] { };
                if (!string.IsNullOrEmpty(user.Facility))
                {
                    if (user.Facility.Any(char.IsDigit))
                    {
                        facilities = user.Facility.Split(',');
                    }
                }
                if (facilities.Length > 0)
                {
                    facility = new int[facilities.Length];
                    int count = 0;
                    foreach (var n in facilities)
                    {
                        if (n.All(char.IsDigit))
                        {
                            int t = Convert.ToInt32(n);
                            facility[count] = t;
                            count++;
                        }
                    }
                }
                var selecetedModules = _context.ASelecetedModules.FirstOrDefault(o => o.UserId == userId);

                if (selecetedModules != null)
                {
                    var modules = selecetedModules.SelectedModules.Split(',');
                    foreach (var a in modules)
                    {
                        if (a.All(char.IsDigit))
                        {
                            int b = Convert.ToInt32(a);
                            switch (b)
                            {
                                case 1://caases
                                    if (data.ViewCases == null)
                                    {
                                        data.ViewCases = await _context.ViewMobileCase.ToListAsync();
                                    }
                                    break;
                                case 2://contacts
                                    if (data.ViewCases == null)
                                    {
                                        data.ViewCases = await _context.ViewMobileCase.ToListAsync();
                                    }
                                    if (data.ViewContacts == null)
                                    {
                                        data.ViewContacts = await _context.CoreCaseContacts.Where(o => o.AssignFollowUpContactPerson == employee.EmployeeId).ToListAsync();
                                    }
                                    break;
                                case 3://followup
                                    if (data.ViewContacts == null)
                                    {
                                        data.ViewContacts = await _context.CoreCaseContacts.Where(o => o.AssignFollowUpContactPerson == employee.EmployeeId).ToListAsync();
                                    }
                                    data.ADays = await _context.ADays.AsNoTracking().ToListAsync();
                                    data.FollowUp = await _context.ViewFollowUp.Where(o => o.AssignFollowUpContactPerson == employee.EmployeeId).ToListAsync();
                                    data.FollowUpTest = await _context.ViewFollowupTests.Where(o => o.AssignFollowUpContactPerson == employee.EmployeeId).ToListAsync();
                                    break;
                                case 4:///compliance
                                    var t = await _context.CoreComplianceVisit.ToListAsync();
                                    data.ViewComplianceVisit = t.Where(o => (o.FacilityCode != null && facility.Any(x => x == o.FacilityCode))).ToList();
                                    break;
                                case 5:/// community surveillance
                                    data.ViewSurveillance = await _context.CoreCommunitySurveillance.Where(o => o.AssignFollowUpContactPerson == employee.EmployeeId).ToListAsync();

                                    break;
                                case 6:

                                    break;
                                case 7:///sit Rep
                                    data.ViewSitRepPrimary = await _context.ViewSitRepPrimary.ToListAsync();
                                    break;
                            }
                        }
                    }
                }
                data.AClassification = await _context.AClassification.AsNoTracking().ToListAsync();
                data.ACountry = await _context.ACountry.AsNoTracking().ToListAsync();
                data.ACounty = await _context.ACounty.AsNoTracking().ToListAsync();
                data.ADistrict = await _context.ADistrict.AsNoTracking().ToListAsync();
                var _facility = await _context.AFacilityUnit.AsNoTracking().ToListAsync();
                data.AFacilityUnit = _facility.Where(o => facility.Any(a => a == o.FacilityCode) || facility.Length == 0).ToList();
                data.AExposureType = await _context.AExposureType.AsNoTracking().ToListAsync();
                data.ALabResults = await _context.ALabResults.AsNoTracking().ToListAsync();
                data.AOccupation = await _context.AOccupation.AsNoTracking().ToListAsync();
                data.AOperationalArea = await _context.AOperationalArea.AsNoTracking().ToListAsync();
                data.AOutcome = await _context.AOutcome.AsNoTracking().ToListAsync();
                data.AParish = await _context.AParish.AsNoTracking().ToListAsync();
                data.ARelationship = await _context.ARelationship.AsNoTracking().ToListAsync();
                data.ARiskLevel = await _context.ARiskLevel.AsNoTracking().ToListAsync();
                data.ASex = await _context.ASex.AsNoTracking().ToListAsync();
                data.ASubcounty = await _context.ASubcounty.AsNoTracking().ToListAsync();
                data.ASymptoms = await _context.ASymptoms.AsNoTracking().ToListAsync();
                data.ATitle = await _context.ATitle.AsNoTracking().ToListAsync();
                data.AVillage = await _context.AVillage.AsNoTracking().ToListAsync();
                data.AYesNo = await _context.AYesNo.AsNoTracking().ToListAsync();
                data.Employee = await _context.ViewEmployee.ToListAsync();
                data.ADiseaseType = await _context.ADiseaseType.ToListAsync();
                data.VwAdminUnits = await _context.VwAdminUnits.ToListAsync();



                string publicTransport = "-PUBLIC-TRANSPORT-" + employee.EmployeeId;
                //data.corePublicTransport = await _context.CorePublicTransport.Where(o => o.Code.Contains(publicTransport)).ToListAsync();
                //data.corePublicTransportDetials = await _context.CorePublicTransportDetials.Where(o => o.BusCode.Contains(publicTransport)).ToListAsync();


                if (data == null)
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {

            }

            return data;
        }
        [HttpGet("CheckServer")]
        [ProducesResponseType(200)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<List<AClassification>>> CheckServer()
        {
            var data = await _context.AClassification.AsNoTracking().ToListAsync();
            return data;
        }
        [HttpGet("ADiseaseType")]
        [ProducesResponseType(200)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<List<ADiseaseType>>> ADiseaseType()
        {
            var data = await _context.ADiseaseType.AsNoTracking().ToListAsync();
            return data;
        }

        [HttpPost("Login")]
        [ProducesResponseType(200)]
        [ProducesResponseType(500)]
        public async Task<ActionResult> Login(LoginCredentials Credentials)
        {
            string Username = Encoding.UTF8.GetString(WebEncoders.Base64UrlDecode(Credentials.UserName));
            string password = Encoding.UTF8.GetString(WebEncoders.Base64UrlDecode(Credentials.Password));
            var user = _context.AspNetUsers.FirstOrDefault(o => (o.Email == Username || o.UserName == Username));
            if (user == null)
            {
                return NotFound();
            }
            Username = user.UserName;
            var result = await _signInManager.PasswordSignInAsync(Username,
                  password, false, false);
            if (!result.Succeeded)
            {
                return NotFound();
            }
            var employeedata = await _context.AEmployees.FirstOrDefaultAsync(o => o.UserId == user.Id);
            var userrole = await _context.ViewUserManagement.FirstOrDefaultAsync(o => o.UserId == user.Id);
            if (userrole == null)
            {
                return NotFound();
            }
            Random random = new Random();
            int code = random.Next(100, 100000);
            string _code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(code.ToString()));

            SucessLoginCredentials data = new SucessLoginCredentials();
            data.AccessToken = _code;
            data.Email = employeedata.Email;
            data.EmployeeId = employeedata.EmployeeId;
            data.LoginDate = DateTime.Now;
            data.ExpiryDate = data.LoginDate.AddDays(7);
            data.FullName = employeedata.Name;
            data.Tel = employeedata.Telephone;
            data.UserId = user.Id;
            data.UserRole = userrole.RoleName;
            data.assignedfacilities = user.Facility;
            var module = _context.ASelecetedModules.FirstOrDefault(o => o.UserId == user.Id);
            if (module != null)
            {
                data.assignedModules = module.SelectedModules;
            }

            MobileLogin m = new MobileLogin()
            {
                LoginCode = _code,
                LoginDate = data.LoginDate,
                ExpiryDate = data.ExpiryDate,
                UserId = user.Id,
            };
            if (_context.MobileLogin.ToList().Count == 0)
            {
                m.LoginId = 1;
            }
            else
            {
                m.LoginId = (_context.MobileLogin.ToList().Last().LoginId + 1);
            }
            _context.MobileLogin.Add(m);
            await _context.SaveChangesAsync();
            return Ok(data);
        }
        [HttpPost("UploadData")]
        [ProducesResponseType(200)]
        [ProducesResponseType(500)]
        public async Task<ActionResult> UploadData(UploadedData uploads)
        {
            try
            {
                //string code = Encoding.UTF8.GetString(WebEncoders.Base64UrlDecode(uploads.AccessCode));
                var loginvalid = await _context.MobileLogin.FirstOrDefaultAsync(o => o.LoginCode == uploads.AccessCode && o.ExpiryDate > DateTime.Now);
                if (loginvalid == null) { return NotFound(); }

                if (uploads.CoreCases != null)
                {
                    foreach (var n in uploads.CoreCases)
                    {
                        var exists = _context.CoreCases.FirstOrDefault(o => o.CaseNo == n.CaseNo);
                        if (exists == null)
                        {
                            _context.CoreCases.Add(n);
                        }
                        else
                        {
                            _context.Entry(exists).CurrentValues.SetValues(n);
                            _context.Entry(exists).State = EntityState.Modified;
                        }
                        await _context.SaveChangesAsync();

                    }
                }
                if (uploads.CoreCaseContacts != null)
                {
                    foreach (var n in uploads.CoreCaseContacts)
                    {

                        try
                        {
                            var exists = _context.CoreCaseContacts.FirstOrDefault(o => o.ContactCaseNo == n.ContactCaseNo);
                            if (exists == null)
                            {
                                _context.CoreCaseContacts.Add(n);
                            }
                            else
                            {
                                _context.Entry(exists).CurrentValues.SetValues(n);
                                _context.Entry(exists).State = EntityState.Modified;
                            }
                            await _context.SaveChangesAsync();
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }
                if (uploads.CoreCaseContactsMonitoring != null)
                {
                    foreach (var n in uploads.CoreCaseContactsMonitoring)
                    {
                        try
                        {
                            var exists = _context.CoreCaseContactsMonitoring.FirstOrDefault(o => o.ContactCaseNo == n.ContactCaseNo && o.Days == n.Days);
                            if (exists == null)
                            {
                                _context.CoreCaseContactsMonitoring.Add(n);
                            }
                            else
                            {
                                _context.Entry(exists).CurrentValues.SetValues(n);
                                _context.Entry(exists).State = EntityState.Modified;
                            }
                            await _context.SaveChangesAsync();
                        }
                        catch (Exception ex)
                        {

                            //throw;
                        }
                    }
                }
                if (uploads.CoreCommunitySurveillance != null)
                {
                    foreach (var n in uploads.CoreCommunitySurveillance)
                    {
                        var exists = _context.CoreCommunitySurveillance.FirstOrDefault(o => o.CaseNo == n.CaseNo);
                        if (exists == null)
                        {
                            _context.CoreCommunitySurveillance.Add(n);
                        }
                        else
                        {
                            _context.Entry(exists).CurrentValues.SetValues(n);
                            _context.Entry(exists).State = EntityState.Modified;
                        }
                        await _context.SaveChangesAsync();
                    }
                }
                if (uploads.CoreComplianceVisit != null)
                {
                    foreach (var n in uploads.CoreComplianceVisit)
                    {
                        var exists = _context.CoreComplianceVisit.FirstOrDefault(o => o.ComplianceCode == n.ComplianceCode);
                        if (exists == null)
                        {
                            _context.CoreComplianceVisit.Add(n);
                        }
                        else
                        {
                            _context.Entry(exists).CurrentValues.SetValues(n);
                            _context.Entry(exists).State = EntityState.Modified;
                        }
                        await _context.SaveChangesAsync();
                    }
                }
                if (uploads.CorePublicTransport != null)
                {
                    foreach (var n in uploads.CorePublicTransport)
                    {
                        var exists = _context.CorePublicTransport.FirstOrDefault(o => o.Code == n.Code);
                        if (exists == null)
                        {
                            _context.CorePublicTransport.Add(n);
                        }
                        else
                        {
                            _context.Entry(exists).CurrentValues.SetValues(n);
                            _context.Entry(exists).State = EntityState.Modified;
                        }
                        await _context.SaveChangesAsync();
                    }
                }
                if (uploads.CorePublicTransportDetials != null)
                {
                    foreach (var n in uploads.CorePublicTransportDetials)
                    {
                        var exists = _context.CorePublicTransportDetials.FirstOrDefault(o => o.PassengerCode == n.PassengerCode);
                        if (exists == null)
                        {
                            _context.CorePublicTransportDetials.Add(n);
                        }
                        else
                        {
                            _context.Entry(exists).CurrentValues.SetValues(n);
                            _context.Entry(exists).State = EntityState.Modified;
                        }
                        await _context.SaveChangesAsync();
                    }
                }
                if (uploads.CoreSitRepHeader != null)
                {
                    foreach (var n in uploads.CoreSitRepHeader)
                    {
                        var exists = _context.CoreSitRepHeader.FirstOrDefault(o => o.Division == n.Division && o.ReportingDate.Date == n.ReportingDate.Date);
                        if (exists == null)
                        {
                            _context.CoreSitRepHeader.Add(n);
                        }
                        else
                        {
                            _context.Entry(exists).CurrentValues.SetValues(n);
                            _context.Entry(exists).State = EntityState.Modified;
                        }
                        await _context.SaveChangesAsync();

                    }
                }
                if (uploads.CoreSitRep != null)
                {
                    foreach (var n in uploads.CoreSitRep)
                    {
                        var exists = _context.CoreSitRep.FirstOrDefault(o => o.Division == n.Division && o.ReportingDate.Date == n.ReportingDate.Date && o.SitRepCategoryId == n.SitRepCategoryId);
                        if (exists == null)
                        {
                            _context.CoreSitRep.Add(n);
                        }
                        else
                        {
                            _context.Entry(exists).CurrentValues.SetValues(n);
                            _context.Entry(exists).State = EntityState.Modified;
                        }
                        await _context.SaveChangesAsync();

                    }
                }
                UploadReturnData data = new UploadReturnData();
                int[] facility = new int[] { };
                var user = _context.AspNetUsers.Find(loginvalid.UserId);
                var facilities = new string[] { };
                if (!string.IsNullOrEmpty(user.Facility))
                {
                    if (user.Facility.Any(char.IsDigit))
                    {
                        facilities = user.Facility.Split(',');
                    }
                }
                if (facilities.Length > 0)
                {
                    facility = new int[facilities.Length];
                    int count = 0;
                    foreach (var n in facilities)
                    {
                        if (n.All(char.IsDigit))
                        {
                            int t = Convert.ToInt32(n);
                            facility[count] = t;
                            count++;
                        }
                    }
                }

                var employee = _context.AEmployees.FirstOrDefault(o => o.UserId == loginvalid.UserId);
                var selecetedModules = _context.ASelecetedModules.FirstOrDefault(o => o.UserId == loginvalid.UserId);

                if (selecetedModules != null)
                {
                    var modules = selecetedModules.SelectedModules.Split(',');
                    foreach (var a in modules)
                    {
                        if (a.All(char.IsDigit))
                        {
                            int b = Convert.ToInt32(a);
                            switch (b)
                            {
                                case 1://caases
                                    if (data.ViewCases == null)
                                    {
                                        data.ViewCases = await _context.ViewMobileCase.ToListAsync();
                                    }
                                    break;
                                case 2://contacts
                                    if (data.ViewCases == null)
                                    {
                                        data.ViewCases = await _context.ViewMobileCase.ToListAsync();
                                    }
                                    if (data.ViewContacts == null)
                                    {
                                        data.ViewContacts = await _context.CoreCaseContacts.Where(o => o.AssignFollowUpContactPerson == employee.EmployeeId).ToListAsync();
                                    }
                                    break;
                                case 3://followup
                                    if (data.ViewContacts == null)
                                    {
                                        data.ViewContacts = await _context.CoreCaseContacts.Where(o => o.AssignFollowUpContactPerson == employee.EmployeeId).ToListAsync();
                                    }
                                    data.ADays = await _context.ADays.AsNoTracking().ToListAsync();
                                    // data.FollowUp = await _context.ViewFollowUp.Where(o => o.AssignFollowUpContactPerson == employee.EmployeeId).ToListAsync();
                                    //data.FollowUpTest = await _context.ViewFollowupTests.Where(o => o.AssignFollowUpContactPerson == employee.EmployeeId).ToListAsync();
                                    break;
                                case 4:///compliance
                                    //data.ViewComplianceVisit = await _context.CoreComplianceVisit.Where(o => (o.FacilityCode != null && facility.Any(x => x == o.FacilityCode))).ToListAsync();
                                    break;
                                case 5:/// community surveillance
                                   // data.ViewSurveillance = await _context.CoreCommunitySurveillance.Where(o => o.AssignFollowUpContactPerson == employee.EmployeeId).ToListAsync();

                                    break;
                                case 6:

                                    break;
                                case 7:///sit Rep
                                    data.ViewSitRepPrimary = await _context.ViewSitRepPrimary.ToListAsync();
                                    break;
                            }
                        }
                    }
                }
                data.AClassification = await _context.AClassification.AsNoTracking().ToListAsync();
                data.ACountry = await _context.ACountry.AsNoTracking().ToListAsync();
                data.ACounty = await _context.ACounty.AsNoTracking().ToListAsync();
                data.ADistrict = await _context.ADistrict.AsNoTracking().ToListAsync();
                var _facility = await _context.AFacilityUnit.AsNoTracking().ToListAsync();
                data.AFacilityUnit = _facility.Where(o => facility.Any(a => a == o.FacilityCode) || facility.Length == 0).ToList();
                data.AExposureType = await _context.AExposureType.AsNoTracking().ToListAsync();
                data.ALabResults = await _context.ALabResults.AsNoTracking().ToListAsync();
                data.AOccupation = await _context.AOccupation.AsNoTracking().ToListAsync();
                data.AOperationalArea = await _context.AOperationalArea.AsNoTracking().ToListAsync();
                data.AOutcome = await _context.AOutcome.AsNoTracking().ToListAsync();
                data.AParish = await _context.AParish.AsNoTracking().ToListAsync();
                data.ARelationship = await _context.ARelationship.AsNoTracking().ToListAsync();
                data.ARiskLevel = await _context.ARiskLevel.AsNoTracking().ToListAsync();
                data.ASex = await _context.ASex.AsNoTracking().ToListAsync();
                data.ASubcounty = await _context.ASubcounty.AsNoTracking().ToListAsync();
                data.ASymptoms = await _context.ASymptoms.AsNoTracking().ToListAsync();
                data.ATitle = await _context.ATitle.AsNoTracking().ToListAsync();
                data.AVillage = await _context.AVillage.AsNoTracking().ToListAsync();
                data.AYesNo = await _context.AYesNo.AsNoTracking().ToListAsync();
                data.Employee = await _context.ViewEmployee.ToListAsync();
                data.ADiseaseType = await _context.ADiseaseType.ToListAsync();
                data.VwAdminUnits = await _context.VwAdminUnits.ToListAsync();

                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
        [HttpPost("UpdateEmployee")]
        [ProducesResponseType(200)]
        [ProducesResponseType(500)]
        public async Task<ActionResult> UpdateEmployee(ViewEmployee employee)
        {
            if (employee != null)
            {
                var exist = await _context.AEmployees.FirstOrDefaultAsync(o => o.EmployeeId == employee.EmployeeId);
                if (exist != null)
                {
                    exist.Name = employee.Name;
                    exist.Email = employee.Email;
                    exist.Phone = employee.Phone;
                    exist.Telephone = employee.Telephone;
                    exist.NinNo = employee.NinNo;
                    exist.Gender = employee.Gender;
                    exist.Nationality = employee.Nationality;
                    exist.Title = employee.Title;
                    exist.UserId = employee.UserId;
                    exist.DateOfBirth = employee.DateOfBirth;
                    exist.PhysicalAddress = employee.PhysicalAddress;
                    _context.Entry(exist).State = EntityState.Modified;
                    await _context.SaveChangesAsync();
                }
            }
            return Ok();
        }
        [HttpPost("ChangePassword")]
        [ProducesResponseType(200)]
        [ProducesResponseType(500)]
        public async Task<ActionResult> ChangePassword(ChangePassword changePassword)
        {
            try
            {
                //string code = Encoding.UTF8.GetString(WebEncoders.Base64UrlDecode(uploads.AccessCode));
                var loginvalid = await _context.MobileLogin.FirstOrDefaultAsync(o => o.LoginCode == changePassword.AccessCode && o.ExpiryDate > DateTime.Now);
                if (loginvalid == null) { return NotFound(); }
                var user = await _userManager.FindByIdAsync(changePassword.UserId);
                var response1 = await _userManager.RemovePasswordAsync(user);
                if (response1.Succeeded)
                {
                    string password = Encoding.UTF8.GetString(WebEncoders.Base64UrlDecode(changePassword.Password));
                    var response2 = await _userManager.AddPasswordAsync(user, password);
                    if (!response2.Succeeded)
                    {
                        return BadRequest();
                    }
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
            return Ok();
        }
        //public string GetCaseNo(int diseaseType)
        //{
        //    //CASE-1594989856-CS
        //    DateTime _date = System.DateTime.Now;
        //    int year = _date.Year;
        //    string month = _date.Month.ToString();
        //    if (month.Length < 2)
        //        month = "0" + month;

        //    string day = _date.Day.ToString();
        //    if (day.Length < 2)
        //        day = "0" + day;
        //    string disease = _context.ADiseaseType.FirstOrDefault(o => o.DiseaseId == diseaseType).Disease;
        //    //string partial_no = "";
        //    string partial_no = year + "-" + month + "-" + day;//Day + Month + Year

        //    string code = disease + "-" + "CASE-" + partial_no + "- 00";
        //    int result = 0;
        //    result = _context.CoreCases.ToList().Count();
        //    code += (result + 1);

        //    return code;
        //}
    }
}
