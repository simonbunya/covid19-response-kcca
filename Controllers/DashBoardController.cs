﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CHMIS.Models;
using Microsoft.AspNetCore.Mvc;
using Syncfusion.EJ2.Charts;

namespace CHMIS.Controllers
{
    public class DashBoardController : Controller
    {
        private CHMISContext _context;
        public DashBoardController(CHMISContext Context)
        {
            this._context = Context;
        }
        public IActionResult DashBoard()
        {
           var data = _context.CoreCases.Where(a => a.FirstName != null).ToList().Count();

           ViewBag.dataSource = data;

            List<RoundedColumnChartData> chartData = new List<RoundedColumnChartData>
            {
                  new RoundedColumnChartData { x= "Cases", y= 5, text= "Cases" },
                  new RoundedColumnChartData { x= "Contacts", y= 16, text= "Contacts" }
                  //new RoundedColumnChartData { x= "Misc", y= 198, text= "Misc" },
                  //new RoundedColumnChartData { x= "Tea", y= 189, text= "Tea" },
                  //new RoundedColumnChartData { x= "Fruits", y= 250, text= "Fruits" }
            };
            ViewBag.dataSource = chartData;
            return View();
        }

         public class RoundedColumnChartData
        {
            public string x;
            public double y;
            public string text;
        }
    }
}
