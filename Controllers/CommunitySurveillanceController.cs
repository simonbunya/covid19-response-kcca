﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using CHMIS.Models;
using System;
using Microsoft.EntityFrameworkCore;
using Syncfusion.EJ2;
using System.Collections;
using Syncfusion.EJ2.Base;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Http;
using System.Net.Http.Headers;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using CHMIS.BLL;
using System.Linq.Expressions;

namespace CHMIS.Controllers
{
    public class CommunitySurveillanceController : Controller
    {
        private CHMISContext _context;
        private IWebHostEnvironment hostingEnv;
        private IUserService userInfor;
        public CommunitySurveillanceController(CHMISContext Context, IWebHostEnvironment env, IUserService _userinfo)
        {
            this.userInfor = _userinfo;
            this._context = Context;
            this.hostingEnv = env;
        }
        // View for adding a new case
        public IActionResult CommunitySurveillance()
        {
            //Risk Level
            var risklevel = _context.ARiskLevel.AsNoTracking().OrderBy(a => a.Id).ToList();
            ViewBag.risklevel = risklevel;
            //Classification
            var classfn = _context.AClassification.AsNoTracking().OrderBy(a => a.Id).ToList();
            ViewBag.classfn = classfn;

            ViewBag.promptbutton = new
            {
                content = "Save Contact",
                isPrimary = true
            };
            ViewBag.promptbutton1 = new
            {
                content = "Cancel",
            };
            //logins
            var _logins = _context.ALoginLog.AsNoTracking().ToList();
            var logins = _logins.Where(o => Convert.ToDateTime(o.LogInDate).Date == DateTime.Now.Date).ToList().Count;
            ViewBag.LogCount = logins;
            //YesNo
            var YesNo = _context.AYesNo.AsNoTracking().ToList();
            ViewBag.YesNo = YesNo;
            //Classification
            var Classification = _context.AClassification.AsNoTracking().ToList();
            ViewBag.Classification = Classification;
            //County
            var County = _context.ACounty.AsNoTracking().ToList();
            ViewBag.County = County;
            //District
            var District = _context.ADistrict.AsNoTracking().ToList();
            ViewBag.District = District;
            //Sub-county
            var Subcounty = _context.ASubcounty.AsNoTracking().ToList();
            ViewBag.Subcounty = Subcounty;
            //Parish
            var Parish = _context.AParish.AsNoTracking().ToList();
            ViewBag.Parish = Parish;
            //Village
            var Village = _context.AVillage.AsNoTracking().ToList();
            ViewBag.Village = Village;
            ViewBag.Employee = _context.ViewEmployee.ToList();
            //Address
            var Address = _context.VwAdminUnits.AsNoTracking().ToList();
            ViewBag.Address = Address;
            //COVID 19 OUTCOMES
            var Outcomes = _context.AOutcome.AsNoTracking().ToList();
            ViewBag.Outcomes = Outcomes;
            //Disease Type
            var Disease = _context.ADiseaseType.AsNoTracking().ToList();
            ViewBag.Disease = Disease;
            //Facility Type
            var Facility = _context.AFacilityUnit.AsNoTracking().ToList();
            ViewBag.Facility = Facility;
            //Lab Results
            var LabResults = _context.ALabResults.AsNoTracking().ToList();
            ViewBag.LabResults = LabResults;
            //Lab Results
            var Risk = _context.ARiskLevel.AsNoTracking().ToList();
            ViewBag.Risk = Risk;
            //Sex
            var Sex = _context.ASex.AsNoTracking().ToList();
            ViewBag.Sex = Sex;
            //Occupation
            var Occupation = _context.AOccupation.AsNoTracking().ToList();
            ViewBag.Occupation = Occupation;
            //Symptoms
            var Symptoms = _context.ASymptoms.AsNoTracking().ToList();
            ViewBag.Symptoms = Symptoms;
            ViewBag.Employee = _context.ViewEmployee.ToList();
            return View();
        }

        //Partial View for adding a new case
        public IActionResult AddPartial([FromBody] CRUDModel<CoreCommunitySurveillance> value)
        {
            var facility = _context.AFacilityUnit.ToList();
            var m = _context.AspNetUsers.ToList();
            var fac_supervisor = m.FirstOrDefault(a => a.Id == userInfor.GetFullNameID() && a.Facility != null);
            IEnumerable members = _context.AFacilityUnit.ToList();

            if (User.IsInRole("Super Admin") || User.IsInRole("Administrator") || User.IsInRole("KCCA General") || User.IsInRole("Viewer"))
            {
                members = _context.AFacilityUnit.ToList();
            }
            else if (fac_supervisor != null)
            {
                List<AFacilityUnit> dis = new List<AFacilityUnit>();
                string[] fccode = fac_supervisor.Facility.Split(',');
                foreach (var n in fccode)
                {
                    var _facility = facility.FirstOrDefault(a => a.FacilityCode == Convert.ToInt32(n));
                    dis.Add(_facility);
                }
                members = dis;
            }


            ViewBag.Facility = members;// _context.AFacilityUnit.ToList();

            //logins
            var _logins = _context.ALoginLog.AsNoTracking().ToList();
            var logins = _logins.Where(o => Convert.ToDateTime(o.LogInDate).Date == DateTime.Now.Date).ToList().Count;
            ViewBag.LogCount = logins;
            //YesNo
            var YesNo = _context.AYesNo.AsNoTracking().ToList();
            ViewBag.YesNo = YesNo;
            //Classification
            var Classification = _context.AClassification.AsNoTracking().ToList();
            ViewBag.Classification = Classification;
            //County
            var County = _context.ACounty.AsNoTracking().ToList();
            ViewBag.County = County;
            //District
            var District = _context.ADistrict.AsNoTracking().ToList();
            ViewBag.District = District;
            //Sub-county
            var Subcounty = _context.ASubcounty.AsNoTracking().ToList();
            ViewBag.Subcounty = Subcounty;
            //Parish
            var Parish = _context.AParish.AsNoTracking().ToList();
            ViewBag.Parish = Parish;
            //Village
            var Village = _context.AVillage.AsNoTracking().ToList();
            ViewBag.Village = Village;
            //Address
            var Address = _context.VwAdminUnits.AsNoTracking().ToList();
            ViewBag.Address = Address;
            //COVID 19 OUTCOMES
            var Outcomes = _context.AOutcome.AsNoTracking().ToList();
            ViewBag.Outcomes = Outcomes;
            //Disease Type
            var Disease = _context.ADiseaseType.AsNoTracking().ToList();
            ViewBag.Disease = Disease;
            //Facility Type
            //var Facility = _context.AFacilityUnit.AsNoTracking().ToList();
            //ViewBag.Facility = Facility;
            //Lab Results
            var LabResults = _context.ALabResults.AsNoTracking().ToList();
            ViewBag.LabResults = LabResults;
            //Lab Results
            var Risk = _context.ARiskLevel.AsNoTracking().ToList();
            ViewBag.Risk = Risk;
            //Sex
            var Sex = _context.ASex.AsNoTracking().ToList();
            ViewBag.Sex = Sex;
            //Occupation
            var Occupation = _context.AOccupation.AsNoTracking().ToList();
            ViewBag.Occupation = Occupation;
            //case No.
            if (value.Value.CaseNo == null)
            {
                value.Value.CaseNo = GetCaseNo();
            }
            //Symptoms
            if (!string.IsNullOrEmpty(value.Value.Symptoms))
            {
                var Symptomsvalue = new int[] { };
                var _Symptoms = value.Value.Symptoms.Split(',');
                int count = 0;
                Symptomsvalue = new int[_Symptoms.Count()];
                foreach (var n in _Symptoms)
                {
                    Symptomsvalue[count] = Convert.ToInt32(n);
                    count++;
                }
                ViewBag.SymptomsValue = Symptomsvalue;
            }
            else
            {
                ViewBag.SymptomsValue = null;
            }
            var Symptoms = _context.ASymptoms.AsNoTracking().ToList();
            ViewBag.Symptoms = Symptoms;

            ViewBag.Employee = _context.ViewEmployee.ToList();

            return PartialView("_DialogAddPartial", value.Value);
        }
       
        public string GetCaseNo()
        {
            //CASE-1594989856-CS
            DateTime _date = System.DateTime.Now;
            int year = _date.Year;
            string month = _date.Month.ToString();
            if (month.Length < 2)
                month = "0" + month;

            string day = _date.Day.ToString();
            if (day.Length < 2)
                day = "0" + day;

            //string partial_no = "";
            string partial_no = year + "-" + month + "-" + day;//Day + Month + Year

            string _disease = string.Empty;
            var disease = _context.ADiseaseType.FirstOrDefault(o => o.DiseaseId == userInfor.getCurrentuserDisease());
            if (disease != null)
            {
                _disease = disease.Disease;
            }


            string code = _disease + "-COMMUNITY-SURV" + userInfor.GetEmployeeID() + "-" + partial_no + "- 00";

            int result = 0;
            result = _context.CoreCommunitySurveillance.ToList().Count();
            code += (result + 1);
            var x = new CoreCommunitySurveillance()
            {
                CaseNo = code,
                AddedBy = userInfor.GetFullName(),
                DateAdded = DateTime.Now
            };
            _context.CoreCommunitySurveillance.Add(x);
            _context.SaveChanges();

            return code;
        }
        public ActionResult GetCommunitySurveillance([FromBody] DataManagerRequest dm)
        {
            //To take care of facilitylist supervisors in several facility
            var facilitylist = userInfor.getUserFacilitySupervisorList();

            IEnumerable data = _context.CoreCommunitySurveillance.Where(a=> a.Disease == userInfor.getCurrentuserDisease() && a.DeleteStatus != 1).OrderByDescending(o => o.DateOfReporting).ToList();
            int count = 0;
            //var muserrole = User.IsInRole("Administrator");
            if (User.IsInRole("Super Admin") || User.IsInRole("Administrator") || User.IsInRole("KCCA General") || User.IsInRole("Viewer"))
            {
                data = _context.CoreCommunitySurveillance.Where(a => a.Disease == userInfor.getCurrentuserDisease() && a.DeleteStatus != 1).OrderByDescending(o => o.DateOfReporting).ToList();
                count = _context.CoreCommunitySurveillance.Where(a => a.Disease == userInfor.getCurrentuserDisease() && a.DeleteStatus != 1).OrderByDescending(o => o.DateOfReporting).ToList().Count();
            }
            else
            {
                //Write a Where In () like statement
                // The following 2 Statements are equivalent
                data = _context.CoreCommunitySurveillance.Where(BuildContainsExpression<CoreCommunitySurveillance, int>(e => (int)e.FacilityUnit, facilitylist)).ToList();
                count = _context.CoreCommunitySurveillance.Where(BuildContainsExpression<CoreCommunitySurveillance, int>(e => (int)e.FacilityUnit, facilitylist)).ToList().Count();
            }

            //int count = _context.CoreCommunitySurveillance.Where(a => a.Disease == userInfor.getCurrentuserDisease()).ToList().Count();
            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip != 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take != 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = count }, new Newtonsoft.Json.JsonSerializerSettings());
        }


        public static Expression<Func<TElement, bool>> BuildContainsExpression<TElement, TValue>(Expression<Func<TElement, TValue>> valueSelector, IEnumerable<TValue> values)
        {
            if (null == valueSelector) { throw new ArgumentNullException("valueSelector"); }
            if (null == values) { throw new ArgumentNullException("values"); }
            ParameterExpression p = valueSelector.Parameters.Single();
            // p => valueSelector(p) == values[0] || valueSelector(p) == ...
            if (!values.Any())
            {
                return e => false;
            }
            var equals = values.Select(value => (Expression)Expression.Equal(valueSelector.Body, Expression.Constant(value, typeof(TValue))));
            var body = equals.Aggregate<Expression>((accumulate, equal) => Expression.Or(accumulate, equal));
            return Expression.Lambda<Func<TElement, bool>>(body, p);
        }


        public ActionResult DialogUpdate([FromBody] CRUDModel<CoreCommunitySurveillance> value)
        {
            CoreCommunitySurveillance table = _context.CoreCommunitySurveillance.FirstOrDefault(o => o.CaseNo == value.Value.CaseNo);

            if (table != null)
            {
                table.ExternalCaseNo = value.Value.ExternalCaseNo;
                table.FirstName = value.Value.FirstName;
                table.MiddleName = value.Value.MiddleName;
                table.LastName = value.Value.LastName;
                table.WasContact = value.Value.WasContact;
                table.Occupation = value.Value.Occupation;
                table.OccupationSpecify = value.Value.OccupationSpecify;
                table.RiskLevel = value.Value.RiskLevel;
                table.Sex = value.Value.Sex;
                table.DoB = value.Value.DoB;
                table.Age = value.Value.Age;
                table.DateOfReporting = value.Value.DateOfReporting;
                table.RiskReason = value.Value.RiskReason;
                table.Address = value.Value.Address;
                table.DistrictId = value.Value.DistrictId;
                table.CountyId = value.Value.CountyId;
                table.SubcountyId = value.Value.SubcountyId;
                table.ParishId = value.Value.ParishId;
                table.VillageId = value.Value.VillageId;
                table.FacilityUnit = value.Value.FacilityUnit;
                table.Tel = value.Value.Tel;
                table.Email = value.Value.Email;
                table.Longitude = value.Value.Longitude;
                table.Latitude = value.Value.Latitude;
                table.Classification = value.Value.Classification;
                table.AssignFollowUpContactPerson = value.Value.AssignFollowUpContactPerson;
                table.DateEdited = DateTime.Now;
                table.EditedBy = userInfor.GetFullName();  //value.Value.CreatedBy;
                table.ClassificationSpecify = value.Value.ClassificationSpecify;
                table.DateOnset = value.Value.DateOnset;
                table.CaseDate = value.Value.CaseDate;
                table.DateOfInfection = value.Value.DateOfInfection;
                table.Outcome = value.Value.Outcome;
                table.OutcomeSpecify = value.Value.OutcomeSpecify;
                table.DateOfOutcome = value.Value.DateOfOutcome;
                table.LabTestDate = value.Value.LabTestDate;
                table.LabResults = value.Value.LabResults;
                table.Disease = userInfor.getCurrentuserDisease();// value.Value.Disease;
                table.Symptoms = value.Value.Symptoms;
                table.SymptomsOthers = value.Value.SymptomsOthers;
                table.NextAction = value.Value.NextAction;
                table.StatusFlag = value.Value.StatusFlag;
                table.GeneralComments = value.Value.GeneralComments;
                _context.Entry(table).CurrentValues.SetValues(value);
                _context.Entry(table).State = EntityState.Modified;

                try
                {
                    // Your code...
                    // Could also be before try if you know the exception occurs in SaveChanges
                    _context.SaveChanges();

                    TempData["Success"] = "Record Saved Successfully!";
                }
                catch
                {
                    //foreach (var validationErrors in dbEx.EntityValidationErrors)
                    //{
                    //    foreach (var validationError in validationErrors.ValidationErrors)
                    //    {
                    //        TempData["Success"] += string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    //        System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    //    }
                    //}
                }
            }

            return Json(value.Value);
        }

        public ActionResult DialogInsert([FromBody] CRUDModel<CoreCommunitySurveillance> value)
        {
            CoreCommunitySurveillance _table = _context.CoreCommunitySurveillance.FirstOrDefault(o => o.CaseNo == value.Value.CaseNo);
            //EmailJob obx = new EmailJob();
            if (_table == null)
            {
                CoreCommunitySurveillance table = new CoreCommunitySurveillance();
                table.CaseNo = value.Value.CaseNo;
                table.ExternalCaseNo = value.Value.ExternalCaseNo;
                table.FirstName = value.Value.FirstName;
                table.MiddleName = value.Value.MiddleName;
                table.LastName = value.Value.LastName;
                table.WasContact = value.Value.WasContact;
                table.Occupation = value.Value.Occupation;
                table.OccupationSpecify = value.Value.OccupationSpecify;
                table.RiskLevel = value.Value.RiskLevel;
                table.Sex = value.Value.Sex;
                table.DoB = value.Value.DoB;
                table.Age = value.Value.Age;
                table.DateOfReporting = value.Value.DateOfReporting;
                table.RiskReason = value.Value.RiskReason;
                table.Address = value.Value.Address;
                table.DistrictId = value.Value.DistrictId;
                table.CountyId = value.Value.CountyId;
                table.SubcountyId = value.Value.SubcountyId;
                table.ParishId = value.Value.ParishId;
                table.VillageId = value.Value.VillageId;
                table.AssignFollowUpContactPerson = value.Value.AssignFollowUpContactPerson;
                table.FacilityUnit = value.Value.FacilityUnit;
                table.Tel = value.Value.Tel;
                table.Email = value.Value.Email;
                table.Longitude = value.Value.Longitude;
                table.Latitude = value.Value.Latitude;
                table.Classification = value.Value.Classification;
                table.DateAdded = DateTime.Now;
                table.AddedBy = userInfor.GetFullName();  //value.Value.CreatedBy;
                table.ClassificationSpecify = value.Value.ClassificationSpecify;
                table.DateOnset = value.Value.DateOnset;
                table.CaseDate = value.Value.CaseDate;
                table.DateOfInfection = value.Value.DateOfInfection;
                table.Outcome = value.Value.Outcome;
                table.OutcomeSpecify = value.Value.OutcomeSpecify;
                table.DateOfOutcome = value.Value.DateOfOutcome;
                table.LabTestDate = value.Value.LabTestDate;
                table.LabResults = value.Value.LabResults;
                table.Disease = userInfor.getCurrentuserDisease();// value.Value.Disease;
                table.Symptoms = value.Value.Symptoms;
                table.SymptomsOthers = value.Value.SymptomsOthers;
                table.NextAction = value.Value.NextAction;
                table.StatusFlag = value.Value.StatusFlag;
                table.GeneralComments = value.Value.GeneralComments;
                _context.CoreCommunitySurveillance.Add(table);
                _context.SaveChanges();
            }
            else
            {
                this.DialogUpdate(value);
            }
            return Json(value.Value);
        }
        //get facility details
        public ActionResult getfacilityDetails(string code)
        {
            var hhmembers = _context.AFacilityUnit.Where(c => c.FacilityCode ==Convert.ToInt32(code)).Distinct();
            return Json(hhmembers);
        }

        public ActionResult RemoveRecord(string CaseNo)
        {
            _context.CoreCommunitySurveillance.Remove(_context.CoreCommunitySurveillance.FirstOrDefault(o => o.CaseNo == CaseNo));
            int result = _context.SaveChanges();

            return Json(result);
        }

        public ActionResult DeleteUrl([FromBody] CRUDModel<CoreCommunitySurveillance> value)
        {
            string obj = Request.Headers["additional_key"]; //key1 
            //string obj1 = Request.Headers["additional_key1"]; //key2 
            CoreCommunitySurveillance table = _context.CoreCommunitySurveillance.FirstOrDefault(or => or.CaseNo == obj);
            //_context.Remove(table);
            //_context.SaveChanges();
            if (table != null)
            {
                if (value.Deleted != null)
                {
                    foreach (var temp in value.Deleted)
                    {
                        temp.DeleteStatus = 1;
                        temp.DeletedBy = userInfor.GetFullName();
                        temp.DateDeleted = System.DateTime.Now;
                        _context.Entry(temp).State = EntityState.Modified;
                        try
                        {
                            _context.SaveChanges();
                        }
                        catch (Exception dbEx)
                        {
                            throw (dbEx);
                        }
                    }
                }
                else
                {
                    table.DeleteStatus = 1;
                    table.DeletedBy = userInfor.GetFullName();
                    table.DateDeleted = System.DateTime.Now;
                    _context.Entry(table).State = EntityState.Modified;
                    try
                    {
                        _context.SaveChanges();
                    }
                    catch (Exception dbEx)
                    {
                        throw (dbEx);
                    }
                }
            }
            //var msg = "Case No. " + obj + " Has been Deleted Successfully !!";
            return Json(value);
        }
    }
}
