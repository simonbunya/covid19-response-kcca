﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using CHMIS.BLL;
using CHMIS.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Syncfusion.EJ2.Base;

namespace CHMIS.Controllers
{
    public class MonitoringController : Controller
    {
        private IWebHostEnvironment hostingEnv;
        private CHMISContext context;
        private IUserService userInfor;

        public MonitoringController(CHMISContext Context, IWebHostEnvironment env, IUserService _userinfo)

        {
            this.hostingEnv = env;
            this.context = Context;
            this.userInfor = _userinfo;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult MonitoringTable()
        {
            ViewBag.Village = context.AVillage.ToList();
            ViewBag.gender = context.ASex.ToList();
            ViewBag.classfn = context.AClassification.ToList();
            ViewBag.risklevel = context.ARiskLevel.ToList();
            ViewBag.Employee = context.ViewEmployee.ToList();
            return View();
        }
        public IActionResult EditMonitoringTable()
        {
            ViewBag.Village = context.AVillage.ToList();
            ViewBag.gender = context.ASex.ToList();
            ViewBag.classfn = context.AClassification.ToList();
            ViewBag.risklevel = context.ARiskLevel.ToList();
            ViewBag.Employee = context.ViewEmployee.ToList();
            return View();
        }
        public ActionResult MonitoringDataSource([FromBody] DataManagerRequest dm/*,string StartDate, string EndDate, int? status*/)
        {
            var _data = context.ViewCoreCaseContactsMonitoring.AsNoTracking().OrderBy(o => o.RowNo).ToList();

            IEnumerable data = _data;
            int count = _data.Count();

            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip > 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take > 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = count }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        //Editing Datasource
        public ActionResult EditMonitoringDataSource([FromBody] DataManagerRequest dm/*,string StartDate, string EndDate, int? status*/)
        {
            var _data = context.ViewCoreCaseContactsMonitoringEdit.AsNoTracking().OrderBy(o => o.ContactCaseNo).ThenBy(o => o.Days).ToList();

            IEnumerable data = _data;
            int count = _data.Count();

            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip > 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take > 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = count }, new Newtonsoft.Json.JsonSerializerSettings());
        }


        public string GetContactID()
        {
            string code = "KCCA-" + "1595013081" + "-" + "CT";
            int result = 0;
            result = context.CoreCaseContactsMonitoring.ToList().Count();
            code += (result);
            var x = new CoreCaseContactsMonitoring()
            {
                ContactCaseNo = code
            };
            return code;
        }


        //public IActionResult AddPartial([FromBody] CRUDModel<ViewCoreCaseContactsMonitoring> value)
        public IActionResult AddPartial([FromBody] CRUDModel<CoreCaseContactsMonitoring> value)        
        {    
            //Days
            var Days = context.ADays.AsNoTracking().OrderBy(a => a.Id).ToList();
            ViewBag.Days = Days;

            //Yes/No - Vacciness Received
            var YesNo = context.AYesNo.AsNoTracking().OrderBy(a => a.Id).ToList();
            ViewBag.yesno = YesNo;

           
            return PartialView("_DialogAddPartial", value.Value);
        }

        public ActionResult EditPartial([FromBody] CRUDModel<CoreCaseContactsMonitoring> value)
        //public ActionResult EditPartial([FromBody] CRUDModel<ViewCoreCaseContactsMonitoring> value)                   
        
        {
            //Days
            var Days = context.ADays.AsNoTracking().OrderBy(a => a.Id).ToList();
            ViewBag.Days = Days;

            //Yes/No - Vacciness Received
            var YesNo = context.AYesNo.AsNoTracking().OrderBy(a => a.Id).ToList();
            ViewBag.yesno = YesNo;

            return PartialView(value.Value);
        }

        public string CheckCoreCaseMonLog()
        {
            string code = "0";
            int result = 0;
            result = context.CoreCaseContactsMonitoringLog.ToList().Count();
            code += (result + 1);
            var x = new CoreCaseContactsMonitoringLog()
            {
                CoresCaseContactMonLogId = code
            };

            return (code);
        }

        public ActionResult DialogInsert([FromBody] CRUDModel<CoreCaseContactsMonitoring> value)
        {
            CoreCaseContactsMonitoring _table = context.CoreCaseContactsMonitoring.FirstOrDefault(o =>
            ((o.ContactCaseNo == value.Value.ContactCaseNo) && (o.Days == value.Value.Days)));

            CoreCaseContactsMonitoringLog _tablelog = context.CoreCaseContactsMonitoringLog.FirstOrDefault(o =>
           (o.CoresCaseContactMonLogId == value.Value.ContactCaseNo));

            if (_table == null)
            {

                if (value.Value.MonitoringDate.Value.Hour == 0)
                {
                }
                else
                {
                    var tt = value.Value.MonitoringDate.Value.Hour;
                    var timediff = 24 - tt;
                    value.Value.MonitoringDate = value.Value.MonitoringDate.Value.AddHours(timediff);
                }

                //Save into Core_CaseContacts table

                CoreCaseContactsMonitoring table = new CoreCaseContactsMonitoring();
                CoreCaseContactsMonitoringLog tablelog = new CoreCaseContactsMonitoringLog();

                table.ContactCaseNo = value.Value.ContactCaseNo;
                table.MonitoringDate = value.Value.MonitoringDate;
                table.Days = value.Value.Days;
                table.CaseNo = value.Value.CaseNo;
                table.Temperature = value.Value.Temperature;
                table.Chills = value.Value.Chills;
                table.Cough = value.Value.Cough;
                table.SoreThroat = value.Value.SoreThroat;
                table.Breath = value.Value.Breath;
                table.BodyPains = value.Value.BodyPains;
                table.Diarrhea = value.Value.Diarrhea;
                table.GeneralComments = value.Value.GeneralComments;
                table.Disease = userInfor.getCurrentuserDisease();
                table.DateAdded = value.Value.DateAdded;
                table.AddedBy = value.Value.AddedBy;
                table.DateEdited = value.Value.DateEdited;
                table.EditedBy = value.Value.EditedBy;

                //table.DayDropDown = 1;


                if (_tablelog == null)
                {
                    tablelog.CoresCaseContactMonLogId = CheckCoreCaseMonLog();
                    tablelog.MonitoringDate = value.Value.MonitoringDate;
                    tablelog.Days = value.Value.Days;
                    tablelog.CaseNo = value.Value.CaseNo;
                    tablelog.ContactCaseNo = value.Value.ContactCaseNo;
                    tablelog.Temperature = value.Value.Temperature;
                    tablelog.Chills = value.Value.Chills;
                    tablelog.Cough = value.Value.Cough;
                    tablelog.SoreThroat = value.Value.SoreThroat;
                    tablelog.Breath = value.Value.Breath;
                    tablelog.BodyPains = value.Value.BodyPains;
                    tablelog.Diarrhea = value.Value.Diarrhea;
                    tablelog.GeneralComments = value.Value.GeneralComments;
                    tablelog.DateAdded = value.Value.DateAdded;
                    tablelog.AddedBy = value.Value.AddedBy;
                    tablelog.DateEdited = value.Value.DateEdited;
                    tablelog.EditedBy = value.Value.EditedBy;
                    tablelog.DateAdded = DateTime.Now;
                    //table.AddedBy = userInfor.GetUser().Identity.Name;

                    context.CoreCaseContactsMonitoringLog.Add(tablelog);
                    context.SaveChanges();
                }

                else if (_tablelog != null)
                {
                    tablelog.CoresCaseContactMonLogId = CheckCoreCaseMonLog();
                    tablelog.MonitoringDate = value.Value.MonitoringDate;
                    tablelog.Days = value.Value.Days;
                    tablelog.CaseNo = value.Value.CaseNo;
                    tablelog.ContactCaseNo = value.Value.ContactCaseNo;
                    tablelog.Temperature = value.Value.Temperature;
                    tablelog.Chills = value.Value.Chills;
                    tablelog.Cough = value.Value.Cough;
                    tablelog.SoreThroat = value.Value.SoreThroat;
                    tablelog.Breath = value.Value.Breath;
                    tablelog.BodyPains = value.Value.BodyPains;
                    tablelog.Diarrhea = value.Value.Diarrhea;
                    tablelog.GeneralComments = value.Value.GeneralComments;
                    tablelog.DateAdded = value.Value.DateAdded;
                    tablelog.AddedBy = value.Value.AddedBy;
                    tablelog.DateEdited = value.Value.DateEdited;
                    tablelog.EditedBy = value.Value.EditedBy;
                    tablelog.DateAdded = DateTime.Now;
                    tablelog.DateEdited = DateTime.Now;
                    //tablelog.EditedBy = userInfor.GetUser().Identity.Name;

                    context.Entry(tablelog).State = EntityState.Modified;
                }

                context.CoreCaseContactsMonitoring.Add(table);
                context.SaveChanges();

            }
            //else
            //{
            //    this.DialogUpdate(value);
            //}
            return Json(value.Value);
        }

        // Update using Table CoreCaseContacts
        public ActionResult DialogUpdate([FromBody] CRUDModel<CoreCaseContactsMonitoring> value)
        {
            CoreCaseContactsMonitoring table = context.CoreCaseContactsMonitoring.FirstOrDefault(o =>
            ((o.ContactCaseNo == value.Value.ContactCaseNo) /*&& (o.MonitoringDate == value.Value.MonitoringDate)*/ 
            && (o.Days == value.Value.Days)));

            CoreCaseContactsMonitoringLog tablelog = context.CoreCaseContactsMonitoringLog.FirstOrDefault(o =>
           (o.CoresCaseContactMonLogId == value.Value.ContactCaseNo));

            if (table != null)
            {
                //check this

                if (tablelog != null)
                {
                    if (value.Value.MonitoringDate.Value.Hour == 0)
                    {
                    }
                    else
                    {
                        var tt = value.Value.MonitoringDate.Value.Hour;
                        var timediff = 24 - tt;
                        value.Value.MonitoringDate = value.Value.MonitoringDate.Value.AddHours(timediff);
                    }

                    //CoreCaseContactsLog change = new CoreCaseContactsLog();

                    //Save updates into Core_CaseContacts
                    tablelog.CoresCaseContactMonLogId = CheckCoreCaseMonLog();
                    tablelog.MonitoringDate = value.Value.MonitoringDate;
                    tablelog.Days = value.Value.Days;
                    tablelog.CaseNo = value.Value.CaseNo;
                    tablelog.ContactCaseNo = value.Value.ContactCaseNo;
                    tablelog.Temperature = value.Value.Temperature;
                    tablelog.Chills = value.Value.Chills;
                    tablelog.Cough = value.Value.Cough;
                    tablelog.SoreThroat = value.Value.SoreThroat;
                    tablelog.Breath = value.Value.Breath;
                    tablelog.BodyPains = value.Value.BodyPains;
                    tablelog.Diarrhea = value.Value.Diarrhea;
                    tablelog.GeneralComments = value.Value.GeneralComments;
                    //tablelog.DateAdded = value.Value.DateAdded;
                    //tablelog.AddedBy = value.Value.AddedBy;
                    tablelog.DateEdited = DateTime.Now;                   
                    tablelog.EditedBy = userInfor.GetUser().Identity.Name;

                   

                    context.CoreCaseContactsMonitoringLog.Add(tablelog);
                    context.SaveChanges();
                }

                else if (tablelog == null)
                {
                    CoreCaseContactsMonitoringLog corecalog = new CoreCaseContactsMonitoringLog();

                    //Save new record into edpm_summitdecisions_log

                    corecalog.CoresCaseContactMonLogId = CheckCoreCaseMonLog();
                    corecalog.MonitoringDate = value.Value.MonitoringDate;
                    corecalog.Days = value.Value.Days;
                    corecalog.CaseNo = value.Value.CaseNo;
                    corecalog.ContactCaseNo = value.Value.ContactCaseNo;
                    corecalog.Temperature = value.Value.Temperature;
                    corecalog.Chills = value.Value.Chills;
                    corecalog.Cough = value.Value.Cough;
                    corecalog.SoreThroat = value.Value.SoreThroat;
                    corecalog.Breath = value.Value.Breath;
                    corecalog.BodyPains = value.Value.BodyPains;
                    corecalog.Diarrhea = value.Value.Diarrhea;
                    corecalog.GeneralComments = value.Value.GeneralComments;                   
                    corecalog.DateEdited = DateTime.Now;
                   
                    corecalog.EditedBy = userInfor.GetUser().Identity.Name;
                    context.CoreCaseContactsMonitoringLog.Add(corecalog);
                    context.SaveChanges();
                }


                //table.CaseNo = value.Value.CaseNo;
                table.Temperature = value.Value.Temperature;
                table.MonitoringDate = value.Value.MonitoringDate;
                table.Chills = value.Value.Chills;
                table.Cough = value.Value.Cough;
                table.SoreThroat = value.Value.SoreThroat;
                table.Breath = value.Value.Breath;
                table.BodyPains = value.Value.BodyPains;
                table.Diarrhea = value.Value.Diarrhea;
                table.GeneralComments = value.Value.GeneralComments;
                table.Disease = userInfor.getCurrentuserDisease();
                table.DateEdited = value.Value.DateEdited;
                table.EditedBy = value.Value.EditedBy;

                //context.Entry(table).State = EntityState.Modified;

                context.Entry(table).CurrentValues.SetValues(value);
                context.Entry(table).State = EntityState.Modified;

                context.SaveChanges();

                try
                {

                    TempData["Success"] = "Record Saved Successfully!";
                }

                //catch { 

                //}

                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            TempData["Success"] += string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                            System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }
                }
            }
            else
            {
                this.DialogInsert(value);
            }

            return Json(value.Value);
        }
        
        public ActionResult DeleteSchedule(string ContactCaseNo)
        {
            string result = string.Empty;

            //try
            //{
            //    _context.ScheduleAppointments.Remove(_context.ScheduleAppointments.FirstOrDefault(o => o.Id == key));
            //    _context.SaveChanges();
            //    result = "Appointment Successfully deleted";
            //}
            //catch (Exception ex)
            //{
            //    result = ex.Message;
            //}
            return Json(result);
        }

        public ActionResult ContactsDataSource([FromBody] DataManagerRequest dm/*,string StartDate, string EndDate, int? status*/)
        {

            var _data = context.CoreCaseContacts.Where(a=>a.Disease == userInfor.getCurrentuserDisease() && a.DeleteStatus != 1).AsNoTracking().OrderByDescending(o => o.ContactCaseNo).ToList();
            IEnumerable data = _data;
            int count = _data.Count(); ;

            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip > 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take > 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = count }, new Newtonsoft.Json.JsonSerializerSettings());
        }
        public ActionResult SearchDays(int? Days, string ContactCaseNo)
        {
            string _day = string.Empty;
            var _data = context.CoreCaseContactsMonitoring.ToList();
            var data = _data.FirstOrDefault(e => e.ContactCaseNo == ContactCaseNo && e.Days == Days);
            if (data != null)
            {
                _day = data.Days.ToString();
            }
            return Json(_day, new Newtonsoft.Json.JsonSerializerSettings());
        }

        public ActionResult DeleteChildUrl([FromBody] CRUDModel<CoreCaseContactsMonitoring> value)
        {
            string obj = Request.Headers["additional_key"]; //key1 
            string obj1 = Request.Headers["additional_key1"]; //key2 
            CoreCaseContactsMonitoring table = context.CoreCaseContactsMonitoring.FirstOrDefault(or => or.ContactCaseNo + or.Days == obj + obj1);
            if (table != null)
            {
                if (value.Deleted != null)
                {
                    foreach (var temp in value.Deleted)
                    {
                        context.Remove(temp);
                        try
                        {
                            context.SaveChanges();
                        }
                        catch (Exception dbEx)
                        {
                            throw (dbEx);
                        }
                    }
                }
                else
                {
                    context.Remove(table);
                    //table.DeleteStatus = 1;
                    //table.DeletedBy = userInfor.GetFullName();
                    //table.DateDeleted = System.DateTime.Now;
                    //_context.Entry(table).State = EntityState.Modified;
                    try
                    {
                        context.SaveChanges();
                    }
                    catch (Exception dbEx)
                    {
                        throw (dbEx);
                    }
                }

                
            }
            //var msg = "Contact Case No. " + obj + " Has been Deleted Successfully !!";
            return Json(value);

        }
    }
}