﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using CHMIS.BLL;
using CHMIS.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Syncfusion.EJ2.Base;

namespace CHMIS.Controllers
{
    public class ContactController : Controller
    {
        private IWebHostEnvironment hostingEnv;
        private CHMISContext context;
        private IUserService userInfor;
        
        public ContactController(CHMISContext Context, IWebHostEnvironment env, IUserService _userinfo)
       
        {
            this.hostingEnv = env;
            this.context = Context;
            this.userInfor = _userinfo;
        }

        public IActionResult Index()
        {
            return View();
        }

        # region RegisterContact
        public IActionResult RegisterContact()
        {
            //Risk Level
            var risklevel = context.ARiskLevel.AsNoTracking().OrderBy(a => a.Id).ToList();
            ViewBag.risklevel = risklevel;

            //Gender
            var gender = context.ASex.AsNoTracking().OrderBy(a => a.Id).ToList();
            ViewBag.gender = gender;

            //Classification
            var classfn = context.AClassification.AsNoTracking().OrderBy(a => a.Id).ToList();
            ViewBag.classfn = classfn;
            //Address
            var Address = context.VwAdminUnits.AsNoTracking().ToList();
            ViewBag.Address = Address;
            //Village
            var Village = context.AVillage.AsNoTracking().ToList();
            ViewBag.Village = Village;
            ViewBag.Employee = context.ViewEmployee.ToList();
            ViewBag.EmployeeContacts = context.ViewEmployee.Where(x => x.Title == 2).ToList();
            ViewBag.Tests = context.ALabResults.ToList();
            ViewBag.Elevate = context.AElevateContact.Where(x => x.Id == 1).ToList();
            //
            ViewBag.promptbutton = new
            {
                content = "Assign Selected Records",
                isPrimary = true
            };
            ViewBag.promptbutton1 = new
            {
                content = "Cancel",
            };
            return View();
        }

        // //Stored Procedure DataSource
        //public ActionResult ContactsDataSource([FromBody] DataManagerRequest dm/*,string StartDate, string EndDate, int? status*/)
        //{
        //    ContactsModel obj = new ContactsModel(context);

        //    IEnumerable data = obj.GetAll(null).ToList().OrderByDescending(o => o.Contact_CaseNo).ToList();
        //    int count = obj.GetAll(null).ToList().OrderByDescending(o => o.Contact_CaseNo).ToList().Count();;

        //    DataOperations operation = new DataOperations();
        //    //Performing filtering operation
        //    if (dm.Where != null)
        //    {
        //        data = operation.PerformFiltering(data, dm.Where, "and");
        //        var filtered = (IEnumerable<object>)data;
        //        count = filtered.Count();
        //    }
        //    //Performing search operation
        //    if (dm.Search != null)
        //    {
        //        data = operation.PerformSearching(data, dm.Search);
        //        var searched = (IEnumerable<object>)data;
        //        count = searched.Count();
        //    }
        //    //Performing sorting operation
        //    if (dm.Sorted != null)
        //        data = operation.PerformSorting(data, dm.Sorted);

        //    //Performing paging operations
        //    if (dm.Skip != 0)
        //        data = operation.PerformSkip(data, dm.Skip);
        //    if (dm.Take != 0)
        //        data = operation.PerformTake(data, dm.Take);

        //    return Json(new { result = data, count = count }, new Newtonsoft.Json.JsonSerializerSettings());
        //}

        public ActionResult ContactsDataSource([FromBody] DataManagerRequest dm/*,string StartDate, string EndDate, int? status*/)
        {

            var _data = context.CoreCaseContacts.Where(a => /*a.FirstName != null &&*/ a.DeleteStatus != 1 && a.Disease == userInfor.getCurrentuserDisease()).OrderByDescending(o => o.ContactCaseNo).ToList();
            IEnumerable data = _data;
            int count = context.CoreCaseContacts.Where(a => /*a.FirstName != null &&*/ a.DeleteStatus != 1 && a.Disease == userInfor.getCurrentuserDisease()).OrderByDescending(o => o.ContactCaseNo).ToList().Count(); ;

            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip > 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take > 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = count }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        public IActionResult AddPartial([FromBody] CRUDModel<CoreCaseContacts> value)
        { //logins
            var _logins = context.ALoginLog.AsNoTracking().ToList();
            var logins = _logins.Where(o => Convert.ToDateTime(o.LogInDate).Date == DateTime.Now.Date).ToList().Count;
            ViewBag.LogCount = logins;
            //YesNo
            var YesNo = context.AYesNo.AsNoTracking().ToList();
            ViewBag.YesNo = YesNo;
            //Classification
            var Classification = context.AClassification.AsNoTracking().ToList();
            ViewBag.Classification = Classification;
            //County
            var County = context.ACounty.AsNoTracking().ToList();
            ViewBag.County = County;
            //District
            var District = context.ADistrict.AsNoTracking().ToList();
            ViewBag.District = District;
            //Sub-county
            var Subcounty = context.ASubcounty.AsNoTracking().ToList();
            ViewBag.Subcounty = Subcounty;
            //Parish
            var Parish = context.AParish.AsNoTracking().ToList();
            ViewBag.Parish = Parish;
            //Village
            var Village = context.AVillage.AsNoTracking().ToList();
            ViewBag.Village = Village;
            //Address
            var Address = context.VwAdminUnits.AsNoTracking().ToList();
            ViewBag.Address = Address;
            //COVID 19 OUTCOMES
            var Outcomes = context.AOutcome.AsNoTracking().ToList();
            ViewBag.Outcomes = Outcomes;
            //Disease Type
            var Disease = context.ADiseaseType.AsNoTracking().ToList();
            ViewBag.Disease = Disease;
            //Facility Type
            var Facility = context.AFacilityUnit.AsNoTracking().ToList();
            ViewBag.Facility = Facility;
            //Lab Results
            var LabResults = context.ALabResults.AsNoTracking().ToList();
            ViewBag.LabResults = LabResults;
            //Lab Results
            var Risk = context.ARiskLevel.AsNoTracking().ToList();
            ViewBag.Risk = Risk;
            //Sex
            var Sex = context.ASex.AsNoTracking().ToList();
            ViewBag.Sex = Sex;
            //Occupation
            var Occupation = context.AOccupation.AsNoTracking().ToList();
            ViewBag.Occupation = Occupation;
            //Symptoms
            var Relationship = context.ARelationship.AsNoTracking().ToList();
            ViewBag.Relationship = Relationship;
            ViewBag.Employee = context.ViewEmployee.ToList();

            if (!string.IsNullOrEmpty(value.Value.Symptoms))
            {
                var Symptoms = new int[] { };
                var _Symptoms = value.Value.Symptoms.Split(',');
                int count = 0;
                Symptoms = new int[_Symptoms.Count()];
                foreach (var n in _Symptoms)
                {
                    Symptoms[count] = Convert.ToInt32(n);
                    count++;
                }
                ViewBag.Symptoms = Symptoms;
            }
            else
            {
                var Symptoms = context.ASymptoms.AsNoTracking().ToList();
                ViewBag.Symptoms = Symptoms;
            }

            //ExposureType
            var ExposureType = context.AExposureType.AsNoTracking().ToList();
            ViewBag.ExposureType = ExposureType;
            ViewBag.Elevate = context.AElevateContact.Where(x => x.Id == 1).ToList();
            return PartialView("_DialogAddPartial", value.Value);
        }

        public ActionResult EditPartial([FromBody] CRUDModel<CoreCaseContacts> value)
        {
            //Risk Level
            var risklevel = context.ARiskLevel.AsNoTracking().OrderBy(a => a.Id).ToList();
            ViewBag.risklevel = risklevel;

            //Gender
            var gender = context.ASex.AsNoTracking().OrderBy(a => a.Id).ToList();
            ViewBag.gender = gender;

            //Classification
            var classfn = context.AClassification.AsNoTracking().OrderBy(a => a.Id).ToList();
            ViewBag.classfn = classfn;

            //Yes/No - Vacciness Received
            var YesNo = context.AYesNo.AsNoTracking().OrderBy(a => a.Id).ToList();
            ViewBag.yesno = YesNo;

            //Occupation
            var Occupation = context.AOccupation.AsNoTracking().OrderBy(a => a.Occupation).ToList();
            ViewBag.occupation = Occupation;

            //District
            var District = context.ADistrict.AsNoTracking().OrderBy(a => a.DistrictName).ToList();
            ViewBag.District = District;

            //County
            var County = context.ACounty.AsNoTracking().OrderBy(a => a.CountyName).ToList();
            ViewBag.County = County;

            //Country
            var Country = context.ACountry.AsNoTracking().OrderBy(a => a.CountryName).ToList();
            ViewBag.Country = Country;

            //SubCounty
            var SubCounty = context.ASubcounty.AsNoTracking().OrderBy(a => a.SubcountyName).ToList();
            ViewBag.SubCounty = SubCounty;

            //Parish
            var Parish = context.AParish.AsNoTracking().OrderBy(a => a.ParishName).ToList();
            ViewBag.Parish = Parish;

            //Village
            var Village = context.AVillage.AsNoTracking().OrderBy(a => a.VillageName).ToList();
            ViewBag.Village = Village;

            //Classification
            var Classification = context.AClassification.AsNoTracking().OrderBy(a => a.Classification).ToList();
            ViewBag.Classification = Classification;

            //OutCome
            var Outcome = context.AOutcome.AsNoTracking().OrderBy(a => a.Outcome).ToList();
            ViewBag.Outcome = Outcome;
            //Sex
            var Sex = context.ASex.AsNoTracking().ToList();
            ViewBag.Sex = Sex;

            //Lab Results
            var LabResults = context.ALabResults.AsNoTracking().ToList();
            ViewBag.LabResults = LabResults;

            //ExposureType
            var ExposureType = context.AExposureType.AsNoTracking().ToList();
            ViewBag.ExposureType = ExposureType;

            // value.Value.ContactCaseNo = GetContactIDD();

            //value.Value.CaseNo = value.Value.CaseNo;
            ViewBag.Elevate = context.AElevateContact.Where(x => x.Id == 1).ToList();

            return PartialView(value.Value);
        }

        //////Auto Increment Contact ID on Add

        //public string GetContactID()
        //{
        //    string code = "KCCA-" + "1595013081" + "-" + "CT";
        //    int result = 0;
        //    result = context.CoreCaseContacts.ToList().Count();
        //    code += (result + 1);
        //    var x = new CoreCaseContacts()
        //    {
        //        CaseNo = code
        //    };
        //    return code;
        //}


        ////Auto Increment Contact ID on Edit

        //public string GetContactIDD()
        //{
        //    string code = "KCCA-" + "1595013081" + "-" + "CT";
        //    int result = 0;
        //    result = context.CoreCaseContacts.ToList().Count();
        //    //code += (result + 1);
        //    code += (result);
        //    var x = new CoreCaseContacts()
        //    {
        //        CaseNo = code
        //    };

        //    return code;
        //}

        public string GetContactNo(string CaseNo)
        {
            //CASE-1594989856-CS
            DateTime _date = System.DateTime.Now;
            int year = _date.Year;
            string month = _date.Month.ToString();
            if (month.Length < 2)
                month = "0" + month;

            string day = _date.Day.ToString();
            if (day.Length < 2)
                day = "0" + day;

            //string partial_no = "";
            string partial_no = year + "-" + month + "-" + day;//Day + Month + Year

            string code = "COVID-19-" + "CASE-CONTACT" + partial_no + "-";
            int result = 0;
            result = context.CoreCaseContacts.ToList().Count();
            code += (result + 1);
            var x = new CoreCaseContacts()
            {
                CaseNo = CaseNo,
                ContactCaseNo = code,
                AddedBy = userInfor.GetFullName(),
                DateAdded = DateTime.Now
            };
            context.CoreCaseContacts.Add(x);
            context.SaveChanges();

            return code;
        }
        //Auto Increment Contact ID on Add

        public string GetContactID()
        {
            string code = "KCCA-" + "1595013081" + "-" + "CT";
            int result = 0;
            result = context.CoreCaseContacts.ToList().Count();
            code += (result + 1);
            var x = new CoreCaseContacts()
            {
                ContactCaseNo = code
            };
            return code;
        }


        //Auto Increment Contact ID on Edit

        public string GetContactIDD()
        {
            string code = "KCCA-" + "1595013081" + "-" + "CT";
            int result = 0;
            result = context.CoreCaseContacts.ToList().Count();
            //code += (result + 1);
            code += (result);
            var x = new CoreCaseContacts()
            {
                ContactCaseNo = code
            };

            return code;
        }

        [AcceptVerbs("Post")]
        public IActionResult Save(IList<IFormFile> UploadFiles, string CaseNo)
        {
            try
            {
                foreach (var file in UploadFiles)
                {
                    if (UploadFiles != null)
                    {
                        var filename = Path.GetFileName(file.FileName); ///ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                       // filename = hostingEnv.WebRootPath + $@"\{filename}";
                        var fileSavePath = Path.Combine(hostingEnv.WebRootPath + "\\UploadedFiles\\", filename);
                        if (!System.IO.File.Exists(fileSavePath))
                        {
                            byte[] fileBytes;
                            using (MemoryStream ms = new MemoryStream())
                            {
                                file.OpenReadStream().CopyTo(ms);
                                fileBytes = ms.ToArray();
                            }

                            string ext = Path.GetExtension(filename);
                            CoreContactsFileAttachment doc = new CoreContactsFileAttachment()
                            {
                                CaseNo = CaseNo,
                                FileType = ext,
                                FileUrl = fileBytes,
                                Details = filename,
                                Id = 1,
                                AddedBy = userInfor.GetFullName(),
                                DateAdded = System.DateTime.Now
                            };
                            var evidence = context.CoreContactsFileAttachment.OrderByDescending(o => o.Id).FirstOrDefault();
                            if (evidence != null)
                            {
                                doc.Id = (evidence.Id + 1);
                            }
                            context.CoreContactsFileAttachment.Add(doc);
                            context.SaveChanges();
                        }
                        else
                        {
                            Response.Clear();
                            Response.StatusCode = 204;
                            Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = "File already exists.";
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Response.Clear();
                Response.ContentType = "application/json; charset=utf-8";
                Response.StatusCode = 204;
                Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = "No Content";
                Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = e.Message;
            }
            return Content("");
        }

        [AcceptVerbs("Post")]
        public IActionResult Remove(IList<IFormFile> UploadFiles, string CaseNo)
        {
            try
            {
                foreach (var file in UploadFiles)
                {
                    var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    var filePath = Path.Combine(hostingEnv.WebRootPath + "\\UploadedFiles");
                    var fileSavePath = filePath + "\\" + fileName;
                    if (!System.IO.File.Exists(fileSavePath))
                    {
                        System.IO.File.Delete(fileSavePath);
                    }
                    var exists = context.CoreContactsFileAttachment.FirstOrDefault(o => o.Details == fileName && o.CaseNo == CaseNo);
                    if (exists != null)
                    {
                        context.CoreContactsFileAttachment.Remove(exists);
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                Response.Clear();
                Response.StatusCode = 200;
                Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = "File removed successfully";
                Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = e.Message;
            }
            return Content("");
        }
        public ActionResult GetSupportingDocuments([FromBody] DataManagerRequest dm, string CaseNo)
        {
            IEnumerable data = context.CoreContactsFileAttachment.Where(o => o.CaseNo == CaseNo).Select(o => new { o.Id, o.Details, o.FileType, o.CaseNo }).ToList();
            int count = context.CoreContactsFileAttachment.Where(o => o.CaseNo == CaseNo).ToList().Count;

            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip != 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take != 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = count });
        }
        public FileResult DownloadCoreCaseContactFile(int? DocumentCode)
        {
            byte[] bytes = null;
            string fileName = string.Empty, contentType = string.Empty;
            var file = context.CoreContactsFileAttachment.FirstOrDefault(o => o.Id == DocumentCode);
            if (file != null)
            {
                bytes = file.FileUrl;
                fileName = file.Details;
                contentType = "application/" + file.FileType.Replace('.', ' ').Trim();
            }

            return File(bytes, contentType, fileName);
        }

        //Save Photoz
        [AcceptVerbs("Post")]
        public IActionResult SavePhoto(IList<IFormFile> UploadPhoto, string CaseNo)
        {
            try
            {
                foreach (var file in UploadPhoto)
                {
                    if (UploadPhoto != null)
                    {
                        var filename = Path.GetFileName(file.FileName); ///ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                       // filename = hostingEnv.WebRootPath + $@"\{filename}";
                        var fileSavePath = Path.Combine(hostingEnv.WebRootPath + "\\UploadedFiles\\", filename);
                        if (!System.IO.File.Exists(fileSavePath))
                        {
                            byte[] fileBytes;
                            using (MemoryStream ms = new MemoryStream())
                            {
                                file.OpenReadStream().CopyTo(ms);
                                fileBytes = ms.ToArray();
                            }

                            string ext = Path.GetExtension(filename);
                            CoreContactCaseNoPhoto doc = new CoreContactCaseNoPhoto()
                            {
                                ContactCaseNo = CaseNo,
                                FileType = ext,
                                Photo = fileBytes

                            };
                            var evidence = context.CoreContactCaseNoPhoto.FirstOrDefault();
                            context.CoreContactCaseNoPhoto.Add(doc);
                            context.SaveChanges();
                        }
                        else
                        {
                            Response.Clear();
                            Response.StatusCode = 204;
                            Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = "File already exists.";
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Response.Clear();
                Response.ContentType = "application/json; charset=utf-8";
                Response.StatusCode = 204;
                Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = "No Content";
                Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = e.Message;
            }
            return Content("");
        }
        [AcceptVerbs("Post")]
        public IActionResult RemovePhoto(IList<IFormFile> UploadPhoto, string CaseNo)
        {
            try
            {
                foreach (var file in UploadPhoto)
                {
                    var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    var filePath = Path.Combine(hostingEnv.WebRootPath + "\\UploadedFiles");
                    var fileSavePath = filePath + "\\" + fileName;
                    if (!System.IO.File.Exists(fileSavePath))
                    {
                        System.IO.File.Delete(fileSavePath);
                    }
                    var exists = context.CoreContactCaseNoPhoto.FirstOrDefault(o => o.ContactCaseNo == CaseNo);
                    if (exists != null)
                    {
                        context.CoreContactCaseNoPhoto.Remove(exists);
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                Response.Clear();
                Response.StatusCode = 200;
                Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = "File removed successfully";
                Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = e.Message;
            }
            return Content("");
        }
        public ActionResult GetPhotoz([FromBody] DataManagerRequest dm, string CaseNo)
        {
            IEnumerable data = context.CoreContactCaseNoPhoto.Where(o => o.ContactCaseNo == CaseNo).Select(o => new { o.Photo, o.FileType, o.ContactCaseNo }).ToList();
            int count = context.CoreContactCaseNoPhoto.Where(o => o.ContactCaseNo == CaseNo).ToList().Count;

            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip != 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take != 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = count });
        }
        public FileResult DownloadCasePhotoFile(string DocumentCode)
        {
            byte[] bytes = null;
            string fileName = string.Empty, contentType = string.Empty;
            var file = context.CoreCasePhoto.FirstOrDefault(o => o.CaseNo == DocumentCode);
            if (file != null)
            {
                bytes = file.Photo;
                contentType = "application/" + file.FileType.Replace('.', ' ').Trim();
            }

            return File(bytes, contentType, fileName);
        }


        public string CheckCoreCaseConLog()
        {
            string code = "0";
            int result = 0;
            result = context.CoreCaseContactsLog.ToList().Count();
            code += (result + 1);
            var x = new CoreCaseContactsLog()
            {
                ContactCaseLogId = code
            };

            return (code);
        }

        //public string CheckCoreCaseConLog()
        //{
        //    var corecasconlog = context.CoreCaseContactsLog.OrderByDescending(o => o.ContactCaseLogId).Take(1).Select(f => f.ContactCaseLogId).FirstOrDefault();
        //    return (corecasconlog);
        //}

        //Save using Table CoreCaseContacts

        public ActionResult DialogInsert([FromBody] CRUDModel<CoreCaseContacts> value)
        {

            CoreCaseContacts _table = context.CoreCaseContacts.FirstOrDefault(o =>
            ((o.ContactCaseNo == value.Value.ContactCaseNo)&& (o.CaseNo == value.Value.CaseNo)));

            CoreCaseContactsLog _tablelog = context.CoreCaseContactsLog.FirstOrDefault(o =>
           (o.ContactCaseLogId == value.Value.ContactCaseNo));

            if (_table == null)
            {
                //Save into Core_CaseContacts table

                CoreCaseContacts table = new CoreCaseContacts();
                CoreCaseContactsLog tablelog = new CoreCaseContactsLog();

                table.ContactCaseNo = value.Value.ContactCaseNo;
                table.CaseNo = value.Value.CaseNo;
                table.CaseNo = value.Value.CaseNo;
                table.FirstName = value.Value.FirstName;
                table.MiddleName = value.Value.MiddleName;
                table.LastName = value.Value.LastName;
                table.Occupation = value.Value.Occupation;
                table.OccupationSpecify = value.Value.OccupationSpecify;
                table.VaccinesReceived = value.Value.VaccinesReceived;
                table.Vaccines = value.Value.Vaccines;
                table.Sex = value.Value.Sex;
                table.DoB = value.Value.DoB;
                table.Age = value.Value.Age;
                table.DateOfReporting = value.Value.DateOfReporting;
                table.RiskLevel = value.Value.RiskLevel;
                table.RiskReason = value.Value.RiskReason;
                table.Address = value.Value.Address;
                table.DistrictId = value.Value.DistrictId;
                table.CountyId = value.Value.CountyId;
                table.SubcountyId = value.Value.SubcountyId;
                table.ParishId = value.Value.ParishId;
                table.VillageId = value.Value.VillageId;
                table.FacilityUnit = value.Value.FacilityUnit;
                table.Tel = value.Value.Tel;
                table.Email = value.Value.Email;
                table.Longitude = value.Value.Longitude;
                table.Latitude = value.Value.Latitude;
                table.Classification = value.Value.Classification;
                table.ClassificationSpecify = value.Value.ClassificationSpecify;
                table.DateOnset = value.Value.DateOnset;
                table.CaseDate = value.Value.CaseDate;
                table.DateOfInfection = value.Value.DateOfInfection;
                table.Outcome = value.Value.Outcome;
                table.OutcomeSpecify = value.Value.OutcomeSpecify;
                table.DateOfOutcome = value.Value.DateOfOutcome;
                table.LabTestDate = value.Value.LabTestDate;
                table.LabResults = value.Value.LabResults;
                table.DateOfFirstContact = value.Value.DateOfFirstContact;
                table.DateOfLastContact = value.Value.DateOfLastContact;
                table.IsContactDateEstimated = value.Value.IsContactDateEstimated;
                table.CertaintyLevel = value.Value.CertaintyLevel;
                table.Disease = userInfor.getCurrentuserDisease();// value.Value.Disease;
                table.Symptoms = value.Value.Symptoms;
                table.SymptomsOthers = value.Value.SymptomsOthers;
                table.ExposureType = value.Value.ExposureType;
                table.ExposureTypeSpecify = value.Value.ExposureTypeSpecify;
                table.Relationship = value.Value.Relationship;
                table.RelationshipSpecify = value.Value.RelationshipSpecify;
                table.NextAction = value.Value.NextAction;
                table.StatusFlag = value.Value.StatusFlag;
                table.AssignFollowUpContactPerson = value.Value.AssignFollowUpContactPerson;
                table.GeneralComments = value.Value.GeneralComments;
                table.StatusFlag = value.Value.StatusFlag;
                table.LostFollowUp = value.Value.LostFollowUp;
                table.EditedBy = userInfor.GetFullName();
                table.DateEdited = DateTime.Now;
                ////Save into Core_CaseContactsLog table
                //if (string.IsNullOrEmpty(tablelog.ContactCaseLogId.ToString()) || tablelog.ContactCaseLogId == 0)
                //{
                //    var newCode = CheckCoreCaseConLog();
                //    tablelog.ContactCaseLogId = ++newCode;
                //}

                if (_tablelog == null)
                {
                    tablelog.ContactCaseLogId = CheckCoreCaseConLog();
                    tablelog.ContactCaseNo = value.Value.ContactCaseNo;
                    tablelog.CaseNo = value.Value.CaseNo;
                    tablelog.FirstName = value.Value.FirstName;
                    tablelog.MiddleName = value.Value.MiddleName;
                    tablelog.LastName = value.Value.LastName;
                    tablelog.Occupation = value.Value.Occupation;
                    tablelog.OccupationSpecify = value.Value.OccupationSpecify;
                    tablelog.VaccinesReceived = value.Value.VaccinesReceived;
                    tablelog.Vaccines = value.Value.Vaccines;
                    tablelog.Sex = value.Value.Sex;
                    tablelog.DoB = value.Value.DoB;
                    tablelog.Age = value.Value.Age;
                    tablelog.DateOfReporting = value.Value.DateOfReporting;
                    tablelog.RiskLevel = value.Value.RiskLevel;
                    tablelog.RiskReason = value.Value.RiskReason;
                    tablelog.Address = value.Value.Address;
                    tablelog.DistrictId = value.Value.DistrictId;
                    tablelog.CountyId = value.Value.CountyId;
                    tablelog.SubcountyId = value.Value.SubcountyId;
                    tablelog.ParishId = value.Value.ParishId;
                    tablelog.VillageId = value.Value.VillageId;
                    tablelog.FacilityUnit = value.Value.FacilityUnit;
                    tablelog.Tel = value.Value.Tel;
                    tablelog.Email = value.Value.Email;
                    tablelog.Longitude = value.Value.Longitude;
                    tablelog.Latitude = value.Value.Latitude;
                    tablelog.Classification = value.Value.Classification;
                    tablelog.ClassificationSpecify = value.Value.ClassificationSpecify;
                    tablelog.DateOnset = value.Value.DateOnset;
                    tablelog.CaseDate = value.Value.CaseDate;
                    tablelog.DateOfInfection = value.Value.DateOfInfection;
                    tablelog.Outcome = value.Value.Outcome;
                    tablelog.OutcomeSpecify = value.Value.OutcomeSpecify;
                    tablelog.DateOfOutcome = value.Value.DateOfOutcome;
                    tablelog.LabTestDate = value.Value.LabTestDate;
                    tablelog.LabResults = value.Value.LabResults;
                    tablelog.DateOfFirstContact = value.Value.DateOfFirstContact;
                    tablelog.DateOfLastContact = value.Value.DateOfLastContact;
                    tablelog.IsContactDateEstimated = value.Value.IsContactDateEstimated;
                    tablelog.CertaintyLevel = value.Value.CertaintyLevel;
                    tablelog.Disease = userInfor.getCurrentuserDisease();// value.Value.Disease;
                    tablelog.Symptoms = value.Value.Symptoms;
                    tablelog.SymptomsOthers = value.Value.SymptomsOthers;
                    tablelog.ExposureType = value.Value.ExposureType;
                    tablelog.ExposureTypeSpecify = value.Value.ExposureTypeSpecify;
                    tablelog.Relationship = value.Value.Relationship;
                    tablelog.RelationshipSpecify = value.Value.RelationshipSpecify;
                    tablelog.NextAction = value.Value.NextAction;
                    tablelog.StatusFlag = value.Value.StatusFlag;
                    tablelog.AssignFollowUpContactPerson = value.Value.AssignFollowUpContactPerson;
                    tablelog.GeneralComments = value.Value.GeneralComments;
                    tablelog.DateAdded = DateTime.Now;
                    //table.AddedBy = userInfor.GetUser().Identity.Name;

                    context.CoreCaseContactsLog.Add(tablelog);
                    context.SaveChanges();
                }

                else if (_tablelog != null)
                {
                    tablelog.ContactCaseLogId = CheckCoreCaseConLog();
                    tablelog.ContactCaseNo = value.Value.ContactCaseNo;
                    tablelog.CaseNo = value.Value.CaseNo;
                    tablelog.FirstName = value.Value.FirstName;
                    tablelog.MiddleName = value.Value.MiddleName;
                    tablelog.LastName = value.Value.LastName;
                    tablelog.Occupation = value.Value.Occupation;
                    tablelog.OccupationSpecify = value.Value.OccupationSpecify;
                    tablelog.VaccinesReceived = value.Value.VaccinesReceived;
                    tablelog.Vaccines = value.Value.Vaccines;
                    tablelog.Sex = value.Value.Sex;
                    tablelog.DoB = value.Value.DoB;
                    tablelog.Age = value.Value.Age;
                    tablelog.DateOfReporting = value.Value.DateOfReporting;
                    tablelog.RiskLevel = value.Value.RiskLevel;
                    tablelog.RiskReason = value.Value.RiskReason;
                    tablelog.Address = value.Value.Address;
                    tablelog.DistrictId = value.Value.DistrictId;
                    tablelog.CountyId = value.Value.CountyId;
                    tablelog.SubcountyId = value.Value.SubcountyId;
                    tablelog.ParishId = value.Value.ParishId;
                    tablelog.VillageId = value.Value.VillageId;
                    tablelog.FacilityUnit = value.Value.FacilityUnit;
                    tablelog.Tel = value.Value.Tel;
                    tablelog.Email = value.Value.Email;
                    tablelog.Longitude = value.Value.Longitude;
                    tablelog.Latitude = value.Value.Latitude;
                    tablelog.Classification = value.Value.Classification;
                    tablelog.ClassificationSpecify = value.Value.ClassificationSpecify;
                    tablelog.DateOnset = value.Value.DateOnset;
                    tablelog.CaseDate = value.Value.CaseDate;
                    tablelog.DateOfInfection = value.Value.DateOfInfection;
                    tablelog.Outcome = value.Value.Outcome;
                    tablelog.OutcomeSpecify = value.Value.OutcomeSpecify;
                    tablelog.DateOfOutcome = value.Value.DateOfOutcome;
                    tablelog.LabTestDate = value.Value.LabTestDate;
                    tablelog.LabResults = value.Value.LabResults;
                    tablelog.DateOfFirstContact = value.Value.DateOfFirstContact;
                    tablelog.DateOfLastContact = value.Value.DateOfLastContact;
                    tablelog.IsContactDateEstimated = value.Value.IsContactDateEstimated;
                    tablelog.CertaintyLevel = value.Value.CertaintyLevel;
                    tablelog.Disease = userInfor.getCurrentuserDisease();// value.Value.Disease;
                    tablelog.Symptoms = value.Value.Symptoms;
                    tablelog.SymptomsOthers = value.Value.SymptomsOthers;
                    tablelog.ExposureType = value.Value.ExposureType;
                    tablelog.ExposureTypeSpecify = value.Value.ExposureTypeSpecify;
                    tablelog.Relationship = value.Value.Relationship;
                    tablelog.RelationshipSpecify = value.Value.RelationshipSpecify;
                    tablelog.NextAction = value.Value.NextAction;
                    tablelog.StatusFlag = value.Value.StatusFlag;
                    tablelog.AssignFollowUpContactPerson = value.Value.AssignFollowUpContactPerson;
                    tablelog.GeneralComments = value.Value.GeneralComments;
                    tablelog.DateEdited = DateTime.Now;
                    //tablelog.EditedBy = userInfor.GetUser().Identity.Name;

                    context.Entry(tablelog).State = EntityState.Modified;
                }

                context.CoreCaseContacts.Add(table);
                context.SaveChanges();

            }
            else
            {
                this.DialogUpdate(value);
            }
            return Json(value.Value);
        }

        // Update using Table CoreCaseContacts
        public ActionResult DialogUpdate([FromBody] CRUDModel<CoreCaseContacts> value)
        {
            CoreCaseContacts table = context.CoreCaseContacts.FirstOrDefault(o =>
            (o.ContactCaseNo == value.Value.ContactCaseNo)&&(o.CaseNo == value.Value.CaseNo));

            CoreCaseContactsLog tablelog = context.CoreCaseContactsLog.FirstOrDefault(o =>
            (o.ContactCaseLogId == value.Value.ContactCaseNo));

            if (table != null)
            {
                //check this

                // var checker = context.CoreCaseContactsLog.FirstOrDefault(o => o.ContactCaseNo == value.Value.Contact_CaseNo);

                if (tablelog != null)
                {
                    //CoreCaseContactsLog change = new CoreCaseContactsLog();

                    //Save updates into Core_CaseContacts

                    tablelog.ContactCaseNo = CheckCoreCaseConLog();
                    tablelog.CaseNo = value.Value.CaseNo;
                    tablelog.FirstName = value.Value.FirstName;
                    tablelog.MiddleName = value.Value.MiddleName;
                    tablelog.LastName = value.Value.LastName;
                    tablelog.Occupation = value.Value.Occupation;
                    tablelog.OccupationSpecify = value.Value.OccupationSpecify;
                    tablelog.VaccinesReceived = value.Value.VaccinesReceived;
                    tablelog.Vaccines = value.Value.Vaccines;
                    tablelog.Sex = value.Value.Sex;
                    tablelog.DoB = value.Value.DoB;
                    tablelog.Age = value.Value.Age;
                    tablelog.DateOfReporting = value.Value.DateOfReporting;
                    tablelog.RiskLevel = value.Value.RiskLevel;
                    tablelog.RiskReason = value.Value.RiskReason;
                    tablelog.Address = value.Value.Address;
                    tablelog.DistrictId = value.Value.DistrictId;
                    tablelog.CountyId = value.Value.CountyId;
                    tablelog.SubcountyId = value.Value.SubcountyId;
                    tablelog.ParishId = value.Value.ParishId;
                    tablelog.VillageId = value.Value.VillageId;
                    tablelog.FacilityUnit = value.Value.FacilityUnit;
                    tablelog.Tel = value.Value.Tel;
                    tablelog.Email = value.Value.Email;
                    tablelog.Longitude = value.Value.Longitude;
                    tablelog.Latitude = value.Value.Latitude;
                    tablelog.Classification = value.Value.Classification;
                    tablelog.ClassificationSpecify = value.Value.ClassificationSpecify;
                    tablelog.DateOnset = value.Value.DateOnset;
                    tablelog.CaseDate = value.Value.CaseDate;
                    tablelog.DateOfInfection = value.Value.DateOfInfection;
                    tablelog.Outcome = value.Value.Outcome;
                    tablelog.OutcomeSpecify = value.Value.OutcomeSpecify;
                    tablelog.DateOfOutcome = value.Value.DateOfOutcome;
                    tablelog.LabTestDate = value.Value.LabTestDate;
                    tablelog.LabResults = value.Value.LabResults;
                    tablelog.DateOfFirstContact = value.Value.DateOfFirstContact;
                    tablelog.DateOfLastContact = value.Value.DateOfLastContact;
                    tablelog.IsContactDateEstimated = value.Value.IsContactDateEstimated;
                    tablelog.CertaintyLevel = value.Value.CertaintyLevel;
                    tablelog.Disease = userInfor.getCurrentuserDisease();// value.Value.Disease;
                    tablelog.Symptoms = value.Value.Symptoms;
                    tablelog.SymptomsOthers = value.Value.SymptomsOthers;
                    tablelog.ExposureType = value.Value.ExposureType;
                    tablelog.ExposureTypeSpecify = value.Value.ExposureTypeSpecify;
                    tablelog.Relationship = value.Value.Relationship;
                    tablelog.RelationshipSpecify = value.Value.RelationshipSpecify;
                    tablelog.NextAction = value.Value.NextAction;
                    tablelog.StatusFlag = value.Value.StatusFlag;
                    tablelog.AssignFollowUpContactPerson = value.Value.AssignFollowUpContactPerson;
                    tablelog.Vaccines = value.Value.Vaccines;
                    tablelog.ExposureType = value.Value.ExposureType;
                    

                    tablelog.GeneralComments = value.Value.GeneralComments;
                    tablelog.DateEdited = DateTime.Now;
                    table.EditedBy = userInfor.GetUser().Identity.Name;

                    context.CoreCaseContactsLog.Add(tablelog);
                    context.SaveChanges();
                }

                else if (tablelog == null)
                {
                    CoreCaseContactsLog corecalog = new CoreCaseContactsLog();

                    //Save new record into edpm_summitdecisions_log

                    corecalog.ContactCaseLogId = CheckCoreCaseConLog();
                    corecalog.ContactCaseNo = value.Value.ContactCaseNo;
                    corecalog.CaseNo = value.Value.CaseNo;
                    corecalog.FirstName = value.Value.FirstName;
                    corecalog.MiddleName = value.Value.MiddleName;
                    corecalog.LastName = value.Value.LastName;
                    corecalog.Occupation = value.Value.Occupation;
                    corecalog.OccupationSpecify = value.Value.OccupationSpecify;
                    corecalog.VaccinesReceived = value.Value.VaccinesReceived;
                    corecalog.Vaccines = value.Value.Vaccines;
                    corecalog.Sex = value.Value.Sex;
                    corecalog.DoB = value.Value.DoB;
                    corecalog.Age = value.Value.Age;
                    corecalog.DateOfReporting = value.Value.DateOfReporting;
                    corecalog.RiskLevel = value.Value.RiskLevel;
                    corecalog.RiskReason = value.Value.RiskReason;
                    corecalog.Address = value.Value.Address;
                    corecalog.DistrictId = value.Value.DistrictId;
                    corecalog.CountyId = value.Value.CountyId;
                    corecalog.SubcountyId = value.Value.SubcountyId;
                    corecalog.ParishId = value.Value.ParishId;
                    corecalog.VillageId = value.Value.VillageId;
                    corecalog.FacilityUnit = value.Value.FacilityUnit;
                    corecalog.Tel = value.Value.Tel;
                    corecalog.Email = value.Value.Email;
                    corecalog.Longitude = value.Value.Longitude;
                    corecalog.Latitude = value.Value.Latitude;
                    corecalog.Classification = value.Value.Classification;
                    corecalog.ClassificationSpecify = value.Value.ClassificationSpecify;
                    corecalog.DateOnset = value.Value.DateOnset;
                    corecalog.CaseDate = value.Value.CaseDate;
                    corecalog.DateOfInfection = value.Value.DateOfInfection;
                    corecalog.Outcome = value.Value.Outcome;
                    corecalog.OutcomeSpecify = value.Value.OutcomeSpecify;
                    corecalog.DateOfOutcome = value.Value.DateOfOutcome;
                    corecalog.LabTestDate = value.Value.LabTestDate;
                    corecalog.LabResults = value.Value.LabResults;
                    corecalog.DateOfFirstContact = value.Value.DateOfFirstContact;
                    corecalog.DateOfLastContact = value.Value.DateOfLastContact;
                    corecalog.IsContactDateEstimated = value.Value.IsContactDateEstimated;
                    corecalog.CertaintyLevel = value.Value.CertaintyLevel;
                    corecalog.Disease = userInfor.getCurrentuserDisease();// value.Value.Disease;
                    corecalog.Symptoms = value.Value.Symptoms;
                    corecalog.SymptomsOthers = value.Value.SymptomsOthers;
                    corecalog.ExposureType = value.Value.ExposureType;
                    corecalog.ExposureTypeSpecify = value.Value.ExposureTypeSpecify;
                    corecalog.Relationship = value.Value.Relationship;
                    corecalog.RelationshipSpecify = value.Value.RelationshipSpecify;
                    corecalog.NextAction = value.Value.NextAction;
                    corecalog.StatusFlag = value.Value.StatusFlag;
                    corecalog.AssignFollowUpContactPerson = value.Value.AssignFollowUpContactPerson;

                    corecalog.Vaccines = value.Value.Vaccines;
                    corecalog.ExposureType = value.Value.ExposureType;

                    corecalog.GeneralComments = value.Value.GeneralComments;
                    corecalog.AddedBy = userInfor.GetUser().Identity.Name;
                    corecalog.DateEdited = DateTime.Now;

                    context.CoreCaseContactsLog.Add(corecalog);
                    context.SaveChanges();
                }

                table.FirstName = value.Value.FirstName;
                table.MiddleName = value.Value.MiddleName;
                table.LastName = value.Value.LastName;
                table.Occupation = value.Value.Occupation;
                table.OccupationSpecify = value.Value.OccupationSpecify;
                table.VaccinesReceived = value.Value.VaccinesReceived;
                table.Vaccines = value.Value.Vaccines;
                table.Sex = value.Value.Sex;
                table.DoB = value.Value.DoB;
                table.Age = value.Value.Age;
                table.DateOfReporting = value.Value.DateOfReporting;
                table.RiskLevel = value.Value.RiskLevel;
                table.RiskReason = value.Value.RiskReason;
                table.Address = value.Value.Address;
                table.DistrictId = value.Value.DistrictId;
                table.CountyId = value.Value.CountyId;
                table.SubcountyId = value.Value.SubcountyId;
                table.ParishId = value.Value.ParishId;
                table.VillageId = value.Value.VillageId;
                table.FacilityUnit = value.Value.FacilityUnit;
                table.Tel = value.Value.Tel;
                table.Email = value.Value.Email;
                table.Longitude = value.Value.Longitude;
                table.Latitude = value.Value.Latitude;
                table.Classification = value.Value.Classification;
                table.ClassificationSpecify = value.Value.ClassificationSpecify;
                table.DateOnset = value.Value.DateOnset;
                table.CaseDate = value.Value.CaseDate;
                table.DateOfInfection = value.Value.DateOfInfection;
                table.Outcome = value.Value.Outcome;
                table.OutcomeSpecify = value.Value.OutcomeSpecify;
                table.DateOfOutcome = value.Value.DateOfOutcome;
                table.LabTestDate = value.Value.LabTestDate;
                table.LabResults = value.Value.LabResults;
                table.DateOfFirstContact = value.Value.DateOfFirstContact;
                table.DateOfLastContact = value.Value.DateOfLastContact;
                table.IsContactDateEstimated = value.Value.IsContactDateEstimated;
                table.CertaintyLevel = value.Value.CertaintyLevel;
                table.Disease = userInfor.getCurrentuserDisease();// value.Value.Disease;
                table.Symptoms = value.Value.Symptoms;
                table.SymptomsOthers = value.Value.SymptomsOthers;
                table.ExposureType = value.Value.ExposureType;
                table.ExposureTypeSpecify = value.Value.ExposureTypeSpecify;
                table.Relationship = value.Value.Relationship;
                table.RelationshipSpecify = value.Value.RelationshipSpecify;
                table.NextAction = value.Value.NextAction;
                table.StatusFlag = value.Value.StatusFlag;
                table.AssignFollowUpContactPerson = value.Value.AssignFollowUpContactPerson;
                table.GeneralComments = value.Value.GeneralComments;
                table.StatusFlag = value.Value.StatusFlag;
                table.Vaccines = value.Value.Vaccines;
                table.ExposureType = value.Value.ExposureType;
                table.LostFollowUp = value.Value.LostFollowUp;
                //context.CoreCaseContacts.Add(table);
                //context.SaveChanges();

                context.Entry(table).State = EntityState.Modified;
                context.SaveChanges();

                try
                {

                    TempData["Success"] = "Record Saved Successfully!";
                }

                //catch { 

                //}

                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            TempData["Success"] += string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                            System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }
                }
            }
            ///call the contacts table to save a new case
            ///elevated contact to a case..........
            
            if (table.StatusFlag == 1)
            {
                //this.SaveElevateContact(value.Value.ToString());
                CoreCases cases = context.CoreCases.FirstOrDefault(o => (o.FirstName == value.Value.FirstName)
                && (o.LastName == value.Value.LastName) && (o.Age == value.Value.Age) && (o.Sex == value.Value.Sex)
                && (o.Tel == value.Value.Tel) && (o.CaseNo.Contains(value.Value.ContactCaseNo)));

                if (cases == null)
                {
                    //var x = new CoreCases()
                    //{
                    //    CaseNo = "ELEVATED - " + value.Value.ContactCaseNo,
                    //    AddedBy = userInfor.GetFullName(),
                    //    DateAdded = DateTime.Now
                    //};
                    CoreCases x = new CoreCases();
                    x.CaseNo = "ELEVATED-" + value.Value.ContactCaseNo;
                    x.FirstName = value.Value.FirstName;
                    x.MiddleName = value.Value.MiddleName;
                    x.LastName = value.Value.LastName;
                    x.WasContact = 1;
                    x.Occupation = value.Value.Occupation;
                    x.OccupationSpecify = value.Value.OccupationSpecify;
                    x.RiskLevel = value.Value.RiskLevel;
                    x.Sex = value.Value.Sex;
                    x.DoB = value.Value.DoB;
                    x.Age = value.Value.Age;
                    x.DateOfReporting = value.Value.DateOfReporting;
                    x.RiskReason = value.Value.RiskReason;
                    x.Address = value.Value.Address;
                    x.DistrictId = value.Value.DistrictId;
                    x.CountyId = value.Value.CountyId;
                    x.SubcountyId = value.Value.SubcountyId;
                    x.ParishId = value.Value.ParishId;
                    x.VillageId = value.Value.VillageId;
                    x.FacilityUnit = value.Value.FacilityUnit;
                    x.Tel = value.Value.Tel;
                    x.Email = value.Value.Email;
                    x.StatusFlag = 2;
                    x.Longitude = value.Value.Longitude;
                    x.Latitude = value.Value.Latitude;
                    x.Classification = value.Value.Classification;
                    x.DateAdded = DateTime.Now;
                    x.AddedBy = userInfor.GetFullName();  //value.Value.CreatedBy;
                    x.ClassificationSpecify = value.Value.ClassificationSpecify;
                    x.DateOnset = value.Value.DateOnset;
                    x.CaseDate = value.Value.CaseDate;
                    x.DateOfInfection = value.Value.DateOfInfection;
                    x.Outcome = value.Value.Outcome;
                    x.OutcomeSpecify = value.Value.OutcomeSpecify;
                    x.DateOfOutcome = value.Value.DateOfOutcome;
                    x.LabTestDate = value.Value.LabTestDate;
                    x.LabResults = value.Value.LabResults;
                    x.Disease = userInfor.getCurrentuserDisease();// value.Value.Disease;
                    x.Symptoms = value.Value.Symptoms;
                    x.SymptomsOthers = value.Value.SymptomsOthers;
                    x.NextAction = value.Value.NextAction;
                    x.StatusFlag = value.Value.StatusFlag;
                    x.GeneralComments = "Please note that this is a contact eleved to a case";
                    context.CoreCases.Add(x);
                    context.SaveChanges();
                }
            }

            //send email to assigned person
            if(value.Value.AssignFollowUpContactPerson != null)
            {
                EmailModel email = new EmailModel(context);
                email.SendAssigmentEmail(value.Value.ContactCaseNo, value.Value.FirstName, value.Value.LastName, value.Value.Tel, value.Value.AssignFollowUpContactPerson);
            }
            return Json(value.Value);
        }

        //call save new case contact
        
        //County Change
        public ActionResult getcounty(string DistrictId)
        {
            var county = context.ACounty.Where(o => o.DistrictId == DistrictId).AsNoTracking().ToList();
            return Json(county);
        }

        //Division Change
        public ActionResult getdivision(string CountyId)
        {
            var subcounty = context.ASubcounty.Where(o => o.CountyId == CountyId).AsNoTracking().ToList();
            return Json(subcounty);
        }

        //SubCounty/Parish Change
        public ActionResult getparish(string SubcountyId)
        {
            var parish = context.AParish.Where(o => o.SubcountyId == SubcountyId).AsNoTracking().ToList();
            return Json(parish);
        }

        //Village Change
        public ActionResult getvillage(string ParishId)
        {
            var village = context.AVillage.Where(o => o.ParishId == ParishId).AsNoTracking().ToList();
            return Json(village);
        }
        #endregion RegisterContact

        public ActionResult SaveSupervisor(int? EmployeeID, string CaseNo, string ContactCaseNo)
        {
            CoreCaseContacts table = context.CoreCaseContacts.FirstOrDefault(o =>
            (o.ContactCaseNo == ContactCaseNo) && (o.CaseNo == CaseNo));

            if (table != null)
            {
                table.AssignFollowUpContactPerson = EmployeeID;
                context.Entry(table).CurrentValues.SetValues(EmployeeID);
                context.Entry(table).State = EntityState.Modified;
                try
                {
                    context.SaveChanges();
                }

                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            TempData["Success"] += string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                            System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }
                }
            }
            //send email to assigned person
            if (EmployeeID != null)
            {
                EmailModel email = new EmailModel(context);
                email.SendAssigmentEmail(ContactCaseNo, table.FirstName, table.LastName, table.Tel, EmployeeID);
            }
            var msg = "Record Assigned successfully";
            //return Json(msg);
            return RedirectToAction("RegisterContact");
        }


        public ActionResult BatchUpdate(string key, List<CoreCaseContacts> changed, List<CoreCaseContacts> added, List<CoreCaseContacts> deleted, int? EmployeeID, string CaseNo, string ContactCaseNo)
        {
            CoreCaseContacts table = context.CoreCaseContacts.FirstOrDefault(o =>
            (o.ContactCaseNo == ContactCaseNo) && (o.CaseNo == CaseNo));

            if (changed != null && changed.Count() > 0)
            {
                foreach (var temp in changed)
                {
                    table.AssignFollowUpContactPerson = EmployeeID;
                    table.Disease = userInfor.getCurrentuserDisease();// 
                    context.Entry(table).CurrentValues.SetValues(EmployeeID);
                    context.Entry(table).State = EntityState.Modified;
                    context.SaveChanges();
                }
            }
            return RedirectToAction("RegisterContact");
        }

        public ActionResult ContactTestDetails([FromBody] DataManagerRequest dm, string contactNo, string caseNo)
        {
            var _data = context.CoreCaseContactsMutitpleTests.Where(o => o.CaseNo == caseNo && o.ContactCaseNo == contactNo && o.DeleteStatus != 1).AsNoTracking().ToList();
            IEnumerable data = _data;
            int count = _data.Count;

            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip > 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take > 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = count });

        }

        public ActionResult UpdateDetailsUrl([FromBody] CRUDModel<CoreCaseContactsMutitpleTests> value/*, string key*/)
        {
            if (value.Action == "insert")
            {
                int result = context.CoreCaseContactsMutitpleTests.OrderBy(a => a.Code).ToList().Count();
                if(result == 0)
                {
                    result = 1;
                }
                else
                {
                    result= ++context.CoreCaseContactsMutitpleTests.OrderBy(a => a.Code).ToList().Last().Code;
                }
                value.Value.Code = result;
                value.Value.AddedBy = userInfor.GetFullName();
                value.Value.DateAdded = DateTime.Now;
                context.CoreCaseContactsMutitpleTests.Add(value.Value);
                context.SaveChanges();
            }
            if (value.Action == "update")
            {
                CoreCaseContactsMutitpleTests test = context.CoreCaseContactsMutitpleTests.FirstOrDefault(z => z.Code == value.Value.Code);
                test.EditedBy = userInfor.GetFullName();
                test.DateEdited = DateTime.Now;
                context.Entry(test).CurrentValues.SetValues(value.Value);
                context.Entry(test).State = EntityState.Modified;

                context.SaveChanges();
            }

            if (value.Action == "remove")
            {
                var code = context.CoreCaseContactsMutitpleTests.FirstOrDefault(z => z.Code ==Convert.ToInt32(value.Key));
                if (code != null)
                {
                    context.CoreCaseContactsMutitpleTests.Remove(code);
                    context.SaveChanges();
                }
            }
            return Json(value);
        }

        public IActionResult UpdateDetails([FromBody] CRUDModel<CoreCaseContactsMutitpleTests> value)
        {
            ViewBag.TestResults = context.ALabResults.AsNoTracking().ToList();
            if (value.Value.Code == 0)
            {
                value.Value.Code = GetIDNo();
            }
            return PartialView("_DialogTestResults", value.Value);
            //return PartialView("_Dialog", value.Value);
        }

        public int GetIDNo()
        {
            int result  = ++context.CoreCaseContactsMutitpleTests.OrderBy(a => a.Code).ToList().Last().Code;
            var x = new CoreCaseContactsMutitpleTests()
            {
                Code = result,
                AddedBy = userInfor.GetFullName(),
                DateAdded = DateTime.Now
            };
            context.CoreCaseContactsMutitpleTests.Add(x);
            context.SaveChanges();

            return result;
        }
        //remove cases
        public ActionResult RemoveRecord2(string code)
        {
            context.CoreCaseContactsMutitpleTests.Remove(context.CoreCaseContactsMutitpleTests.FirstOrDefault(o => o.Code == Convert.ToInt32(code)));
            int result = context.SaveChanges();

            return Json(result);
        }

        public ActionResult DeleteChildUrl([FromBody] CRUDModel<CoreCaseContacts> value)
        {
            string obj = Request.Headers["additional_key"]; //key1 
            string obj1 = Request.Headers["additional_key1"]; //key2 
            CoreCaseContacts table = context.CoreCaseContacts.FirstOrDefault(or => or.ContactCaseNo + or.CaseNo == obj + obj1);
            if (table != null)
            {
                if (value.Deleted != null)
                {
                    foreach (var temp in value.Deleted)
                    {
                        temp.DeleteStatus = 1;
                        temp.DeletedBy = userInfor.GetFullName();
                        temp.DateDeleted = System.DateTime.Now;
                        context.Entry(temp).State = EntityState.Modified;
                        try
                        {
                            context.SaveChanges();
                        }
                        catch (Exception dbEx)
                        {
                            throw (dbEx);
                        }
                    }
                }
                else
                {
                    table.DeleteStatus = 1;
                    table.DeletedBy = userInfor.GetFullName();
                    table.DateDeleted = System.DateTime.Now;
                    context.Entry(table).State = EntityState.Modified;
                    try
                    {
                        context.SaveChanges();
                    }
                    catch (Exception dbEx)
                    {
                        throw (dbEx);
                    }
                }
            }
            //var msg = "Contact Case No. " + obj + " Has been Deleted Successfully !!";
            return Json(value);

        }

    }
}