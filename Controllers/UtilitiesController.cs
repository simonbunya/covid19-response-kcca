﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using CHMIS.BLL;
using CHMIS.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Syncfusion.EJ2.Base;

namespace CHMIS.Controllers
{
    public class UtilitiesController : Controller
    {
        private IWebHostEnvironment hostingEnv;
        private CHMISContext context;
        private IUserService userInfor;

        public UtilitiesController(CHMISContext Context, IWebHostEnvironment env, IUserService _userinfo)

        {
            this.hostingEnv = env;
            this.context = Context;
            this.userInfor = _userinfo;
        }

        public IActionResult Index()
        {
            return View();
        }

        #region Country
        public IActionResult Country()
        {

            return View();
        }

        public IActionResult AddCountryPartial([FromBody] CRUDModel<ACountry> value)
        {
           

            return PartialView("_DialogCountryAddPartial", value.Value);
        }

        public ActionResult EditCountryPartial([FromBody] CRUDModel<ACountry> value)
       
        {
           
            return PartialView(value.Value);
        }
              
        public ActionResult CountryDataSource([FromBody] DataManagerRequest dm)
        {

            var _data = context.ACountry.AsNoTracking().OrderByDescending(o => o.CountryCode).ToList();
            IEnumerable data = _data;
            int count = _data.Count(); 

            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip > 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take > 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = count }, new Newtonsoft.Json.JsonSerializerSettings());
        }
               
        public int CheckCountryCode()
        {
            var councode = context.ACountry.OrderByDescending(o => o.CountryId).Take(1).Select(f => f.CountryId).FirstOrDefault();
            return (Convert.ToInt32(councode));
        }

        public ActionResult DialogInsert([FromBody] CRUDModel<ACountry> value)
        {
            string result = string.Empty;

            ACountry _table = context.ACountry.FirstOrDefault(o =>
            ((o.CountryCode == value.Value.CountryCode) && (o.CountryId == value.Value.CountryId)));
            
            if (_table == null)
            {
                //Save into ACountry table
                ACountry table = new ACountry();

                if (string.IsNullOrEmpty(table.CountryId.ToString()) || table.CountryId == 0)
                {
                    var newCode = CheckCountryCode();
                    table.CountryId = ++newCode; //Save Country ID
                }
                
                table.CountryCode = value.Value.CountryCode;
               // table.CountryId = value.Value.CountryId;
                table.CountryName = value.Value.CountryName;
                
                context.ACountry.Add(table);
                context.SaveChanges();

            }
            else
            {
                this.DialogUpdate(value);
            }
            return Json(value.Value);
        }


        // Update using Table CoreCaseContacts
        public ActionResult DialogUpdate([FromBody] CRUDModel<ACountry> value)
        {
            ACountry table = context.ACountry.FirstOrDefault(o =>
            ((o.CountryCode == value.Value.CountryCode) && (o.CountryId == value.Value.CountryId)));


            if (table != null)
            {
                //table.CountryCode = value.Value.CountryCode;
                table.CountryId = value.Value.CountryId;
                table.CountryName = value.Value.CountryName;

                context.Entry(table).CurrentValues.SetValues(value);
                context.Entry(table).State = EntityState.Modified;

                context.SaveChanges();

                try
                {

                    TempData["Success"] = "Record Saved Successfully!";
                }

              

                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            TempData["Success"] += string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                            System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }
                }
            }
            else
            {
                this.DialogInsert(value);
            }

            return Json(value.Value);
        }

        #endregion Country

        #region Region
        public IActionResult Region()
        {

            return View();
        }

        public ActionResult RegionDataSource([FromBody] DataManagerRequest dm)
        {

            var _data = context.ARegion.AsNoTracking().OrderByDescending(o => o.RegionId).ToList();
            IEnumerable data = _data;
            int count = _data.Count();

            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip > 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take > 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = count }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        public IActionResult AddRegionPartial([FromBody] CRUDModel<ARegion> value)
        {


            return PartialView("_DialogRegionAddPartial", value.Value);
        }

        public ActionResult EditRegionPartial([FromBody] CRUDModel<ARegion> value)

        {

            return PartialView(value.Value);
        }

        public string CheckRegionCode()
        {
            string code = "0";
            int result = 0;
            result = context.ARegion.ToList().Count();
            code += (result + 1);
            var x = new ARegion()
            {
                RegionId = code
            };

            return (code);
        }

        //Insert into Region Table
        public ActionResult DialogInsertRegion([FromBody] CRUDModel<ARegion> value)
        {
            string result = string.Empty;

            ARegion _table = context.ARegion.FirstOrDefault(o =>
            (o.RegionId == value.Value.RegionId));

            if (_table == null)
            {
                //Save into ACountry table
                ARegion table = new ARegion();

                table.RegionId = CheckRegionCode();
                table.RegionName = value.Value.RegionName;
               
                context.ARegion.Add(table);
                context.SaveChanges();

            }
            else
            {
                this.DialogUpdateRegion(value);
            }
            return Json(value.Value);
        }


        // Update using Table CoreCaseContacts
        public ActionResult DialogUpdateRegion([FromBody] CRUDModel<ARegion> value)
        {
            ARegion table = context.ARegion.FirstOrDefault(o =>
            (o.RegionId == value.Value.RegionId));

            if (table != null)
            {
                table.RegionName = value.Value.RegionName;

                context.Entry(table).CurrentValues.SetValues(value);
                context.Entry(table).State = EntityState.Modified;

                context.SaveChanges();

                try
                {

                    TempData["Success"] = "Record Saved Successfully!";
                }



                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            TempData["Success"] += string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                            System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }
                }
            }
            else
            {
                this.DialogInsertRegion(value);
            }

            return Json(value.Value);
        }

        #endregion Region

        #region District
        public IActionResult District()
        {
            //Region
            var YesNo = context.ARegion.AsNoTracking().OrderBy(a => a.RegionId).ToList();
            ViewBag.region = YesNo;

            return View();
        }

        public ActionResult DistrictDataSource([FromBody] DataManagerRequest dm)
        {

            var _data = context.ADistrict.AsNoTracking().OrderByDescending(o => o.DistrictId).ToList();
            IEnumerable data = _data;
            int count = _data.Count();

            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip > 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take > 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = count }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        public IActionResult AddDistrictPartial([FromBody] CRUDModel<ADistrict> value)
        {
            //Region
            var YesNo = context.ARegion.AsNoTracking().OrderBy(a => a.RegionId).ToList();
            ViewBag.region = YesNo;

            return PartialView("_DialogDistrictAddPartial", value.Value);
        }

        public ActionResult EditDistrictPartial([FromBody] CRUDModel<ADistrict> value)
        {
            //Region
            var YesNo = context.ARegion.AsNoTracking().OrderBy(a => a.RegionId).ToList();
            ViewBag.region = YesNo;

            return PartialView(value.Value);
        }

        public string CheckDistrictCode()
        {
            string code = "0";
            int result = 0;
            result = context.ADistrict.ToList().Count();
            code += (result + 1);
            var x = new ADistrict()
            {
                DistrictId = code
            };

            return (code);
        }

        //Insert into Region Table
        public ActionResult DialogInsertDistrict([FromBody] CRUDModel<ADistrict> value)
        {
            string result = string.Empty;

            ADistrict _table = context.ADistrict.FirstOrDefault(o =>
            (o.DistrictId == value.Value.DistrictId));

            if (_table == null)
            {
                //Save into ACountry table
                ADistrict table = new ADistrict();

                table.DistrictId = CheckDistrictCode();
                table.DistrictName = value.Value.DistrictName;
                table.RegionId = value.Value.RegionId;

                context.ADistrict.Add(table);
                context.SaveChanges();

            }
            else
            {
                this.DialogUpdateDistrict(value);
            }
            return Json(value.Value);
        }
                
        public ActionResult DialogUpdateDistrict([FromBody] CRUDModel<ADistrict> value)
        {
            ADistrict table = context.ADistrict.FirstOrDefault(o =>
            (o.DistrictId == value.Value.DistrictId));

            if (table != null)
            {
               
                table.DistrictName = value.Value.DistrictName;
                table.RegionId = value.Value.RegionId;


                context.Entry(table).CurrentValues.SetValues(value);
                context.Entry(table).State = EntityState.Modified;

                context.SaveChanges();

                try
                {

                    TempData["Success"] = "Record Saved Successfully!";
                }



                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            TempData["Success"] += string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                            System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }
                }
            }
            else
            {
                this.DialogInsertDistrict(value);
            }

            return Json(value.Value);
        }

        #endregion District

        #region County

        public IActionResult County()
        {
            //District
            var Dist = context.ADistrict.AsNoTracking().OrderBy(a => a.DistrictId).ToList();
            ViewBag.district = Dist;

            return View();
        }

        public ActionResult CountyDataSource([FromBody] DataManagerRequest dm)
        {

            var _data = context.ACounty.AsNoTracking().OrderByDescending(o => o.CountyId).ToList();
            IEnumerable data = _data;
            int count = _data.Count();

            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip > 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take > 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = count }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        public IActionResult AddCountyPartial([FromBody] CRUDModel<ACounty> value)
        {
            //District
            var Dist = context.ADistrict.AsNoTracking().OrderBy(a => a.DistrictId).ToList();
            ViewBag.district = Dist;

            return PartialView("_DialogCountyAddPartial", value.Value);
        }

        public ActionResult EditCountyPartial([FromBody] CRUDModel<ACounty> value)
        {
            //District
            var Dist = context.ADistrict.AsNoTracking().OrderBy(a => a.DistrictId).ToList();
            ViewBag.district = Dist;

            return PartialView(value.Value);
        }

        public string CheckCountyCode()
        {
            string code = "0";
            int result = 0;
            result = context.ACounty.ToList().Count();
            code += (result + 1);
            var x = new ACounty()
            {
                CountyId = code
            };

            return (code);
        }

        //Insert into Region Table
        public ActionResult DialogInsertCounty([FromBody] CRUDModel<ACounty> value)
        {
            string result = string.Empty;

            ACounty _table = context.ACounty.FirstOrDefault(o =>
            (o.CountyId == value.Value.CountyId));

            if (_table == null)
            {
                //Save into ACountry table
                ACounty table = new ACounty();

                table.CountyId = CheckCountyCode();
                table.DistrictId = value.Value.DistrictId;
                table.CountyName = value.Value.CountyName;

                context.ACounty.Add(table);
                context.SaveChanges();

            }
            else
            {
                this.DialogUpdateCounty(value);
            }
            return Json(value.Value);
        }

        public ActionResult DialogUpdateCounty([FromBody] CRUDModel<ACounty> value)
        {
            ACounty table = context.ACounty.FirstOrDefault(o =>
            (o.CountyId == value.Value.CountyId));

            if (table != null)
            {                
                table.DistrictId = value.Value.DistrictId;
                table.CountyName = value.Value.CountyName;
                
                context.Entry(table).CurrentValues.SetValues(value);
                context.Entry(table).State = EntityState.Modified;

                context.SaveChanges();

                try
                {

                    TempData["Success"] = "Record Saved Successfully!";
                }



                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            TempData["Success"] += string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                            System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }
                }
            }
            else
            {
                this.DialogInsertCounty(value);
            }

            return Json(value.Value);
        }

        #endregion County

        #region SubCounty

        public IActionResult SubCounty()
        {
            //District
            var Dist = context.ADistrict.AsNoTracking().OrderBy(a => a.DistrictId).ToList();
            ViewBag.district = Dist;

            //County
            var Count = context.ACounty.AsNoTracking().OrderBy(a => a.CountyId).ToList();
            ViewBag.county = Count;

            return View();
        }

        public ActionResult SubCountyDataSource([FromBody] DataManagerRequest dm)
        {

            var _data = context.ASubcounty.AsNoTracking().OrderByDescending(o => o.SubcountyId).ToList();
            IEnumerable data = _data;
            int count = _data.Count();

            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip > 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take > 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = count }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        public IActionResult AddSubCountyPartial([FromBody] CRUDModel<ASubcounty> value)
        {
            //District
            var Dist = context.ADistrict.AsNoTracking().OrderBy(a => a.DistrictId).ToList();
            ViewBag.district = Dist;

            //County
            var Count = context.ACounty.AsNoTracking().OrderBy(a => a.CountyId).ToList();
            ViewBag.county = Count;

            return PartialView("_DialogSubCountyAddPartial", value.Value);
        }

        public ActionResult EditSubCountyPartial([FromBody] CRUDModel<ASubcounty> value)
        {
            //District
            var Dist = context.ADistrict.AsNoTracking().OrderBy(a => a.DistrictId).ToList();
            ViewBag.district = Dist;

            //County
            var Count = context.ACounty.AsNoTracking().OrderBy(a => a.CountyId).ToList();
            ViewBag.county = Count;

            return PartialView(value.Value);
        }

        public string CheckSubCountyCode()
        {
            string code = "0";
            int result = 0;
            result = context.ASubcounty.ToList().Count();
            code += (result + 1);
            var x = new ASubcounty()
            {
                SubcountyId = code
            };

            return (code);
        }

        //Insert into SubCounty Table
        public ActionResult DialogInsertSubCounty([FromBody] CRUDModel<ASubcounty> value)
        {
            string result = string.Empty;

            ASubcounty _table = context.ASubcounty.FirstOrDefault(o =>
            (o.SubcountyId == value.Value.SubcountyId));


            if (_table == null)
            {
                //Save into ASubcounty table
                ASubcounty table = new ASubcounty();

                table.SubcountyId = CheckSubCountyCode();
                table.DistrictId = value.Value.DistrictId;
                table.CountyId = value.Value.CountyId;
                table.SubcountyName = value.Value.SubcountyName;

                context.ASubcounty.Add(table);
                context.SaveChanges();

            }
            else
            {
                this.DialogUpdateSubCounty(value);
            }
            return Json(value.Value);
        }

        public ActionResult DialogUpdateSubCounty([FromBody] CRUDModel<ASubcounty> value)
        {
            ASubcounty table = context.ASubcounty.FirstOrDefault(o =>
            (o.SubcountyId == value.Value.SubcountyId));

            if (table != null)
            {               
                table.CountyId = value.Value.CountyId;
                table.DistrictId = value.Value.DistrictId;
                table.SubcountyName = value.Value.SubcountyName;

                context.Entry(table).CurrentValues.SetValues(value);
                context.Entry(table).State = EntityState.Modified;

                context.SaveChanges();

                try
                {

                    TempData["Success"] = "Record Saved Successfully!";
                }



                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            TempData["Success"] += string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                            System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }
                }
            }
            else
            {
                this.DialogInsertSubCounty(value);
            }

            return Json(value.Value);
        }

        public ActionResult getcounty(string DistrictId)
        {
            //var county = context.ACounty.Where(o => o.DistrictId == DistrictId).AsNoTracking().ToList();
            var county = context.ACounty.Where(o => o.DistrictId == DistrictId).Select(a => new { a.CountyId, a.CountyName }).Distinct().OrderBy(a => a.CountyName).ToList();

            return Json(county);
        }

        #endregion SubCounty

        #region Facility
        public IActionResult Facility()
        {
            //District
            var Dist = context.ADistrict.AsNoTracking().OrderByDescending(a => a.DistrictId).ToList();
            ViewBag.district = Dist;

            //County
            var Count = context.ACounty.AsNoTracking().OrderByDescending(a => a.CountyName).ToList();
            ViewBag.county = Count;

            //SubCounty
            var SubCounty = context.ASubcounty.AsNoTracking().OrderByDescending(a => a.SubcountyName).ToList();
            ViewBag.subcounty = SubCounty;

            //Country
            var Country = context.ACountry.AsNoTracking().OrderByDescending(a => a.CountryName).ToList();
            ViewBag.country = Country;

            //Parish
            var Parish = context.AParish.AsNoTracking().OrderByDescending(a => a.ParishName).ToList();
            ViewBag.parish = Parish;

            //Village
            var Village = context.AVillage.AsNoTracking().OrderByDescending(a => a.VillageName).ToList();
            ViewBag.village = Village;

            //OperationalArea
            var OperArea = context.AOperationalArea.AsNoTracking().OrderByDescending(a => a.OperationalArea).ToList();
            ViewBag.operarea = OperArea;

            return View();
        }
        public ActionResult FacilityDataSource([FromBody] DataManagerRequest dm)
        {

            var _data = context.AFacilityUnit.AsNoTracking().OrderByDescending(o => o.FacilityCode).ToList();
            IEnumerable data = _data;
            int count = _data.Count();

            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip > 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take > 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = count }, new Newtonsoft.Json.JsonSerializerSettings());
        }
        public IActionResult AddFacilityPartial([FromBody] CRUDModel<AFacilityUnit> value)
        {
            //District
            var Dist = context.ADistrict.AsNoTracking().OrderByDescending(a => a.DistrictId).ToList();
            ViewBag.district = Dist;

            //County
            var Count = context.ACounty.AsNoTracking().OrderByDescending(a => a.CountyName).ToList();
            ViewBag.county = Count;

            //SubCounty
            var SubCounty = context.ASubcounty.AsNoTracking().OrderByDescending(a => a.SubcountyName).ToList();
            ViewBag.subcounty = SubCounty;

            //Country
            var Country = context.ACountry.AsNoTracking().OrderByDescending(a => a.CountryName).ToList();
            ViewBag.country = Country;

            //Parish
            var Parish = context.AParish.AsNoTracking().OrderByDescending(a => a.ParishName).ToList();
            ViewBag.parish = Parish;

            //Village
            var Village = context.AVillage.AsNoTracking().OrderByDescending(a => a.VillageName).ToList();
            ViewBag.village = Village;

            //OperationalArea
            var OperArea = context.AOperationalArea.AsNoTracking().OrderByDescending(a => a.OperationalArea).ToList();
            ViewBag.operarea = OperArea;

            //Yes/No - Vacciness Received
            var YesNo = context.AYesNo.AsNoTracking().OrderBy(a => a.Id).ToList();
            ViewBag.yesno = YesNo;

            return PartialView("_DialogFacilityAddPartial", value.Value);
        }
        public ActionResult EditFacilityPartial([FromBody] CRUDModel<AFacilityUnit> value)
        {
            //District
            var Dist = context.ADistrict.AsNoTracking().OrderByDescending(a => a.DistrictId).ToList();
            ViewBag.district = Dist;

            //County
            var Count = context.ACounty.AsNoTracking().OrderByDescending(a => a.CountyName).ToList();
            ViewBag.county = Count;

            //SubCounty
            var SubCounty = context.ASubcounty.AsNoTracking().OrderByDescending(a => a.SubcountyName).ToList();
            ViewBag.subcounty = SubCounty;

            //Country
            var Country = context.ACountry.AsNoTracking().OrderByDescending(a => a.CountryName).ToList();
            ViewBag.country = Country;

            //Parish
            var Parish = context.AParish.AsNoTracking().OrderByDescending(a => a.ParishName).ToList();
            ViewBag.parish = Parish;

            //Village
            var Village = context.AVillage.AsNoTracking().OrderByDescending(a => a.VillageName).ToList();
            ViewBag.village = Village;

            //OperationalArea
            var OperArea = context.AOperationalArea.AsNoTracking().OrderByDescending(a => a.OperationalArea).ToList();
            ViewBag.operarea = OperArea;

            //Yes/No - Vacciness Received
            var YesNo = context.AYesNo.AsNoTracking().OrderBy(a => a.Id).ToList();
            ViewBag.yesno = YesNo;

            return PartialView(value.Value);
        }

        //public ActionResult CheckFacilityCodee()
        //{
        //    int result = 0;
        //    var data = context.AFacilityUnit.OrderByDescending(o => o.FacilityCode).FirstOrDefault();
        //    if (data != null)
        //    {
        //        result = data.FacilityCode;
        //    }
        //    return Json(result);

        //}


        //public int CheckFacilityCode()
        //{
        //    var faccode = context.AFacilityUnit.OrderByDescending(o => o.FacilityCode).Take(1).Select(f => f.FacilityCode).FirstOrDefault();
        //    return (faccode);
        //}

        //Insert into SubCounty Table
        public ActionResult DialogInsertFacility([FromBody] CRUDModel<AFacilityUnit> value)
        {
            string result = string.Empty;

            AFacilityUnit _table = context.AFacilityUnit.FirstOrDefault(o =>
            (o.FacilityCode == value.Value.FacilityCode));


            if (_table == null)
            {
                //Save into ASubcounty table
                AFacilityUnit table = new AFacilityUnit();

                //if (string.IsNullOrEmpty(table.FacilityCode.ToString()) || table.FacilityCode == 0)
                //{
                //    var newCode = CheckFacilityCode();
                //    table.FacilityCode = ++newCode; //Save Country ID
                //}

                //table.FacilityCode = value.Value.FacilityCode;

                table.OperationalArea = value.Value.OperationalArea;
                table.Facility = value.Value.Facility;
                table.DetailsOfFacility = value.Value.DetailsOfFacility;
                table.EstimatedPopulation = value.Value.EstimatedPopulation;
                table.FacilityStatus = value.Value.FacilityStatus;
                table.DistrictId = value.Value.DistrictId;
                table.CountyId = value.Value.CountyId;
                table.SubcountyId = value.Value.SubcountyId;
                table.ParishId = value.Value.ParishId;
                table.VillageId = value.Value.VillageId;
                table.Longitude = value.Value.Longitude;
                table.Latitude = value.Value.Latitude;
                table.DateAdded = DateTime.Now;
                table.AddedBy = userInfor.GetUser().Identity.Name;
               
                context.AFacilityUnit.Add(table);
                context.SaveChanges();

            }
            else
            {
                this.DialogUpdateFacility(value);
            }
            return Json(value.Value);
        }

        public ActionResult DialogUpdateFacility([FromBody] CRUDModel<AFacilityUnit> value)
        {
            AFacilityUnit table = context.AFacilityUnit.FirstOrDefault(o =>
             (o.FacilityCode == value.Value.FacilityCode));

            if (table != null)
            {               
                table.OperationalArea = value.Value.OperationalArea;
                table.Facility = value.Value.Facility;
                table.DetailsOfFacility = value.Value.DetailsOfFacility;
                table.EstimatedPopulation = value.Value.EstimatedPopulation;
                table.FacilityStatus = value.Value.FacilityStatus;
                table.DistrictId = value.Value.DistrictId;
                table.CountyId = value.Value.CountyId;
                table.SubcountyId = value.Value.SubcountyId;
                table.ParishId = value.Value.ParishId;
                table.VillageId = value.Value.VillageId;
                table.Longitude = value.Value.Longitude;
                table.Latitude = value.Value.Latitude;
                table.DateEdited = DateTime.Now;
                table.EditedBy = userInfor.GetUser().Identity.Name;

                context.Entry(table).CurrentValues.SetValues(value);
                context.Entry(table).State = EntityState.Modified;

                context.SaveChanges();

                try
                {

                    TempData["Success"] = "Record Saved Successfully!";
                }



                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            TempData["Success"] += string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                            System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }
                }
            }
            else
            {
                this.DialogInsertFacility(value);
            }

            return Json(value.Value);
        }

        //County Change
        public ActionResult getcountyfac(string DistrictId)
        {
            var county = context.ACounty.Where(o => o.DistrictId == DistrictId).AsNoTracking().ToList();
            return Json(county);
        }

        //Division Change
        public ActionResult getdivisionfac(string CountyId)
        {
            var subcounty = context.ASubcounty.Where(o => o.CountyId == CountyId).AsNoTracking().ToList();
            return Json(subcounty);
        }

        //SubCounty/Parish Change
        public ActionResult getparishfac(string SubcountyId)
        {
            var parish = context.AParish.Where(o => o.SubcountyId == SubcountyId).AsNoTracking().ToList();
            return Json(parish);
        }

        //Village Change
        public ActionResult getvillagefac(string ParishId)
        {
            var village = context.AVillage.Where(o => o.ParishId == ParishId).AsNoTracking().ToList();
            return Json(village);
        }

        #endregion Facility


        #region OperationalAreal
        public IActionResult OperationalArea()
        {
           
            return View();
        }
        public ActionResult OperationalAreaDataSource([FromBody] DataManagerRequest dm)
        {

            var _data = context.AOperationalArea.AsNoTracking().OrderByDescending(o => o.OperationalCode).ToList();
            IEnumerable data = _data;
            int count = _data.Count();

            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip > 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take > 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = count }, new Newtonsoft.Json.JsonSerializerSettings());
        }
        public IActionResult AddOperationalAreaPartial([FromBody] CRUDModel<AOperationalArea> value)
        {
           

            return PartialView("_DialogOperationalAreaAddPartial", value.Value);
        }
        public ActionResult EditOperationalAreaPartial([FromBody] CRUDModel<AOperationalArea> value)
        {
           

            return PartialView(value.Value);
        }


        public int CheckOperationalAreaCode()
        {
            var faccode = context.AOperationalArea.OrderByDescending(o => o.OperationalCode).Take(1).Select(f => f.OperationalCode).FirstOrDefault();
            return (faccode);
        }

        //Insert into SubCounty Table
        public ActionResult DialogInsertOperationalArea([FromBody] CRUDModel<AOperationalArea> value)
        {
            string result = string.Empty;

            AOperationalArea _table = context.AOperationalArea.FirstOrDefault(o =>
            (o.OperationalCode == value.Value.OperationalCode));


            if (_table == null)
            {
                //Save into ASubcounty table
                AOperationalArea table = new AOperationalArea();

                if (string.IsNullOrEmpty(table.OperationalCode.ToString()) || table.OperationalCode == 0)
                {
                    var newCode = CheckOperationalAreaCode();
                    table.OperationalCode = ++newCode; //Save Country ID
                }

                table.OperationalArea = value.Value.OperationalArea;
                
                context.AOperationalArea.Add(table);
                context.SaveChanges();

            }
            else
            {
                this.DialogUpdateOperationalArea(value);
            }
            return Json(value.Value);
        }

        public ActionResult DialogUpdateOperationalArea([FromBody] CRUDModel<AOperationalArea> value)
        {
            AOperationalArea table = context.AOperationalArea.FirstOrDefault(o =>
             (o.OperationalCode == value.Value.OperationalCode));

            if (table != null)
            {
                table.OperationalArea = value.Value.OperationalArea;
               

                context.Entry(table).CurrentValues.SetValues(value);
                context.Entry(table).State = EntityState.Modified;

                context.SaveChanges();

                try
                {

                    TempData["Success"] = "Record Saved Successfully!";
                }



                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            TempData["Success"] += string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                            System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }
                }
            }
            else
            {
                this.DialogInsertOperationalArea(value);
            }

            return Json(value.Value);
        }

        #endregion OperationalAreal
        
        #region Parish

        public IActionResult Parish()
        {
            //District
            var Dist = context.ADistrict.AsNoTracking().OrderBy(a => a.DistrictId).ToList();
            ViewBag.district = Dist;

            //County
            var Count = context.ACounty.AsNoTracking().OrderBy(a => a.CountyId).ToList();
            ViewBag.county = Count;

            //SubCounty
            var SubCount = context.ASubcounty.AsNoTracking().OrderBy(a => a.SubcountyId).ToList();
            ViewBag.subcounty = SubCount;

            
            return View();
        }

        public ActionResult ParishDataSource([FromBody] DataManagerRequest dm)
        {

            var _data = context.ViewParishes.AsNoTracking().OrderBy(o => o.ParishId).ToList();
            IEnumerable data = _data;
            int count = _data.Count();

            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip > 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take > 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = count }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        public IActionResult AddParishPartial([FromBody] CRUDModel<ViewParishes> value)
        {
            //District
            var Dist = context.ADistrict.AsNoTracking().OrderBy(a => a.DistrictId).ToList();
            ViewBag.district = Dist;

            //County
            var Count = context.ACounty.AsNoTracking().OrderBy(a => a.CountyId).ToList();
            ViewBag.county = Count;

            //SubCounty
            var SubCount = context.ASubcounty.AsNoTracking().OrderBy(a => a.SubcountyId).ToList();
            ViewBag.subcounty = SubCount;

            return PartialView("_DialogParishAddPartial", value.Value);
        }

        public ActionResult EditParishPartial([FromBody] CRUDModel<ViewParishes> value)
        {
            //District
            var Dist = context.ADistrict.AsNoTracking().OrderBy(a => a.DistrictId).ToList();
            ViewBag.district = Dist;

            //County
            var Count = context.ACounty.AsNoTracking().OrderBy(a => a.CountyId).ToList();
            ViewBag.county = Count;

            //SubCounty
            var SubCount = context.ASubcounty.AsNoTracking().OrderBy(a => a.SubcountyId).ToList();
            ViewBag.subcounty = SubCount;

            return PartialView(value.Value);
        }

        public string CheckParishCode()
        {
            string code = "0";
            int result = 0;
            result = context.AParish.ToList().Count();
            code += (result + 1);
            var x = new AParish()
            {
                ParishId = code
            };

            return (code);
        }

        //Insert into Region Table
        public ActionResult DialogInsertParish([FromBody] CRUDModel<ViewParishes> value)
        {
            string result = string.Empty;

            AParish _table = context.AParish.FirstOrDefault(o =>
            (o.ParishId == value.Value.ParishId));

            if (_table == null)
            {
                //Save into AParish table
                AParish table = new AParish();

                table.ParishId = CheckParishCode();
                table.ParishName = value.Value.ParishName;
                table.SubcountyId = value.Value.SubcountyId;

                context.AParish.Add(table);
                context.SaveChanges();

            }
            else
            {
                this.DialogUpdateParish(value);
            }
            return Json(value.Value);
        }

        public ActionResult DialogUpdateParish([FromBody] CRUDModel<ViewParishes> value)
        {
            AParish table = context.AParish.FirstOrDefault(o =>
           (o.ParishId == value.Value.ParishId));

            if (table != null)
            {
                table.ParishName = value.Value.ParishName;
                table.SubcountyId = value.Value.SubcountyId;

                context.Entry(table).CurrentValues.SetValues(value);
                context.Entry(table).State = EntityState.Modified;

                context.SaveChanges();

                try
                {

                    TempData["Success"] = "Record Saved Successfully!";
                }



                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            TempData["Success"] += string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                            System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }
                }
            }
            else
            {
                this.DialogInsertParish(value);
            }

            return Json(value.Value);
        }

        //County Change
        public ActionResult getcountyparish(string DistrictId)
        {
            var county = context.ACounty.Where(o => o.DistrictId == DistrictId).AsNoTracking().ToList();
            return Json(county);
        }

        //Division Change
        public ActionResult getdivisionparish(string CountyId)
        {
            var subcounty = context.ASubcounty.Where(o => o.CountyId == CountyId).AsNoTracking().ToList();
            return Json(subcounty);
        }

        //SubCounty/Parish Change
        public ActionResult getparishparish(string SubcountyId)
        {
            var parish = context.AParish.Where(o => o.SubcountyId == SubcountyId).AsNoTracking().ToList();
            return Json(parish);
        }

        //Village Change
        public ActionResult getvillageparish(string ParishId)
        {
            var village = context.AVillage.Where(o => o.ParishId == ParishId).AsNoTracking().ToList();
            return Json(village);
        }

        #endregion Parish


        #region Village

        public IActionResult Village()
        {
            //District
            var Dist = context.ADistrict.AsNoTracking().OrderBy(a => a.DistrictId).ToList();
            ViewBag.district = Dist;

            //County
            var Count = context.ACounty.AsNoTracking().OrderBy(a => a.CountyId).ToList();
            ViewBag.county = Count;

            //SubCounty
            var SubCount = context.ASubcounty.AsNoTracking().OrderBy(a => a.SubcountyId).ToList();
            ViewBag.subcounty = SubCount;

            //Parish
            var Parish = context.AParish.AsNoTracking().OrderBy(a => a.ParishId).ToList();
            ViewBag.parish = Parish;


            return View();
        }

        public ActionResult VillageDataSource([FromBody] DataManagerRequest dm)
        {

            var _data = context.ViewVillages.AsNoTracking().OrderBy(o => o.VillageId).ToList();
            IEnumerable data = _data;
            int count = _data.Count();

            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip > 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take > 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = count }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        public IActionResult AddVillagePartial([FromBody] CRUDModel<ViewVillages> value)
        {
            //District
            var Dist = context.ADistrict.AsNoTracking().OrderBy(a => a.DistrictId).ToList();
            ViewBag.district = Dist;

            //County
            var Count = context.ACounty.AsNoTracking().OrderBy(a => a.CountyId).ToList();
            ViewBag.county = Count;

            //SubCounty
            var SubCount = context.ASubcounty.AsNoTracking().OrderBy(a => a.SubcountyId).ToList();
            ViewBag.subcounty = SubCount;

            //Parish
            var Parish = context.AParish.AsNoTracking().OrderBy(a => a.ParishId).ToList();
            ViewBag.parish = Parish;

            return PartialView("_DialogVillageAddPartial", value.Value);
        }

        public ActionResult EditVillagePartial([FromBody] CRUDModel<ViewVillages> value)
        {
            //District
            var Dist = context.ADistrict.AsNoTracking().OrderBy(a => a.DistrictId).ToList();
            ViewBag.district = Dist;

            //County
            var Count = context.ACounty.AsNoTracking().OrderBy(a => a.CountyId).ToList();
            ViewBag.county = Count;

            //SubCounty
            var SubCount = context.ASubcounty.AsNoTracking().OrderBy(a => a.SubcountyId).ToList();
            ViewBag.subcounty = SubCount;

            //Parish
            var Parish = context.AParish.AsNoTracking().OrderBy(a => a.ParishId).ToList();
            ViewBag.parish = Parish;

            return PartialView(value.Value);
        }

        public string CheckVillageCode()
        {
            string code = "0";
            int result = 0;
            result = context.AVillage.ToList().Count();
            code += (result + 1);
            var x = new AVillage()
            {
                VillageId = code
            };

            return (code);
        }

        //Insert into Region Table
        public ActionResult DialogInsertVillage([FromBody] CRUDModel<ViewVillages> value)
        {
            string result = string.Empty;

            AVillage _table = context.AVillage.FirstOrDefault(o =>
            (o.VillageId == value.Value.VillageId));

            if (_table == null)
            {
                //Save into AParish table
                AVillage table = new AVillage();

                table.VillageId = CheckVillageCode();
                table.VillageName = value.Value.VillageName;
                table.ParishId = value.Value.ParishId;
              
                context.AVillage.Add(table);
                context.SaveChanges();

            }
            else
            {
                this.DialogUpdateVillage(value);
            }
            return Json(value.Value);
        }

        public ActionResult DialogUpdateVillage([FromBody] CRUDModel<ViewVillages> value)
        {
            AVillage table = context.AVillage.FirstOrDefault(o =>
            (o.VillageId == value.Value.VillageId));

            if (table != null)
            {
                table.VillageName = value.Value.VillageName;
                table.ParishId = value.Value.ParishId;

                context.Entry(table).CurrentValues.SetValues(value);
                context.Entry(table).State = EntityState.Modified;

                context.SaveChanges();

                try
                {

                    TempData["Success"] = "Record Saved Successfully!";
                }



                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            TempData["Success"] += string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                            System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }
                }
            }
            else
            {
                this.DialogInsertVillage(value);
            }

            return Json(value.Value);
        }

        //County Change
        public ActionResult getcountyvillage(string DistrictId)
        {
            var county = context.ACounty.Where(o => o.DistrictId == DistrictId).AsNoTracking().ToList();
            return Json(county);
        }

        //Division Change
        public ActionResult getdivisionvillage(string CountyId)
        {
            var subcounty = context.ASubcounty.Where(o => o.CountyId == CountyId).AsNoTracking().ToList();
            return Json(subcounty);
        }

        //SubCounty/Parish Change
        public ActionResult getparishvillage(string SubcountyId)
        {
            var parish = context.AParish.Where(o => o.SubcountyId == SubcountyId).AsNoTracking().ToList();
            return Json(parish);
        }

        //Village Change
        public ActionResult getvillagevillage(string ParishId)
        {
            var village = context.AVillage.Where(o => o.ParishId == ParishId).AsNoTracking().ToList();
            return Json(village);
        }

        #endregion Village
    }
}