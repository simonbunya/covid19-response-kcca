﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using CHMIS.Models;
using System;
using Microsoft.EntityFrameworkCore;
using Syncfusion.EJ2;
using System.Collections;
using Syncfusion.EJ2.Base;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Http;
using System.Net.Http.Headers;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using CHMIS.BLL;


namespace CHMIS.Controllers
{
    public class PublicTransportController : Controller
    {
        private CHMISContext _context;
        private IWebHostEnvironment hostingEnv;
        private IUserService userInfor;
        public PublicTransportController(CHMISContext Context, IWebHostEnvironment env, IUserService _userinfo)
        {
            this.userInfor = _userinfo;
            this._context = Context;
            this.hostingEnv = env;
        }
        public IActionResult PublicTransport()
        {
            ViewBag.YesNo = _context.AYesNo.AsNoTracking().ToList();
            return View();
        }

        public ActionResult GetPublicTransport([FromBody] DataManagerRequest dm)
        {
            var _data = _context.CorePublicTransport.AsNoTracking().Where(a=> a.DeleteStatus != 1).ToList();
            IEnumerable data = _data;
            int count = _data.Count;

            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip > 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take > 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = count });
        }

        public ActionResult DialogueSave([FromBody] CRUDModel<CorePublicTransport> value )
        {
            if (value.Action == "insert")
            {
                _context.CorePublicTransport.Add(value.Value);
                _context.SaveChanges();
            }
            if (value.Action == "update")
            {
                CorePublicTransport code = _context.CorePublicTransport.FirstOrDefault(z => z.Code == value.Value.Code);
                _context.Entry(code).CurrentValues.SetValues(value.Value);
                _context.Entry(code).State = EntityState.Modified;

                _context.SaveChanges();
            }

            if (value.Action == "remove")
            {
                var code = _context.CorePublicTransport.FirstOrDefault(z => z.Code == value.Key.ToString());
                if (code != null)
                {
                    _context.CorePublicTransport.Remove(code);
                    _context.SaveChanges();
                }
            }
            return Json(value);
        }

        public ActionResult DetailsDatasource([FromBody] DataManagerRequest dm, string Code)
        {
            var _data = _context.CorePublicTransportDetials.Where(o => o.BusCode == Code && o.DeleteStatus != 1).AsNoTracking().ToList();
            IEnumerable data = _data;
            int count = _data.Count;

            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip > 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take > 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = count });

        }

        public ActionResult UpdateDetailsUrl([FromBody] CRUDModel<CorePublicTransportDetials> value )
        {
            if (value.Action == "insert")
            {
                _context.CorePublicTransportDetials.Add(value.Value);
                _context.SaveChanges();
            }
            if (value.Action == "update")
            {
                CorePublicTransportDetials code = _context.CorePublicTransportDetials.FirstOrDefault(z => z.BusCode == value.Value.BusCode && z.PassengerCode == value.Value.PassengerCode);
                _context.Entry(code).CurrentValues.SetValues(value.Value);
                _context.Entry(code).State = EntityState.Modified;

                _context.SaveChanges();
            }

            if (value.Action == "remove")
            {
                //var code = _context.CorePublicTransportDetials.FirstOrDefault(z => z.Code == value.Key.ToString());
                //if (code != null)
                //{
                //    _context.CorePublicTransportDetials.Remove(code);
                //    _context.SaveChanges();
                //}
            }
            return Json(value);
        }

        public IActionResult AddDetails([FromBody] CRUDModel<CorePublicTransportDetials> value)
        {
            ViewBag.YesNo = _context.AYesNo.AsNoTracking().ToList();

            if (value.Value.PassengerCode == null)
            {
                value.Value.PassengerCode = GetPassengerCode(value.Value.BusCode);
            }
            return PartialView("_DialogAddPassenger", value.Value);
        }
        public string GetPassengerCode(string spyc)
        {
            int result = 0;
            result = _context.CorePublicTransportDetials.ToList().Count();
            string code2 = spyc + "-PASSENGER-" + "-";
            code2 += (result + 1);
            var act = new CorePublicTransportDetials()
            { 
                PassengerCode = code2,
                BusCode = spyc,
                AddedBy = userInfor.GetFullName(),
                DateAdded = DateTime.Now
            };
            _context.CorePublicTransportDetials.Add(act);
            _context.SaveChanges();
            return code2;
        }

        public IActionResult AddPublicTransport([FromBody] CRUDModel<CorePublicTransport> value)
        {
            ViewBag.YesNo = _context.AYesNo.AsNoTracking().ToList();
            if (value.Value.Code == null)
            {
                value.Value.Code = GetCode();
            }
            return PartialView("_DialogPartial", value.Value);
        }
        public string GetCode()
        {
            DateTime _date = System.DateTime.Now;
            int year = _date.Year;
            string month = _date.Month.ToString();
            if (month.Length < 2)
                month = "0" + month;

            string day = _date.Day.ToString();
            if (day.Length < 2)
                day = "0" + day;

            //string partial_no = "";
            string partial_no = year + "-" + month + "-" + day;//Day + Month + Year

            string _disease = string.Empty;
            var disease = _context.ADiseaseType.FirstOrDefault(o => o.DiseaseId == userInfor.getCurrentuserDisease());
            if (disease != null)
            {
                _disease = disease.Disease;
            }

            string code = _disease + "-PUBLIC-TRANSPORT-" + userInfor.GetEmployeeID() + "-" + partial_no + "- 00";

            int result = 0;
            result = _context.CorePublicTransport.ToList().Count();
            code += (result + 1);
            var x = new CorePublicTransport()
            {
                Code = code,
                AddedBy = userInfor.GetFullName(),
                DateAdded = DateTime.Now
            };
            _context.CorePublicTransport.Add(x);
            _context.SaveChanges();

            return code;
        }

        public ActionResult DialogUpdate([FromBody] CRUDModel<CorePublicTransport> value)
        {
            CorePublicTransport table = _context.CorePublicTransport.FirstOrDefault(o => o.Code == value.Value.Code);

            if (table != null)
            {
                table.TransportDate = value.Value.TransportDate;
                table.BusCompany = value.Value.BusCompany;
                table.BusNo = value.Value.BusNo;
                table.Origin = value.Value.Origin;
                table.Destination = value.Value.Destination;
                table.DriverName = value.Value.DriverName;
                table.DriverTel = value.Value.DriverTel;
                table.DriverAddress = value.Value.DriverAddress;
                table.ConductorName = value.Value.ConductorName;
                table.ConductorTel = value.Value.ConductorTel;
                table.ConductorAddress = value.Value.ConductorAddress;
                table.GeneralComments = value.Value.GeneralComments;
                table.DateEdited = DateTime.Now;
                table.EditedBy = userInfor.GetFullName();  //value.Value.CreatedBy;
                table.Disease = userInfor.getCurrentuserDisease();// value.Value.Disease;
               
                _context.Entry(table).CurrentValues.SetValues(value);
                _context.Entry(table).State = EntityState.Modified;

                try
                {
                    // Your code...
                    // Could also be before try if you know the exception occurs in SaveChanges
                    _context.SaveChanges();

                    TempData["Success"] = "Record Saved Successfully!";
                }
                catch
                {
                    //foreach (var validationErrors in dbEx.EntityValidationErrors)
                    //{
                    //    foreach (var validationError in validationErrors.ValidationErrors)
                    //    {
                    //        TempData["Success"] += string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    //        System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    //    }
                    //}
                }
            }

            return Json(value.Value);
        }

        public ActionResult DialogInsert([FromBody] CRUDModel<CorePublicTransport> value)
        {
            CorePublicTransport _table = _context.CorePublicTransport.FirstOrDefault(o => o.Code == value.Value.Code);
            //EmailJob obx = new EmailJob();
            if (_table == null)
            {
                CorePublicTransport table = new CorePublicTransport();
                table.Code = GetCode();// value.Value.CaseNo;
                table.TransportDate = value.Value.TransportDate;
                table.BusCompany = value.Value.BusCompany;
                table.BusNo = value.Value.BusNo;
                table.Origin = value.Value.Origin;
                table.Destination = value.Value.Destination;
                table.DriverName = value.Value.DriverName;
                table.DriverTel = value.Value.DriverTel;
                table.DriverAddress = value.Value.DriverAddress;
                table.ConductorName = value.Value.ConductorName;
                table.ConductorTel = value.Value.ConductorTel;
                table.ConductorAddress = value.Value.ConductorAddress;
                table.GeneralComments = value.Value.GeneralComments;
                table.Disease = userInfor.getCurrentuserDisease();// value.Value.Disease;
                table.DateAdded = DateTime.Now;
                table.AddedBy = userInfor.GetFullName();  //value.Value.CreatedBy;
               
                _context.CorePublicTransport.Add(table);
                _context.SaveChanges();
            }
            else
            {
                this.DialogUpdate(value);
            }
            return Json(value.Value);
        }

        public ActionResult DialogPassengerUpdate([FromBody] CRUDModel<CorePublicTransportDetials> value)
        {
            CorePublicTransportDetials table = _context.CorePublicTransportDetials.FirstOrDefault(o => o.BusCode == value.Value.BusCode && o.PassengerCode == value.Value.PassengerCode);

            if (table != null)
            {
                table.NationalId = value.Value.NationalId;
                table.Tel = value.Value.Tel;
                table.WhereFrom = value.Value.WhereFrom;
                table.WhereTo = value.Value.WhereTo;
                table.WearingMask = value.Value.WearingMask;
                table.Temperature = value.Value.Temperature;
                table.Remarks = value.Value.Remarks;
                table.Comments = value.Value.Comments;
                table.DateEdited = DateTime.Now;
                table.EditedBy = userInfor.GetFullName();  //value.Value.CreatedBy;
                table.Disease = userInfor.getCurrentuserDisease();// value.Value.Disease;

                _context.Entry(table).CurrentValues.SetValues(value);
                _context.Entry(table).State = EntityState.Modified;

                try
                {
                    // Your code...
                    // Could also be before try if you know the exception occurs in SaveChanges
                    _context.SaveChanges();

                    TempData["Success"] = "Record Saved Successfully!";
                }
                catch
                {
                    //foreach (var validationErrors in dbEx.EntityValidationErrors)
                    //{
                    //    foreach (var validationError in validationErrors.ValidationErrors)
                    //    {
                    //        TempData["Success"] += string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    //        System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    //    }
                    //}
                }
            }

            return Json(value.Value);
        }

        public ActionResult DialogPassengerInsert([FromBody] CRUDModel<CorePublicTransportDetials> value)
        {
            CorePublicTransportDetials _table = _context.CorePublicTransportDetials.FirstOrDefault(o => o.BusCode == value.Value.BusCode && o.PassengerCode == value.Value.PassengerCode);
            //EmailJob obx = new EmailJob();
            if (_table == null)
            {
                CorePublicTransportDetials table = new CorePublicTransportDetials();
                //table.PassengerCode = GetPassengerCode(value.Value.Code);
                table.BusCode =  value.Value.BusCode;
                table.NationalId = value.Value.NationalId;
                table.Tel = value.Value.Tel;
                table.WhereFrom = value.Value.WhereFrom;
                table.WhereTo = value.Value.WhereTo;
                table.WearingMask = value.Value.WearingMask;
                table.Temperature = value.Value.Temperature;
                table.Remarks = value.Value.Remarks;
                table.Comments = value.Value.Comments;
                table.Disease = userInfor.getCurrentuserDisease();
                table.DateAdded = DateTime.Now;
                table.AddedBy = userInfor.GetFullName();  //value.Value.CreatedBy;

                _context.CorePublicTransportDetials.Add(table);
                _context.SaveChanges();
            }
            else
            {
                this.DialogPassengerUpdate(value);
            }
            return Json(value.Value);
        }

        public ActionResult RemoveRecordChild(string BusCode, string PassengerCode)
        {
            _context.CorePublicTransportDetials.Remove(_context.CorePublicTransportDetials.FirstOrDefault(o => o.BusCode == BusCode && o.PassengerCode == PassengerCode));
            int result = _context.SaveChanges();

            return Json(result);
        }

        public ActionResult RemoveRecord(string Code)
        {
            _context.CorePublicTransport.Remove(_context.CorePublicTransport.FirstOrDefault(o => o.Code == Code));
            int result = _context.SaveChanges();

            return Json(result);
        }

        public ActionResult DeleteChildUrl([FromBody] CRUDModel<CorePublicTransportDetials> value)
        {
            string obj = Request.Headers["additional_key"]; //key1 
            string obj1 = Request.Headers["additional_key1"]; //key2 
            CorePublicTransportDetials table = _context.CorePublicTransportDetials.FirstOrDefault(or => or.BusCode + or.PassengerCode == obj + obj1);
            if (table != null)
            {
                if (value.Deleted != null)
                {
                    foreach (var temp in value.Deleted)
                    {
                        temp.DeleteStatus = 1;
                        temp.DeletedBy = userInfor.GetFullName();
                        temp.DateDeleted = System.DateTime.Now;
                        _context.Entry(temp).State = EntityState.Modified;
                        try
                        {
                            _context.SaveChanges();
                        }
                        catch (Exception dbEx)
                        {
                            throw (dbEx);
                        }
                    }
                }
                else
                {
                    table.DeleteStatus = 1;
                    table.DeletedBy = userInfor.GetFullName();
                    table.DateDeleted = System.DateTime.Now;
                    _context.Entry(table).State = EntityState.Modified;
                    try
                    {
                        _context.SaveChanges();
                    }
                    catch (Exception dbEx)
                    {
                        throw (dbEx);
                    }
                }
            }
            //var msg = "Contact Case No. " + obj + " Has been Deleted Successfully !!";
            return Json(value);

        }

        public ActionResult DeleteUrl([FromBody] CRUDModel<CorePublicTransport> value)
        {
            string obj = Request.Headers["additional_key"]; //key1 
            //string obj1 = Request.Headers["additional_key1"]; //key2 
            CorePublicTransport table = _context.CorePublicTransport.FirstOrDefault(or => or.Code == obj);
            //_context.Remove(table);
            //_context.SaveChanges();
            if (table != null)
            {
                if (value.Deleted != null)
                {
                    foreach (var temp in value.Deleted)
                    {
                        temp.DeleteStatus = 1;
                        temp.DeletedBy = userInfor.GetFullName();
                        temp.DateDeleted = System.DateTime.Now;
                        _context.Entry(temp).State = EntityState.Modified;
                        try
                        {
                            _context.SaveChanges();
                        }
                        catch (Exception dbEx)
                        {
                            throw (dbEx);
                        }
                    }
                }
                else
                {
                    table.DeleteStatus = 1;
                    table.DeletedBy = userInfor.GetFullName();
                    table.DateDeleted = System.DateTime.Now;
                    _context.Entry(table).State = EntityState.Modified;
                    try
                    {
                        _context.SaveChanges();
                    }
                    catch (Exception dbEx)
                    {
                        throw (dbEx);
                    }
                }
            }
            //var msg = "Case No. " + obj + " Has been Deleted Successfully !!";
            return Json(value);
        }
    }
}
