﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using CHMIS.Models;
using System;
using Microsoft.EntityFrameworkCore;
using Syncfusion.EJ2;
using System.Collections;
using Syncfusion.EJ2.Base;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Http;
using System.Net.Http.Headers;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using CHMIS.BLL;
using System.Data.Entity.Validation;
using System.Globalization;
//using System.Web.Services;

namespace CHMIS.Controllers
{
    public class SitRepController : Controller
    {
        private CHMISContext _context;
        private IWebHostEnvironment hostingEnv;
        private IUserService userInfor;

        public SitRepController(CHMISContext Context, IWebHostEnvironment env, IUserService _userinfo)
        {
            this.userInfor = _userinfo;
            this._context = Context;
            this.hostingEnv = env;
        }
        public IActionResult SitRep()
        {
            ViewBag.division = _context.ACounty.ToList();
            return View();
        }
        public IActionResult SitRepGrid()
        {
            ViewBag.division = _context.ACounty.ToList();
            return View();
        }
        //Partial View for adding a new sitrep
        public IActionResult AddPartial([FromBody] CRUDModel<ViewSitRepAdd> value)
        {
            //TempData["ReportingDate"] =DateTime.Now.Date;
            ViewBag.division = _context.ACounty.ToList();
            //get date
            if(value.Value.ReportingDate == null)
            {
                value.Value.ReportingDate = DateTime.Now.Date;
            }
             
            // Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd")).Date;
            return PartialView("_DialogAddPartial", value.Value);
        }
        public ActionResult GetSitRep([FromBody] DataManagerRequest dm, string ReportingDate, string division)
        {
            string format = "dd/MM/yyyy";
            CultureInfo current = new CultureInfo("en-US");
            DateTime _date = DateTime.Now;
            IEnumerable data=null;
            int count =0;
            
            if (ReportingDate != null)
            {
                if (ReportingDate == "01/01/1970")
                {
                    ReportingDate = DateTime.Now.ToString();
                }
                DateTime edate = DateTime.Now;
                //var _ReportingDate = ReportingDate.ToString(format);
                //edate = Convert.ToDateTime(ReportingDate);
                if (DateTime.TryParseExact(ReportingDate, format, null, DateTimeStyles.None, out _date))
                {
                    edate = _date;
                }
                
                //edate = Convert.ToDateTime(ReportingDate);
                var _data = _context.ViewSitRepAdd.Where(o => o.ReportingDate.Value.Date == edate.Date && o.Division == division).OrderBy(o => o.SitRepCategoryId).ToList();
                if (_data.Count == 0)
                {
                    _data = _context.ViewSitRepAdd.FromSqlRaw("EXECUTE spView_SitRep_Edit {0},{1}", division, edate.Date).AsEnumerable().OrderBy(o => o.SitRepCategoryId).ToList();
                }
                else
                {
                    var xdata = _context.ViewSitRepAdd.FromSqlRaw("EXECUTE spView_SitRep_Edit {0},{1}", division, edate.Date).AsEnumerable().OrderBy(o => o.SitRepCategoryId).ToList();
                    _data.AddRange(xdata.Where(o => _data.Any(x => x.SitRepCategoryId == o.SitRepCategoryId) == false));
                }
                data = _data.OrderBy(o => o.SitRepCategoryId);
                count = _data.Count();
            }
            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip != 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take != 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = count }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        public ActionResult GetData([FromBody] DataManagerRequest dm)
        {
            IEnumerable data = null;
            int count = 0;

            {
                var _data = _context.ViewSitRepAdd.OrderBy(o => o.SitRepCategoryId).ToList();
                //if (_data.Count == 0)
                //{
                //    _data = _context.ViewSitRepAdd.FromSqlRaw("EXECUTE spView_SitRep_Edit {0},{1}", division, edate.Date).AsEnumerable().OrderBy(o => o.SitRepCategoryId).ToList();
                //}
                //else
                //{
                //    var xdata = _context.ViewSitRepAdd.FromSqlRaw("EXECUTE spView_SitRep_Edit {0},{1}", division, edate.Date).AsEnumerable().OrderBy(o => o.SitRepCategoryId).ToList();
                //    _data.AddRange(xdata.Where(o => _data.Any(x => x.SitRepCategoryId == o.SitRepCategoryId) == false));
                //}
                data = _data.OrderBy(o => o.SitRepCategoryId);
                count = _data.Count();
            }
            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip != 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take != 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = count }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        //get data

        public ActionResult GetSitRepNewData([FromBody] DataManagerRequest dm)
        {
            IEnumerable data = null;
            int count = 0;
            //var _data = _context.ViewSitRepRecords.OrderBy(o => o.ReportingDate).ToList();

            if ((User.IsInRole("Super Admin")) || (User.IsInRole("Administrator")) || (User.IsInRole("KCCA General")) || (User.IsInRole("Viewer")))
            {
                var _data = _context.CoreSitRepHeader.ToList();
                data = _data.OrderByDescending(o => o.ReportingDate);
                count = _data.Count();
            }
            else
            {
                var _data = _context.CoreSitRepHeader.Where(a => a.AddedBy == userInfor.GetFullName()).ToList();
                data = _data.OrderByDescending(o => o.ReportingDate);
                count = _data.Count();
            }
           
            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip != 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take != 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = count }, new Newtonsoft.Json.JsonSerializerSettings());
        }
        public ActionResult GetSitRepData([FromBody] DataManagerRequest dm, string ReportingDate, string division)
        {
            string format = "dd/MM/yyyy";
            CultureInfo current = new CultureInfo("en-US");
            DateTime _date = DateTime.Now;
            IEnumerable data = null;
            int count = 0;
            DateTime edate = DateTime.Now;
            if (ReportingDate != null)
            {
                edate = Convert.ToDateTime(ReportingDate);

                if ((User.IsInRole("Super Admin")) || (User.IsInRole("Administrator")) || (User.IsInRole("KCCA General")) || (User.IsInRole("Viewer")))
                {
                    var _data = _context.ViewSitRepRecords.Where(o => o.ReportingDate.Date == edate.Date && o.Division == division).OrderBy(o => o.ReportingDate).ToList();
                    data = _data.OrderBy(o => o.ReportingDate);
                    count = _data.Count();
                }
                else 
                {
                    var _data = _context.ViewSitRepRecords.Where(o => o.ReportingDate.Date == edate.Date && o.Division == division && o.AddedBy == userInfor.GetFullName()).OrderBy(o => o.ReportingDate).ToList();
                    data = _data.OrderBy(o => o.ReportingDate);
                    count = _data.Count();
                }
            }
            DataOperations operation = new DataOperations();
            //Performing filtering operation
            if (dm.Where != null)
            {
                data = operation.PerformFiltering(data, dm.Where, "and");
                var filtered = (IEnumerable<object>)data;
                count = filtered.Count();
            }
            //Performing search operation
            if (dm.Search != null)
            {
                data = operation.PerformSearching(data, dm.Search);
                var searched = (IEnumerable<object>)data;
                count = searched.Count();
            }
            //Performing sorting operation
            if (dm.Sorted != null)
                data = operation.PerformSorting(data, dm.Sorted);

            //Performing paging operations
            if (dm.Skip != 0)
                data = operation.PerformSkip(data, dm.Skip);
            if (dm.Take != 0)
                data = operation.PerformTake(data, dm.Take);

            return Json(new { result = data, count = count }, new Newtonsoft.Json.JsonSerializerSettings());
        }

        public ActionResult UpdateSitRep([FromBody] CRUDModel<ViewSitRepAdd> value)
        {
            string format = "dd/MM/yyyy";
            CultureInfo current = new CultureInfo("en-US");
            DateTime _date = DateTime.Now;
            DateTime _date2 = DateTime.Now;

            string ReportingDate = string.Empty;
            if(TempData["ReportingDate"] != null)
            {
                ReportingDate = TempData["ReportingDate"].ToString();
            }
            //Performing insert operation
            if (value.Added != null && value.Added.Count() > 0)
            {
                foreach (var temp in value.Added)
                {
                    var timediff = 0;
                    if (temp.ReportingDate.Value.Hour == 0)
                    {
                    }
                    else
                    {
                        var tt = temp.ReportingDate.Value.Hour;
                        timediff = 24 - tt;
                        temp.ReportingDate = temp.ReportingDate.Value.AddHours(timediff);
                    }
                    CoreSitRep x = new CoreSitRep()
                    {
                        SitRepCategoryId = temp.SitRepCategoryId.Value,
                        Division = temp.Division,
                        ReportingDate = temp.ReportingDate.Value.AddHours(timediff).Date,
                        ValueResponse = temp.ValueResponse,
                        GeneralComments = temp.GeneralComments,
                        DateofConfirmation = temp.DateofConfirmation,
                        UpdateComments = temp.UpdateComments,
                        DateAdded = DateTime.Now,
                        AddedBy = userInfor.GetFullName(),
                        DateEdited = DateTime.Now,
                        EditedBy = userInfor.GetFullName(),
                    };
                    var exists = _context.CoreSitRep.FirstOrDefault(o => o.ReportingDate.Date == temp.ReportingDate.Value.Date && o.Division == temp.Division && o.SitRepCategoryId == temp.SitRepCategoryId);
                    if(exists == null)
                    {
                        _context.CoreSitRep.Add(x);
                    }
                    else
                    {
                        _context.Entry(exists).CurrentValues.SetValues(x);
                        _context.Entry(exists).State = EntityState.Modified;
                    }
                    _context.SaveChanges();
                }
            }

            ////Performing update operation
            if (value.Changed != null && value.Changed.Count() > 0)
            {
                foreach (var temp in value.Changed)
                {
                    var timediff=0;
                    if (temp.ReportingDate.Value.Hour == 0)
                    {
                        temp.ReportingDate = temp.ReportingDate.Value.Date;
                    }
                    else
                    {
                        var tt = temp.ReportingDate.Value.Hour;
                        timediff = 24 - tt;
                        temp.ReportingDate = temp.ReportingDate.Value.AddHours(timediff).Date;
                    }

                    //if (ReportingDate != null && !string.IsNullOrEmpty(ReportingDate))
                    //{
                    //    DateTime edate = DateTime.Now;
                    //    if (DateTime.TryParseExact(ReportingDate, format, current, DateTimeStyles.None, out _date))
                    //    {
                    //        edate = _date;
                    //    }
                    //    if (_date.Hour == 0)
                    //    {
                    //        temp.ReportingDate = _date.Date;
                    //    }
                    //    else
                    //    {
                    //        var tt = _date.Hour;
                    //        timediff = 24 - tt;
                    //        temp.ReportingDate = _date.AddHours(timediff).Date;
                    //    }
                    //}

                    if(temp.Category != null)
                    {
                        if (temp.Category.All(Char.IsDigit))
                        {
                            temp.ValueResponse = temp.Category;
                        }
                    }
                    
                    CoreSitRep x = new CoreSitRep()
                    {
                        SitRepCategoryId = temp.SitRepCategoryId.Value,
                        Division = temp.Division,
                        ReportingDate = temp.ReportingDate.Value.AddHours(timediff).Date,
                        ValueResponse = temp.ValueResponse,
                        GeneralComments = temp.GeneralComments,
                        UpdateComments = temp.UpdateComments,
                        DateofConfirmation = temp.DateofConfirmation,
                        DateEdited = DateTime.Now,
                        EditedBy = userInfor.GetFullName(),
                        DateAdded = DateTime.Now,
                        AddedBy = userInfor.GetFullName(),
                    };

                    //var exists = _context.CoreSitRep.FirstOrDefault(o => o.ReportingDate.Date == Convert.ToDateTime(ReportingDate).Date && o.Division == temp.Division && o.SitRepCategoryId == temp.SitRepCategoryId);
                    var exists = _context.CoreSitRep.FirstOrDefault(o => o.ReportingDate.Date == temp.ReportingDate.Value.Date && o.Division == temp.Division && o.SitRepCategoryId == temp.SitRepCategoryId);
                    if (exists == null)
                    {
                        _context.CoreSitRep.Add(x);
                    }
                    else
                    {
                        _context.Entry(exists).CurrentValues.SetValues(x);
                        _context.Entry(exists).State = EntityState.Modified;
                    }
                    _context.SaveChanges();
                }
            }

            //Performing delete operation
            //if (value.Deleted != null && value.Deleted.Count() > 0)
            //{
            //    foreach (var temp in value.Deleted)
            //    {
            //        _context.NtbFocalPerson.Remove(temp);
            //        _context.SaveChanges();
            //    }
            //}

            return Json(value);
            //return RedirectToAction("SitRepGrid");
        }

        private int GetInteger(string valueResponse)
        {
            int result=0;
            if (!string.IsNullOrEmpty(valueResponse))
            {
                result = Convert.ToInt32(valueResponse);
            }
            return result;
        }

        //[WebMethod]
        public ActionResult BatchDataKount(DataManager dataManager)
        {
            IEnumerable DataSource = _context.ViewSitRepEdit.ToList();
            DataResult result = new DataResult();
            result.result = DataSource;
            result.count = _context.ViewSitRepEdit.Count();

            return Json(result.count);
        }
        public class DataResult
        {
            public IEnumerable result { get; set; }
            public int count { get; set; }
        }
        //[HttpPost]
        public int SaveSitRepHeader(string ReportingDate, string Division, string DateofConfirmation, string WashComments, string RiskComments, string UpdateComments, string GeneralComments )
        {
            TempData["ReportingDate"] = ReportingDate;
            CoreSitRepHeader obj = new CoreSitRepHeader();
            string format = "dd/MM/yyyy";
            CultureInfo current = new CultureInfo("en-US");
            DateTime _date = DateTime.Now;
            DateTime _date2 = DateTime.Now;
            int recstat = 0;
            if (ReportingDate != null && !string.IsNullOrEmpty(ReportingDate))
            {
                DateTime edate = DateTime.Now;
                if (DateTime.TryParseExact(ReportingDate, format, current, DateTimeStyles.None, out _date))
                {
                    edate = _date;
                }
                //obj.ReportingDate = edate.Date; ;
                var timediff = 0;
                if (_date.Hour == 0)
                {
                    obj.ReportingDate = _date.Date;
                }
                else
                {
                    var tt = _date.Hour;
                    timediff = 24 - tt;
                    obj.ReportingDate = _date.AddHours(timediff).Date;
                }
            }
            if (DateofConfirmation != null && !string.IsNullOrEmpty(DateofConfirmation))
            {
                DateTime edate2 = DateTime.Now;
                if (DateTime.TryParseExact(DateofConfirmation, format, null, DateTimeStyles.None, out _date2))
                {
                    edate2 = _date2;
                }
                //obj.DateofConfirmation = edate2;
                var timediff = 0;
                if (_date.Hour == 0)
                {
                    obj.DateofConfirmation = _date2.Date;
                }
                else
                {
                    var tt = _date2.Hour;
                    timediff = 24 - tt;
                    obj.DateofConfirmation = _date2.AddHours(timediff).Date;
                }
            }
            obj.Division = Division;
            obj.WashComments = WashComments;
            obj.RiskComments = RiskComments;
            obj.UpdateComments = UpdateComments;
            obj.GeneralComments = GeneralComments;

            try
            {
                //Check for the existance of the record
                var ca = _context.CoreSitRepHeader.FirstOrDefault(c => c.Division == Division && c.ReportingDate == System.Convert.ToDateTime(ReportingDate));

                if (ca == null)
                {
                    _context.CoreSitRepHeader.Add(obj);
                    obj.DateAdded = DateTime.Now;
                    obj.AddedBy = userInfor.GetFullName();
                    obj.DateEdited = DateTime.Now;
                    obj.EditedBy = userInfor.GetFullName();
                    recstat = _context.SaveChanges();
                }
                else
                {
                    CoreSitRepHeader table = _context.CoreSitRepHeader.FirstOrDefault(o => o.Division == ca.Division && o.ReportingDate == ca.ReportingDate);
                    obj.DateEdited = DateTime.Now;
                    obj.EditedBy = userInfor.GetFullName();
                    obj.DateAdded = DateTime.Now;
                    obj.AddedBy = userInfor.GetFullName();
                    _context.Entry(table).CurrentValues.SetValues(obj);
                    _context.Entry(table).State = EntityState.Modified;
                    recstat = _context.SaveChanges();
                }

            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }

            return recstat;
        }

        public ActionResult DialogUpdate(CoreSitRepHeader value)
        {
            TempData["ReportingDate"] = value.ReportingDate;
            CoreSitRepHeader table = _context.CoreSitRepHeader.FirstOrDefault(o => o.Division == value.Division && o.ReportingDate == value.ReportingDate.Date);
            value.DateAdded = DateTime.Now;
            value.AddedBy = userInfor.GetFullName();
            value.DateEdited = DateTime.Now;
            value.EditedBy = userInfor.GetFullName();
            _context.Entry(table).CurrentValues.SetValues(value);
            _context.Entry(table).State = EntityState.Modified;
            _context.SaveChanges();
            return Json(value);
        }
        //[HttpPost]
        public ActionResult DialogInsert([FromBody] CRUDModel<CoreSitRepHeader>  value)
        {
            TempData["ReportingDate"] = value.Value.ReportingDate;
           
            var timediff = 0;
            if (value.Value.ReportingDate.Hour == 0)
            {
                value.Value.ReportingDate = value.Value.ReportingDate.Date;
            }
            else
            {
                var tt = value.Value.ReportingDate.Hour;
                timediff = 24 - tt;
                value.Value.ReportingDate = value.Value.ReportingDate.AddHours(timediff).Date;
            }

            //Check for the existance of the record
            var ca = _context.CoreSitRepHeader.FirstOrDefault(o => o.Division == value.Value.Division && o.ReportingDate == value.Value.ReportingDate.Date);

            if (ca == null)
            {
               
                //_context.CoreSitRepHeader.Add(value.Value);
                CoreSitRepHeader x = new CoreSitRepHeader()
                {
                    Division = value.Value.Division,
                    ReportingDate = value.Value.ReportingDate.AddHours(timediff).Date, //DateTime.Now.Date, //value.Value.ReportingDate.AddHours(timediff).Date,
                    GeneralComments = value.Value.GeneralComments,
                    UpdateComments = value.Value.UpdateComments,
                    WashComments = value.Value.WashComments,
                    RiskComments = value.Value.RiskComments,
                    DateofConfirmation = DateTime.Now,//temp.DateofConfirmation,
                    //DateEdited = DateTime.Now,
                    //EditedBy = userInfor.GetFullName(),
                    DateAdded = DateTime.Now,
                    AddedBy = userInfor.GetFullName(),
                };
                
                //value.Value.DateAdded = DateTime.Now;
                //value.Value.AddedBy = userInfor.GetFullName();
                //value.Value.DateEdited = DateTime.Now;
                //value.Value.EditedBy = userInfor.GetFullName();
                _context.CoreSitRepHeader.Add(x);
            }
            else
            {
                CoreSitRepHeader table = _context.CoreSitRepHeader.FirstOrDefault(o => o.Division == ca.Division && o.ReportingDate == ca.ReportingDate);
                value.Value.DateEdited = DateTime.Now;
                value.Value.EditedBy = userInfor.GetFullName();
                //value.Value.DateAdded = DateTime.Now;
                //value.Value.AddedBy = userInfor.GetFullName();
                _context.Entry(table).CurrentValues.SetValues(value);
                _context.Entry(table).State = EntityState.Modified;
            }

            try
            {
                _context.SaveChanges();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            return Json(value);
        }


        public ActionResult BatchUpdateSitRep(string key, List<ViewSitRepAdd> changed, List<ViewSitRepAdd> added, List<ViewSitRepAdd> deleted)
        {
            string format = "dd/MM/yyyy";
            CultureInfo current = new CultureInfo("en-US");
            DateTime _date = DateTime.Now;
            DateTime _date2 = DateTime.Now;

            string ReportingDate = string.Empty;
            if (TempData["ReportingDate"] != null)
            {
                ReportingDate = TempData["ReportingDate"].ToString();
            }

            //Performing insert operation
            if (added != null && added.Count() > 0)
            {
                foreach (var temp in added)
                {
                    var timediff = 0;
                    if (temp.ReportingDate.Value.Hour == 0)
                    {
                        temp.ReportingDate = temp.ReportingDate.Value.Date;
                    }
                    else
                    {
                        var tt = temp.ReportingDate.Value.Hour;
                        timediff = 24 - tt;
                        temp.ReportingDate = temp.ReportingDate.Value.AddHours(timediff).Date;
                    }

                    if (ReportingDate != null && !string.IsNullOrEmpty(ReportingDate))
                    {
                        DateTime edate = DateTime.Now;
                        if (DateTime.TryParseExact(ReportingDate, format, current, DateTimeStyles.None, out _date))
                        {
                            edate = _date;
                        }
                        if (_date.Hour == 0)
                        {
                            temp.ReportingDate = _date.Date;
                        }
                        else
                        {
                            var tt = _date.Hour;
                            timediff = 24 - tt;
                            temp.ReportingDate = _date.AddHours(timediff).Date;
                        }
                    }

                    if (temp.Category != null)
                    {
                        if (temp.Category.All(Char.IsDigit))
                        {
                            temp.ValueResponse = temp.Category;
                        }
                    }

                    CoreSitRep x = new CoreSitRep()
                    {
                        SitRepCategoryId = temp.SitRepCategoryId.Value,
                        Division = temp.Division,
                        ReportingDate = temp.ReportingDate.Value.AddHours(timediff).Date,
                        ValueResponse = temp.ValueResponse,
                        GeneralComments = temp.GeneralComments,
                        UpdateComments = temp.UpdateComments,
                        DateofConfirmation = temp.DateofConfirmation,
                        DateEdited = DateTime.Now,
                        EditedBy = userInfor.GetFullName(),
                        DateAdded = DateTime.Now,
                        AddedBy = userInfor.GetFullName(),
                    };

                    var exists = _context.CoreSitRep.FirstOrDefault(o => o.ReportingDate.Date == Convert.ToDateTime(ReportingDate).Date && o.Division == temp.Division && o.SitRepCategoryId == temp.SitRepCategoryId);
                    if (exists == null)
                    {
                        _context.CoreSitRep.Add(x);
                    }
                    else
                    {
                        _context.Entry(exists).CurrentValues.SetValues(x);
                        _context.Entry(exists).State = EntityState.Modified;
                    }
                    _context.SaveChanges();
                }
            }

            ////Performing update operation
            if (changed != null && changed.Count() > 0)
            {
                foreach (var temp in changed)
                {
                    var timediff = 0;
                    if (temp.ReportingDate.Value.Hour == 0)
                    {
                        temp.ReportingDate = temp.ReportingDate.Value.Date;
                    }
                    else
                    {
                        var tt = temp.ReportingDate.Value.Hour;
                        timediff = 24 - tt;
                        temp.ReportingDate = temp.ReportingDate.Value.AddHours(timediff).Date;
                    }

                    if (ReportingDate != null && !string.IsNullOrEmpty(ReportingDate))
                    {
                        DateTime edate = DateTime.Now;
                        if (DateTime.TryParseExact(ReportingDate, format, current, DateTimeStyles.None, out _date))
                        {
                            edate = _date;
                        }
                        if (_date.Hour == 0)
                        {
                            temp.ReportingDate = _date.Date;
                        }
                        else
                        {
                            var tt = _date.Hour;
                            timediff = 24 - tt;
                            temp.ReportingDate = _date.AddHours(timediff).Date;
                        }
                    }

                    if (temp.Category != null)
                    {
                        if (temp.Category.All(Char.IsDigit))
                        {
                            temp.ValueResponse = temp.Category;
                        }
                    }

                    CoreSitRep x = new CoreSitRep()
                    {
                        SitRepCategoryId = temp.SitRepCategoryId.Value,
                        Division = temp.Division,
                        ReportingDate = temp.ReportingDate.Value.AddHours(timediff).Date,
                        ValueResponse = temp.ValueResponse,
                        GeneralComments = temp.GeneralComments,
                        UpdateComments = temp.UpdateComments,
                        DateofConfirmation = temp.DateofConfirmation,
                        DateEdited = DateTime.Now,
                        EditedBy = userInfor.GetFullName(),
                        DateAdded = DateTime.Now,
                        AddedBy = userInfor.GetFullName(),
                    };

                    var exists = _context.CoreSitRep.FirstOrDefault(o => o.ReportingDate.Date == Convert.ToDateTime(ReportingDate).Date && o.Division == temp.Division && o.SitRepCategoryId == temp.SitRepCategoryId);
                    if (exists == null)
                    {
                        _context.CoreSitRep.Add(x);
                    }
                    else
                    {
                        _context.Entry(exists).CurrentValues.SetValues(x);
                        _context.Entry(exists).State = EntityState.Modified;
                    }
                    _context.SaveChanges();
                }
            }

            return RedirectToAction("SitRepGrid");
        }

    }
}
