﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ASitRepVariable
    {
        public ASitRepVariable()
        {
            ASitRepCategory = new HashSet<ASitRepCategory>();
        }

        public int SitRepId { get; set; }
        public string SitRep { get; set; }

        public virtual ICollection<ASitRepCategory> ASitRepCategory { get; set; }
    }
}
