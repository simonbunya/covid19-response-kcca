﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class CoreContactsFileAttachment
    {
        public int Id { get; set; }
        public string CaseNo { get; set; }
        public byte[] FileUrl { get; set; }
        public string FileType { get; set; }
        public string Details { get; set; }
        public DateTime? DateAdded { get; set; }
        public string AddedBy { get; set; }
        public DateTime? DateEdited { get; set; }
        public string EditedBy { get; set; }
        public DateTime? DateDeleted { get; set; }
        public string DeletedBy { get; set; }
    }
}
