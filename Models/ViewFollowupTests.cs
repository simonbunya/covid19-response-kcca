﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ViewFollowupTests
    {
        public string CaseNo { get; set; }
        public string ContactCaseNo { get; set; }
        public int? FacilityUnit { get; set; }
        public int? AssignFollowUpContactPerson { get; set; }
        public int Code { get; set; }
        public DateTime? LabTestDate { get; set; }
        public int? LabResults { get; set; }
        public string Hospital { get; set; }
        public string GeneralComments { get; set; }
        public DateTime? DateAdded { get; set; }
        public string AddedBy { get; set; }
        public DateTime? DateEdited { get; set; }
        public string EditedBy { get; set; }
        public DateTime? DateDeleted { get; set; }
        public string DeletedBy { get; set; }
        public int? DeleteStatus { get; set; }
    }
}
