﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class AElevateContact
    {
        public AElevateContact()
        {
            CoreCaseContacts = new HashSet<CoreCaseContacts>();
            CoreCases = new HashSet<CoreCases>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<CoreCaseContacts> CoreCaseContacts { get; set; }
        public virtual ICollection<CoreCases> CoreCases { get; set; }
    }
}
