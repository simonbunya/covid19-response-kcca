﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ACountry
    {
        public ACountry()
        {
            AEmployees = new HashSet<AEmployees>();
        }

        public string CountryCode { get; set; }
        public int? CountryId { get; set; }
        public string CountryName { get; set; }

        public virtual ICollection<AEmployees> AEmployees { get; set; }
    }
}
