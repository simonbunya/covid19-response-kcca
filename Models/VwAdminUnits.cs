﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class VwAdminUnits
    {
        public string VillageId { get; set; }
        public string VillageName { get; set; }
    }
}
