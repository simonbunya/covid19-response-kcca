﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class AEmployees
    {
        public AEmployees()
        {
            CoreCaseContacts = new HashSet<CoreCaseContacts>();
            CoreComplianceVisit = new HashSet<CoreComplianceVisit>();
        }

        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Telephone { get; set; }
        public string NinNo { get; set; }
        public int? Gender { get; set; }
        public string Nationality { get; set; }
        public string PhotoType { get; set; }
        public int? Title { get; set; }
        public string UserId { get; set; }
        public byte[] Photo { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreationDate { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditionDate { get; set; }
        public string PhysicalAddress { get; set; }

        public virtual ASex GenderNavigation { get; set; }
        public virtual ACountry NationalityNavigation { get; set; }
        public virtual AVillage PhysicalAddressNavigation { get; set; }
        public virtual ATitle TitleNavigation { get; set; }
        public virtual AspNetUsers User { get; set; }
        public virtual ICollection<CoreCaseContacts> CoreCaseContacts { get; set; }
        public virtual ICollection<CoreComplianceVisit> CoreComplianceVisit { get; set; }
    }
}
