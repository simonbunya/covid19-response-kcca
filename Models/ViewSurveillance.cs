﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ViewSurveillance
    {
        public string CaseNo { get; set; }
        public string FullName { get; set; }
        public int? DeleteStatus { get; set; }
    }
}
