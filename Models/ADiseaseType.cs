﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ADiseaseType
    {
        public ADiseaseType()
        {
            ALoginLog = new HashSet<ALoginLog>();
            ASymptoms = new HashSet<ASymptoms>();
            CoreCaseContacts = new HashSet<CoreCaseContacts>();
            CoreCases = new HashSet<CoreCases>();
            CoreCommunitySurveillance = new HashSet<CoreCommunitySurveillance>();
        }

        public int DiseaseId { get; set; }
        public string Disease { get; set; }

        public virtual ICollection<ALoginLog> ALoginLog { get; set; }
        public virtual ICollection<ASymptoms> ASymptoms { get; set; }
        public virtual ICollection<CoreCaseContacts> CoreCaseContacts { get; set; }
        public virtual ICollection<CoreCases> CoreCases { get; set; }
        public virtual ICollection<CoreCommunitySurveillance> CoreCommunitySurveillance { get; set; }
    }
}
