﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ASex
    {
        public ASex()
        {
            AEmployees = new HashSet<AEmployees>();
            CoreCaseContacts = new HashSet<CoreCaseContacts>();
            CoreCaseContactsLog = new HashSet<CoreCaseContactsLog>();
            CoreCases = new HashSet<CoreCases>();
            CoreCommunitySurveillance = new HashSet<CoreCommunitySurveillance>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<AEmployees> AEmployees { get; set; }
        public virtual ICollection<CoreCaseContacts> CoreCaseContacts { get; set; }
        public virtual ICollection<CoreCaseContactsLog> CoreCaseContactsLog { get; set; }
        public virtual ICollection<CoreCases> CoreCases { get; set; }
        public virtual ICollection<CoreCommunitySurveillance> CoreCommunitySurveillance { get; set; }
    }
}
