﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ViewMobileCase
    {
        public string CaseNo { get; set; }
        public string ExternalCaseNo { get; set; }
        public string CaseName { get; set; }
    }
}
