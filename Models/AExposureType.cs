﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class AExposureType
    {
        public AExposureType()
        {
            CoreCaseContacts = new HashSet<CoreCaseContacts>();
        }

        public int Id { get; set; }
        public string ExposureType { get; set; }

        public virtual ICollection<CoreCaseContacts> CoreCaseContacts { get; set; }
    }
}
