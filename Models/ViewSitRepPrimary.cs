﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ViewSitRepPrimary
    {
        public int SitRepCategoryId { get; set; }
        public int? SitRepId { get; set; }
        public string SitRep { get; set; }
        public string Category { get; set; }
    }
}
