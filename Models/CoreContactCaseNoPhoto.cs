﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class CoreContactCaseNoPhoto
    {
        public string ContactCaseNo { get; set; }
        public byte[] Photo { get; set; }
        public string FileType { get; set; }
    }
}
