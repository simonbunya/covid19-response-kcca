﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class AOccupation
    {
        public AOccupation()
        {
            CoreCaseContacts = new HashSet<CoreCaseContacts>();
            CoreCases = new HashSet<CoreCases>();
            CoreCommunitySurveillance = new HashSet<CoreCommunitySurveillance>();
        }

        public int OccupationId { get; set; }
        public string Occupation { get; set; }

        public virtual ICollection<CoreCaseContacts> CoreCaseContacts { get; set; }
        public virtual ICollection<CoreCases> CoreCases { get; set; }
        public virtual ICollection<CoreCommunitySurveillance> CoreCommunitySurveillance { get; set; }
    }
}
