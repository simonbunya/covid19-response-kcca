﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ViewComplianceVisit
    {
        public string ComplianceCode { get; set; }
        public int? FacilityCode { get; set; }
        public DateTime? DateOfVisit { get; set; }
        public int? DeleteStatus { get; set; }
    }
}
