﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class AParish
    {
        public AParish()
        {
            AFacilityUnit = new HashSet<AFacilityUnit>();
            AVillage = new HashSet<AVillage>();
            CoreCaseContacts = new HashSet<CoreCaseContacts>();
            CoreCaseContactsLog = new HashSet<CoreCaseContactsLog>();
            CoreCases = new HashSet<CoreCases>();
            CoreCommunitySurveillance = new HashSet<CoreCommunitySurveillance>();
        }

        public string ParishId { get; set; }
        public string ParishName { get; set; }
        public string SubcountyId { get; set; }
        public string ParishMinistryCode { get; set; }

        public virtual ASubcounty Subcounty { get; set; }
        public virtual ICollection<AFacilityUnit> AFacilityUnit { get; set; }
        public virtual ICollection<AVillage> AVillage { get; set; }
        public virtual ICollection<CoreCaseContacts> CoreCaseContacts { get; set; }
        public virtual ICollection<CoreCaseContactsLog> CoreCaseContactsLog { get; set; }
        public virtual ICollection<CoreCases> CoreCases { get; set; }
        public virtual ICollection<CoreCommunitySurveillance> CoreCommunitySurveillance { get; set; }
    }
}
