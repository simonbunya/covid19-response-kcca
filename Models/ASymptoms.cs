﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ASymptoms
    {
        public int Id { get; set; }
        public int Disease { get; set; }
        public string Symptoms { get; set; }

        public virtual ADiseaseType DiseaseNavigation { get; set; }
    }
}
