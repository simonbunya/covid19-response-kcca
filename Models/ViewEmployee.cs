﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ViewEmployee
    {
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Telephone { get; set; }
        public string NinNo { get; set; }
        public int? Gender { get; set; }
        public string GenderDesc { get; set; }
        public string Nationality { get; set; }
        public string CountryName { get; set; }
        public int? Title { get; set; }
        public string TitleDesc { get; set; }
        public string UserId { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string PhysicalAddress { get; set; }
        public string VillageName { get; set; }
        public string ParishId { get; set; }
        public string ParishName { get; set; }
        public string SubcountyId { get; set; }
        public string SubcountyName { get; set; }
        public string CountyId { get; set; }
        public string CountyName { get; set; }
        public string DistrictId { get; set; }
        public string DistrictName { get; set; }
    }
}
