﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ViewParishes
    {
        public string ParishId { get; set; }
        public string ParishName { get; set; }
        public string DistrictId { get; set; }
        public string CountyId { get; set; }
        public string SubcountyId { get; set; }
    }
}
