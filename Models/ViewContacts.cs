﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ViewContacts
    {
        public string ContactCaseNo { get; set; }
        public string FullName { get; set; }
        public int? DeleteStatus { get; set; }
    }
}
