﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class CorePublicTransportDetials
    {
        public string BusCode { get; set; }
        public string PassengerCode { get; set; }
        public string NationalId { get; set; }
        public string Tel { get; set; }
        public string WhereFrom { get; set; }
        public string WhereTo { get; set; }
        public int? WearingMask { get; set; }
        public string Temperature { get; set; }
        public string Remarks { get; set; }
        public string Comments { get; set; }
        public int? Disease { get; set; }
        public DateTime? DateAdded { get; set; }
        public string AddedBy { get; set; }
        public DateTime? DateEdited { get; set; }
        public string EditedBy { get; set; }
        public DateTime? DateDeleted { get; set; }
        public string DeletedBy { get; set; }
        public int? DeleteStatus { get; set; }

        public virtual CorePublicTransport BusCodeNavigation { get; set; }
        public virtual AYesNo WearingMaskNavigation { get; set; }
    }
}
