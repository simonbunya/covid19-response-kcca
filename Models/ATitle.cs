﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ATitle
    {
        public ATitle()
        {
            AEmployees = new HashSet<AEmployees>();
        }

        public int TitleId { get; set; }
        public string TitleDesc { get; set; }

        public virtual ICollection<AEmployees> AEmployees { get; set; }
    }
}
