﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ALabResults
    {
        public ALabResults()
        {
            CoreCaseContacts = new HashSet<CoreCaseContacts>();
            CoreCaseContactsMutitpleTests = new HashSet<CoreCaseContactsMutitpleTests>();
            CoreCases = new HashSet<CoreCases>();
            CoreCommunitySurveillance = new HashSet<CoreCommunitySurveillance>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<CoreCaseContacts> CoreCaseContacts { get; set; }
        public virtual ICollection<CoreCaseContactsMutitpleTests> CoreCaseContactsMutitpleTests { get; set; }
        public virtual ICollection<CoreCases> CoreCases { get; set; }
        public virtual ICollection<CoreCommunitySurveillance> CoreCommunitySurveillance { get; set; }
    }
}
