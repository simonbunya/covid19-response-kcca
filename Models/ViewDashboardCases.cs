﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ViewDashboardCases
    {
        public string CaseNo { get; set; }
        public string ExternalCaseNo { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string RiskLevel { get; set; }
        public string Age { get; set; }
        public string AgeCategory { get; set; }
        public DateTime? DateOfReporting { get; set; }
        public string VillageName { get; set; }
        public string ParishName { get; set; }
        public string SubcountyName { get; set; }
        public string CountyName { get; set; }
        public string DistrictName { get; set; }
        public DateTime? CaseDate { get; set; }
        public DateTime? DateOfInfection { get; set; }
        public DateTime? DateOfOutcome { get; set; }
        public DateTime? LabTestDate { get; set; }
        public string Labresults { get; set; }
        public string Outcome { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public int? DeleteStatus { get; set; }
    }
}
