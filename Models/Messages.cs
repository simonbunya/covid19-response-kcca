﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class Messages
    {
        public int MessageId { get; set; }
        public string MessageDetails { get; set; }
        public string SentBy { get; set; }
        public DateTime? SentDate { get; set; }
        public string ReceivedBy { get; set; }
        public bool IsRead { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsAchieved { get; set; }
    }
}
