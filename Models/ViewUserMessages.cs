﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ViewUserMessages
    {
        public string NameOfUserAccountHolder { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
    }
}
