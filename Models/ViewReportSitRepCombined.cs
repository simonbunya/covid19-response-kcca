﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ViewReportSitRepCombined
    {
        public string Division { get; set; }
        public DateTime ReportingDate { get; set; }
        public int SitRepCategoryId { get; set; }
        public string Variable { get; set; }
        public string Category { get; set; }
        public string KampalaCentralDivision { get; set; }
        public string KawempeDivision { get; set; }
        public string MakindyeDivision { get; set; }
        public string RubagaDivision { get; set; }
        public string NakawaDivision { get; set; }
    }
}
