﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class CorePublicTransport
    {
        public CorePublicTransport()
        {
            CorePublicTransportDetials = new HashSet<CorePublicTransportDetials>();
        }

        public string Code { get; set; }
        public DateTime? TransportDate { get; set; }
        public string BusCompany { get; set; }
        public string BusNo { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public string DriverName { get; set; }
        public string DriverTel { get; set; }
        public string DriverAddress { get; set; }
        public string ConductorName { get; set; }
        public string ConductorTel { get; set; }
        public string ConductorAddress { get; set; }
        public string GeneralComments { get; set; }
        public int? Disease { get; set; }
        public DateTime? DateAdded { get; set; }
        public string AddedBy { get; set; }
        public DateTime? DateEdited { get; set; }
        public string EditedBy { get; set; }
        public DateTime? DateDeleted { get; set; }
        public string DeletedBy { get; set; }
        public int? DeleteStatus { get; set; }

        public virtual ICollection<CorePublicTransportDetials> CorePublicTransportDetials { get; set; }
    }
}
