﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class CoreComplianceVisit
    {
        public string ComplianceCode { get; set; }
        public int? FacilityCode { get; set; }
        public DateTime? DateOfVisit { get; set; }
        public int? TemperatureGuns { get; set; }
        public int? HandWashers { get; set; }
        public int? SocialDistancing { get; set; }
        public int? RecordKeeping { get; set; }
        public int? AirConditioning { get; set; }
        public int? MaskWearing { get; set; }
        public int? WashRooms { get; set; }
        public string Remarks { get; set; }
        public int? SuperviorCode { get; set; }
        public string GeneralComments { get; set; }
        public int? Disease { get; set; }
        public DateTime? DateAdded { get; set; }
        public string AddedBy { get; set; }
        public DateTime? DateEdited { get; set; }
        public string EditedBy { get; set; }
        public DateTime? DateDeleted { get; set; }
        public string DeletedBy { get; set; }
        public int? DeleteStatus { get; set; }

        public virtual AFacilityUnit FacilityCodeNavigation { get; set; }
        public virtual AYesNo HandWashersNavigation { get; set; }
        public virtual AYesNo MaskWearingNavigation { get; set; }
        public virtual AYesNo RecordKeepingNavigation { get; set; }
        public virtual AYesNo SocialDistancingNavigation { get; set; }
        public virtual AEmployees SuperviorCodeNavigation { get; set; }
        public virtual AYesNo TemperatureGunsNavigation { get; set; }
        public virtual AYesNo WashRoomsNavigation { get; set; }
    }
}
