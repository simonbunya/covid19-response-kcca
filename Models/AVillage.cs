﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class AVillage
    {
        public AVillage()
        {
            AEmployees = new HashSet<AEmployees>();
            AFacilityUnit = new HashSet<AFacilityUnit>();
            CoreCaseContacts = new HashSet<CoreCaseContacts>();
            CoreCaseContactsLog = new HashSet<CoreCaseContactsLog>();
            CoreCases = new HashSet<CoreCases>();
            CoreCommunitySurveillance = new HashSet<CoreCommunitySurveillance>();
        }

        public string VillageId { get; set; }
        public string VillageName { get; set; }
        public string ParishId { get; set; }
        public string VillageMinistryCode { get; set; }

        public virtual AParish Parish { get; set; }
        public virtual ICollection<AEmployees> AEmployees { get; set; }
        public virtual ICollection<AFacilityUnit> AFacilityUnit { get; set; }
        public virtual ICollection<CoreCaseContacts> CoreCaseContacts { get; set; }
        public virtual ICollection<CoreCaseContactsLog> CoreCaseContactsLog { get; set; }
        public virtual ICollection<CoreCases> CoreCases { get; set; }
        public virtual ICollection<CoreCommunitySurveillance> CoreCommunitySurveillance { get; set; }
    }
}
