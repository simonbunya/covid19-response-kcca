﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ViewUserManagement
    {
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public DateTimeOffset? LockoutEnd { get; set; }
        public string RoleId { get; set; }
        public string RoleName { get; set; }
    }
}
