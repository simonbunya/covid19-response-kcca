﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class MobileLogin
    {
        public int LoginId { get; set; }
        public string UserId { get; set; }
        public string LoginCode { get; set; }
        public DateTime? LoginDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
    }
}
