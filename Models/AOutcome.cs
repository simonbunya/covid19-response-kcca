﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class AOutcome
    {
        public AOutcome()
        {
            CoreCaseContacts = new HashSet<CoreCaseContacts>();
            CoreCases = new HashSet<CoreCases>();
            CoreCommunitySurveillance = new HashSet<CoreCommunitySurveillance>();
        }

        public int Id { get; set; }
        public string Outcome { get; set; }

        public virtual ICollection<CoreCaseContacts> CoreCaseContacts { get; set; }
        public virtual ICollection<CoreCases> CoreCases { get; set; }
        public virtual ICollection<CoreCommunitySurveillance> CoreCommunitySurveillance { get; set; }
    }
}
