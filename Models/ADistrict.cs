﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ADistrict
    {
        public ADistrict()
        {
            ACounty = new HashSet<ACounty>();
            AFacilityUnit = new HashSet<AFacilityUnit>();
            ASubcounty = new HashSet<ASubcounty>();
            CoreCaseContacts = new HashSet<CoreCaseContacts>();
            CoreCaseContactsLog = new HashSet<CoreCaseContactsLog>();
            CoreCases = new HashSet<CoreCases>();
            CoreCommunitySurveillance = new HashSet<CoreCommunitySurveillance>();
            CoreSitRep = new HashSet<CoreSitRep>();
            CoreSitRepHeader = new HashSet<CoreSitRepHeader>();
        }

        public string DistrictId { get; set; }
        public string DistrictName { get; set; }
        public string RegionId { get; set; }
        public bool? IsUrban { get; set; }
        public string DistrictMinistryCode { get; set; }
        public bool? IsMunicipality { get; set; }

        public virtual ICollection<ACounty> ACounty { get; set; }
        public virtual ICollection<AFacilityUnit> AFacilityUnit { get; set; }
        public virtual ICollection<ASubcounty> ASubcounty { get; set; }
        public virtual ICollection<CoreCaseContacts> CoreCaseContacts { get; set; }
        public virtual ICollection<CoreCaseContactsLog> CoreCaseContactsLog { get; set; }
        public virtual ICollection<CoreCases> CoreCases { get; set; }
        public virtual ICollection<CoreCommunitySurveillance> CoreCommunitySurveillance { get; set; }
        public virtual ICollection<CoreSitRep> CoreSitRep { get; set; }
        public virtual ICollection<CoreSitRepHeader> CoreSitRepHeader { get; set; }
    }
}
