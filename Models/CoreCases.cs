﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class CoreCases
    {
        public CoreCases()
        {
            CoreCaseContacts = new HashSet<CoreCaseContacts>();
        }

        public string CaseNo { get; set; }
        public string ExternalCaseNo { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public int? WasContact { get; set; }
        public int? Occupation { get; set; }
        public string OccupationSpecify { get; set; }
        public int? RiskLevel { get; set; }
        public int? Sex { get; set; }
        public DateTime? DoB { get; set; }
        public string Age { get; set; }
        public DateTime? DateOfReporting { get; set; }
        public string RiskReason { get; set; }
        public string Address { get; set; }
        public string DistrictId { get; set; }
        public string CountyId { get; set; }
        public string SubcountyId { get; set; }
        public string ParishId { get; set; }
        public string VillageId { get; set; }
        public int? FacilityUnit { get; set; }
        public string Tel { get; set; }
        public string Email { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public int? Classification { get; set; }
        public string ClassificationSpecify { get; set; }
        public DateTime? DateOnset { get; set; }
        public DateTime? CaseDate { get; set; }
        public DateTime? DateOfInfection { get; set; }
        public int? Outcome { get; set; }
        public string OutcomeSpecify { get; set; }
        public DateTime? DateOfOutcome { get; set; }
        public DateTime? LabTestDate { get; set; }
        public int? LabResults { get; set; }
        public int? Disease { get; set; }
        public string Symptoms { get; set; }
        public string SymptomsOthers { get; set; }
        public string NextAction { get; set; }
        public int? StatusFlag { get; set; }
        public string GeneralComments { get; set; }
        public string SamplingDistrict { get; set; }
        public string SamplingFacility { get; set; }
        public DateTime? SamplingDate { get; set; }
        public string TestingLab { get; set; }
        public string AlertType { get; set; }
        public int? CaseEvacuated { get; set; }
        public int? EvacuationOfficer { get; set; }
        public string EvacuationFacility { get; set; }
        public string EvacuationComments { get; set; }
        public DateTime? DateAdded { get; set; }
        public string AddedBy { get; set; }
        public DateTime? DateEdited { get; set; }
        public string EditedBy { get; set; }
        public DateTime? DateDeleted { get; set; }
        public string DeletedBy { get; set; }
        public int? DeleteStatus { get; set; }

        public virtual AYesNo CaseEvacuatedNavigation { get; set; }
        public virtual AClassification ClassificationNavigation { get; set; }
        public virtual ACounty County { get; set; }
        public virtual ADiseaseType DiseaseNavigation { get; set; }
        public virtual ADistrict District { get; set; }
        public virtual AFacilityUnit FacilityUnitNavigation { get; set; }
        public virtual ALabResults LabResultsNavigation { get; set; }
        public virtual AOccupation OccupationNavigation { get; set; }
        public virtual AOutcome OutcomeNavigation { get; set; }
        public virtual AParish Parish { get; set; }
        public virtual ARiskLevel RiskLevelNavigation { get; set; }
        public virtual ASex SexNavigation { get; set; }
        public virtual AElevateContact StatusFlagNavigation { get; set; }
        public virtual ASubcounty Subcounty { get; set; }
        public virtual AVillage Village { get; set; }
        public virtual AYesNo WasContactNavigation { get; set; }
        public virtual ICollection<CoreCaseContacts> CoreCaseContacts { get; set; }
    }
}
