﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class AOperationalArea
    {
        public AOperationalArea()
        {
            AFacilityUnit = new HashSet<AFacilityUnit>();
            CoreCommunitySurveillance = new HashSet<CoreCommunitySurveillance>();
        }

        public int OperationalCode { get; set; }
        public string OperationalArea { get; set; }
        public int? DeleteStatus { get; set; }

        public virtual ICollection<AFacilityUnit> AFacilityUnit { get; set; }
        public virtual ICollection<CoreCommunitySurveillance> CoreCommunitySurveillance { get; set; }
    }
}
