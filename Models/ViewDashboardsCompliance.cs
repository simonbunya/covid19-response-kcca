﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ViewDashboardsCompliance
    {
        public string ComplianceCode { get; set; }
        public string Facility { get; set; }
        public int? OperationalArea { get; set; }
        public string OperationalAreaName { get; set; }
        public DateTime? DateOfVisit { get; set; }
        public string TemperatureGuns { get; set; }
        public string HandWashers { get; set; }
        public string SocialDistancing { get; set; }
        public string RecordKeeping { get; set; }
        public string MaskWearing { get; set; }
        public string AirConditioning { get; set; }
        public string WashRooms { get; set; }
        public string SuperviorCode { get; set; }
        public string EmployeesName { get; set; }
        public int? DeleteStatus { get; set; }
    }
}
