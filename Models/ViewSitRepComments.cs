﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ViewSitRepComments
    {
        public string CountyName { get; set; }
        public string Division { get; set; }
        public DateTime ReportingDate { get; set; }
        public DateTime? DateofConfirmation { get; set; }
        public string GeneralComments { get; set; }
        public string UpdateComments { get; set; }
        public string WashComments { get; set; }
        public string RiskComments { get; set; }
        public DateTime? DateAdded { get; set; }
        public string AddedBy { get; set; }
        public DateTime? DateEdited { get; set; }
        public string EditedBy { get; set; }
    }
}
