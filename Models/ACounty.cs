﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ACounty
    {
        public ACounty()
        {
            AFacilityUnit = new HashSet<AFacilityUnit>();
            CoreCaseContacts = new HashSet<CoreCaseContacts>();
            CoreCaseContactsLog = new HashSet<CoreCaseContactsLog>();
            CoreCases = new HashSet<CoreCases>();
            CoreCommunitySurveillance = new HashSet<CoreCommunitySurveillance>();
            CoreSitRep = new HashSet<CoreSitRep>();
        }

        public string CountyId { get; set; }
        public string DistrictId { get; set; }
        public string CountyName { get; set; }
        public string CountyMinistryCode { get; set; }

        public virtual ADistrict District { get; set; }
        public virtual ICollection<AFacilityUnit> AFacilityUnit { get; set; }
        public virtual ICollection<CoreCaseContacts> CoreCaseContacts { get; set; }
        public virtual ICollection<CoreCaseContactsLog> CoreCaseContactsLog { get; set; }
        public virtual ICollection<CoreCases> CoreCases { get; set; }
        public virtual ICollection<CoreCommunitySurveillance> CoreCommunitySurveillance { get; set; }
        public virtual ICollection<CoreSitRep> CoreSitRep { get; set; }
    }
}
