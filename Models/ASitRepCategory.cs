﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ASitRepCategory
    {
        public ASitRepCategory()
        {
            CoreSitRep = new HashSet<CoreSitRep>();
        }

        public int SitRepCategoryId { get; set; }
        public int? SitRepId { get; set; }
        public string Category { get; set; }

        public virtual ASitRepVariable SitRep { get; set; }
        public virtual ICollection<CoreSitRep> CoreSitRep { get; set; }
    }
}
