﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ASubcounty
    {
        public ASubcounty()
        {
            AFacilityUnit = new HashSet<AFacilityUnit>();
            AParish = new HashSet<AParish>();
            CoreCaseContacts = new HashSet<CoreCaseContacts>();
            CoreCaseContactsLog = new HashSet<CoreCaseContactsLog>();
            CoreCases = new HashSet<CoreCases>();
            CoreCommunitySurveillance = new HashSet<CoreCommunitySurveillance>();
        }

        public string SubcountyId { get; set; }
        public string DistrictId { get; set; }
        public string CountyId { get; set; }
        public string SubcountyName { get; set; }
        public string SubcountyMinistryCode { get; set; }

        public virtual ADistrict District { get; set; }
        public virtual ICollection<AFacilityUnit> AFacilityUnit { get; set; }
        public virtual ICollection<AParish> AParish { get; set; }
        public virtual ICollection<CoreCaseContacts> CoreCaseContacts { get; set; }
        public virtual ICollection<CoreCaseContactsLog> CoreCaseContactsLog { get; set; }
        public virtual ICollection<CoreCases> CoreCases { get; set; }
        public virtual ICollection<CoreCommunitySurveillance> CoreCommunitySurveillance { get; set; }
    }
}
