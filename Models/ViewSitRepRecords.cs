﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ViewSitRepRecords
    {
        public string Division { get; set; }
        public string CountyName { get; set; }
        public DateTime ReportingDate { get; set; }
        public DateTime? DateAdded { get; set; }
        public string AddedBy { get; set; }
        public DateTime? DateEdited { get; set; }
        public string EditedBy { get; set; }
    }
}
