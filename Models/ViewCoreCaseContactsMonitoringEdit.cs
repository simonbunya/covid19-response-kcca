﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ViewCoreCaseContactsMonitoringEdit
    {
        public string ContactCaseNo { get; set; }
        public string CaseNo { get; set; }
        public string MonitoringContactCaseNo { get; set; }
        public DateTime? MonitoringDate { get; set; }
        public int Days { get; set; }
        public string DaysDetails { get; set; }
        public string MonitoringCaseNo { get; set; }
        public string Temperature { get; set; }
        public int? Chills { get; set; }
        public string ChillsDetails { get; set; }
        public int? Cough { get; set; }
        public string CoughDetails { get; set; }
        public int? SoreThroat { get; set; }
        public string SoreThroatDetails { get; set; }
        public int? Breath { get; set; }
        public string BreathDetails { get; set; }
        public int? BodyPains { get; set; }
        public string BodyPainsDetails { get; set; }
        public int? Diarrhea { get; set; }
        public string DiarrheaDetails { get; set; }
        public string GeneralComments { get; set; }
        public DateTime? DateAdded { get; set; }
        public string AddedBy { get; set; }
        public DateTime? DateEdited { get; set; }
        public string EditedBy { get; set; }
        public DateTime? DateDeleted { get; set; }
        public string DeletedBy { get; set; }
    }
}
