﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ASystemSection
    {
        public int SectionId { get; set; }
        public int? ModuleId { get; set; }
        public string SectionHeader { get; set; }
        public string SectionDescription { get; set; }

        public virtual ASystemModules Module { get; set; }
    }
}
