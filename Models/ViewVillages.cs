﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ViewVillages
    {
        public string DistrictId { get; set; }
        public string CountyId { get; set; }
        public string SubcountyId { get; set; }
        public string ParishId { get; set; }
        public string VillageId { get; set; }
        public string VillageName { get; set; }
    }
}
