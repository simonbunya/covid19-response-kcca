﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ViewDashboardReportSitRepCombined
    {
        public int SitRepCategoryId { get; set; }
        public string Variable { get; set; }
        public string Category { get; set; }
        public int? KampalaCentralDivision { get; set; }
        public int? KawempeDivision { get; set; }
        public int? MakindyeDivision { get; set; }
        public int? RubagaDivision { get; set; }
        public int? NakawaDivision { get; set; }
        public int? TotalCount { get; set; }
    }
}
