﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ASystemModules
    {
        public ASystemModules()
        {
            ASystemSection = new HashSet<ASystemSection>();
        }

        public int ModuleId { get; set; }
        public string ModuleDescription { get; set; }

        public virtual ICollection<ASystemSection> ASystemSection { get; set; }
    }
}
