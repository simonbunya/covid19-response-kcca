﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ViewReportSitRep
    {
        public int? SitRepCategoryId { get; set; }
        public int SitRepId { get; set; }
        public string SitRep { get; set; }
        public string Category { get; set; }
        public string ValueResponse { get; set; }
        public string District { get; set; }
        public string CountyName { get; set; }
        public string Division { get; set; }
        public DateTime? ReportingDate { get; set; }
        public DateTime? DateofConfirmation { get; set; }
        public string GeneralComments { get; set; }
        public string UpdateComments { get; set; }
        public string WashComments { get; set; }
        public string RiskComments { get; set; }
        public DateTime? DateAdded { get; set; }
        public string AddedBy { get; set; }
        public DateTime? DateEdited { get; set; }
        public string EditedBy { get; set; }
        public DateTime? DateDeleted { get; set; }
        public string DeletedBy { get; set; }
        public int? DeleteStatus { get; set; }
    }
}
