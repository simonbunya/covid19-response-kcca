﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class CoreCasePhoto
    {
        public string CaseNo { get; set; }
        public byte[] Photo { get; set; }
        public string FileType { get; set; }
    }
}
