﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ASelecetedModules
    {
        public string UserId { get; set; }
        public string SelectedModules { get; set; }
    }
}
