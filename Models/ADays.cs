﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ADays
    {
        public ADays()
        {
            CoreCaseContactsMonitoring = new HashSet<CoreCaseContactsMonitoring>();
        }

        public int Id { get; set; }
        public string Days { get; set; }

        public virtual ICollection<CoreCaseContactsMonitoring> CoreCaseContactsMonitoring { get; set; }
    }
}
