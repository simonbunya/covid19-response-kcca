﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ViewCases
    {
        public string CaseNo { get; set; }
        public string Name { get; set; }
        public int? DeleteStatus { get; set; }
    }
}
