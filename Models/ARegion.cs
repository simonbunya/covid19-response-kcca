﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ARegion
    {
        public string RegionId { get; set; }
        public string RegionName { get; set; }
        public string AddedBy { get; set; }
        public DateTime? AddedDate { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditedDate { get; set; }
    }
}
