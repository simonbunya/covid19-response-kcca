﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace CHMIS.Models
{
    public partial class CHMISContext : DbContext
    {
        public CHMISContext()
        {
        }

        public CHMISContext(DbContextOptions<CHMISContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AClassification> AClassification { get; set; }
        public virtual DbSet<ACountry> ACountry { get; set; }
        public virtual DbSet<ACounty> ACounty { get; set; }
        public virtual DbSet<ADays> ADays { get; set; }
        public virtual DbSet<ADiseaseType> ADiseaseType { get; set; }
        public virtual DbSet<ADistrict> ADistrict { get; set; }
        public virtual DbSet<AElevateContact> AElevateContact { get; set; }
        public virtual DbSet<AEmployees> AEmployees { get; set; }
        public virtual DbSet<AExposureType> AExposureType { get; set; }
        public virtual DbSet<AFacilityUnit> AFacilityUnit { get; set; }
        public virtual DbSet<ALabResults> ALabResults { get; set; }
        public virtual DbSet<ALoginLog> ALoginLog { get; set; }
        public virtual DbSet<AOccupation> AOccupation { get; set; }
        public virtual DbSet<AOperationalArea> AOperationalArea { get; set; }
        public virtual DbSet<AOutcome> AOutcome { get; set; }
        public virtual DbSet<AParish> AParish { get; set; }
        public virtual DbSet<ARegion> ARegion { get; set; }
        public virtual DbSet<ARelationship> ARelationship { get; set; }
        public virtual DbSet<ARiskLevel> ARiskLevel { get; set; }
        public virtual DbSet<ASelecetedModules> ASelecetedModules { get; set; }
        public virtual DbSet<ASelecetedSections> ASelecetedSections { get; set; }
        public virtual DbSet<ASex> ASex { get; set; }
        public virtual DbSet<ASitRepCategory> ASitRepCategory { get; set; }
        public virtual DbSet<ASitRepVariable> ASitRepVariable { get; set; }
        public virtual DbSet<ASubcounty> ASubcounty { get; set; }
        public virtual DbSet<ASymptoms> ASymptoms { get; set; }
        public virtual DbSet<ASystemModules> ASystemModules { get; set; }
        public virtual DbSet<ASystemSection> ASystemSection { get; set; }
        public virtual DbSet<ATitle> ATitle { get; set; }
        public virtual DbSet<AVillage> AVillage { get; set; }
        public virtual DbSet<AYesNo> AYesNo { get; set; }
        public virtual DbSet<AspNetRoleClaims> AspNetRoleClaims { get; set; }
        public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUserRoles> AspNetUserRoles { get; set; }
        public virtual DbSet<AspNetUserTokens> AspNetUserTokens { get; set; }
        public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
        public virtual DbSet<CoreCaseContacts> CoreCaseContacts { get; set; }
        public virtual DbSet<CoreCaseContactsLog> CoreCaseContactsLog { get; set; }
        public virtual DbSet<CoreCaseContactsMonitoring> CoreCaseContactsMonitoring { get; set; }
        public virtual DbSet<CoreCaseContactsMonitoringLog> CoreCaseContactsMonitoringLog { get; set; }
        public virtual DbSet<CoreCaseContactsMutitpleTests> CoreCaseContactsMutitpleTests { get; set; }
        public virtual DbSet<CoreCaseFileAttachment> CoreCaseFileAttachment { get; set; }
        public virtual DbSet<CoreCasePhoto> CoreCasePhoto { get; set; }
        public virtual DbSet<CoreCases> CoreCases { get; set; }
        public virtual DbSet<CoreCommunitySurveillance> CoreCommunitySurveillance { get; set; }
        public virtual DbSet<CoreComplianceVisit> CoreComplianceVisit { get; set; }
        public virtual DbSet<CoreContactCaseNoPhoto> CoreContactCaseNoPhoto { get; set; }
        public virtual DbSet<CoreContactsFileAttachment> CoreContactsFileAttachment { get; set; }
        public virtual DbSet<CorePublicTransport> CorePublicTransport { get; set; }
        public virtual DbSet<CorePublicTransportDetials> CorePublicTransportDetials { get; set; }
        public virtual DbSet<CoreSitRep> CoreSitRep { get; set; }
        public virtual DbSet<CoreSitRepHeader> CoreSitRepHeader { get; set; }
        public virtual DbSet<Messages> Messages { get; set; }
        public virtual DbSet<MobileLogin> MobileLogin { get; set; }
        public virtual DbSet<ViewCases> ViewCases { get; set; }
        public virtual DbSet<ViewComplianceVisit> ViewComplianceVisit { get; set; }
        public virtual DbSet<ViewContacts> ViewContacts { get; set; }
        public virtual DbSet<ViewCoreCaseContactsMonitoring> ViewCoreCaseContactsMonitoring { get; set; }
        public virtual DbSet<ViewCoreCaseContactsMonitoringEdit> ViewCoreCaseContactsMonitoringEdit { get; set; }
        public virtual DbSet<ViewDashboardCases> ViewDashboardCases { get; set; }
        public virtual DbSet<ViewDashboardCasesContacts> ViewDashboardCasesContacts { get; set; }
        public virtual DbSet<ViewDashboardCommunitySurveillance> ViewDashboardCommunitySurveillance { get; set; }
        public virtual DbSet<ViewDashboardReportSitRepCombined> ViewDashboardReportSitRepCombined { get; set; }
        public virtual DbSet<ViewDashboardsCompliance> ViewDashboardsCompliance { get; set; }
        public virtual DbSet<ViewEmployee> ViewEmployee { get; set; }
        public virtual DbSet<ViewFollowUp> ViewFollowUp { get; set; }
        public virtual DbSet<ViewFollowupTests> ViewFollowupTests { get; set; }
        public virtual DbSet<ViewMobileCase> ViewMobileCase { get; set; }
        public virtual DbSet<ViewParishes> ViewParishes { get; set; }
        public virtual DbSet<ViewReportSitRep> ViewReportSitRep { get; set; }
        public virtual DbSet<ViewReportSitRepCombined> ViewReportSitRepCombined { get; set; }
        public virtual DbSet<ViewReportSitRepCombinedNew> ViewReportSitRepCombinedNew { get; set; }
        public virtual DbSet<ViewReportSitRepCombinedNew1> ViewReportSitRepCombinedNew1 { get; set; }
        public virtual DbSet<ViewSitRepAdd> ViewSitRepAdd { get; set; }
        public virtual DbSet<ViewSitRepComments> ViewSitRepComments { get; set; }
        public virtual DbSet<ViewSitRepEdit> ViewSitRepEdit { get; set; }
        public virtual DbSet<ViewSitRepPrimary> ViewSitRepPrimary { get; set; }
        public virtual DbSet<ViewSitRepRecords> ViewSitRepRecords { get; set; }
        public virtual DbSet<ViewSurveillance> ViewSurveillance { get; set; }
        public virtual DbSet<ViewUserManagement> ViewUserManagement { get; set; }
        public virtual DbSet<ViewUserManagementNew> ViewUserManagementNew { get; set; }
        public virtual DbSet<ViewUserMessages> ViewUserMessages { get; set; }
        public virtual DbSet<ViewVillages> ViewVillages { get; set; }
        public virtual DbSet<VwAdminUnits> VwAdminUnits { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                //optionsBuilder.UseSqlServer("Server=.;Database=CHMIS_COVID19;User Id=sa;password=root85;Trusted_Connection=False;MultipleActiveResultSets=true;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AClassification>(entity =>
            {
                entity.ToTable("A_Classification");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Classification).HasMaxLength(150);
            });

            modelBuilder.Entity<ACountry>(entity =>
            {
                entity.HasKey(e => e.CountryCode);

                entity.ToTable("A_Country");

                entity.Property(e => e.CountryCode)
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.CountryId).HasColumnName("CountryID");

                entity.Property(e => e.CountryName).HasMaxLength(150);
            });

            modelBuilder.Entity<ACounty>(entity =>
            {
                entity.HasKey(e => e.CountyId);

                entity.ToTable("A_County");

                entity.Property(e => e.CountyId)
                    .HasColumnName("County_Id")
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.CountyMinistryCode)
                    .HasColumnName("County_Ministry_Code")
                    .HasMaxLength(1)
                    .IsFixedLength();

                entity.Property(e => e.CountyName)
                    .HasColumnName("County_Name")
                    .HasMaxLength(50);

                entity.Property(e => e.DistrictId)
                    .HasColumnName("District_Id")
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.HasOne(d => d.District)
                    .WithMany(p => p.ACounty)
                    .HasForeignKey(d => d.DistrictId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_A_County_A_District");
            });

            modelBuilder.Entity<ADays>(entity =>
            {
                entity.ToTable("A_Days");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Days).HasMaxLength(15);
            });

            modelBuilder.Entity<ADiseaseType>(entity =>
            {
                entity.HasKey(e => e.DiseaseId);

                entity.ToTable("A_DiseaseType");

                entity.Property(e => e.DiseaseId).HasColumnName("DiseaseID");

                entity.Property(e => e.Disease).HasMaxLength(150);
            });

            modelBuilder.Entity<ADistrict>(entity =>
            {
                entity.HasKey(e => e.DistrictId);

                entity.ToTable("A_District");

                entity.Property(e => e.DistrictId)
                    .HasColumnName("District_Id")
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.DistrictMinistryCode)
                    .HasColumnName("District_Ministry_Code")
                    .HasMaxLength(2)
                    .IsFixedLength();

                entity.Property(e => e.DistrictName)
                    .HasColumnName("District_Name")
                    .HasMaxLength(50);

                entity.Property(e => e.IsMunicipality).HasColumnName("Is_Municipality");

                entity.Property(e => e.IsUrban).HasColumnName("Is_Urban");

                entity.Property(e => e.RegionId)
                    .HasColumnName("Region_Id")
                    .HasMaxLength(2)
                    .IsFixedLength();
            });

            modelBuilder.Entity<AElevateContact>(entity =>
            {
                entity.ToTable("A_ElevateContact");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Name).HasMaxLength(50);
            });

            modelBuilder.Entity<AEmployees>(entity =>
            {
                entity.HasKey(e => e.EmployeeId);

                entity.ToTable("A_Employees");

                entity.Property(e => e.EmployeeId)
                    .HasColumnName("Employee_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.CreationDate).HasColumnType("datetime");

                entity.Property(e => e.DateOfBirth)
                    .HasColumnName("Date_Of_Birth")
                    .HasColumnType("date");

                entity.Property(e => e.EditedBy)
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.EditionDate).HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.Nationality)
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.NinNo)
                    .HasColumnName("NIN_No")
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.PhotoType).HasMaxLength(50);

                entity.Property(e => e.PhysicalAddress)
                    .HasColumnName("Physical_Address")
                    .HasMaxLength(5)
                    .IsFixedLength();

                entity.Property(e => e.Telephone)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.UserId)
                    .HasColumnName("User_Id")
                    .HasMaxLength(450);

                entity.HasOne(d => d.GenderNavigation)
                    .WithMany(p => p.AEmployees)
                    .HasForeignKey(d => d.Gender)
                    .HasConstraintName("FK_A_Employees_A_Sex");

                entity.HasOne(d => d.NationalityNavigation)
                    .WithMany(p => p.AEmployees)
                    .HasForeignKey(d => d.Nationality)
                    .HasConstraintName("FK_A_Employees_A_Country");

                entity.HasOne(d => d.PhysicalAddressNavigation)
                    .WithMany(p => p.AEmployees)
                    .HasForeignKey(d => d.PhysicalAddress)
                    .HasConstraintName("FK_A_Employees_A_Village");

                entity.HasOne(d => d.TitleNavigation)
                    .WithMany(p => p.AEmployees)
                    .HasForeignKey(d => d.Title)
                    .HasConstraintName("FK_A_Employees_A_Title");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AEmployees)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_A_Employees_A_Employees");
            });

            modelBuilder.Entity<AExposureType>(entity =>
            {
                entity.ToTable("A_ExposureType");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ExposureType).HasMaxLength(50);
            });

            modelBuilder.Entity<AFacilityUnit>(entity =>
            {
                entity.HasKey(e => e.FacilityCode);

                entity.ToTable("A_FacilityUnit");

                entity.Property(e => e.AddedBy).HasMaxLength(150);

                entity.Property(e => e.CountyId)
                    .HasColumnName("County_Id")
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.DateAdded).HasColumnType("datetime");

                entity.Property(e => e.DateDeleted).HasColumnType("datetime");

                entity.Property(e => e.DateEdited).HasColumnType("datetime");

                entity.Property(e => e.DeleteStatus).HasColumnName("deleteStatus");

                entity.Property(e => e.DeletedBy).HasMaxLength(150);

                entity.Property(e => e.DetailsOfFacility)
                    .HasColumnName("Details_of_Facility")
                    .HasMaxLength(4000);

                entity.Property(e => e.DistrictId)
                    .HasColumnName("District_Id")
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.EditedBy).HasMaxLength(150);

                entity.Property(e => e.EstimatedPopulation)
                    .HasColumnName("Estimated_Population")
                    .HasMaxLength(50);

                entity.Property(e => e.Facility).HasMaxLength(150);

                entity.Property(e => e.FacilityStatus).HasColumnName("Facility_Status");

                entity.Property(e => e.ParishId)
                    .HasColumnName("Parish_Id")
                    .HasMaxLength(5)
                    .IsFixedLength();

                entity.Property(e => e.SubcountyId)
                    .HasColumnName("Subcounty_Id")
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.VillageId)
                    .HasColumnName("Village_Id")
                    .HasMaxLength(5)
                    .IsFixedLength();

                entity.HasOne(d => d.County)
                    .WithMany(p => p.AFacilityUnit)
                    .HasForeignKey(d => d.CountyId)
                    .HasConstraintName("FK_A_FacilityUnit_A_County");

                entity.HasOne(d => d.District)
                    .WithMany(p => p.AFacilityUnit)
                    .HasForeignKey(d => d.DistrictId)
                    .HasConstraintName("FK_A_FacilityUnit_A_District");

                entity.HasOne(d => d.FacilityStatusNavigation)
                    .WithMany(p => p.AFacilityUnit)
                    .HasForeignKey(d => d.FacilityStatus)
                    .HasConstraintName("FK_A_FacilityUnit_A_YesNo");

                entity.HasOne(d => d.OperationalAreaNavigation)
                    .WithMany(p => p.AFacilityUnit)
                    .HasForeignKey(d => d.OperationalArea)
                    .HasConstraintName("FK_A_FacilityUnit_A_OperationalArea");

                entity.HasOne(d => d.Parish)
                    .WithMany(p => p.AFacilityUnit)
                    .HasForeignKey(d => d.ParishId)
                    .HasConstraintName("FK_A_FacilityUnit_A_Parish");

                entity.HasOne(d => d.Subcounty)
                    .WithMany(p => p.AFacilityUnit)
                    .HasForeignKey(d => d.SubcountyId)
                    .HasConstraintName("FK_A_FacilityUnit_A_Subcounty");

                entity.HasOne(d => d.Village)
                    .WithMany(p => p.AFacilityUnit)
                    .HasForeignKey(d => d.VillageId)
                    .HasConstraintName("FK_A_FacilityUnit_A_Village");
            });

            modelBuilder.Entity<ALabResults>(entity =>
            {
                entity.ToTable("A_LabResults");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Name).HasMaxLength(50);
            });

            modelBuilder.Entity<ALoginLog>(entity =>
            {
                entity.ToTable("A_LoginLog");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.LogInDate).HasColumnType("datetime");

                entity.Property(e => e.UserId)
                    .IsRequired()
                    .HasMaxLength(450);

                entity.HasOne(d => d.DiseaseNavigation)
                    .WithMany(p => p.ALoginLog)
                    .HasForeignKey(d => d.Disease)
                    .HasConstraintName("FK_A_LoginLog_A_DiseaseType");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.ALoginLog)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_A_LoginLog_AspNetUsers");
            });

            modelBuilder.Entity<AOccupation>(entity =>
            {
                entity.HasKey(e => e.OccupationId);

                entity.ToTable("A_Occupation");

                entity.Property(e => e.OccupationId).HasColumnName("OccupationID");

                entity.Property(e => e.Occupation).HasMaxLength(100);
            });

            modelBuilder.Entity<AOperationalArea>(entity =>
            {
                entity.HasKey(e => e.OperationalCode);

                entity.ToTable("A_OperationalArea");

                entity.Property(e => e.OperationalCode).ValueGeneratedNever();

                entity.Property(e => e.DeleteStatus).HasColumnName("deleteStatus");

                entity.Property(e => e.OperationalArea).HasMaxLength(150);
            });

            modelBuilder.Entity<AOutcome>(entity =>
            {
                entity.ToTable("A_Outcome");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Outcome).HasMaxLength(150);
            });

            modelBuilder.Entity<AParish>(entity =>
            {
                entity.HasKey(e => e.ParishId);

                entity.ToTable("A_Parish");

                entity.Property(e => e.ParishId)
                    .HasColumnName("Parish_Id")
                    .HasMaxLength(5)
                    .IsFixedLength();

                entity.Property(e => e.ParishMinistryCode)
                    .HasColumnName("Parish_Ministry_Code")
                    .HasMaxLength(2)
                    .IsFixedLength();

                entity.Property(e => e.ParishName)
                    .HasColumnName("Parish_Name")
                    .HasMaxLength(50);

                entity.Property(e => e.SubcountyId)
                    .HasColumnName("Subcounty_Id")
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.HasOne(d => d.Subcounty)
                    .WithMany(p => p.AParish)
                    .HasForeignKey(d => d.SubcountyId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_A_Parish_A_Subcounty");
            });

            modelBuilder.Entity<ARegion>(entity =>
            {
                entity.HasKey(e => e.RegionId)
                    .HasName("PK_a:region");

                entity.ToTable("A_Region");

                entity.Property(e => e.RegionId)
                    .HasColumnName("Region_Id")
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.AddedBy)
                    .HasColumnName("Added_By")
                    .HasMaxLength(100);

                entity.Property(e => e.AddedDate)
                    .HasColumnName("Added_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.EditedBy)
                    .HasColumnName("Edited_By")
                    .HasMaxLength(100);

                entity.Property(e => e.EditedDate)
                    .HasColumnName("Edited_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.RegionName)
                    .HasColumnName("Region_name")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<ARelationship>(entity =>
            {
                entity.ToTable("A_Relationship");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Relationship).HasMaxLength(50);
            });

            modelBuilder.Entity<ARiskLevel>(entity =>
            {
                entity.ToTable("A_RiskLevel");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<ASelecetedModules>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.ToTable("A_Seleceted_Modules");

                entity.Property(e => e.UserId).HasMaxLength(128);

                entity.Property(e => e.SelectedModules)
                    .HasColumnName("Selected_Modules")
                    .HasMaxLength(1000);
            });

            modelBuilder.Entity<ASelecetedSections>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.ToTable("A_Seleceted_Sections");

                entity.Property(e => e.UserId).HasMaxLength(128);

                entity.Property(e => e.SelectedSectors)
                    .HasColumnName("Selected_sectors")
                    .HasMaxLength(1000);
            });

            modelBuilder.Entity<ASex>(entity =>
            {
                entity.ToTable("A_Sex");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Name).HasMaxLength(50);
            });

            modelBuilder.Entity<ASitRepCategory>(entity =>
            {
                entity.HasKey(e => e.SitRepCategoryId);

                entity.ToTable("A_SitRepCategory");

                entity.Property(e => e.SitRepCategoryId)
                    .HasColumnName("SitRepCategoryID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Category).HasMaxLength(4000);

                entity.Property(e => e.SitRepId).HasColumnName("SitRepID");

                entity.HasOne(d => d.SitRep)
                    .WithMany(p => p.ASitRepCategory)
                    .HasForeignKey(d => d.SitRepId)
                    .HasConstraintName("FK_A_SitRepCategory_A_SitRepVariable");
            });

            modelBuilder.Entity<ASitRepVariable>(entity =>
            {
                entity.HasKey(e => e.SitRepId);

                entity.ToTable("A_SitRepVariable");

                entity.Property(e => e.SitRepId)
                    .HasColumnName("SitRepID")
                    .ValueGeneratedNever();

                entity.Property(e => e.SitRep).HasMaxLength(4000);
            });

            modelBuilder.Entity<ASubcounty>(entity =>
            {
                entity.HasKey(e => e.SubcountyId);

                entity.ToTable("A_Subcounty");

                entity.Property(e => e.SubcountyId)
                    .HasColumnName("Subcounty_Id")
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.CountyId)
                    .HasColumnName("County_Id")
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.DistrictId)
                    .HasColumnName("District_Id")
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.SubcountyMinistryCode)
                    .HasColumnName("Subcounty_Ministry_Code")
                    .HasMaxLength(2)
                    .IsFixedLength();

                entity.Property(e => e.SubcountyName)
                    .HasColumnName("Subcounty_Name")
                    .HasMaxLength(50);

                entity.HasOne(d => d.District)
                    .WithMany(p => p.ASubcounty)
                    .HasForeignKey(d => d.DistrictId)
                    .HasConstraintName("FK_A_Subcounty_A_District");
            });

            modelBuilder.Entity<ASymptoms>(entity =>
            {
                entity.ToTable("A_Symptoms");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Symptoms).HasMaxLength(100);

                entity.HasOne(d => d.DiseaseNavigation)
                    .WithMany(p => p.ASymptoms)
                    .HasForeignKey(d => d.Disease)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_A_COVID19_Symptoms_A_DiseaseType");
            });

            modelBuilder.Entity<ASystemModules>(entity =>
            {
                entity.HasKey(e => e.ModuleId);

                entity.ToTable("A_System_modules");

                entity.Property(e => e.ModuleId)
                    .HasColumnName("Module_id")
                    .ValueGeneratedNever();

                entity.Property(e => e.ModuleDescription)
                    .HasColumnName("Module_description")
                    .HasMaxLength(1000);
            });

            modelBuilder.Entity<ASystemSection>(entity =>
            {
                entity.HasKey(e => e.SectionId);

                entity.ToTable("A_System_Section");

                entity.Property(e => e.SectionId)
                    .HasColumnName("Section_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ModuleId).HasColumnName("Module_id");

                entity.Property(e => e.SectionDescription)
                    .HasColumnName("Section_description")
                    .HasMaxLength(3000);

                entity.Property(e => e.SectionHeader)
                    .HasColumnName("Section_Header")
                    .HasMaxLength(1000);

                entity.HasOne(d => d.Module)
                    .WithMany(p => p.ASystemSection)
                    .HasForeignKey(d => d.ModuleId)
                    .HasConstraintName("FK_A_System_Section_A_System_modules");
            });

            modelBuilder.Entity<ATitle>(entity =>
            {
                entity.HasKey(e => e.TitleId);

                entity.ToTable("A_Title");

                entity.Property(e => e.TitleId)
                    .HasColumnName("Title_id")
                    .ValueGeneratedNever();

                entity.Property(e => e.TitleDesc)
                    .HasColumnName("Title_desc")
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<AVillage>(entity =>
            {
                entity.HasKey(e => e.VillageId);

                entity.ToTable("A_Village");

                entity.Property(e => e.VillageId)
                    .HasColumnName("Village_Id")
                    .HasMaxLength(5)
                    .IsFixedLength();

                entity.Property(e => e.ParishId)
                    .HasColumnName("Parish_Id")
                    .HasMaxLength(5)
                    .IsFixedLength();

                entity.Property(e => e.VillageMinistryCode)
                    .HasColumnName("Village_Ministry_Code")
                    .HasMaxLength(2)
                    .IsFixedLength();

                entity.Property(e => e.VillageName)
                    .HasColumnName("Village_Name")
                    .HasMaxLength(50);

                entity.HasOne(d => d.Parish)
                    .WithMany(p => p.AVillage)
                    .HasForeignKey(d => d.ParishId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_A_Village_A_Parish");
            });

            modelBuilder.Entity<AYesNo>(entity =>
            {
                entity.ToTable("A_YesNo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.YesNo).HasMaxLength(10);
            });

            modelBuilder.Entity<AspNetRoleClaims>(entity =>
            {
                entity.HasIndex(e => e.RoleId);

                entity.Property(e => e.RoleId).IsRequired();

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.AspNetRoleClaims)
                    .HasForeignKey(d => d.RoleId);
            });

            modelBuilder.Entity<AspNetRoles>(entity =>
            {
                entity.HasIndex(e => e.NormalizedName)
                    .HasName("RoleNameIndex")
                    .IsUnique()
                    .HasFilter("([NormalizedName] IS NOT NULL)");

                entity.Property(e => e.Name).HasMaxLength(256);

                entity.Property(e => e.NormalizedName).HasMaxLength(256);
            });

            modelBuilder.Entity<AspNetUserClaims>(entity =>
            {
                entity.HasIndex(e => e.UserId);

                entity.Property(e => e.UserId).IsRequired();

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserClaims)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserLogins>(entity =>
            {
                entity.HasKey(e => new { e.LoginProvider, e.ProviderKey });

                entity.HasIndex(e => e.UserId);

                entity.Property(e => e.LoginProvider).HasMaxLength(128);

                entity.Property(e => e.ProviderKey).HasMaxLength(128);

                entity.Property(e => e.UserId).IsRequired();

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserLogins)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserRoles>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.RoleId });

                entity.HasIndex(e => e.RoleId);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.AspNetUserRoles)
                    .HasForeignKey(d => d.RoleId);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserRoles)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserTokens>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.LoginProvider, e.Name });

                entity.Property(e => e.LoginProvider).HasMaxLength(128);

                entity.Property(e => e.Name).HasMaxLength(128);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserTokens)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUsers>(entity =>
            {
                entity.HasIndex(e => e.NormalizedEmail)
                    .HasName("EmailIndex");

                entity.HasIndex(e => e.NormalizedUserName)
                    .HasName("UserNameIndex")
                    .IsUnique()
                    .HasFilter("([NormalizedUserName] IS NOT NULL)");

                entity.Property(e => e.Email).HasMaxLength(256);

                entity.Property(e => e.Facility).HasMaxLength(500);

                entity.Property(e => e.NormalizedEmail).HasMaxLength(256);

                entity.Property(e => e.NormalizedUserName).HasMaxLength(256);

                entity.Property(e => e.UserName).HasMaxLength(256);
            });

            modelBuilder.Entity<CoreCaseContacts>(entity =>
            {
                entity.HasKey(e => new { e.ContactCaseNo, e.CaseNo })
                    .HasName("PK_Core_CaseContacts_1");

                entity.ToTable("Core_CaseContacts");

                entity.Property(e => e.ContactCaseNo)
                    .HasColumnName("Contact_CaseNo")
                    .HasMaxLength(100);

                entity.Property(e => e.CaseNo).HasMaxLength(100);

                entity.Property(e => e.AddedBy).HasMaxLength(150);

                entity.Property(e => e.Address).HasMaxLength(150);

                entity.Property(e => e.Age).HasMaxLength(50);

                entity.Property(e => e.AssignFollowUpContactPerson).HasColumnName("AssignFollowUp_ContactPerson");

                entity.Property(e => e.CaseDate)
                    .HasColumnName("Case_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.CertaintyLevel).HasColumnName("Certainty_Level");

                entity.Property(e => e.ClassificationSpecify).HasMaxLength(4000);

                entity.Property(e => e.CountyId)
                    .HasColumnName("County_Id")
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.DateAdded).HasColumnType("datetime");

                entity.Property(e => e.DateDeleted).HasColumnType("datetime");

                entity.Property(e => e.DateEdited).HasColumnType("datetime");

                entity.Property(e => e.DateOfFirstContact)
                    .HasColumnName("Date_of_First_Contact")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateOfInfection)
                    .HasColumnName("Date_of_infection")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateOfLastContact)
                    .HasColumnName("Date_of_Last_Contact")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateOfOutcome)
                    .HasColumnName("Date_of_Outcome")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateOfReporting)
                    .HasColumnName("Date_of_Reporting")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateOnset)
                    .HasColumnName("Date_Onset")
                    .HasColumnType("datetime");

                entity.Property(e => e.DeleteStatus).HasColumnName("deleteStatus");

                entity.Property(e => e.DeletedBy).HasMaxLength(150);

                entity.Property(e => e.DistrictId)
                    .HasColumnName("District_Id")
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.DoB).HasColumnType("datetime");

                entity.Property(e => e.EditedBy).HasMaxLength(150);

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(50);

                entity.Property(e => e.ExposureTypeSpecify)
                    .HasColumnName("ExposureType_Specify")
                    .HasMaxLength(4000);

                entity.Property(e => e.FirstName).HasMaxLength(100);

                entity.Property(e => e.GeneralComments).HasMaxLength(4000);

                entity.Property(e => e.IsContactDateEstimated).HasColumnName("IsContactDate_Estimated");

                entity.Property(e => e.LabResults).HasColumnName("Lab_Results");

                entity.Property(e => e.LabTestDate)
                    .HasColumnName("LabTest_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.LastName).HasMaxLength(100);

                entity.Property(e => e.Latitude).HasMaxLength(50);

                entity.Property(e => e.Longitude).HasMaxLength(50);

                entity.Property(e => e.MiddleName).HasMaxLength(100);

                entity.Property(e => e.NextAction).HasMaxLength(4000);

                entity.Property(e => e.OccupationSpecify).HasMaxLength(4000);

                entity.Property(e => e.OutcomeSpecify)
                    .HasColumnName("Outcome_Specify")
                    .HasMaxLength(4000);

                entity.Property(e => e.ParishId)
                    .HasColumnName("Parish_Id")
                    .HasMaxLength(5)
                    .IsFixedLength();

                entity.Property(e => e.RelationshipSpecify)
                    .HasColumnName("Relationship_Specify")
                    .HasMaxLength(4000);

                entity.Property(e => e.RiskReason).HasMaxLength(4000);

                entity.Property(e => e.SubcountyId)
                    .HasColumnName("Subcounty_Id")
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.Symptoms).HasMaxLength(4000);

                entity.Property(e => e.SymptomsOthers)
                    .HasColumnName("Symptoms_Others")
                    .HasMaxLength(4000);

                entity.Property(e => e.Tel).HasMaxLength(50);

                entity.Property(e => e.Vaccines).HasMaxLength(4000);

                entity.Property(e => e.VillageId)
                    .HasColumnName("Village_Id")
                    .HasMaxLength(5)
                    .IsFixedLength();

                entity.HasOne(d => d.AssignFollowUpContactPersonNavigation)
                    .WithMany(p => p.CoreCaseContacts)
                    .HasForeignKey(d => d.AssignFollowUpContactPerson)
                    .HasConstraintName("FK_Core_CaseContacts_A_Employees");

                entity.HasOne(d => d.CaseNoNavigation)
                    .WithMany(p => p.CoreCaseContacts)
                    .HasForeignKey(d => d.CaseNo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Core_CaseContacts_Core_Cases");

                entity.HasOne(d => d.CertaintyLevelNavigation)
                    .WithMany(p => p.CoreCaseContactsCertaintyLevelNavigation)
                    .HasForeignKey(d => d.CertaintyLevel)
                    .HasConstraintName("FK_Core_CaseContacts_A_RiskLevel1");

                entity.HasOne(d => d.ClassificationNavigation)
                    .WithMany(p => p.CoreCaseContacts)
                    .HasForeignKey(d => d.Classification)
                    .HasConstraintName("FK_Core_CaseContacts_A_Classification");

                entity.HasOne(d => d.County)
                    .WithMany(p => p.CoreCaseContacts)
                    .HasForeignKey(d => d.CountyId)
                    .HasConstraintName("FK_Core_CaseContacts_A_County");

                entity.HasOne(d => d.DiseaseNavigation)
                    .WithMany(p => p.CoreCaseContacts)
                    .HasForeignKey(d => d.Disease)
                    .HasConstraintName("FK_Core_CaseContacts_A_DiseaseType");

                entity.HasOne(d => d.District)
                    .WithMany(p => p.CoreCaseContacts)
                    .HasForeignKey(d => d.DistrictId)
                    .HasConstraintName("FK_Core_CaseContacts_A_District");

                entity.HasOne(d => d.ExposureTypeNavigation)
                    .WithMany(p => p.CoreCaseContacts)
                    .HasForeignKey(d => d.ExposureType)
                    .HasConstraintName("FK_Core_CaseContacts_A_ExposureType");

                entity.HasOne(d => d.FacilityUnitNavigation)
                    .WithMany(p => p.CoreCaseContacts)
                    .HasForeignKey(d => d.FacilityUnit)
                    .HasConstraintName("FK_Core_CaseContacts_A_FacilityUnit");

                entity.HasOne(d => d.IsContactDateEstimatedNavigation)
                    .WithMany(p => p.CoreCaseContactsIsContactDateEstimatedNavigation)
                    .HasForeignKey(d => d.IsContactDateEstimated)
                    .HasConstraintName("FK_Core_CaseContacts_A_YesNo1");

                entity.HasOne(d => d.LabResultsNavigation)
                    .WithMany(p => p.CoreCaseContacts)
                    .HasForeignKey(d => d.LabResults)
                    .HasConstraintName("FK_Core_CaseContacts_A_LabResults");

                entity.HasOne(d => d.OccupationNavigation)
                    .WithMany(p => p.CoreCaseContacts)
                    .HasForeignKey(d => d.Occupation)
                    .HasConstraintName("FK_Core_CaseContacts_A_Occupation");

                entity.HasOne(d => d.OutcomeNavigation)
                    .WithMany(p => p.CoreCaseContacts)
                    .HasForeignKey(d => d.Outcome)
                    .HasConstraintName("FK_Core_CaseContacts_A_COVID19_Outcome");

                entity.HasOne(d => d.Parish)
                    .WithMany(p => p.CoreCaseContacts)
                    .HasForeignKey(d => d.ParishId)
                    .HasConstraintName("FK_Core_CaseContacts_A_Parish");

                entity.HasOne(d => d.RelationshipNavigation)
                    .WithMany(p => p.CoreCaseContacts)
                    .HasForeignKey(d => d.Relationship)
                    .HasConstraintName("FK_Core_CaseContacts_A_Relationship");

                entity.HasOne(d => d.RiskLevelNavigation)
                    .WithMany(p => p.CoreCaseContactsRiskLevelNavigation)
                    .HasForeignKey(d => d.RiskLevel)
                    .HasConstraintName("FK_Core_CaseContacts_A_RiskLevel");

                entity.HasOne(d => d.SexNavigation)
                    .WithMany(p => p.CoreCaseContacts)
                    .HasForeignKey(d => d.Sex)
                    .HasConstraintName("FK_Core_CaseContacts_A_Sex");

                entity.HasOne(d => d.StatusFlagNavigation)
                    .WithMany(p => p.CoreCaseContacts)
                    .HasForeignKey(d => d.StatusFlag)
                    .HasConstraintName("FK_Core_CaseContacts_A_ElevateContact");

                entity.HasOne(d => d.Subcounty)
                    .WithMany(p => p.CoreCaseContacts)
                    .HasForeignKey(d => d.SubcountyId)
                    .HasConstraintName("FK_Core_CaseContacts_A_Subcounty");

                entity.HasOne(d => d.VaccinesReceivedNavigation)
                    .WithMany(p => p.CoreCaseContactsVaccinesReceivedNavigation)
                    .HasForeignKey(d => d.VaccinesReceived)
                    .HasConstraintName("FK_Core_CaseContacts_A_YesNo");

                entity.HasOne(d => d.Village)
                    .WithMany(p => p.CoreCaseContacts)
                    .HasForeignKey(d => d.VillageId)
                    .HasConstraintName("FK_Core_CaseContacts_A_Village");
            });

            modelBuilder.Entity<CoreCaseContactsLog>(entity =>
            {
                entity.HasKey(e => e.ContactCaseLogId);

                entity.ToTable("Core_CaseContactsLog");

                entity.Property(e => e.ContactCaseLogId)
                    .HasColumnName("Contact_CaseLogID")
                    .HasMaxLength(100);

                entity.Property(e => e.AddedBy).HasMaxLength(150);

                entity.Property(e => e.Address).HasMaxLength(150);

                entity.Property(e => e.Age).HasMaxLength(50);

                entity.Property(e => e.AssignFollowUpContactPerson).HasColumnName("AssignFollowUp_ContactPerson");

                entity.Property(e => e.CaseDate)
                    .HasColumnName("Case_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.CaseNo).HasMaxLength(100);

                entity.Property(e => e.CertaintyLevel).HasColumnName("Certainty_Level");

                entity.Property(e => e.ClassificationSpecify).HasMaxLength(4000);

                entity.Property(e => e.ContactCaseNo)
                    .HasColumnName("Contact_CaseNo")
                    .HasMaxLength(100);

                entity.Property(e => e.CountyId)
                    .HasColumnName("County_Id")
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.DateAdded).HasColumnType("datetime");

                entity.Property(e => e.DateDeleted).HasColumnType("datetime");

                entity.Property(e => e.DateEdited).HasColumnType("datetime");

                entity.Property(e => e.DateOfFirstContact)
                    .HasColumnName("Date_of_First_Contact")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateOfInfection)
                    .HasColumnName("Date_of_infection")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateOfLastContact)
                    .HasColumnName("Date_of_Last_Contact")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateOfOutcome)
                    .HasColumnName("Date_of_Outcome")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateOfReporting)
                    .HasColumnName("Date_of_Reporting")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateOnset)
                    .HasColumnName("Date_Onset")
                    .HasColumnType("datetime");

                entity.Property(e => e.DeletedBy).HasMaxLength(150);

                entity.Property(e => e.DistrictId)
                    .HasColumnName("District_Id")
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.DoB).HasColumnType("datetime");

                entity.Property(e => e.EditedBy).HasMaxLength(150);

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(50);

                entity.Property(e => e.ExposureTypeSpecify)
                    .HasColumnName("ExposureType_Specify")
                    .HasMaxLength(4000);

                entity.Property(e => e.FirstName).HasMaxLength(100);

                entity.Property(e => e.GeneralComments).HasMaxLength(4000);

                entity.Property(e => e.IsContactDateEstimated).HasColumnName("IsContactDate_Estimated");

                entity.Property(e => e.LabResults).HasColumnName("Lab_Results");

                entity.Property(e => e.LabTestDate)
                    .HasColumnName("LabTest_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.LastName).HasMaxLength(100);

                entity.Property(e => e.Latitude).HasMaxLength(50);

                entity.Property(e => e.Longitude).HasMaxLength(50);

                entity.Property(e => e.MiddleName).HasMaxLength(100);

                entity.Property(e => e.NextAction).HasMaxLength(4000);

                entity.Property(e => e.OccupationSpecify).HasMaxLength(4000);

                entity.Property(e => e.OutcomeSpecify)
                    .HasColumnName("Outcome_Specify")
                    .HasMaxLength(4000);

                entity.Property(e => e.ParishId)
                    .HasColumnName("Parish_Id")
                    .HasMaxLength(5)
                    .IsFixedLength();

                entity.Property(e => e.RelationshipSpecify)
                    .HasColumnName("Relationship_Specify")
                    .HasMaxLength(4000);

                entity.Property(e => e.RiskReason).HasMaxLength(4000);

                entity.Property(e => e.SubcountyId)
                    .HasColumnName("Subcounty_Id")
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.Symptoms).HasMaxLength(4000);

                entity.Property(e => e.SymptomsOthers)
                    .HasColumnName("Symptoms_Others")
                    .HasMaxLength(4000);

                entity.Property(e => e.Tel).HasMaxLength(50);

                entity.Property(e => e.Vaccines).HasMaxLength(4000);

                entity.Property(e => e.VillageId)
                    .HasColumnName("Village_Id")
                    .HasMaxLength(5)
                    .IsFixedLength();

                entity.HasOne(d => d.County)
                    .WithMany(p => p.CoreCaseContactsLog)
                    .HasForeignKey(d => d.CountyId)
                    .HasConstraintName("FK_Core_CaseContactsLog_A_County");

                entity.HasOne(d => d.District)
                    .WithMany(p => p.CoreCaseContactsLog)
                    .HasForeignKey(d => d.DistrictId)
                    .HasConstraintName("FK_Core_CaseContactsLog_A_District");

                entity.HasOne(d => d.FacilityUnitNavigation)
                    .WithMany(p => p.CoreCaseContactsLog)
                    .HasForeignKey(d => d.FacilityUnit)
                    .HasConstraintName("FK_Core_CaseContactsLog_A_FacilityUnit");

                entity.HasOne(d => d.Parish)
                    .WithMany(p => p.CoreCaseContactsLog)
                    .HasForeignKey(d => d.ParishId)
                    .HasConstraintName("FK_Core_CaseContactsLog_A_Parish");

                entity.HasOne(d => d.RiskLevelNavigation)
                    .WithMany(p => p.CoreCaseContactsLog)
                    .HasForeignKey(d => d.RiskLevel)
                    .HasConstraintName("FK_Core_CaseContactsLog_A_RiskLevel");

                entity.HasOne(d => d.SexNavigation)
                    .WithMany(p => p.CoreCaseContactsLog)
                    .HasForeignKey(d => d.Sex)
                    .HasConstraintName("FK_Core_CaseContactsLog_A_Sex");

                entity.HasOne(d => d.Subcounty)
                    .WithMany(p => p.CoreCaseContactsLog)
                    .HasForeignKey(d => d.SubcountyId)
                    .HasConstraintName("FK_Core_CaseContactsLog_A_Subcounty");

                entity.HasOne(d => d.Village)
                    .WithMany(p => p.CoreCaseContactsLog)
                    .HasForeignKey(d => d.VillageId)
                    .HasConstraintName("FK_Core_CaseContactsLog_A_Village");
            });

            modelBuilder.Entity<CoreCaseContactsMonitoring>(entity =>
            {
                entity.HasKey(e => new { e.ContactCaseNo, e.Days });

                entity.ToTable("Core_CaseContacts_Monitoring");

                entity.Property(e => e.ContactCaseNo)
                    .HasColumnName("Contact_CaseNo")
                    .HasMaxLength(100);

                entity.Property(e => e.AddedBy).HasMaxLength(150);

                entity.Property(e => e.CaseNo).HasMaxLength(100);

                entity.Property(e => e.DateAdded).HasColumnType("datetime");

                entity.Property(e => e.DateDeleted).HasColumnType("datetime");

                entity.Property(e => e.DateEdited).HasColumnType("datetime");

                entity.Property(e => e.DeleteStatus).HasColumnName("deleteStatus");

                entity.Property(e => e.DeletedBy).HasMaxLength(150);

                entity.Property(e => e.EditedBy).HasMaxLength(150);

                entity.Property(e => e.GeneralComments).HasMaxLength(4000);

                entity.Property(e => e.MonitoringDate).HasColumnType("datetime");

                entity.Property(e => e.Temperature).HasMaxLength(10);

                entity.HasOne(d => d.BodyPainsNavigation)
                    .WithMany(p => p.CoreCaseContactsMonitoringBodyPainsNavigation)
                    .HasForeignKey(d => d.BodyPains)
                    .HasConstraintName("FK_Core_CaseContact_Monitoring_A_YesNo4");

                entity.HasOne(d => d.BreathNavigation)
                    .WithMany(p => p.CoreCaseContactsMonitoringBreathNavigation)
                    .HasForeignKey(d => d.Breath)
                    .HasConstraintName("FK_Core_CaseContact_Monitoring_A_YesNo3");

                entity.HasOne(d => d.ChillsNavigation)
                    .WithMany(p => p.CoreCaseContactsMonitoringChillsNavigation)
                    .HasForeignKey(d => d.Chills)
                    .HasConstraintName("FK_Core_CaseContact_Monitoring_A_YesNo");

                entity.HasOne(d => d.CoughNavigation)
                    .WithMany(p => p.CoreCaseContactsMonitoringCoughNavigation)
                    .HasForeignKey(d => d.Cough)
                    .HasConstraintName("FK_Core_CaseContact_Monitoring_A_YesNo1");

                entity.HasOne(d => d.DaysNavigation)
                    .WithMany(p => p.CoreCaseContactsMonitoring)
                    .HasForeignKey(d => d.Days)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Core_CaseContacts_Monitoring_A_Days");

                entity.HasOne(d => d.DiarrheaNavigation)
                    .WithMany(p => p.CoreCaseContactsMonitoringDiarrheaNavigation)
                    .HasForeignKey(d => d.Diarrhea)
                    .HasConstraintName("FK_Core_CaseContact_Monitoring_A_YesNo5");

                entity.HasOne(d => d.SoreThroatNavigation)
                    .WithMany(p => p.CoreCaseContactsMonitoringSoreThroatNavigation)
                    .HasForeignKey(d => d.SoreThroat)
                    .HasConstraintName("FK_Core_CaseContact_Monitoring_A_YesNo2");

                entity.HasOne(d => d.C)
                    .WithMany(p => p.CoreCaseContactsMonitoring)
                    .HasForeignKey(d => new { d.ContactCaseNo, d.CaseNo })
                    .HasConstraintName("FK_Core_CaseContact_Monitoring_Core_CaseContacts");
            });

            modelBuilder.Entity<CoreCaseContactsMonitoringLog>(entity =>
            {
                entity.HasKey(e => e.CoresCaseContactMonLogId);

                entity.ToTable("Core_CaseContacts_MonitoringLog");

                entity.Property(e => e.CoresCaseContactMonLogId)
                    .HasColumnName("CoresCaseContact_MonLogID")
                    .HasMaxLength(100);

                entity.Property(e => e.AddedBy).HasMaxLength(150);

                entity.Property(e => e.CaseNo).HasMaxLength(50);

                entity.Property(e => e.ContactCaseNo)
                    .HasColumnName("Contact_CaseNo")
                    .HasMaxLength(50);

                entity.Property(e => e.DateAdded).HasColumnType("datetime");

                entity.Property(e => e.DateDeleted).HasColumnType("datetime");

                entity.Property(e => e.DateEdited).HasColumnType("datetime");

                entity.Property(e => e.DeletedBy).HasMaxLength(150);

                entity.Property(e => e.EditedBy).HasMaxLength(150);

                entity.Property(e => e.GeneralComments).HasMaxLength(4000);

                entity.Property(e => e.MonitoringDate).HasColumnType("datetime");

                entity.Property(e => e.Temperature).HasMaxLength(10);
            });

            modelBuilder.Entity<CoreCaseContactsMutitpleTests>(entity =>
            {
                entity.HasKey(e => e.Code);

                entity.ToTable("Core_CaseContacts_MutitpleTests");

                entity.Property(e => e.Code).ValueGeneratedNever();

                entity.Property(e => e.AddedBy).HasMaxLength(150);

                entity.Property(e => e.CaseNo).HasMaxLength(100);

                entity.Property(e => e.ContactCaseNo)
                    .HasColumnName("Contact_CaseNo")
                    .HasMaxLength(100);

                entity.Property(e => e.DateAdded).HasColumnType("datetime");

                entity.Property(e => e.DateDeleted).HasColumnType("datetime");

                entity.Property(e => e.DateEdited).HasColumnType("datetime");

                entity.Property(e => e.DeleteStatus).HasColumnName("deleteStatus");

                entity.Property(e => e.DeletedBy).HasMaxLength(150);

                entity.Property(e => e.EditedBy).HasMaxLength(150);

                entity.Property(e => e.GeneralComments).HasMaxLength(4000);

                entity.Property(e => e.Hospital).HasMaxLength(400);

                entity.Property(e => e.LabResults).HasColumnName("Lab_Results");

                entity.Property(e => e.LabTestDate)
                    .HasColumnName("LabTest_Date")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.LabResultsNavigation)
                    .WithMany(p => p.CoreCaseContactsMutitpleTests)
                    .HasForeignKey(d => d.LabResults)
                    .HasConstraintName("FK_Core_CaseContacts_MutitpleTests_A_LabResults");

                entity.HasOne(d => d.C)
                    .WithMany(p => p.CoreCaseContactsMutitpleTests)
                    .HasForeignKey(d => new { d.ContactCaseNo, d.CaseNo })
                    .HasConstraintName("FK_Core_CaseContacts_MutitpleTests_Core_CaseContacts");
            });

            modelBuilder.Entity<CoreCaseFileAttachment>(entity =>
            {
                entity.ToTable("CoreCase_FileAttachment");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.AddedBy).HasMaxLength(150);

                entity.Property(e => e.CaseNo).HasMaxLength(100);

                entity.Property(e => e.DateAdded).HasColumnType("datetime");

                entity.Property(e => e.DateDeleted).HasColumnType("datetime");

                entity.Property(e => e.DateEdited).HasColumnType("datetime");

                entity.Property(e => e.DeletedBy).HasMaxLength(150);

                entity.Property(e => e.Details).HasMaxLength(256);

                entity.Property(e => e.EditedBy).HasMaxLength(150);

                entity.Property(e => e.FileType).HasMaxLength(50);

                entity.Property(e => e.FileUrl).HasColumnName("FileURL");
            });

            modelBuilder.Entity<CoreCasePhoto>(entity =>
            {
                entity.HasKey(e => e.CaseNo);

                entity.ToTable("Core_Case_Photo");

                entity.Property(e => e.CaseNo).HasMaxLength(100);

                entity.Property(e => e.FileType).HasMaxLength(50);
            });

            modelBuilder.Entity<CoreCases>(entity =>
            {
                entity.HasKey(e => e.CaseNo);

                entity.ToTable("Core_Cases");

                entity.Property(e => e.CaseNo).HasMaxLength(100);

                entity.Property(e => e.AddedBy).HasMaxLength(150);

                entity.Property(e => e.Address).HasMaxLength(150);

                entity.Property(e => e.Age).HasMaxLength(50);

                entity.Property(e => e.AlertType).HasMaxLength(4000);

                entity.Property(e => e.CaseDate)
                    .HasColumnName("Case_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.ClassificationSpecify).HasMaxLength(4000);

                entity.Property(e => e.CountyId)
                    .HasColumnName("County_Id")
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.DateAdded).HasColumnType("datetime");

                entity.Property(e => e.DateDeleted).HasColumnType("datetime");

                entity.Property(e => e.DateEdited).HasColumnType("datetime");

                entity.Property(e => e.DateOfInfection)
                    .HasColumnName("Date_of_infection")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateOfOutcome)
                    .HasColumnName("Date_of_Outcome")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateOfReporting)
                    .HasColumnName("Date_of_Reporting")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateOnset)
                    .HasColumnName("Date_Onset")
                    .HasColumnType("datetime");

                entity.Property(e => e.DeleteStatus).HasColumnName("deleteStatus");

                entity.Property(e => e.DeletedBy).HasMaxLength(150);

                entity.Property(e => e.DistrictId)
                    .HasColumnName("District_Id")
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.DoB).HasColumnType("datetime");

                entity.Property(e => e.EditedBy).HasMaxLength(150);

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(50);

                entity.Property(e => e.EvacuationComments).HasMaxLength(4000);

                entity.Property(e => e.EvacuationFacility).HasMaxLength(4000);

                entity.Property(e => e.ExternalCaseNo)
                    .HasColumnName("External_CaseNo")
                    .HasMaxLength(100);

                entity.Property(e => e.FirstName).HasMaxLength(100);

                entity.Property(e => e.GeneralComments).HasMaxLength(4000);

                entity.Property(e => e.LabResults).HasColumnName("Lab_Results");

                entity.Property(e => e.LabTestDate)
                    .HasColumnName("LabTest_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.LastName).HasMaxLength(100);

                entity.Property(e => e.Latitude).HasMaxLength(50);

                entity.Property(e => e.Longitude).HasMaxLength(50);

                entity.Property(e => e.MiddleName).HasMaxLength(100);

                entity.Property(e => e.NextAction).HasMaxLength(4000);

                entity.Property(e => e.OccupationSpecify).HasMaxLength(4000);

                entity.Property(e => e.OutcomeSpecify)
                    .HasColumnName("Outcome_Specify")
                    .HasMaxLength(4000);

                entity.Property(e => e.ParishId)
                    .HasColumnName("Parish_Id")
                    .HasMaxLength(5)
                    .IsFixedLength();

                entity.Property(e => e.RiskReason).HasMaxLength(4000);

                entity.Property(e => e.SamplingDate).HasColumnType("datetime");

                entity.Property(e => e.SamplingDistrict).HasMaxLength(500);

                entity.Property(e => e.SamplingFacility).HasMaxLength(4000);

                entity.Property(e => e.SubcountyId)
                    .HasColumnName("Subcounty_Id")
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.Symptoms).HasMaxLength(4000);

                entity.Property(e => e.SymptomsOthers)
                    .HasColumnName("Symptoms_Others")
                    .HasMaxLength(4000);

                entity.Property(e => e.Tel).HasMaxLength(50);

                entity.Property(e => e.TestingLab).HasMaxLength(4000);

                entity.Property(e => e.VillageId)
                    .HasColumnName("Village_Id")
                    .HasMaxLength(5)
                    .IsFixedLength();

                entity.HasOne(d => d.CaseEvacuatedNavigation)
                    .WithMany(p => p.CoreCasesCaseEvacuatedNavigation)
                    .HasForeignKey(d => d.CaseEvacuated)
                    .HasConstraintName("FK_Core_Cases_A_YesNo1");

                entity.HasOne(d => d.ClassificationNavigation)
                    .WithMany(p => p.CoreCases)
                    .HasForeignKey(d => d.Classification)
                    .HasConstraintName("FK_Core_Cases_A_Classification");

                entity.HasOne(d => d.County)
                    .WithMany(p => p.CoreCases)
                    .HasForeignKey(d => d.CountyId)
                    .HasConstraintName("FK_Core_Cases_A_County");

                entity.HasOne(d => d.DiseaseNavigation)
                    .WithMany(p => p.CoreCases)
                    .HasForeignKey(d => d.Disease)
                    .HasConstraintName("FK_Core_Cases_A_DiseaseType");

                entity.HasOne(d => d.District)
                    .WithMany(p => p.CoreCases)
                    .HasForeignKey(d => d.DistrictId)
                    .HasConstraintName("FK_Core_Cases_A_District");

                entity.HasOne(d => d.FacilityUnitNavigation)
                    .WithMany(p => p.CoreCases)
                    .HasForeignKey(d => d.FacilityUnit)
                    .HasConstraintName("FK_Core_Cases_A_FacilityUnit");

                entity.HasOne(d => d.LabResultsNavigation)
                    .WithMany(p => p.CoreCases)
                    .HasForeignKey(d => d.LabResults)
                    .HasConstraintName("FK_Core_Cases_A_LabResults");

                entity.HasOne(d => d.OccupationNavigation)
                    .WithMany(p => p.CoreCases)
                    .HasForeignKey(d => d.Occupation)
                    .HasConstraintName("FK_Core_Cases_A_Occupation");

                entity.HasOne(d => d.OutcomeNavigation)
                    .WithMany(p => p.CoreCases)
                    .HasForeignKey(d => d.Outcome)
                    .HasConstraintName("FK_Core_Cases_A_COVID19_Outcome");

                entity.HasOne(d => d.Parish)
                    .WithMany(p => p.CoreCases)
                    .HasForeignKey(d => d.ParishId)
                    .HasConstraintName("FK_Core_Cases_A_Parish");

                entity.HasOne(d => d.RiskLevelNavigation)
                    .WithMany(p => p.CoreCases)
                    .HasForeignKey(d => d.RiskLevel)
                    .HasConstraintName("FK_Core_Cases_A_RiskLevel");

                entity.HasOne(d => d.SexNavigation)
                    .WithMany(p => p.CoreCases)
                    .HasForeignKey(d => d.Sex)
                    .HasConstraintName("FK_Core_Cases_A_Sex");

                entity.HasOne(d => d.StatusFlagNavigation)
                    .WithMany(p => p.CoreCases)
                    .HasForeignKey(d => d.StatusFlag)
                    .HasConstraintName("FK_Core_Cases_A_ElevateContact");

                entity.HasOne(d => d.Subcounty)
                    .WithMany(p => p.CoreCases)
                    .HasForeignKey(d => d.SubcountyId)
                    .HasConstraintName("FK_Core_Cases_A_Subcounty");

                entity.HasOne(d => d.Village)
                    .WithMany(p => p.CoreCases)
                    .HasForeignKey(d => d.VillageId)
                    .HasConstraintName("FK_Core_Cases_A_Village");

                entity.HasOne(d => d.WasContactNavigation)
                    .WithMany(p => p.CoreCasesWasContactNavigation)
                    .HasForeignKey(d => d.WasContact)
                    .HasConstraintName("FK_Core_Cases_A_YesNo");
            });

            modelBuilder.Entity<CoreCommunitySurveillance>(entity =>
            {
                entity.HasKey(e => e.CaseNo);

                entity.ToTable("Core_CommunitySurveillance");

                entity.Property(e => e.CaseNo).HasMaxLength(100);

                entity.Property(e => e.AddedBy).HasMaxLength(150);

                entity.Property(e => e.Address).HasMaxLength(150);

                entity.Property(e => e.Age).HasMaxLength(50);

                entity.Property(e => e.AssignFollowUpContactPerson).HasColumnName("AssignFollowUp_ContactPerson");

                entity.Property(e => e.CaseDate)
                    .HasColumnName("Case_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.ClassificationSpecify).HasMaxLength(4000);

                entity.Property(e => e.CountyId)
                    .HasColumnName("County_Id")
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.DateAdded).HasColumnType("datetime");

                entity.Property(e => e.DateDeleted).HasColumnType("datetime");

                entity.Property(e => e.DateEdited).HasColumnType("datetime");

                entity.Property(e => e.DateOfInfection)
                    .HasColumnName("Date_of_infection")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateOfOutcome)
                    .HasColumnName("Date_of_Outcome")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateOfReporting)
                    .HasColumnName("Date_of_Reporting")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateOnset)
                    .HasColumnName("Date_Onset")
                    .HasColumnType("datetime");

                entity.Property(e => e.DeleteStatus).HasColumnName("deleteStatus");

                entity.Property(e => e.DeletedBy).HasMaxLength(150);

                entity.Property(e => e.DistrictId)
                    .HasColumnName("District_Id")
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.DoB).HasColumnType("datetime");

                entity.Property(e => e.EditedBy).HasMaxLength(150);

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(50);

                entity.Property(e => e.ExternalCaseNo)
                    .HasColumnName("External_CaseNo")
                    .HasMaxLength(100);

                entity.Property(e => e.FirstName).HasMaxLength(100);

                entity.Property(e => e.GeneralComments).HasMaxLength(4000);

                entity.Property(e => e.LabResults).HasColumnName("Lab_Results");

                entity.Property(e => e.LabTestDate)
                    .HasColumnName("LabTest_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.LastName).HasMaxLength(100);

                entity.Property(e => e.Latitude).HasMaxLength(50);

                entity.Property(e => e.Longitude).HasMaxLength(50);

                entity.Property(e => e.MiddleName).HasMaxLength(100);

                entity.Property(e => e.NextAction).HasMaxLength(4000);

                entity.Property(e => e.OccupationSpecify).HasMaxLength(4000);

                entity.Property(e => e.OutcomeSpecify)
                    .HasColumnName("Outcome_Specify")
                    .HasMaxLength(4000);

                entity.Property(e => e.ParishId)
                    .HasColumnName("Parish_Id")
                    .HasMaxLength(5)
                    .IsFixedLength();

                entity.Property(e => e.RiskReason).HasMaxLength(4000);

                entity.Property(e => e.SubcountyId)
                    .HasColumnName("Subcounty_Id")
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.Symptoms).HasMaxLength(4000);

                entity.Property(e => e.SymptomsOthers)
                    .HasColumnName("Symptoms_Others")
                    .HasMaxLength(4000);

                entity.Property(e => e.Tel).HasMaxLength(50);

                entity.Property(e => e.VillageId)
                    .HasColumnName("Village_Id")
                    .HasMaxLength(5)
                    .IsFixedLength();

                entity.HasOne(d => d.ClassificationNavigation)
                    .WithMany(p => p.CoreCommunitySurveillance)
                    .HasForeignKey(d => d.Classification)
                    .HasConstraintName("FK_Core_CommunitySurveillance_A_Classification");

                entity.HasOne(d => d.County)
                    .WithMany(p => p.CoreCommunitySurveillance)
                    .HasForeignKey(d => d.CountyId)
                    .HasConstraintName("FK_Core_CommunitySurveillance_A_County");

                entity.HasOne(d => d.DiseaseNavigation)
                    .WithMany(p => p.CoreCommunitySurveillance)
                    .HasForeignKey(d => d.Disease)
                    .HasConstraintName("FK_Core_CommunitySurveillance_A_DiseaseType");

                entity.HasOne(d => d.District)
                    .WithMany(p => p.CoreCommunitySurveillance)
                    .HasForeignKey(d => d.DistrictId)
                    .HasConstraintName("FK_Core_CommunitySurveillance_A_District");

                entity.HasOne(d => d.FacilityUnitNavigation)
                    .WithMany(p => p.CoreCommunitySurveillance)
                    .HasForeignKey(d => d.FacilityUnit)
                    .HasConstraintName("FK_Core_CommunitySurveillance_A_FacilityUnit");

                entity.HasOne(d => d.LabResultsNavigation)
                    .WithMany(p => p.CoreCommunitySurveillance)
                    .HasForeignKey(d => d.LabResults)
                    .HasConstraintName("FK_Core_CommunitySurveillance_A_LabResults");

                entity.HasOne(d => d.OccupationNavigation)
                    .WithMany(p => p.CoreCommunitySurveillance)
                    .HasForeignKey(d => d.Occupation)
                    .HasConstraintName("FK_Core_CommunitySurveillance_A_Occupation");

                entity.HasOne(d => d.OperationalAreaNavigation)
                    .WithMany(p => p.CoreCommunitySurveillance)
                    .HasForeignKey(d => d.OperationalArea)
                    .HasConstraintName("FK_Core_CommunitySurveillance_A_OperationalArea");

                entity.HasOne(d => d.OutcomeNavigation)
                    .WithMany(p => p.CoreCommunitySurveillance)
                    .HasForeignKey(d => d.Outcome)
                    .HasConstraintName("FK_Core_CommunitySurveillance_A_Outcome");

                entity.HasOne(d => d.Parish)
                    .WithMany(p => p.CoreCommunitySurveillance)
                    .HasForeignKey(d => d.ParishId)
                    .HasConstraintName("FK_Core_CommunitySurveillance_A_Parish");

                entity.HasOne(d => d.RiskLevelNavigation)
                    .WithMany(p => p.CoreCommunitySurveillance)
                    .HasForeignKey(d => d.RiskLevel)
                    .HasConstraintName("FK_Core_CommunitySurveillance_A_RiskLevel");

                entity.HasOne(d => d.SexNavigation)
                    .WithMany(p => p.CoreCommunitySurveillance)
                    .HasForeignKey(d => d.Sex)
                    .HasConstraintName("FK_Core_CommunitySurveillance_A_Sex");

                entity.HasOne(d => d.Subcounty)
                    .WithMany(p => p.CoreCommunitySurveillance)
                    .HasForeignKey(d => d.SubcountyId)
                    .HasConstraintName("FK_Core_CommunitySurveillance_A_Subcounty");

                entity.HasOne(d => d.Village)
                    .WithMany(p => p.CoreCommunitySurveillance)
                    .HasForeignKey(d => d.VillageId)
                    .HasConstraintName("FK_Core_CommunitySurveillance_A_Village");

                entity.HasOne(d => d.WasContactNavigation)
                    .WithMany(p => p.CoreCommunitySurveillance)
                    .HasForeignKey(d => d.WasContact)
                    .HasConstraintName("FK_Core_CommunitySurveillance_A_YesNo");
            });

            modelBuilder.Entity<CoreComplianceVisit>(entity =>
            {
                entity.HasKey(e => e.ComplianceCode)
                    .HasName("PK_Core_ComplianceVisit_1");

                entity.ToTable("Core_ComplianceVisit");

                entity.Property(e => e.ComplianceCode).HasMaxLength(100);

                entity.Property(e => e.AddedBy).HasMaxLength(150);

                entity.Property(e => e.DateAdded).HasColumnType("datetime");

                entity.Property(e => e.DateDeleted).HasColumnType("datetime");

                entity.Property(e => e.DateEdited).HasColumnType("datetime");

                entity.Property(e => e.DateOfVisit)
                    .HasColumnName("Date_of_Visit")
                    .HasColumnType("datetime");

                entity.Property(e => e.DeleteStatus).HasColumnName("deleteStatus");

                entity.Property(e => e.DeletedBy).HasMaxLength(150);

                entity.Property(e => e.EditedBy).HasMaxLength(150);

                entity.Property(e => e.GeneralComments).HasMaxLength(4000);

                entity.Property(e => e.Remarks).HasMaxLength(4000);

                entity.Property(e => e.TemperatureGuns).HasColumnName("Temperature_Guns");

                entity.HasOne(d => d.FacilityCodeNavigation)
                    .WithMany(p => p.CoreComplianceVisit)
                    .HasForeignKey(d => d.FacilityCode)
                    .HasConstraintName("FK_Core_ComplianceVisit_A_FacilityUnit1");

                entity.HasOne(d => d.HandWashersNavigation)
                    .WithMany(p => p.CoreComplianceVisitHandWashersNavigation)
                    .HasForeignKey(d => d.HandWashers)
                    .HasConstraintName("FK_Core_ComplianceVisit_A_YesNo1");

                entity.HasOne(d => d.MaskWearingNavigation)
                    .WithMany(p => p.CoreComplianceVisitMaskWearingNavigation)
                    .HasForeignKey(d => d.MaskWearing)
                    .HasConstraintName("FK_Core_ComplianceVisit_A_YesNo4");

                entity.HasOne(d => d.RecordKeepingNavigation)
                    .WithMany(p => p.CoreComplianceVisitRecordKeepingNavigation)
                    .HasForeignKey(d => d.RecordKeeping)
                    .HasConstraintName("FK_Core_ComplianceVisit_A_YesNo3");

                entity.HasOne(d => d.SocialDistancingNavigation)
                    .WithMany(p => p.CoreComplianceVisitSocialDistancingNavigation)
                    .HasForeignKey(d => d.SocialDistancing)
                    .HasConstraintName("FK_Core_ComplianceVisit_A_YesNo2");

                entity.HasOne(d => d.SuperviorCodeNavigation)
                    .WithMany(p => p.CoreComplianceVisit)
                    .HasForeignKey(d => d.SuperviorCode)
                    .HasConstraintName("FK_Core_ComplianceVisit_A_Employees");

                entity.HasOne(d => d.TemperatureGunsNavigation)
                    .WithMany(p => p.CoreComplianceVisitTemperatureGunsNavigation)
                    .HasForeignKey(d => d.TemperatureGuns)
                    .HasConstraintName("FK_Core_ComplianceVisit_A_YesNo");

                entity.HasOne(d => d.WashRoomsNavigation)
                    .WithMany(p => p.CoreComplianceVisitWashRoomsNavigation)
                    .HasForeignKey(d => d.WashRooms)
                    .HasConstraintName("FK_Core_ComplianceVisit_A_YesNo5");
            });

            modelBuilder.Entity<CoreContactCaseNoPhoto>(entity =>
            {
                entity.HasKey(e => e.ContactCaseNo);

                entity.ToTable("Core_Contact_CaseNo_Photo");

                entity.Property(e => e.ContactCaseNo)
                    .HasColumnName("Contact_CaseNo")
                    .HasMaxLength(100);

                entity.Property(e => e.FileType).HasMaxLength(50);
            });

            modelBuilder.Entity<CoreContactsFileAttachment>(entity =>
            {
                entity.ToTable("CoreContacts_FileAttachment");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.AddedBy).HasMaxLength(150);

                entity.Property(e => e.CaseNo).HasMaxLength(100);

                entity.Property(e => e.DateAdded).HasColumnType("datetime");

                entity.Property(e => e.DateDeleted).HasColumnType("datetime");

                entity.Property(e => e.DateEdited).HasColumnType("datetime");

                entity.Property(e => e.DeletedBy).HasMaxLength(150);

                entity.Property(e => e.Details).HasMaxLength(256);

                entity.Property(e => e.EditedBy).HasMaxLength(150);

                entity.Property(e => e.FileType).HasMaxLength(50);

                entity.Property(e => e.FileUrl).HasColumnName("FileURL");
            });

            modelBuilder.Entity<CorePublicTransport>(entity =>
            {
                entity.HasKey(e => e.Code);

                entity.ToTable("Core_PublicTransport");

                entity.Property(e => e.Code).HasMaxLength(150);

                entity.Property(e => e.AddedBy).HasMaxLength(150);

                entity.Property(e => e.BusCompany).HasMaxLength(150);

                entity.Property(e => e.BusNo).HasMaxLength(150);

                entity.Property(e => e.ConductorAddress).HasMaxLength(150);

                entity.Property(e => e.ConductorName).HasMaxLength(150);

                entity.Property(e => e.ConductorTel).HasMaxLength(100);

                entity.Property(e => e.DateAdded).HasColumnType("datetime");

                entity.Property(e => e.DateDeleted).HasColumnType("datetime");

                entity.Property(e => e.DateEdited).HasColumnType("datetime");

                entity.Property(e => e.DeleteStatus).HasColumnName("deleteStatus");

                entity.Property(e => e.DeletedBy).HasMaxLength(150);

                entity.Property(e => e.Destination).HasMaxLength(550);

                entity.Property(e => e.DriverAddress).HasMaxLength(150);

                entity.Property(e => e.DriverName).HasMaxLength(150);

                entity.Property(e => e.DriverTel).HasMaxLength(100);

                entity.Property(e => e.EditedBy).HasMaxLength(150);

                entity.Property(e => e.Origin).HasMaxLength(550);

                entity.Property(e => e.TransportDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<CorePublicTransportDetials>(entity =>
            {
                entity.HasKey(e => new { e.BusCode, e.PassengerCode });

                entity.ToTable("Core_PublicTransportDetials");

                entity.Property(e => e.BusCode).HasMaxLength(150);

                entity.Property(e => e.PassengerCode).HasMaxLength(150);

                entity.Property(e => e.AddedBy).HasMaxLength(150);

                entity.Property(e => e.Comments).HasMaxLength(4000);

                entity.Property(e => e.DateAdded).HasColumnType("datetime");

                entity.Property(e => e.DateDeleted).HasColumnType("datetime");

                entity.Property(e => e.DateEdited).HasColumnType("datetime");

                entity.Property(e => e.DeleteStatus).HasColumnName("deleteStatus");

                entity.Property(e => e.DeletedBy).HasMaxLength(150);

                entity.Property(e => e.EditedBy).HasMaxLength(150);

                entity.Property(e => e.NationalId)
                    .HasColumnName("National_ID")
                    .HasMaxLength(100);

                entity.Property(e => e.Remarks).HasMaxLength(4000);

                entity.Property(e => e.Tel).HasMaxLength(150);

                entity.Property(e => e.Temperature).HasMaxLength(50);

                entity.Property(e => e.WhereFrom).HasMaxLength(550);

                entity.Property(e => e.WhereTo).HasMaxLength(550);

                entity.HasOne(d => d.BusCodeNavigation)
                    .WithMany(p => p.CorePublicTransportDetials)
                    .HasForeignKey(d => d.BusCode)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Core_PublicTransportDetials_Core_PublicTransport");

                entity.HasOne(d => d.WearingMaskNavigation)
                    .WithMany(p => p.CorePublicTransportDetials)
                    .HasForeignKey(d => d.WearingMask)
                    .HasConstraintName("FK_Core_PublicTransportDetials_A_YesNo");
            });

            modelBuilder.Entity<CoreSitRep>(entity =>
            {
                entity.HasKey(e => new { e.Division, e.ReportingDate, e.SitRepCategoryId });

                entity.ToTable("Core_SitRep");

                entity.Property(e => e.Division)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.ReportingDate).HasColumnType("datetime");

                entity.Property(e => e.SitRepCategoryId).HasColumnName("SitRepCategoryID");

                entity.Property(e => e.AddedBy).HasMaxLength(150);

                entity.Property(e => e.DateAdded).HasColumnType("datetime");

                entity.Property(e => e.DateDeleted).HasColumnType("datetime");

                entity.Property(e => e.DateEdited).HasColumnType("datetime");

                entity.Property(e => e.DateofConfirmation).HasColumnType("datetime");

                entity.Property(e => e.DeleteStatus).HasColumnName("deleteStatus");

                entity.Property(e => e.DeletedBy).HasMaxLength(150);

                entity.Property(e => e.District)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.EditedBy).HasMaxLength(150);

                entity.Property(e => e.ValueResponse).HasMaxLength(50);

                entity.HasOne(d => d.DistrictNavigation)
                    .WithMany(p => p.CoreSitRep)
                    .HasForeignKey(d => d.District)
                    .HasConstraintName("FK_Core_SitRep_A_District");

                entity.HasOne(d => d.DivisionNavigation)
                    .WithMany(p => p.CoreSitRep)
                    .HasForeignKey(d => d.Division)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Core_SitRep_A_County");

                entity.HasOne(d => d.SitRepCategory)
                    .WithMany(p => p.CoreSitRep)
                    .HasForeignKey(d => d.SitRepCategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Core_SitRep_A_SitRepCategory");
            });

            modelBuilder.Entity<CoreSitRepHeader>(entity =>
            {
                entity.HasKey(e => new { e.Division, e.ReportingDate });

                entity.ToTable("Core_SitRep_Header");

                entity.Property(e => e.Division)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.ReportingDate).HasColumnType("datetime");

                entity.Property(e => e.AddedBy).HasMaxLength(150);

                entity.Property(e => e.DateAdded).HasColumnType("datetime");

                entity.Property(e => e.DateDeleted).HasColumnType("datetime");

                entity.Property(e => e.DateEdited).HasColumnType("datetime");

                entity.Property(e => e.DateofConfirmation).HasColumnType("datetime");

                entity.Property(e => e.DeleteStatus).HasColumnName("deleteStatus");

                entity.Property(e => e.DeletedBy).HasMaxLength(150);

                entity.Property(e => e.District)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.EditedBy).HasMaxLength(150);

                entity.HasOne(d => d.DistrictNavigation)
                    .WithMany(p => p.CoreSitRepHeader)
                    .HasForeignKey(d => d.District)
                    .HasConstraintName("FK_Core_SitRep_Header_A_District");
            });

            modelBuilder.Entity<Messages>(entity =>
            {
                entity.HasKey(e => e.MessageId);

                entity.Property(e => e.MessageId).HasColumnName("MessageID");

                entity.Property(e => e.IsAchieved).HasColumnName("Is_Achieved");

                entity.Property(e => e.IsDeleted).HasColumnName("Is_Deleted");

                entity.Property(e => e.IsRead).HasColumnName("Is_Read");

                entity.Property(e => e.MessageDetails)
                    .HasColumnName("Message_Details")
                    .IsUnicode(false);

                entity.Property(e => e.ReceivedBy)
                    .HasColumnName("Received_By")
                    .HasMaxLength(2000);

                entity.Property(e => e.SentBy)
                    .HasColumnName("Sent_By")
                    .HasMaxLength(2000);

                entity.Property(e => e.SentDate)
                    .HasColumnName("Sent_Date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<MobileLogin>(entity =>
            {
                entity.HasKey(e => e.LoginId);

                entity.Property(e => e.LoginId).ValueGeneratedNever();

                entity.Property(e => e.ExpiryDate).HasColumnType("datetime");

                entity.Property(e => e.LoginCode).HasMaxLength(450);

                entity.Property(e => e.LoginDate).HasColumnType("datetime");

                entity.Property(e => e.UserId).HasMaxLength(450);
            });

            modelBuilder.Entity<ViewCases>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("View_Cases");

                entity.Property(e => e.CaseNo)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.DeleteStatus).HasColumnName("deleteStatus");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(302);
            });

            modelBuilder.Entity<ViewComplianceVisit>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("View_ComplianceVisit");

                entity.Property(e => e.ComplianceCode)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.DateOfVisit)
                    .HasColumnName("Date_of_Visit")
                    .HasColumnType("datetime");

                entity.Property(e => e.DeleteStatus).HasColumnName("deleteStatus");
            });

            modelBuilder.Entity<ViewContacts>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("View_Contacts");

                entity.Property(e => e.ContactCaseNo)
                    .IsRequired()
                    .HasColumnName("Contact_CaseNo")
                    .HasMaxLength(100);

                entity.Property(e => e.DeleteStatus).HasColumnName("deleteStatus");

                entity.Property(e => e.FullName)
                    .IsRequired()
                    .HasMaxLength(302);
            });

            modelBuilder.Entity<ViewCoreCaseContactsMonitoring>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("View_Core_CaseContacts_Monitoring");

                entity.Property(e => e.ContactCaseNo)
                    .IsRequired()
                    .HasColumnName("Contact_CaseNo")
                    .HasMaxLength(50);

                entity.Property(e => e.DayEight)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.DayEleven)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.DayFive)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.DayFour)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.DayFourteen)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.DayNine)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.DayOne)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.DaySeven)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.DaySix)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.DayTen)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.DayThirteen)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.DayThree)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.DayTwelve)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.DayTwo)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Measures)
                    .IsRequired()
                    .HasMaxLength(25)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ViewCoreCaseContactsMonitoringEdit>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("View_Core_CaseContacts_MonitoringEdit");

                entity.Property(e => e.AddedBy).HasMaxLength(150);

                entity.Property(e => e.BodyPainsDetails)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.BreathDetails)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.CaseNo)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ChillsDetails)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.ContactCaseNo)
                    .IsRequired()
                    .HasColumnName("Contact_CaseNo")
                    .HasMaxLength(50);

                entity.Property(e => e.CoughDetails)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.DateAdded).HasColumnType("datetime");

                entity.Property(e => e.DateDeleted).HasColumnType("datetime");

                entity.Property(e => e.DateEdited).HasColumnType("datetime");

                entity.Property(e => e.DaysDetails)
                    .IsRequired()
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.DeletedBy).HasMaxLength(150);

                entity.Property(e => e.DiarrheaDetails)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.EditedBy).HasMaxLength(150);

                entity.Property(e => e.GeneralComments).HasMaxLength(4000);

                entity.Property(e => e.MonitoringCaseNo).HasMaxLength(50);

                entity.Property(e => e.MonitoringContactCaseNo)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.MonitoringDate).HasColumnType("datetime");

                entity.Property(e => e.SoreThroatDetails)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.Temperature).HasMaxLength(10);
            });

            modelBuilder.Entity<ViewDashboardCases>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("View_Dashboard_Cases");

                entity.Property(e => e.Age).HasMaxLength(50);

                entity.Property(e => e.AgeCategory)
                    .IsRequired()
                    .HasMaxLength(24)
                    .IsUnicode(false);

                entity.Property(e => e.CaseDate)
                    .HasColumnName("Case_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.CaseNo)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.CountyName)
                    .HasColumnName("County_Name")
                    .HasMaxLength(50);

                entity.Property(e => e.DateOfInfection)
                    .HasColumnName("Date_of_infection")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateOfOutcome)
                    .HasColumnName("Date_of_Outcome")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateOfReporting)
                    .HasColumnName("Date_of_Reporting")
                    .HasColumnType("datetime");

                entity.Property(e => e.DeleteStatus).HasColumnName("deleteStatus");

                entity.Property(e => e.DistrictName)
                    .HasColumnName("District_Name")
                    .HasMaxLength(50);

                entity.Property(e => e.ExternalCaseNo)
                    .HasColumnName("External_CaseNo")
                    .HasMaxLength(100);

                entity.Property(e => e.FirstName).HasMaxLength(100);

                entity.Property(e => e.Gender).HasMaxLength(50);

                entity.Property(e => e.LabTestDate)
                    .HasColumnName("LabTest_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Labresults).HasMaxLength(50);

                entity.Property(e => e.LastName).HasMaxLength(100);

                entity.Property(e => e.Latitude).HasMaxLength(50);

                entity.Property(e => e.Longitude).HasMaxLength(50);

                entity.Property(e => e.MiddleName).HasMaxLength(100);

                entity.Property(e => e.Outcome).HasMaxLength(150);

                entity.Property(e => e.ParishName)
                    .HasColumnName("Parish_Name")
                    .HasMaxLength(50);

                entity.Property(e => e.RiskLevel).HasMaxLength(100);

                entity.Property(e => e.SubcountyName)
                    .HasColumnName("Subcounty_Name")
                    .HasMaxLength(50);

                entity.Property(e => e.VillageName)
                    .HasColumnName("Village_Name")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<ViewDashboardCasesContacts>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("View_Dashboard_CasesContacts");

                entity.Property(e => e.Age).HasMaxLength(50);

                entity.Property(e => e.AgeCategory)
                    .IsRequired()
                    .HasMaxLength(24)
                    .IsUnicode(false);

                entity.Property(e => e.CaseDate)
                    .HasColumnName("Case_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.CaseNo)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.ContactCaseNo)
                    .IsRequired()
                    .HasColumnName("Contact_CaseNo")
                    .HasMaxLength(100);

                entity.Property(e => e.CountyName)
                    .HasColumnName("County_Name")
                    .HasMaxLength(50);

                entity.Property(e => e.DateOfInfection)
                    .HasColumnName("Date_of_infection")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateOfOutcome)
                    .HasColumnName("Date_of_Outcome")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateOfReporting)
                    .HasColumnName("Date_of_Reporting")
                    .HasColumnType("datetime");

                entity.Property(e => e.DeleteStatus).HasColumnName("deleteStatus");

                entity.Property(e => e.DistrictName)
                    .HasColumnName("District_Name")
                    .HasMaxLength(50);

                entity.Property(e => e.ExposureType).HasMaxLength(50);

                entity.Property(e => e.FirstName).HasMaxLength(100);

                entity.Property(e => e.Gender).HasMaxLength(50);

                entity.Property(e => e.LabTestDate)
                    .HasColumnName("LabTest_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Labresults).HasMaxLength(50);

                entity.Property(e => e.LastName).HasMaxLength(100);

                entity.Property(e => e.MiddleName).HasMaxLength(100);

                entity.Property(e => e.MonitoringOfficer)
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.Outcome).HasMaxLength(150);

                entity.Property(e => e.ParishName)
                    .HasColumnName("Parish_Name")
                    .HasMaxLength(50);

                entity.Property(e => e.Relationship).HasMaxLength(50);

                entity.Property(e => e.RiskLevel).HasMaxLength(100);

                entity.Property(e => e.SubcountyName)
                    .HasColumnName("Subcounty_Name")
                    .HasMaxLength(50);

                entity.Property(e => e.VillageName)
                    .HasColumnName("Village_Name")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<ViewDashboardCommunitySurveillance>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("View_Dashboard_CommunitySurveillance");

                entity.Property(e => e.Age).HasMaxLength(50);

                entity.Property(e => e.AgeCategory)
                    .IsRequired()
                    .HasMaxLength(24)
                    .IsUnicode(false);

                entity.Property(e => e.CaseDate)
                    .HasColumnName("Case_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.CaseNo)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.CountyName)
                    .HasColumnName("County_Name")
                    .HasMaxLength(50);

                entity.Property(e => e.DateOfInfection)
                    .HasColumnName("Date_of_infection")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateOfOutcome)
                    .HasColumnName("Date_of_Outcome")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateOfReporting)
                    .HasColumnName("Date_of_Reporting")
                    .HasColumnType("datetime");

                entity.Property(e => e.DeleteStatus).HasColumnName("deleteStatus");

                entity.Property(e => e.DistrictName)
                    .HasColumnName("District_Name")
                    .HasMaxLength(50);

                entity.Property(e => e.Employee)
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.ExternalCaseNo)
                    .HasColumnName("External_CaseNo")
                    .HasMaxLength(100);

                entity.Property(e => e.Facility).HasMaxLength(150);

                entity.Property(e => e.FirstName).HasMaxLength(100);

                entity.Property(e => e.Gender).HasMaxLength(50);

                entity.Property(e => e.LabTestDate)
                    .HasColumnName("LabTest_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Labresults).HasMaxLength(50);

                entity.Property(e => e.LastName).HasMaxLength(100);

                entity.Property(e => e.Latitude).HasMaxLength(50);

                entity.Property(e => e.Longitude).HasMaxLength(50);

                entity.Property(e => e.MiddleName).HasMaxLength(100);

                entity.Property(e => e.OperationalAreaName).HasMaxLength(150);

                entity.Property(e => e.Outcome).HasMaxLength(150);

                entity.Property(e => e.ParishName)
                    .HasColumnName("Parish_Name")
                    .HasMaxLength(50);

                entity.Property(e => e.RiskLevel).HasMaxLength(100);

                entity.Property(e => e.SubcountyName)
                    .HasColumnName("Subcounty_Name")
                    .HasMaxLength(50);

                entity.Property(e => e.VillageName)
                    .HasColumnName("Village_Name")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<ViewDashboardReportSitRepCombined>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("View_Dashboard_Report_SitRep_Combined");

                entity.Property(e => e.Category).HasMaxLength(4000);

                entity.Property(e => e.KampalaCentralDivision).HasColumnName("KAMPALA CENTRAL DIVISION");

                entity.Property(e => e.KawempeDivision).HasColumnName("KAWEMPE DIVISION");

                entity.Property(e => e.MakindyeDivision).HasColumnName("MAKINDYE DIVISION");

                entity.Property(e => e.NakawaDivision).HasColumnName("NAKAWA DIVISION");

                entity.Property(e => e.RubagaDivision).HasColumnName("RUBAGA DIVISION");

                entity.Property(e => e.SitRepCategoryId).HasColumnName("SitRepCategoryID");

                entity.Property(e => e.Variable).HasMaxLength(4000);
            });

            modelBuilder.Entity<ViewDashboardsCompliance>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("View_Dashboards_Compliance");

                entity.Property(e => e.AirConditioning)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.ComplianceCode)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.DateOfVisit)
                    .HasColumnName("Date_of_Visit")
                    .HasColumnType("datetime");

                entity.Property(e => e.DeleteStatus).HasColumnName("deleteStatus");

                entity.Property(e => e.EmployeesName)
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.Facility).HasMaxLength(150);

                entity.Property(e => e.HandWashers)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.MaskWearing)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.OperationalAreaName).HasMaxLength(150);

                entity.Property(e => e.RecordKeeping)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.SocialDistancing)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.SuperviorCode)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.TemperatureGuns)
                    .HasColumnName("Temperature_Guns")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.WashRooms)
                    .HasMaxLength(3)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ViewEmployee>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("View_Employee");

                entity.Property(e => e.CountryName).HasMaxLength(150);

                entity.Property(e => e.CountyId)
                    .HasColumnName("County_Id")
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.CountyName)
                    .HasColumnName("County_Name")
                    .HasMaxLength(50);

                entity.Property(e => e.DateOfBirth)
                    .HasColumnName("Date_Of_Birth")
                    .HasColumnType("date");

                entity.Property(e => e.DistrictId)
                    .HasColumnName("District_Id")
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.DistrictName)
                    .HasColumnName("District_Name")
                    .HasMaxLength(50);

                entity.Property(e => e.Email)
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.EmployeeId).HasColumnName("Employee_ID");

                entity.Property(e => e.GenderDesc)
                    .HasColumnName("Gender_desc")
                    .HasMaxLength(50);

                entity.Property(e => e.Name)
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.Nationality)
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.NinNo)
                    .HasColumnName("NIN_No")
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.ParishId)
                    .HasColumnName("Parish_Id")
                    .HasMaxLength(5)
                    .IsFixedLength();

                entity.Property(e => e.ParishName)
                    .HasColumnName("Parish_Name")
                    .HasMaxLength(50);

                entity.Property(e => e.Phone)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.PhysicalAddress)
                    .HasColumnName("Physical_Address")
                    .HasMaxLength(5)
                    .IsFixedLength();

                entity.Property(e => e.SubcountyId)
                    .HasColumnName("Subcounty_Id")
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.SubcountyName)
                    .HasColumnName("Subcounty_Name")
                    .HasMaxLength(50);

                entity.Property(e => e.Telephone)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.TitleDesc)
                    .HasColumnName("Title_desc")
                    .HasMaxLength(200);

                entity.Property(e => e.UserId)
                    .HasColumnName("User_Id")
                    .HasMaxLength(450);

                entity.Property(e => e.VillageName)
                    .HasColumnName("Village_Name")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<ViewFollowUp>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("View_FollowUp");

                entity.Property(e => e.AddedBy).HasMaxLength(150);

                entity.Property(e => e.AssignFollowUpContactPerson).HasColumnName("AssignFollowUp_ContactPerson");

                entity.Property(e => e.CaseNo)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.ContactCaseNo)
                    .IsRequired()
                    .HasColumnName("Contact_CaseNo")
                    .HasMaxLength(100);

                entity.Property(e => e.DateAdded).HasColumnType("datetime");

                entity.Property(e => e.DateDeleted).HasColumnType("datetime");

                entity.Property(e => e.DateEdited).HasColumnType("datetime");

                entity.Property(e => e.DeleteStatus).HasColumnName("deleteStatus");

                entity.Property(e => e.DeletedBy).HasMaxLength(150);

                entity.Property(e => e.EditedBy).HasMaxLength(150);

                entity.Property(e => e.GeneralComments).HasMaxLength(4000);

                entity.Property(e => e.MonitoringDate).HasColumnType("datetime");

                entity.Property(e => e.Temperature).HasMaxLength(10);
            });

            modelBuilder.Entity<ViewFollowupTests>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("View_FollowupTests");

                entity.Property(e => e.AddedBy).HasMaxLength(150);

                entity.Property(e => e.AssignFollowUpContactPerson).HasColumnName("AssignFollowUp_ContactPerson");

                entity.Property(e => e.CaseNo)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.ContactCaseNo)
                    .IsRequired()
                    .HasColumnName("Contact_CaseNo")
                    .HasMaxLength(100);

                entity.Property(e => e.DateAdded).HasColumnType("datetime");

                entity.Property(e => e.DateDeleted).HasColumnType("datetime");

                entity.Property(e => e.DateEdited).HasColumnType("datetime");

                entity.Property(e => e.DeleteStatus).HasColumnName("deleteStatus");

                entity.Property(e => e.DeletedBy).HasMaxLength(150);

                entity.Property(e => e.EditedBy).HasMaxLength(150);

                entity.Property(e => e.GeneralComments).HasMaxLength(4000);

                entity.Property(e => e.Hospital).HasMaxLength(400);

                entity.Property(e => e.LabResults).HasColumnName("Lab_Results");

                entity.Property(e => e.LabTestDate)
                    .HasColumnName("LabTest_Date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<ViewMobileCase>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("View_Mobile_Case");

                entity.Property(e => e.CaseName)
                    .HasColumnName("Case_Name")
                    .HasMaxLength(201);

                entity.Property(e => e.CaseNo)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.ExternalCaseNo)
                    .HasColumnName("External_CaseNo")
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<ViewParishes>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("View_Parishes");

                entity.Property(e => e.CountyId)
                    .IsRequired()
                    .HasColumnName("County_Id")
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.DistrictId)
                    .IsRequired()
                    .HasColumnName("District_Id")
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.ParishId)
                    .IsRequired()
                    .HasColumnName("Parish_Id")
                    .HasMaxLength(5)
                    .IsFixedLength();

                entity.Property(e => e.ParishName)
                    .HasColumnName("Parish_Name")
                    .HasMaxLength(50);

                entity.Property(e => e.SubcountyId)
                    .IsRequired()
                    .HasColumnName("Subcounty_Id")
                    .HasMaxLength(4)
                    .IsFixedLength();
            });

            modelBuilder.Entity<ViewReportSitRep>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("View_Report_SitRep");

                entity.Property(e => e.AddedBy).HasMaxLength(150);

                entity.Property(e => e.Category).HasMaxLength(4000);

                entity.Property(e => e.CountyName)
                    .HasColumnName("County_Name")
                    .HasMaxLength(50);

                entity.Property(e => e.DateAdded).HasColumnType("datetime");

                entity.Property(e => e.DateDeleted).HasColumnType("datetime");

                entity.Property(e => e.DateEdited).HasColumnType("datetime");

                entity.Property(e => e.DateofConfirmation).HasColumnType("datetime");

                entity.Property(e => e.DeleteStatus).HasColumnName("deleteStatus");

                entity.Property(e => e.DeletedBy).HasMaxLength(150);

                entity.Property(e => e.District)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.Division)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.EditedBy).HasMaxLength(150);

                entity.Property(e => e.ReportingDate).HasColumnType("datetime");

                entity.Property(e => e.SitRep).HasMaxLength(4000);

                entity.Property(e => e.SitRepCategoryId).HasColumnName("SitRepCategoryID");

                entity.Property(e => e.SitRepId).HasColumnName("SitRepID");

                entity.Property(e => e.ValueResponse).HasMaxLength(50);
            });

            modelBuilder.Entity<ViewReportSitRepCombined>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("View_Report_SitRep_Combined");

                entity.Property(e => e.Category).HasMaxLength(4000);

                entity.Property(e => e.Division)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.KampalaCentralDivision)
                    .HasColumnName("KAMPALA CENTRAL DIVISION")
                    .HasMaxLength(50);

                entity.Property(e => e.KawempeDivision)
                    .HasColumnName("KAWEMPE DIVISION")
                    .HasMaxLength(50);

                entity.Property(e => e.MakindyeDivision)
                    .HasColumnName("MAKINDYE DIVISION")
                    .HasMaxLength(50);

                entity.Property(e => e.NakawaDivision)
                    .HasColumnName("NAKAWA DIVISION")
                    .HasMaxLength(50);

                entity.Property(e => e.ReportingDate).HasColumnType("datetime");

                entity.Property(e => e.RubagaDivision)
                    .HasColumnName("RUBAGA DIVISION")
                    .HasMaxLength(50);

                entity.Property(e => e.SitRepCategoryId).HasColumnName("SitRepCategoryID");

                entity.Property(e => e.Variable).HasMaxLength(4000);
            });

            modelBuilder.Entity<ViewReportSitRepCombinedNew>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("View_Report_SitRep_Combined_New");

                entity.Property(e => e.Category).HasMaxLength(4000);

                entity.Property(e => e.KampalaCentralDivision)
                    .HasColumnName("KAMPALA CENTRAL DIVISION")
                    .HasMaxLength(50);

                entity.Property(e => e.KawempeDivision)
                    .HasColumnName("KAWEMPE DIVISION")
                    .HasMaxLength(50);

                entity.Property(e => e.MakindyeDivision)
                    .HasColumnName("MAKINDYE DIVISION")
                    .HasMaxLength(50);

                entity.Property(e => e.NakawaDivision)
                    .HasColumnName("NAKAWA DIVISION")
                    .HasMaxLength(50);

                entity.Property(e => e.RubagaDivision)
                    .HasColumnName("RUBAGA DIVISION")
                    .HasMaxLength(50);

                entity.Property(e => e.SitRepCategoryId).HasColumnName("SitRepCategoryID");

                entity.Property(e => e.Variable).HasMaxLength(4000);
            });

            modelBuilder.Entity<ViewReportSitRepCombinedNew1>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("View_Report_SitRep_Combined_New_1");

                entity.Property(e => e.Category).HasMaxLength(4000);

                entity.Property(e => e.KampalaCentralDivision)
                    .HasColumnName("KAMPALA CENTRAL DIVISION")
                    .HasMaxLength(50);

                entity.Property(e => e.KawempeDivision)
                    .HasColumnName("KAWEMPE DIVISION")
                    .HasMaxLength(50);

                entity.Property(e => e.MakindyeDivision)
                    .HasColumnName("MAKINDYE DIVISION")
                    .HasMaxLength(50);

                entity.Property(e => e.NakawaDivision)
                    .HasColumnName("NAKAWA DIVISION")
                    .HasMaxLength(50);

                entity.Property(e => e.RubagaDivision)
                    .HasColumnName("RUBAGA DIVISION")
                    .HasMaxLength(50);

                entity.Property(e => e.SitRepCategoryId).HasColumnName("SitRepCategoryID");

                entity.Property(e => e.Variable).HasMaxLength(4000);
            });

            modelBuilder.Entity<ViewSitRepAdd>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("View_SitRep_Add");

                entity.Property(e => e.AddedBy).HasMaxLength(150);

                entity.Property(e => e.Category).HasMaxLength(4000);

                entity.Property(e => e.DateAdded).HasColumnType("datetime");

                entity.Property(e => e.DateDeleted).HasColumnType("datetime");

                entity.Property(e => e.DateEdited).HasColumnType("datetime");

                entity.Property(e => e.DateofConfirmation).HasColumnType("datetime");

                entity.Property(e => e.DeleteStatus).HasColumnName("deleteStatus");

                entity.Property(e => e.DeletedBy).HasMaxLength(150);

                entity.Property(e => e.District)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.Division)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.EditedBy).HasMaxLength(150);

                entity.Property(e => e.ReportingDate).HasColumnType("datetime");

                entity.Property(e => e.SitRep).HasMaxLength(4000);

                entity.Property(e => e.SitRepCategoryId).HasColumnName("SitRepCategoryID");

                entity.Property(e => e.SitRepId).HasColumnName("SitRepID");

                entity.Property(e => e.ValueResponse).HasMaxLength(50);
            });

            modelBuilder.Entity<ViewSitRepComments>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("View_SitRep_Comments");

                entity.Property(e => e.AddedBy).HasMaxLength(150);

                entity.Property(e => e.CountyName)
                    .HasColumnName("County_Name")
                    .HasMaxLength(50);

                entity.Property(e => e.DateAdded).HasColumnType("datetime");

                entity.Property(e => e.DateEdited).HasColumnType("datetime");

                entity.Property(e => e.DateofConfirmation).HasColumnType("datetime");

                entity.Property(e => e.Division)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.EditedBy).HasMaxLength(150);

                entity.Property(e => e.ReportingDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<ViewSitRepEdit>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("View_SitRep_Edit");

                entity.Property(e => e.AddedBy).HasMaxLength(150);

                entity.Property(e => e.Category).HasMaxLength(4000);

                entity.Property(e => e.DateAdded).HasColumnType("datetime");

                entity.Property(e => e.DateDeleted).HasColumnType("datetime");

                entity.Property(e => e.DateEdited).HasColumnType("datetime");

                entity.Property(e => e.DateofConfirmation).HasColumnType("datetime");

                entity.Property(e => e.DeleteStatus).HasColumnName("deleteStatus");

                entity.Property(e => e.DeletedBy).HasMaxLength(150);

                entity.Property(e => e.District)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.Division)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.EditedBy).HasMaxLength(150);

                entity.Property(e => e.ReportingDate).HasColumnType("datetime");

                entity.Property(e => e.SitRep).HasMaxLength(4000);

                entity.Property(e => e.SitRepCategoryId).HasColumnName("SitRepCategoryID");

                entity.Property(e => e.SitRepId).HasColumnName("SitRepID");

                entity.Property(e => e.ValueResponse).HasMaxLength(50);
            });

            modelBuilder.Entity<ViewSitRepPrimary>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("ViewSitRepPrimary");

                entity.Property(e => e.Category).HasMaxLength(4000);

                entity.Property(e => e.SitRep).HasMaxLength(4000);

                entity.Property(e => e.SitRepCategoryId).HasColumnName("SitRepCategoryID");

                entity.Property(e => e.SitRepId).HasColumnName("SitRepID");
            });

            modelBuilder.Entity<ViewSitRepRecords>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("View_SitRep_Records");

                entity.Property(e => e.AddedBy).HasMaxLength(150);

                entity.Property(e => e.CountyName)
                    .HasColumnName("County_Name")
                    .HasMaxLength(50);

                entity.Property(e => e.DateAdded).HasColumnType("datetime");

                entity.Property(e => e.DateEdited).HasColumnType("datetime");

                entity.Property(e => e.Division)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.EditedBy).HasMaxLength(150);

                entity.Property(e => e.ReportingDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<ViewSurveillance>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("View_Surveillance");

                entity.Property(e => e.CaseNo)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.DeleteStatus).HasColumnName("deleteStatus");

                entity.Property(e => e.FullName)
                    .IsRequired()
                    .HasMaxLength(302);
            });

            modelBuilder.Entity<ViewUserManagement>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("View_UserManagement");

                entity.Property(e => e.Email)
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.EmployeeId).HasColumnName("Employee_ID");

                entity.Property(e => e.Name)
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.RoleId).HasMaxLength(450);

                entity.Property(e => e.RoleName).HasMaxLength(256);

                entity.Property(e => e.UserId)
                    .HasColumnName("User_Id")
                    .HasMaxLength(450);

                entity.Property(e => e.UserName).HasMaxLength(256);
            });

            modelBuilder.Entity<ViewUserManagementNew>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("View_UserManagementNew");

                entity.Property(e => e.Email).HasMaxLength(256);

                entity.Property(e => e.Facility).HasMaxLength(500);

                entity.Property(e => e.Id)
                    .IsRequired()
                    .HasMaxLength(450);

                entity.Property(e => e.Name)
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.RoleName).HasMaxLength(256);

                entity.Property(e => e.SelectedModules)
                    .HasColumnName("Selected_Modules")
                    .HasMaxLength(1000);

                entity.Property(e => e.SelectedSections)
                    .HasColumnName("Selected_Sections")
                    .HasMaxLength(1000);

                entity.Property(e => e.UserName).HasMaxLength(256);
            });

            modelBuilder.Entity<ViewUserMessages>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("View_UserMessages");

                entity.Property(e => e.Email).HasMaxLength(256);

                entity.Property(e => e.NameOfUserAccountHolder)
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.UserName).HasMaxLength(256);
            });

            modelBuilder.Entity<ViewVillages>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("View_Villages");

                entity.Property(e => e.CountyId)
                    .IsRequired()
                    .HasColumnName("County_Id")
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.DistrictId)
                    .IsRequired()
                    .HasColumnName("District_Id")
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.ParishId)
                    .IsRequired()
                    .HasColumnName("Parish_Id")
                    .HasMaxLength(5)
                    .IsFixedLength();

                entity.Property(e => e.SubcountyId)
                    .IsRequired()
                    .HasColumnName("Subcounty_Id")
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.VillageId)
                    .IsRequired()
                    .HasColumnName("Village_Id")
                    .HasMaxLength(5)
                    .IsFixedLength();

                entity.Property(e => e.VillageName)
                    .HasColumnName("Village_Name")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<VwAdminUnits>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_AdminUnits");

                entity.Property(e => e.VillageId)
                    .IsRequired()
                    .HasColumnName("Village_Id")
                    .HasMaxLength(5)
                    .IsFixedLength();

                entity.Property(e => e.VillageName)
                    .HasColumnName("Village_Name")
                    .HasMaxLength(284);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
