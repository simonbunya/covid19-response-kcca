﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ARelationship
    {
        public ARelationship()
        {
            CoreCaseContacts = new HashSet<CoreCaseContacts>();
        }

        public int Id { get; set; }
        public string Relationship { get; set; }

        public virtual ICollection<CoreCaseContacts> CoreCaseContacts { get; set; }
    }
}
