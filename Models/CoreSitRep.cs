﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class CoreSitRep
    {
        public string Division { get; set; }
        public DateTime ReportingDate { get; set; }
        public int SitRepCategoryId { get; set; }
        public string ValueResponse { get; set; }
        public string District { get; set; }
        public DateTime? DateofConfirmation { get; set; }
        public string GeneralComments { get; set; }
        public string UpdateComments { get; set; }
        public DateTime? DateAdded { get; set; }
        public string AddedBy { get; set; }
        public DateTime? DateEdited { get; set; }
        public string EditedBy { get; set; }
        public DateTime? DateDeleted { get; set; }
        public string DeletedBy { get; set; }
        public int? DeleteStatus { get; set; }

        public virtual ADistrict DistrictNavigation { get; set; }
        public virtual ACounty DivisionNavigation { get; set; }
        public virtual ASitRepCategory SitRepCategory { get; set; }
    }
}
