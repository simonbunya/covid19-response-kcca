﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ALoginLog
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public DateTime LogInDate { get; set; }
        public int? Disease { get; set; }
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }

        public virtual ADiseaseType DiseaseNavigation { get; set; }
        public virtual AspNetUsers User { get; set; }
    }
}
