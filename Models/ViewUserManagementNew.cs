﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ViewUserManagementNew
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public bool LockoutEnabled { get; set; }
        public string UserName { get; set; }
        public string RoleName { get; set; }
        public string SelectedSections { get; set; }
        public int? FacilityCode { get; set; }
        public int? OperationalArea { get; set; }
        public string Name { get; set; }
        public string SelectedModules { get; set; }
        public string Facility { get; set; }
    }
}
