﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ARiskLevel
    {
        public ARiskLevel()
        {
            CoreCaseContactsCertaintyLevelNavigation = new HashSet<CoreCaseContacts>();
            CoreCaseContactsLog = new HashSet<CoreCaseContactsLog>();
            CoreCaseContactsRiskLevelNavigation = new HashSet<CoreCaseContacts>();
            CoreCases = new HashSet<CoreCases>();
            CoreCommunitySurveillance = new HashSet<CoreCommunitySurveillance>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<CoreCaseContacts> CoreCaseContactsCertaintyLevelNavigation { get; set; }
        public virtual ICollection<CoreCaseContactsLog> CoreCaseContactsLog { get; set; }
        public virtual ICollection<CoreCaseContacts> CoreCaseContactsRiskLevelNavigation { get; set; }
        public virtual ICollection<CoreCases> CoreCases { get; set; }
        public virtual ICollection<CoreCommunitySurveillance> CoreCommunitySurveillance { get; set; }
    }
}
