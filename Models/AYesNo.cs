﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class AYesNo
    {
        public AYesNo()
        {
            AFacilityUnit = new HashSet<AFacilityUnit>();
            CoreCaseContactsIsContactDateEstimatedNavigation = new HashSet<CoreCaseContacts>();
            CoreCaseContactsMonitoringBodyPainsNavigation = new HashSet<CoreCaseContactsMonitoring>();
            CoreCaseContactsMonitoringBreathNavigation = new HashSet<CoreCaseContactsMonitoring>();
            CoreCaseContactsMonitoringChillsNavigation = new HashSet<CoreCaseContactsMonitoring>();
            CoreCaseContactsMonitoringCoughNavigation = new HashSet<CoreCaseContactsMonitoring>();
            CoreCaseContactsMonitoringDiarrheaNavigation = new HashSet<CoreCaseContactsMonitoring>();
            CoreCaseContactsMonitoringSoreThroatNavigation = new HashSet<CoreCaseContactsMonitoring>();
            CoreCaseContactsVaccinesReceivedNavigation = new HashSet<CoreCaseContacts>();
            CoreCasesCaseEvacuatedNavigation = new HashSet<CoreCases>();
            CoreCasesWasContactNavigation = new HashSet<CoreCases>();
            CoreCommunitySurveillance = new HashSet<CoreCommunitySurveillance>();
            CoreComplianceVisitHandWashersNavigation = new HashSet<CoreComplianceVisit>();
            CoreComplianceVisitMaskWearingNavigation = new HashSet<CoreComplianceVisit>();
            CoreComplianceVisitRecordKeepingNavigation = new HashSet<CoreComplianceVisit>();
            CoreComplianceVisitSocialDistancingNavigation = new HashSet<CoreComplianceVisit>();
            CoreComplianceVisitTemperatureGunsNavigation = new HashSet<CoreComplianceVisit>();
            CoreComplianceVisitWashRoomsNavigation = new HashSet<CoreComplianceVisit>();
            CorePublicTransportDetials = new HashSet<CorePublicTransportDetials>();
        }

        public int Id { get; set; }
        public string YesNo { get; set; }

        public virtual ICollection<AFacilityUnit> AFacilityUnit { get; set; }
        public virtual ICollection<CoreCaseContacts> CoreCaseContactsIsContactDateEstimatedNavigation { get; set; }
        public virtual ICollection<CoreCaseContactsMonitoring> CoreCaseContactsMonitoringBodyPainsNavigation { get; set; }
        public virtual ICollection<CoreCaseContactsMonitoring> CoreCaseContactsMonitoringBreathNavigation { get; set; }
        public virtual ICollection<CoreCaseContactsMonitoring> CoreCaseContactsMonitoringChillsNavigation { get; set; }
        public virtual ICollection<CoreCaseContactsMonitoring> CoreCaseContactsMonitoringCoughNavigation { get; set; }
        public virtual ICollection<CoreCaseContactsMonitoring> CoreCaseContactsMonitoringDiarrheaNavigation { get; set; }
        public virtual ICollection<CoreCaseContactsMonitoring> CoreCaseContactsMonitoringSoreThroatNavigation { get; set; }
        public virtual ICollection<CoreCaseContacts> CoreCaseContactsVaccinesReceivedNavigation { get; set; }
        public virtual ICollection<CoreCases> CoreCasesCaseEvacuatedNavigation { get; set; }
        public virtual ICollection<CoreCases> CoreCasesWasContactNavigation { get; set; }
        public virtual ICollection<CoreCommunitySurveillance> CoreCommunitySurveillance { get; set; }
        public virtual ICollection<CoreComplianceVisit> CoreComplianceVisitHandWashersNavigation { get; set; }
        public virtual ICollection<CoreComplianceVisit> CoreComplianceVisitMaskWearingNavigation { get; set; }
        public virtual ICollection<CoreComplianceVisit> CoreComplianceVisitRecordKeepingNavigation { get; set; }
        public virtual ICollection<CoreComplianceVisit> CoreComplianceVisitSocialDistancingNavigation { get; set; }
        public virtual ICollection<CoreComplianceVisit> CoreComplianceVisitTemperatureGunsNavigation { get; set; }
        public virtual ICollection<CoreComplianceVisit> CoreComplianceVisitWashRoomsNavigation { get; set; }
        public virtual ICollection<CorePublicTransportDetials> CorePublicTransportDetials { get; set; }
    }
}
