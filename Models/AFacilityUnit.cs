﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class AFacilityUnit
    {
        public AFacilityUnit()
        {
            CoreCaseContacts = new HashSet<CoreCaseContacts>();
            CoreCaseContactsLog = new HashSet<CoreCaseContactsLog>();
            CoreCases = new HashSet<CoreCases>();
            CoreCommunitySurveillance = new HashSet<CoreCommunitySurveillance>();
            CoreComplianceVisit = new HashSet<CoreComplianceVisit>();
        }

        public int FacilityCode { get; set; }
        public int? OperationalArea { get; set; }
        public string Facility { get; set; }
        public string DetailsOfFacility { get; set; }
        public string EstimatedPopulation { get; set; }
        public int? FacilityStatus { get; set; }
        public string DistrictId { get; set; }
        public string CountyId { get; set; }
        public string SubcountyId { get; set; }
        public string ParishId { get; set; }
        public string VillageId { get; set; }
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
        public DateTime? DateAdded { get; set; }
        public string AddedBy { get; set; }
        public DateTime? DateEdited { get; set; }
        public string EditedBy { get; set; }
        public DateTime? DateDeleted { get; set; }
        public string DeletedBy { get; set; }
        public int? DeleteStatus { get; set; }

        public virtual ACounty County { get; set; }
        public virtual ADistrict District { get; set; }
        public virtual AYesNo FacilityStatusNavigation { get; set; }
        public virtual AOperationalArea OperationalAreaNavigation { get; set; }
        public virtual AParish Parish { get; set; }
        public virtual ASubcounty Subcounty { get; set; }
        public virtual AVillage Village { get; set; }
        public virtual ICollection<CoreCaseContacts> CoreCaseContacts { get; set; }
        public virtual ICollection<CoreCaseContactsLog> CoreCaseContactsLog { get; set; }
        public virtual ICollection<CoreCases> CoreCases { get; set; }
        public virtual ICollection<CoreCommunitySurveillance> CoreCommunitySurveillance { get; set; }
        public virtual ICollection<CoreComplianceVisit> CoreComplianceVisit { get; set; }
    }
}
