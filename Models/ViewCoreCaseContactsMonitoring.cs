﻿using System;
using System.Collections.Generic;

namespace CHMIS.Models
{
    public partial class ViewCoreCaseContactsMonitoring
    {
        public string ContactCaseNo { get; set; }
        public int RowNo { get; set; }
        public string Measures { get; set; }
        public string DayOne { get; set; }
        public string DayTwo { get; set; }
        public string DayThree { get; set; }
        public string DayFour { get; set; }
        public string DayFive { get; set; }
        public string DaySix { get; set; }
        public string DaySeven { get; set; }
        public string DayEight { get; set; }
        public string DayNine { get; set; }
        public string DayTen { get; set; }
        public string DayEleven { get; set; }
        public string DayTwelve { get; set; }
        public string DayThirteen { get; set; }
        public string DayFourteen { get; set; }
    }
}
